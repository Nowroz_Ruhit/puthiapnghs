<?php

use Illuminate\Database\Seeder;
use Modules\Employee\Database\Seeders\SeedFakeEmployeeRelisionTableSeeder;
use Modules\Academic\Database\Seeders\AcademicDatabaseSeeder;
use Modules\Speech\Database\Seeders\SpeechDatabaseSeeder;
use Modules\General\Database\Seeders\GeneralDatabaseSeeder;
class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            SeedFakeEmployeeRelisionTableSeeder::class,
            AcademicDatabaseSeeder::class,
            SpeechDatabaseSeeder::class,
            GeneralDatabaseSeeder::class,
        ]);
    }
}
