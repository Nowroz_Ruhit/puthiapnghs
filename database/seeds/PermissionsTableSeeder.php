<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'         => '1',
                'title'      => 'user_management_access',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '2',
                'title'      => 'permission_create',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '3',
                'title'      => 'permission_edit',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '4',
                'title'      => 'permission_show',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '5',
                'title'      => 'permission_delete',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '6',
                'title'      => 'permission_access',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '7',
                'title'      => 'role_create',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '8',
                'title'      => 'role_edit',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '9',
                'title'      => 'role_show',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '10',
                'title'      => 'role_delete',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '11',
                'title'      => 'role_access',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '12',
                'title'      => 'user_create',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '13',
                'title'      => 'user_edit',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '14',
                'title'      => 'user_show',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '15',
                'title'      => 'user_delete',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '16',
                'title'      => 'user_access',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '17',
                'title'      => 'product_create',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '18',
                'title'      => 'product_edit',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '19',
                'title'      => 'product_show',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '20',
                'title'      => 'product_delete',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '21',
                'title'      => 'product_access',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'        => '22',
                'title'     => 'news_access',
                'created_at'=> '2019-09-09 05:00:45',
                'updated_at'=> '2019-09-09 05:00:45',
            ],
            [
                'id'        =>'23',
                'title'     =>'news_create',
                'created_at'=>'2019-08-27 06:12:01',
                'updated_at'=>'2019-08-27 06:12:01',
            ],
            [
                'id'        =>'24',
                'title'     =>'news_show',
                'created_at'=>'2019-09-02 05:18:19',
                'updated_at'=>'2019-09-02 05:18:19',
            ],
            [
                'id'        =>'25',
                'title'     =>'news_edit',
                'created_at'=>'2019-09-02 05:18:30',
                'updated_at'=>'2019-09-02 05:18:30',
            ],
            [
                'id'        =>'26',
                'title'     =>'news_delete',
                'created_at'=>'2019-09-02 05:18:42',
                'updated_at'=>'2019-09-02 05:18:42',
            ],
            [
                'id'        =>'27',
                'title'     =>'notice_create',
                'created_at'=>'2019-09-03 08:26:39',
                'updated_at'=>'2019-09-03 08:26:39'
            ],
            [
                'id'        =>'28',
                'title'     =>'notice_show',
                'created_at'=>'2019-09-03 11:06:05',
                'updated_at'=>'2019-09-03 11:06:05',
            ],
            [
                'id'        =>'29',
                'title'     =>'notice_edit',
                'created_at'=>'2019-09-03 11:06:13',
                'updated_at'=>'2019-09-03 11:06:13',
            ],
            [
                'id'        =>'30',
                'title'     =>'notice_delete',
                'created_at'=>'2019-09-03 11:06:25',
                'updated_at'=>'2019-09-03 11:06:25',
            ],
            [
                'id'        =>'31',
                'title'     =>'notice_access',
                'created_at'=>'2019-09-05 04:47:38',
                'updated_at'=>'2019-09-05 04:47:38',
            ],
            [
                'id'        =>'32',
                'title'     =>'photo_access',
                'created_at'=>'2019-09-05 04:47:54',
                'updated_at'=>'2019-09-05 04:47:54',
            ],
            [
                'id'        =>'34',
                'title'     =>'academic_access',
                'created_at'=>'2019-09-05 05:34:58',
                'updated_at'=>'2019-09-05 05:34:58',
            ],
            [
                'id'        =>'35',
                'title'     =>'news_category_create',
                'created_at'=>'2019-09-05 04:59:23',
                'updated_at'=>'2019-09-05 04:59:23',
            ],
            [
                'id'        =>'36',
                'title'     =>'photo_uplode',
                'created_at'=>'2019-09-05 04:59:37',
                'updated_at'=>'2019-09-05 04:59:37',
            ],
            [
                'id'        =>'37',
                'title'     =>'academic_create',
                'created_at'=>'2019-09-05 05:35:08',
                'updated_at'=>'2019-09-05 05:35:08',
            ],
            [
                'id'        =>'38',
                'title'     =>'academic_edit',
                'created_at'=>'2019-09-05 05:35:36',
                'updated_at'=>'2019-09-05 05:35:36',
            ],
            [
                'id'        =>'39',
                'title'     =>'academic_show',
                'created_at'=>'2019-09-05 05:35:46',
                'updated_at'=>'2019-09-05 05:35:46',
            ],
            [
                'id'        =>'40',
                'title'     =>'academic_delete',
                'created_at'=>'2019-09-05 05:35:57',
                'updated_at'=>'2019-09-05 05:35:57',
            ],
            [
                'id'        =>'41',
                'title'     =>'academic_category_access',
                'created_at'=>'2019-09-05 05:58:35',
                'updated_at'=>'2019-09-05 05:58:35',
            ],
            [
                'id'        =>'42',
                'title'     =>'academic_category_create',
                'created_at'=>'2019-09-05 06:00:55',
                'updated_at'=>'2019-09-05 06:00:55',
            ],
            [
                'id'        =>'43',
                'title'     =>'academic_category_edit',
                'created_at'=>'2019-09-05 06:01:08',
                'updated_at'=>'2019-09-05 06:01:08',
            ],
            [
                'id'        =>'44',
                'title'     =>'academic_category_show',
                'created_at'=>'2019-09-05 06:01:18',
                'updated_at'=>'2019-09-05 06:01:18',
            ],
            [
                'id'        =>'45',
                'title'     =>'academic_category_delete',
                'created_at'=>'2019-09-05 06:01:28',
                'updated_at'=>'2019-09-05 06:01:28',
            ],
            [
                'id'        =>'46',
                'title'     =>'importantsitelink_create',
                'created_at'=>'2019-09-07 04:52:16',
                'updated_at'=>'2019-09-07 04:52:16',
            ],
            [
                'id'        =>'47',
                'title'     =>'importantsitelink_access',
                'created_at'=>'2019-09-07 04:52:29',
                'updated_at'=>'2019-09-07 04:52:29',
            ],
            [
                'id'        =>'48',
                'title'     =>'importantsitelink_edit',
                'created_at'=>'2019-09-07 04:52:42',
                'updated_at'=>'2019-09-07 04:52:42',
            ],
            [
                'id'        =>'49',
                'title'     =>'importantsitelink_delete',
                'created_at'=>'2019-09-07 04:52:56',
                'updated_at'=>'2019-09-07 04:52:56',
            ],
            [
                'id'        => '50',
                'title'     => 'employee_access',
                'created_at'=> '2019-09-09 04:57:56',
                'updated_at'=> '2019-09-09 04:57:56',
            ],
            [
                'id'        => '51',
                'title'     => 'employee_create',
                'created_at'=> '2019-09-09 04:58:08',
                'updated_at'=> '2019-09-09 04:58:08',
            ],
            [
                'id'        => '52',
                'title'     => 'employee_show',
                'created_at'=> '2019-09-09 04:58:28',
                'updated_at'=> '2019-09-09 04:58:28',
            ],
            [
                'id'        => '53',
                'title'     => 'employee_edit',
                'created_at'=> '2019-09-09 04:58:38',
                'updated_at'=> '2019-09-09 04:58:38',
            ],
            [
                'id'        => '54',
                'title'     => 'employee_delete',
                'created_at'=> '2019-09-09 04:58:53',
                'updated_at'=> '2019-09-09 04:58:53',
            ],
            [
                'id'        => '55',
                'title'     => 'employee_designation_access',
                'created_at'=> '2019-09-09 04:59:07',
                'updated_at'=> '2019-09-09 04:59:07',
            ],
            [
                'id'        => '56',
                'title'     => 'employee_designation_create',
                'created_at'=> '2019-09-09 04:59:16',
                'updated_at'=> '2019-09-09 04:59:16',
            ],
            [
                'id'        => '57',
                'title'     => 'employee_designation_edit',
                'created_at'=> '2019-09-09 05:00:10',
                'updated_at'=> '2019-09-09 05:00:10',
            ],
            [
                'id'        => '58',
                'title'     => 'employee_designation_delete',
                'created_at'=> '2019-09-09 05:00:45',
                'updated_at'=> '2019-09-09 05:00:45',
            ],
        ];

        Permission::insert($permissions);
    }
}


