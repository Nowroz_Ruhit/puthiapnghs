<?php


Route::redirect('/login', '/login');
Route::redirect('/home', '/admin');
// Route::redirect('/admin', '/schoolAdmin/admin');
Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth', 'throttle:3,1']],   function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');
    
    Route::get('clear', function () {
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
     
    });
});

Route::get('command', function () {
    \Artisan::call('config:clear');
    \Artisan::call('view:clear');
    \Artisan::call('route:clear');
    \Artisan::call('config:cache');
});

// Route::get('/', 'guestuser\SchoolController@index')->name('index');
// Route::get('/about', 'guestuser\SchoolController@about')->name('about');
// Route::get('/contact', 'guestuser\SchoolController@contact')->name('contact');
// Route::get('/gallery', 'guestuser\SchoolController@gallery')->name('gallery');
