@extends('layouts.admin')
@section('content')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Administration</a></li>
    <li class="breadcrumb-item"><a href="#">Home Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Home Slider</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>All Social Links</b></a>

      @can('homeslider_create')
        <form class="form-inline">
          <div class="btn btn-xs btn-info mr-2">You can publish only 5 image for sliding</div>
          <a class="form-control mr-sm-2 btn btn-success" href="{{ route('homeslider.create') }}">Add Image</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>
                        <th>
                            
                        </th>
                        <th>
                            Slider Image
                        </th>
                        <th>
                            Slider Title
                        </th>
                        <th>
                            Sub Title
                        </th>
                        <th>
                            Publish
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($homesliders as $key => $homeslider)
                        <tr data-entry-id="{{ $homeslider->id }}">
                            <td>
                                
                            </td>

                            <td>
                                <img src="{{ $homeslider->image ? asset(trans('global.links.homeslider_show').$homeslider->image) : asset('public/img/logo/006-jpg.png') ?? '' }}" width="200" height="100">
                            </td>

                            <td>
                                {{ $homeslider->title ?? '' }}
                            </td>
                            <td>
                                {{ $homeslider->subtitle ?? '' }}
                            </td>

                            <td>

                                {{ $homeslider->active ? 'Published' : 'Unpublish' ?? '' }}
                            </td>

                            <td>
                                @can('homeslider_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('homeslider.edit',$homeslider) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('homeslider_delete')
                                    <form action="{{ route('homeslider.destroy', $homeslider) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('scripts')
@parent
<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>
<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });

    var hulla = new hullabaloo();
  @if(Session::has('success')) 
          hulla.send("{{ Session::get('success') }}", 'success') 
     @php
       Session::forget('success');
     @endphp
  @endif
  @if(Session::has('error')) 
          hulla.send("{{ Session::get('error') }}", 'danger') 
     @php
       Session::forget('error');
     @endphp
  @endif
</script>
@endsection
@endsection