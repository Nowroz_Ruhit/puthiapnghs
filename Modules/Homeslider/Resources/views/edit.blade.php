@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />


@endpush
@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Administration</a></li>
    <li class="breadcrumb-item"><a href="#">Home Settings</a></li>
    <li class="breadcrumb-item"><a href="{{ route('homeslider.index') }}">Home Slider</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update New Image</li>
  </ol>
</nav>

<div class="card">

    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Update Image</b></a>

      @can('homeslider_create')
        <form class="form-inline">
          <div class="btn btn-info mr-2">Total Image: {{ $pub+$unpub }}</div>
          <div class="btn btn-danger mr-2">Unpublished: {{ $unpub }}</div>
          <div class="btn btn-warning mr-5">Published: {{ $pub }}</div>
          <a class="form-control mr-sm-2 btn btn-success" href="{{ route('homeslider.index') }}">Back</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">

        {{ 
            Aire::open()
            ->route('homeslider.update',$homeslider)
            ->enctype('multipart/form-data')
            ->bind($homeslider)
            ->rules([
                'title' => 'required'
            ])
        }}
        <div class="form-group">
            {{ Aire::input('input', 'Slider Title:')
              ->id('sample-input-field')
              ->name('title')
              ->placeholder('Site Name')
              ->class('form-control mb-2') }}

            {{ Aire::input('input', 'Slider Sub-title:')
              ->id('sample-input-field')
              ->name('subtitle')
              ->placeholder('Site Name')
              ->class('form-control mb-2') }}

              <label class="inline-block mb-2 font-semibold cursor-pointer text-base">Upload Image image size must be 1920 × 750) * :</label>
              <div class="form-inline">
                  <div class="form-group mb-2">
                    {{ Aire::file('image')->id('filePhoto') }}
                  </div>
                  <div class="form-group mx-sm-3 mb-2 col-12">
                    <img id="previewHolder" alt="Uploaded Image Preview Holder" src="{{ $homeslider->image ? asset(trans('global.links.homeslider_show').$homeslider->image) : asset('public/img/logo/006-jpg.png') ?? '' }}" class="form-control" style="width: 50%;height: auto"/>
                  </div>
              </div>
              
              
            {{ Aire::checkbox('active', 'publish') }}

            {{ Aire::submit('Update')->id('updateButton')->class('btn btn-success') }}

            <a id="Hello" class="success btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="{{ route('homeslider.index') }}">cancel</a>

        </div>
        {{ Aire::close() }}
    </div>
</div>

@section('scripts')
@parent
<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>
<script type="text/javascript">
  var hulla = new hullabaloo();
  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#previewHolder').attr('src', e.target.result);
    }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#filePhoto").change(function() {
    readURL(this);
  });

  @if(Session::has('success')) 
          hulla.send("{{ Session::get('success') }}", 'success') 
     @php
       Session::forget('success');
     @endphp
  @endif


  @if(Session::has('publish')) 
          hulla.send("{{ Session::get('publish') }}", 'danger') 
     @php
       Session::forget('publish');
     @endphp
  @endif



</script>

@endsection
@endsection