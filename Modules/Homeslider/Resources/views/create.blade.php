@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush
@section('content')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Administration</a></li>
    <li class="breadcrumb-item"><a href="#">Home Settings</a></li>
    <li class="breadcrumb-item"><a href="{{ route('homeslider.index') }}">Home Slider</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add New Image</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Add New Social Link</b></a>

      @can('homeslider_create')
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-success" href="{{ route('homeslider.index') }}">Back</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">

        {{ 
            Aire::open()
            ->route('homeslider.store')
            ->post()
            ->enctype('multipart/form-data')
            ->rules([
                'title' => 'required',
            ])
        }}
        <div class="form-group">
            {{ Aire::input('input', 'Slider Title:*')
              ->id('sample-input-field')
              ->name('title')
              ->placeholder('Site Name')
              ->value('')
              ->class('form-control mb-2') }}

            {{ Aire::input('input', 'Slider Sub-title:')
              ->id('sample-input-field')
              ->name('subtitle')
              ->placeholder('Site Name')
              ->value('')
              ->class('form-control mb-2') }}


              <label class="inline-block mb-2 font-semibold cursor-pointer text-base">Upload Image (image size must be 1920 × 750) * :</label>
              <div class="form-inline">
                  <div class="form-group mb-2">
                    {{ Aire::file('image')->id('filePhoto') }}
                  </div>
                  <div class="form-group mx-sm-3 mb-3 pb-3 pl-1 col-12">
                    <img id="previewHolder" alt="Uploaded Image Preview Holder" src="{{ asset('public/img/logo/006-jpg.png') }}" class="form-control" style="width: 50%;height: auto"/>
                  </div>
              </div>

            {{ Aire::submit('Save')->class('btn btn-success') }}

            <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="{{ route('homeslider.index') }}">Cancel</a>
        </div>
        {{ Aire::close() }}
          
    </div>
</div>

@section('scripts')
@parent
<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>
<script type="text/javascript">
  function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#previewHolder').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#filePhoto").change(function() {
    readURL(this);
  });

  var hulla = new hullabaloo();
  @if(Session::has('success')) 
          hulla.send("{{ Session::get('success') }}", 'success') 
     @php
       Session::forget('success');
     @endphp
  @endif
</script>
@endsection


@endsection