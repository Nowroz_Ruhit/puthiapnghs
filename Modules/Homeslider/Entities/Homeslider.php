<?php

namespace Modules\Homeslider\Entities;

use Illuminate\Database\Eloquent\Model;

class Homeslider extends Model
{
    // protected $fillable = ['title','subtitle','image','active'];
    protected $guarded = ['id', 'created_at'];
}
