<?php

namespace Modules\Homeslider\Http\Controllers;

use Illuminate\{
    Http\Request,
    Http\Response,
    Routing\Controller
};
use Illuminate\Support\Facades\Auth;
// use Illuminate\Http\Response;
// use Illuminate\Routing\Controller;
use Modules\Homeslider\Entities\Homeslider;
use Intervention\Image\Facades\Image;
class HomesliderController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('homeslider_access'), 403);
        $homesliders = Homeslider::orderBy('active','ASC')->get();
        return view('homeslider::index',compact('homesliders'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('homeslider_create'), 403);
        return view('homeslider::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

        ]);

        $data = $request->all();
        $data['created_by'] = Auth::id();  
        if ($request->hasFile('image')){ 
            $imageName = now()->format('Y-m-d-H-i-s').'-'.request()->image->getClientOriginalName();
            request()->image->move(public_path(trans('global.links.homeslider')), $imageName);
            $data['image'] = $imageName;
            $image = Image::make(public_path(trans('global.links.homeslider')."{$imageName}"))->resize(1920, 750);
            $image->save();
        }

  
        $homeslider = Homeslider::create($data);

        if($homeslider){
            session()->put('success','Created successfully.');
        }else {
            session()->put('error','Not Create.');
        }
        return view('homeslider::create');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Homeslider $homeslider)
    {
        return view('homeslider::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Homeslider $homeslider)
    {
        abort_unless(\Gate::allows('homeslider_edit'), 403);
        $pub = Homeslider::where('active',1)->count();
        $unpub = Homeslider::where('active',0)->count();
        // return $pub." ".$unpub;
        return view('homeslider::edit',compact('homeslider','pub','unpub'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request,Homeslider $homeslider)
    {


        $data = $request->all();
        $data['updated_by'] = Auth::id();
        if( !($request->has('active')) ){
            $data['active'] = 0;            
        }
        elseif($request->has('active')){
            $count = Homeslider::where('active',1)->count();
            if($count >= 5){
                $data['active'] = 0;
                session()->put('publish','Only Support 5 image');
                return redirect()->route('homeslider.edit',compact('homeslider'));
            }
        }

        if ($request->hasFile('image')){
            // if($homeslider->image){
            //     unlink(public_path(trans('global.links.homeslider')).$homeslider->image);
            // }
            if(file_exists(public_path(trans('global.links.homeslider')).$homeslider->image))
            {
                unlink(public_path(trans('global.links.homeslider')).$homeslider->image);
            }
            $imageName = request()->image->getClientOriginalName();
            request()->image->move(public_path(trans('global.links.homeslider')), $imageName);
            $data['image'] = $imageName;
            $image = Image::make(public_path(trans('global.links.homeslider').$imageName))->resize(1920, 750);
            $image->save();
        }

        $homeslider->update($data);
        if($homeslider){
            return redirect()->route('homeslider.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('homeslider.index')->with('error', 'Not Update, Somtheing waswrong');
        }
        // if($homeslider){
        //     session()->put('success','Updated successfully.');
        // }
        // return redirect()->route('homeslider.edit',compact('homeslider'));
    }




    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */

    public function destroy(Homeslider $homeslider)
    {
        // $homeslider->delete();
        // if($homeslider){
        //     session()->put('success','Deleted successfully.');
        // }
        // return redirect()->route('homeslider.index');

        $file= $homeslider->image;
        if($homeslider->delete())
        {
            $filename = public_path(trans('global.links.homeslider')).$file;
             if(\File::exists($filename)){
                 \File::delete($filename);
            }
           return redirect()->route('homeslider.index')->with('delete', 'Delete Successfull');
        }else {
            return redirect()->route('homeslider.index')->with('error', 'Not Deleted, Somtheing waswrong');
        }
    }
}
