<?php

namespace Modules\Speech\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Modules\Employee\Entities\Designation;
use Modules\Employee\Entities\Employee;
use Modules\Speech\Entities\Speech;

use Intervention\Image\Facades\Image;

class SpeechController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // $speech = DB::table('speeches')->orderBy('id', 'DESC')->get();
        $speechs = Speech::all();
        // dd($speech);
        return view('speech::index', compact('speechs'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('speech::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // 
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Speech $speech)
    {
        // dd($speech);
        return view('speech::show', compact('speech'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Speech $speech)
    {
        // $speech=Speech::all();
        // $speech = DB::table('speeches')->orderBy('id', 'DESC')->first();
        // dd($speech);
        return view('speech::edit', compact('speech'));
        // return view('speech::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Speech $speech)
    {
        
        request()->validate([
            'name' => 'required',
            'designation' => 'required',
            'speech' => 'required',
            'files' => 'image|mimes:jpeg,png,jpg',

        ]);
        // dd($request);
        $data = $request->all(); 
        $data['updated_by'] = Auth::id();
        if ($request->hasFile('files')){
        if(file_exists(public_path(trans('global.links.speech')).$speech->files)){
            unlink(public_path(trans('global.links.speech')).$speech->files);
        } 
            
            $imageName = request()->input('name').'.'.request()->file('files')->getClientOriginalExtension();

            request()->file('files')->move(public_path(trans('global.links.speech')), $imageName);
            $image = Image::make(public_path(trans('global.links.speech').$imageName))->resize(450, 450);
            $image->save();

            $data['files'] = $imageName;
        }

        // $speech = Speech::findOrFail($id);
        // $speech->update($data);
        // return redirect()->route('speech.index');
        if($speech->update($data))
        {
        return redirect()->route('speech.index')->with('success', 'Update Successfull');
        } else 
        {
            return redirect()->route('speech.index')->with(['error', 'Somthing was wrong']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
