@extends('layouts.admin')

@section('styles')
@parent
<!--  -->
@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="">Administration</a></li>
    <li class="breadcrumb-item"><a href="">Home Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Spech</li>
  </ol>
</nav>

<div class="card">
  <!-- <nav class="navbar navbar-light bg-light justify-content-between">
       trans('global.speech.title') 
      @can('speech_edit')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-success" href="route('speech.edit',1)">
          <i class="fas fa-edit nav-icon"></i>  trans('global.edit') 
        </a>
      </form>
      @endcan
    </nav> -->
<div class="card-header">
        {{trans('global.speech.title') }}
    </div>
<div class="card-body">
<div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>
                            শিরনাম
                        </th>
                        <th>
                            ছবি
                        </th>
                        <th>
                            বক্তার নাম
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                   @foreach($speechs as $key => $speech)
                        <tr data-entry-id="{{ $speech->id }}">
                            <td>
                                {{ $speech->designation ?? '' }}
                            </td>
                            <td>
                              <img height="70" width="70" src="{{asset(trans('global.links.speech_show').$speech->files)}}">
                            </td>
                            <td>
                                {{ $speech->name ?? '' }}
                            </td>
                            <td>
                            @can('speech_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('speech.show', $speech) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('speech_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('speech.edit', $speech) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                            </td>

                            
                        </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>
</div>
</div>

@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
