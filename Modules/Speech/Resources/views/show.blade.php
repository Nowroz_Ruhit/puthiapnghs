@extends('layouts.admin')

@section('styles')
<style type="text/css">
  .table-borderless td, .table-borderless th { border: 0; }
/*  table, tr, th, td{
    border: 1px solid black;
  }*/
</style>
@parent

@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="">Administration</a></li>
    <li class="breadcrumb-item"><a href="">Home Settings</a></li>
    <li class="breadcrumb-item"><a href="{{route('speech.index')}}">Spech</a></li>
    <li class="breadcrumb-item active" aria-current="page">View</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.speech.title') }}
      @can('speech_edit')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-success" href="{{route('speech.edit',$speech)}}">
          <i class="fas fa-edit nav-icon"></i> {{ trans('global.edit') }}
        </a>
      </form>
      @endcan
    </nav>

    <img class="m-2" height="200" width="200" src="{{asset(trans('global.links.speech_show').$speech->files)}}">

<table class="table table-borderless table-responsive">
    <tr>
      <th scope="row" >Name</th>
      <td>{{$speech->name}}</td>


    </tr>
    <tr>
      <th scope="row" >Title</th>
      <td >{{$speech->designation}}</td>

    </tr>
    <tr>
      <th scope="row" >Speech</th>
      <td>
          <div class="text-justify">{!! $speech->speech !!}
        
      </td>
    </tr>

</table>
</div>
@endsection
