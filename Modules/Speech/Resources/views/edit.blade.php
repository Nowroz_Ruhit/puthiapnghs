@extends('layouts.admin')

@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('scripts')
	@parent
@endsection

@section('content')
<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="">Administration</a></li>
    <li class="breadcrumb-item"><a href="">Home Settings</a></li>
    <li class="breadcrumb-item"><a href="{{route('speech.index')}}">Spech</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.speech.title') }}
      @can('product_create')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-success" href="{{route('speech.index')}}">
          <i class="fas  nav-icon"></i> Back To Speech
        </a>
      </form>
      @endcan
    </nav>
<!-- ->route('speech.update', $speech->id) 
            ->bind($speech)-->
    <div class="card-body">
      {{ 
            Aire::open()
            ->route('speech.store')
            ->route('speech.update', $speech->id) 
            ->bind($speech)
            ->rules([
            'name' => 'required',
            'designation' => 'required',
            'speech' => 'required',
            'files' => 'required'
            ])
            ->messages([
            'designation' => 'You must input this fild',
            ]) 
            ->enctype('multipart/form-data')
        }}

        {{ 
            Aire::input()
            ->label('Teacher Name *')
            ->id('name')
            ->name('name')
            ->class('form-control') 
        }}

        {{ 
            Aire::input()
            ->label('Speech Owner Position *')
            ->id('designation')
            ->name('designation')
            ->class('form-control') 
        }}


        {{ 
            Aire::input()
            ->label('Speech Title *')
            ->id('sortSpeech')
            ->name('sortSpeech')
            ->class('form-control') 
        }}


        {{ 
            Aire::textarea()
            ->label("Teacher's Speech *")
            ->id('speech')
            ->name('speech')
            ->class('form-control')
        }}

        <script>
            CKEDITOR.replace( 'speech' );
        </script>

        <!-- <img src="" id="cover-img-tag" width="200px" class="" /> -->
        <div class=" mx-sm-3 mb-2">
            <img id="cover-img-tag" width="200px" alt="Uploaded Image Preview Holder" src="{{ $speech->files ? asset(trans('global.links.speech_show').$speech->files) : asset('public/img/logo/006-jpg.png') ?? '' }}"  />
        </div>
        
        
        {{
            Aire::input()
            ->type('file')
            ->label('Teacher Picture (image size must be 450 × 450) *')
            ->id('cover-photo')
            ->name('files') 
        }}




        {{ 
            Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}
        {{ Aire::close() }}
    </div>
</div>

<script src="{{ asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js') }}"></script>

<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#cover-photo").change(function(){
        readURL(this);
    });

</script>
@endsection
