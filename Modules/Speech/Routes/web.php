<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('admin/administration', 'administration/home-settings/speech');
Route::redirect('admin/administration/home-settings', 'home-settings/speech');

Route::prefix('admin/administration/home-settings')->group(function() {
    // Route::get('/', 'SpeechController@index');
    Route::resource('speech', 'SpeechController');
});
// Route::resource('speech','SpeechController')
