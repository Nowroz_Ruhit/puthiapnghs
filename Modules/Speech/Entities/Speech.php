<?php

namespace Modules\Speech\Entities;

use Illuminate\Database\Eloquent\Model;

class Speech extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
