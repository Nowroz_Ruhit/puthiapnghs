<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportantlinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *   importantsitelink_create   
     *   importantsitelink_access      
     *   importantsitelink_edit    
     *   importantsitelink_delete
     */
    public function up()
    {
        Schema::create('importantlinks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('link');
            $table->string('logo')->nullable();
            $table->boolean('active')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('importantlinks');
    }
}
