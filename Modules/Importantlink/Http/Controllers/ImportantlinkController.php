<?php

namespace Modules\Importantlink\Http\Controllers;

use Modules\Importantlink\Entities\Importantlink;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class ImportantlinkController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('importantsitelink_access'), 403);
        $importantlinks = Importantlink::all();
        return view('importantlink::index',compact('importantlinks'));
    }



    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('importantsitelink_create'), 403);
        return view('importantlink::create');
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'link' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $data = $request->all();
        $data['created_by'] = Auth::id();
        if ($request->hasFile('logo')){ 
            $imageName = request()->logo->getClientOriginalName();

            request()->logo->move(public_path(trans('global.links.importantlink')), $imageName);

            $data['logo'] = $imageName;
        }

  
        $product = Importantlink::create($data);
        return redirect()->route('importantlink.index');
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Importantlink $importantlink)
    {
        // return $importantlink;
        abort_unless(\Gate::allows('importantsitelink_edit'), 403);
        return view('importantlink::edit',compact('importantlink'));
    }



    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request,Importantlink $importantlink)
    {
        //
        abort_unless(\Gate::allows('importantsitelink_edit'), 403);
        request()->validate([
            'title' => 'required',
            'link' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $data = $request->all(); 
        $data['updated_by'] = Auth::id();

        if( !($request->has('active')) ){
            $data['active'] = 0;            
        }

        if ($request->hasFile('logo')){ 
            if(file_exists(public_path(trans('global.links.importantlink')).$importantlink->logo))
            {
                unlink(public_path(trans('global.links.importantlink')).$importantlink->logo);
            }
            $imageName = request()->logo->getClientOriginalName();
            request()->logo->move(public_path('img/logo'), $imageName);
            $data['logo'] = $imageName;

        }

        $importantlink->update($data);
        // return redirect()->route('importantlink.index');
        if($importantlink){
            return redirect()->route('importantlink.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('importantlink.index')->with('error', 'Not Update, Somtheing waswrong');
        }
    }




    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Importantlink $importantlink)
    {
        $importantlink->delete();
        return redirect()->route('importantlink.index');
    }

}