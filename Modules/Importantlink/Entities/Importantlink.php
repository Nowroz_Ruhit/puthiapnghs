<?php

namespace Modules\Importantlink\Entities;

use Illuminate\Database\Eloquent\Model;

class Importantlink extends Model
{
    // protected $fillable = ['title','link','logo','active'];
    protected $guarded = ['id', 'created_at'];
}
