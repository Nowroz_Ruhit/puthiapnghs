<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('importantlink')->group(function() {

//     Route::get('/', 'ImportantlinkController@index')->name('importantlink.index');
    
 //Route::get('importantlink/{edit}', 'ImportantlinkController@edit')->name('importantlink.edit');
    
//     Route::resource('operation', 'ImportantlinkController');
// });

Route::resource('importantlink','ImportantlinkController');