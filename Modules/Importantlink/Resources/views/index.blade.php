@extends('layouts.admin')
@section('content')
@can('importantsitelink_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href='{{ route("importantlink.create") }}'>
                Add Important Link
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        Important Link List
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>
                        <th>
                            
                        </th>
                        <th>
                            Site tittle
                        </th>
                        <th>
                            Link
                        </th>
                        <th>
                            logo
                        </th>
                        <th>
                            Publish
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($importantlinks as $key => $importantlink)
                        <tr data-entry-id="{{ $importantlink->id }}">
                            <td  width="1">
                                
                            </td>
                            <td>
                                {{ $importantlink->title ?? '' }}
                            </td>
                            <td width="5">
                                {{ $importantlink->link ?? '' }}
                            </td>
                            <td>
                                <img src="{{ $importantlink->logo ? asset(trans('global.links.importantlink').$importantlink->logo) : asset('public/img/logo/006-jpg.png') ?? '' }}" width="50" height="50">
                            </td>

                            <td>

                                {{ $importantlink->active ? 'Published' : 'Unpublish' ?? '' }}
                            </td>

                            <td>
                                @can('importantsitelink_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('importantlink.edit',$importantlink) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('importantsitelink_delete')
                                    <form action="{{ route('importantlink.destroy', $importantlink) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('scripts')
@parent
<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });
</script>
@endsection
@endsection