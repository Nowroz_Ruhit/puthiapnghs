@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush
@section('content')

<div class="card">
    <div class="card-header">
        Add Important link
    </div>

    <div class="card-body">

        {{ 
            Aire::open()
            ->route('importantlink.store')
            ->post()
            ->enctype('multipart/form-data')
            ->rules([
                'title' => ' ',
                'link' => 'url',
            ])->messages([
                'link' => 'Incorect link url'
            ])
        }}
        <div class="form-group">
            {{ Aire::input('input', 'Link Name:*')
              ->id('sample-input-field')
              ->name('title')
              ->placeholder('Site Name')
              ->value('')
              ->class('form-control mb-2') }}


            {{ Aire::input('input', 'Linr Url:*')
              ->id('link')
              ->name('link')
              ->placeholder('http://www.google.com')
              ->value('')
              ->class('form-control' )}}
              <label class="inline-block mb-2 font-semibold cursor-pointer text-base">Upload Logo:</label>
              <div class="form-inline">
                  <div class="form-group mb-2">
                     {{ Aire::file('logo')->id('filePhoto') }}
                  </div>
                  <div class="form-group mx-sm-3 mb-2">
                    <img id="previewHolder" alt="Uploaded Image Preview Holder" src="{{ asset('public/img/logo/006-jpg.png') }}" class="form-control" />
                  </div>
              </div>

            {{ Aire::submit('Create Link')->class('btn btn-success') }}

            <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="{{ route('importantlink.index') }}">Cancel</a>
        </div>
        {{ Aire::close() }}
          
    </div>
</div>

@section('scripts')
@parent

<script type="text/javascript">
  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#previewHolder').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
  }
}

$("#filePhoto").change(function() {
  readURL(this);
});
</script>
@endsection


@endsection