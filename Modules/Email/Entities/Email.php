<?php

namespace Modules\Email\Entities;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
