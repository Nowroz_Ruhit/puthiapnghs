@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        E-mail List
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Subject
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($emails as $key => $email)
                        <tr data-entry-id="{{ $email->id }}">
                            <td>
                                {{ $email->name ?? '' }}
                            </td>
                            <td>
                                {{ $email->form ?? '' }}
                            </td>
                            <td>
                                {{ $email->subject ?? '' }}
                            </td>
                            <td>
                                @can('email_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('email.show', $email->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection