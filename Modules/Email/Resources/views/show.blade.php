@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        E-mail
    </div>

    <div class="card-body">
        <div class="contaner">
            <strong>From : </strong>{{$email->form}}<br>
            <strong>To : </strong>{{$email->to}}<br>
            <strong>Phone : </strong>{{$email->phone}}<br>
            <strong>Telephone : </strong>{{$email->tel_phone}}<br>
            <br>
            <br>
            <h4><strong>Subject : </strong>{{$email->subject}}</h4>
            <br>
            <br>
            {!! $email->detail !!}
        </div>
    </div>
</div>
@endsection