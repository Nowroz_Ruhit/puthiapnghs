<?php

namespace Modules\Visitor\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
Use Modules\Visitor\Entities\Visitor;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('visitor_access'), 403);
        $visitors = Visitor::orderBy('id', 'desc')->get();
        return view('visitor::index', compact('visitors'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
}
