@extends('layouts.admin')
@section('content')

<div class="card">
	<div class="card-header">
		Visitor List
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class=" table table-bordered table-striped table-hover datatable">
				<thead>
					<tr>
						<th>
							id
						</th>
						<th>
							ip
						</th>
						<th>
							hits
						</th>
						<!-- <th>
							date
						</th> -->
						<!-- <th>
							visit_time
						</th> -->
						<th>
							Public IP
						</th>
						<th>
							City
						</th>
						<th>
							Region Name
						</th>
						<th>
							Country
						</th>
						<th>
							created_at
						</th>
						<th>
							updated_at
						</th>
					</tr>
				</thead>
				<tbody>
					@foreach($visitors as $key => $visitor)
					<tr data-entry-id="{{ $visitor->id }}">
						<td>
							{{ $visitor->id ?? '' }}
						</td>
						<td>
							{{ $visitor->ip ?? '' }}
						</td>
						<td>
							{{ $visitor->hits ?? '' }}
						</td>
					<!-- <td>
							 $visitor->date ?? '' 
						</td> -->
						<!-- <td>
							 $visitor->visit_time ?? '' 
						</td> -->
						<td>
							{{ $visitor->public_ip ?? '' }}
						</td>
						<td>
							{{ $visitor->city ?? '' }}
						</td>
						<td>
							{{ $visitor->regionName ?? '' }}
						</td>
						<td>
							{{ $visitor->country ?? '' }}
						</td>
						<td>
							{{ $visitor->created_at->format('d M Y h:i:s a') ?? '' }}
						</td>
						<td>
							{{ $visitor->updated_at->format('d M Y h:i:s a') ?? '' }}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@section('scripts')
@parent
<script>
	$(function () {
		let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
		$('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
	})

</script>
@endsection
@endsection