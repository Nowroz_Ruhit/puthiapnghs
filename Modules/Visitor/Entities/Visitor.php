<?php

namespace Modules\Visitor\Entities;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{

	public $attributes = [ 'hits' => 0 ];

    protected $fillable = [ 'ip', 'date', 'public_ip', 'city', 'regionName', 'country' ];
    protected $table = 'visitors';

    public static function boot() {
        
        parent::boot();
        // Any time the instance is updated (but not created)
        static::saving( function ($model) {
            $model->visit_time = date('H:i:s');
            $model->hits++;
        } );
    }

    public static function hit() {
        $details = json_decode(file_get_contents("http://ip-api.com/json/"));
        static::firstOrCreate([
                  'ip'   => $_SERVER['REMOTE_ADDR'],
                  'date' => date('Y-m-d'),
                  'public_ip' => $details->query, 
                  'city' => $details->city, 
                  'regionName' =>  $details->regionName, 
                  'country' => $details->country,
              ])->save();
    }
}
