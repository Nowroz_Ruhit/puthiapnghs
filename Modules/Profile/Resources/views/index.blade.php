@extends('layouts.admin')
@push('css')
@endpush

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Profile</li>
  </ol>
</nav>

<div class="card">
<div class="card-header">
        {{ trans('global.profile.fields.user') }} {{ trans('global.profile.title_singular') }}
    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-5" style="height: 200px;">
                <!-- <div class="float-right mr-5 mt-2"> -->
                <div class="row justify-content-center align-self-center float-right mr-5" >
                    <img class=" rounded-circle" height="180" width="180" src=" {{asset('public/img/user/default-user-image.png')}}">
                </div>
            </div>
            <div class="col-7 row  align-self-center">
                <table class="table table-striped col-6">
                        <tr>
                        	
                            <th scope="col" width="10">Name</th>
                            <td scope="col"><b>:</b> {{$user->name}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Email</th>
                            <td scope="col"><b>:</b> {{$user->email}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Role</th>
                            <td scope="col"><b>:</b> 
                                @foreach($user->roles as $id => $roles)
                                    <span class="label label-info label-many">{{ $roles->title }}</span>
                                @endforeach
                            </td>
                        </tr>
                        

                </table>
            </div>
        </div>
    </div>
</div>
@endsection

