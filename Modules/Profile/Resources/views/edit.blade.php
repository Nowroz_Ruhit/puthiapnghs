@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')

@if(session()->get('msg'))
 <a href="{{url('notice')}}" class="btn btn-success">Back to Notice List</a>

<div class="alert-success col-12 text-center h1 p-6  mt-5 mb-5">
    "<i>Notice</i>"  Create Successfully
     
</div>

@else
<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{url('employee')}}">Employee</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.notice.title_singular') }}
    </div>

    <div class="card-body">
        {{ 
            Aire::open()
            ->route('profile.update',$user)
            ->bind($user)
            ->rules([
            'name' => 'required',
            'discrioption' => 'required',
            'files'=> 'required|file',
            'email' =>'email',
            'phone' => 'numeric',
            'designation' => 'required',
            ])
            ->messages([
            'name' => 'You must accept the terms',
            ]) 
            ->enctype('multipart/form-data')
        }}

        {{ 
            Aire::input()
            ->label('Name*')
            ->id('name')
            ->name('name')
            ->class('form-control') 
        }}

        {{ 
            Aire::input()
            ->label('Email')
            ->id('email')
            ->name('email')
            ->class('form-control') 
        }}

        
        <!-- <img src="" id="cover-img-tag" width="200px" class="mb-2" /> -->
<!--         <div class=" mx-sm-3 mb-2">
            <img id="cover-img-tag" width="200px" alt="Uploaded Image Preview Holder" src=" $employee->files ? asset('public/img/employee/'.$employee->files) : asset('public/img/logo/006-jpg.png') ?? '' "  />
        </div> -->
        
<!--             Aire::input()
            ->type('file')
            ->label('Employe Picture *')
            ->id('files')
            ->name('files') -->
            
        

        {{ 
            Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>
<script src="{{ asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js') }}"></script>

<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#files").change(function(){
        readURL(this);
    });

</script>
@endif
@endsection

