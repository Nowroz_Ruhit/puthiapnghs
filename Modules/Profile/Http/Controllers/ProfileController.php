<?php

namespace Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        // Get the currently authenticated user...
        $user = Auth::user();
        // return $user;
        // Get the currently authenticated user's ID...
        // $id = Auth::id();
        // echo $user." ".$id;
        // return $user->role;
        return view('profile::index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('user_edit'), 403);
        $user = Auth::user();
        return view('profile::pass', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('profile::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(User $user)
    {
        // return $id;
        abort_unless(\Gate::allows('user_edit'), 403);
        $user = Auth::user();
        return view('profile::edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, User $user, $id)
    {
        abort_unless(\Gate::allows('user_edit'), 403);
        // return $id;
        $user = User::findOrFail($id);
        // $user->update($request->all());
        $data = $request->all();
        $data['updated_by'] = Auth::id();
        if($user->update($data)){
            return redirect()->route('profile.index')->with('success', 'Update Successfull');;
        }else {
            return redirect()->route('profile.index')->with(['error', 'Somthing was wrong']);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
