@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />


@endpush
@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Administration</a></li>
    <li class="breadcrumb-item"><a href="#">System Settings</a></li>
    <li class="breadcrumb-item"><a href="{{ route('sociallink.index') }}">Social Link</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update Social Link</li>
  </ol>
</nav>

<div class="card">

    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Update Social Link</b></a>

      @can('homeslider_create')
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-success" href="{{ route('sociallink.index') }}">Back</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">

        {{ 
            Aire::open()
            ->route('sociallink.update',$sociallink)
            ->enctype('multipart/form-data')
            ->bind($sociallink)
            ->rules([
                'title' => ' ',
                'link' => 'url'
            ])->messages([
                'link' => 'Incorect link url'
            ])
        }}
        <div class="form-group">
            {{ Aire::input('title', 'Link Name:*')
              ->id('title')
              ->placeholder('Site Name')
              ->required()
              ->value()
              ->class('form-control mb-2') }}


            {{ Aire::input('link', 'Linr Url:*')
              ->id('link')
              ->placeholder('http://www.google.com')
              ->required()
              ->class('form-control' )}}

              <label class="inline-block mb-2 font-semibold cursor-pointer text-base">Upload Logo:</label>
              <div class="form-inline">
                  <div class="form-group mb-2">
                    {{ Aire::file('logo')->id('filePhoto') }}
                  </div>
                  <div class="form-group mx-sm-3 mb-2">
                    <img id="previewHolder" alt="Uploaded Image Preview Holder" src="{{ $sociallink->logo ? asset('public/img/logo/'.$sociallink->logo) : asset('public/img/logo/006-jpg.png') ?? '' }}" class="form-control" />
                  </div>
              </div>

            {{ Aire::checkbox('active', 'publish') }}

            {{ Aire::submit('Update Link')->id('updateButton')->class('btn btn-success') }}

            <a id="Hello" class="success btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="{{ route('sociallink.index') }}">cancel</a>

        </div>
        {{ Aire::close() }}
    </div>
</div>

@section('scripts')
@parent
<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>
<script type="text/javascript">
  var hulla = new hullabaloo();
  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#previewHolder').attr('src', e.target.result);
    }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#filePhoto").change(function() {
    readURL(this);
  });

  @if(Session::has('success')) 
          hulla.send("{{ Session::get('success') }}", 'success') 
     @php
       Session::forget('success');
     @endphp
  @endif



</script>

@endsection
@endsection