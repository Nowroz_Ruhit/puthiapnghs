@extends('layouts.admin')
@section('content')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Administration</a></li>
    <li class="breadcrumb-item"><a href="#">System Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Social Links</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>All Social Links</b></a>

      @can('sociallink_create')
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-success" href="{{ route('sociallink.create') }}">Add Social Link</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>
                        <th>
                            
                        </th>
                        <th>
                            Site tittle
                        </th>
                        <th>
                            Link
                        </th>
                        <th>
                            logo
                        </th>
                        <th>
                            Publish
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($sociallinks as $key => $sociallink)
                        <tr data-entry-id="{{ $sociallink->id }}">
                            <td  width="1">
                                
                            </td>
                            <td>
                                {{ $sociallink->title ?? '' }}
                            </td>
                            <td width="5">
                                {{ $sociallink->link ?? '' }}
                            </td>
                            <td>
                                <img src="{{ $sociallink->logo ? asset('public/img/logo/'.$sociallink->logo) : asset('public/img/logo/006-jpg.png') ?? '' }}" width="50" height="50">
                            </td>

                            <td>

                                {{ $sociallink->active ? 'Published' : 'Unpublish' ?? '' }}
                            </td>

                            <td>
                                @can('sociallink_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('sociallink.edit',$sociallink) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('sociallink_delete')
                                    <form action="{{ route('sociallink.destroy', $sociallink) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('scripts')
@parent
<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>
<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });

    var hulla = new hullabaloo();
  @if(Session::has('success')) 
          hulla.send("{{ Session::get('success') }}", 'danger') 
     @php
       Session::forget('success');
     @endphp
  @endif
</script>
@endsection
@endsection