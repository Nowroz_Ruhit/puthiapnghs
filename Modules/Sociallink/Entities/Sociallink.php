<?php

namespace Modules\Sociallink\Entities;

use Illuminate\Database\Eloquent\Model;

class Sociallink extends Model
{
    protected $fillable = ['title','link','logo','active'];
}
