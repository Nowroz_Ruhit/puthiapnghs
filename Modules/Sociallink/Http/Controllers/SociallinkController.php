<?php

namespace Modules\Sociallink\Http\Controllers;

use Modules\Sociallink\Entities\Sociallink;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class SociallinkController extends Controller
{
 /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('sociallink_access'), 403);
        $sociallinks = Sociallink::all();
        return view('sociallink::index',compact('sociallinks'));
    }



    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('sociallink_create'), 403);
        return view('sociallink::create');
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'link' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $data = $request->all();
        $data['created_by'] = Auth::id();  
        if ($request->hasFile('logo')){ 
            $imageName = request()->logo->getClientOriginalName();

            request()->logo->move(public_path('img/logo'), $imageName);

            $data['logo'] = $imageName;
        }

  
        $sociallink = Sociallink::create($data);

        if($sociallink){
            session()->put('success','Created successfully.');
        }
        return view('sociallink::create');
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Sociallink $sociallink)
    {
        abort_unless(\Gate::allows('sociallink_edit'), 403);
        return view('sociallink::edit',compact('sociallink'));
    }



    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request,Sociallink $sociallink)
    {
        abort_unless(\Gate::allows('sociallink_edit'), 403);
        request()->validate([
            'title' => 'required',
            'link' => 'required',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);


        $data = $request->all(); 
        $data['updated_by'] = Auth::id();
        if( !($request->has('active')) ){
            $data['active'] = 0;            
        }

        if ($request->hasFile('logo')){ 
            if($sociallink->logo){
                unlink(public_path('img/logo/').$sociallink->logo);
            }
            $imageName = request()->logo->getClientOriginalName();
            request()->logo->move(public_path('img/logo'), $imageName);
            $data['logo'] = $imageName;

        }

        $sociallink->update($data);
        if($sociallink){
            session()->put('success','Updated successfully.');
        }
        return redirect()->route('sociallink.edit',compact('sociallink'));
    }




    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Sociallink $sociallink)
    {
        $sociallink->delete();
        if($sociallink){
            session()->put('success','Deleted successfully.');
        }
        return redirect()->route('sociallink.index');
    }  
}
