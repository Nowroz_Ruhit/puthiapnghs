@extends('layouts.admin')

@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Notice</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.notice.title_singular') }} {{ trans('global.list') }}
    @can('notice_create')
      <form class="form-inline">

            <a class="btn btn-outline-dark mr-1" href="{{  route('notice.create') }}">
                <i class="fas fa-edit nav-icon"></i> {{ trans('global.add') }} {{ trans('global.notice.title_singular') }}
            </a>

        <a class="btn btn-outline-dark" href="{{ route('notice.category') }}">
          <i class="fas fa-edit nav-icon"></i>   {{ trans('global.add') }} {{ trans('global.notice.fields.catagory') }}
        </a>
      </form>
      @endcan
    </nav>



    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('global.notice.fields.title') }}
                        </th>
                        <th>
                            {{ trans('global.notice.title_singular') }} {{ trans('global.notice.fields.file') }}
                        </th>
                        <th>
                            {{ trans('global.notice.fields.publish') }} {{ trans('global.notice.fields.status') }}
                        </th>
                        <th width="=10">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $notice)
                        <tr data-entry-id="{{ $notice->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $notice->title ?? '' }}
                            </td>
                            <td>
                                <a href="{{ route('notice.download', $notice->id) }}">download</a>
                            </td>
                            <td>
                                {{ $notice->active ?? '' }}
                                @if(($notice->active == NULL) || ($notice->active == 0))
                                  Publish
                                @else
                                  Unpublish
                                @endif
                            </td>
                            <td>
                                @can('notice_show')
                                    <a class="btn btn-xs btn-primary" href=" {{ route('notice.show', $notice) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('notice_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('notice.edit', $notice) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('notice_delete')
                                    <form action="{{ route('notice.destroy', $notice->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.products.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
