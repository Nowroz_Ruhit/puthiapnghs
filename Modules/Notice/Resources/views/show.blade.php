@extends('layouts.admin')
@section('style')
@parent
<!-- STYLE CSS -->
@endsection

@section('content')
<!-- body content -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('notice.index')}}">Notice</a></li>
    <li class="breadcrumb-item active" aria-current="page">view</li>
  </ol>
</nav>


<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Signle Notice View</b></a>
      <form class="form-inline">
        <a class="form-control mr-sm-2" href="{{route('notice.edit', $notice->id)}}">Edit Notice</a>
      </form>
    </nav>
  <div class="card-body">
    <p class="card-text"><h5 class="card-title">Notice Title: {{ $notice->title}}</h5></p>
    <!-- <p class="card-text">lore lip sum</p> -->
    <p class="card-text"><h5>Notice Description:</h5>
      {!!  $notice->discription !!}
    </p>

  </div>
    
   </br>
   <div class="container mb-2">
    Attesment :  <a href="{{ route('notice.download', $notice->id) }}">{{ $notice->file}}</a>
   </div>
   <br>
</div>



@endsection