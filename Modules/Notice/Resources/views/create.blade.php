@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')


<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('notice.index')}}">Notice</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
  </ol>
</nav>

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.notice.title_singular') }}
    </div>

    <div class="card-body">
        {{ 
            Aire::open()
            ->route('notice.store')
            ->rules([
            'title' => 'required',
            'discrioption' => 'required',
            'files'=> 'required|file',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')
        }}

        {{ 
            Aire::input()
            ->label('Title*')
            ->id('title')
            ->name('title')
            ->class('form-control') 
        }}

        {{ 
            Aire::textarea()
            ->label('Discription*')
            ->id('discrioption')
            ->name('discrioption')
            ->class('form-control') 
        }}

        <script>
            CKEDITOR.replace( 'discrioption' );
        </script>

        {{
            Aire::select($catagory)
            ->class('form-control')
            ->label('Notice Catagory *')
            ->name('catagory')
            ->id('catagory')
            
        }}

        {{
            Aire::input()
            ->type('file')
            ->label('Notice File*')
            ->id('files')
            ->name('files')
            
        }}


        {{ 
            Aire::button()
            ->labelHtml('<strong>Next</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>
@endsection

