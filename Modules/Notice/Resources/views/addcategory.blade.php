@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('notice.index')}}">Notice</a></li>
    <li class="breadcrumb-item"><a href="">Catagory</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.notice.fields.catagory') }}
    </div>

    <div class="card-body">
    	{{ 
            Aire::open()
            ->route('notice.storecatagory')
            ->rules([
            'title' => 'required',
            'slag' => 'required',
            ])
            ->messages([
            'title' => 'You must input this fild',
            'slag' => 'You must input this fild',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')
        }}

        {{ 
            Aire::input()
            ->label(trans('global.news.catagory').' *')
            ->id('title')
            ->name('title')
            ->class('form-control') 
        }}


        {{ 
            Aire::button()
            ->labelHtml('<strong>Create</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}
        {{ Aire::close() }}
        
    </div>
</div>
@endsection