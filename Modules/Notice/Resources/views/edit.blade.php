@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')

<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('notice.index')}}">Notice</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        {{ trans('global.update') }} {{ trans('global.notice.title_singular') }}
    </div>

    <div class="card-body">
         <!-- old('name', isset($product) ? $product->name : '')  -->
        {{ 
            Aire::open()
            ->route('notice.update', $notice)
            ->bind($notice)
            ->rules([
            'title' => 'required',
            'discription' => 'required',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
        }}

        {{ 
            Aire::input()
            ->label('Title*')
            ->id('title')
            ->name('title')
            ->class('form-control') 
            
        }}

        {{ 
            Aire::textarea()
            ->label('Discription*')
            ->id('discription')
            ->name('discription')
            ->class('form-control')
            
        }}

        <script>
            CKEDITOR.replace( 'discription' );
        </script>

<div class=" mx-sm-3 mb-2" style="display: block">
    
    <a href="{{route('notice.download', $notice)}}">{{$notice->file}}</a>
</div>
        {{
            Aire::input()
            ->type('file')
            ->label('News Cover Photo*')
            ->id('cover-photo')
            ->name('file')
            
        }}


<!-- <img src="" id="cover-img-tag" width="200px" class="mb-2" /> -->
 


        {{ 
            Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>

@endsection

