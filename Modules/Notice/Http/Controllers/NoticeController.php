<?php

namespace Modules\Notice\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Modules\Notice\Entities\Notice;
use Modules\Notice\Entities\NoticeCatagory;
use Modules\News\Entities\Updatepost;
use Intervention\Image\Facades\Image;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('notice_access'), 403);
        $data = Notice::all();
        return view('notice::index', compact('data'));
        // return view('notice::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('notice_create'), 403);
        $catagorysdata = NoticeCatagory::all();
        $catagory = array();
        $catagory[0] = 'Select Notice Catagory';
        foreach ($catagorysdata as $key => $catagorys) {
            $catagory[$catagorys['id']] = $catagorys['title'];
        }
        return view('notice::create', compact('catagory'));
        // return view('notice::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('notice_create'), 403);
        //dd(request()->file('files')->getClientOriginalName());
        $validatedData = $request->validate([
            'title'=>'required',
            'discrioption'=>'required',
            'files'=>'required',
            'catagory'=>'required',
        ]);

        $notice = new Notice();
        $notice->title = request()->input('title');
        $notice->discription = request()->input('discrioption');
        $notice->catagories_id = request()->input('catagory');
        $notice->file = now()->format('Y-m-d-H-i-s').'-'.request()->file('files')->getClientOriginalName();
        $notice->created_by = Auth::id();
        $data = $notice -> save();
        $fileName = now()->format('Y-m-d-H-i-s').'-'.request()->file('files')->getClientOriginalName();
        // dd($fileName);
        request()->file('files')->move(public_path(trans('global.links.notice')), $fileName);

        $category = NoticeCatagory::findOrFail(request()->input('catagory'))->firstOrFail();
            $updatepost['title'] = request()->input('title');
            $updatepost['type'] = $category->title;
            $updatepost['notice_id'] = $notice->id;
            Updatepost::create($updatepost);



        //return redirect()->back()->with('success', 'Create Successfull');
        if($data){
            return redirect()->route('notice.index')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('notice.index')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    // public function show($id)
    // {
    //     return view('notice::show');
    // }
    public function show(Notice $notice)
    {
        abort_unless(\Gate::allows('notice_show'), 403);
        // $data = DB::table('notices')->where('id', '=', $id)->find();
        return view('notice::show', compact('notice'));
        
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        abort_unless(\Gate::allows('notice_edit'), 403);
        $notice = Notice::find($id);
        return view('notice::edit', compact('notice'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Notice $notice)
    {
        abort_unless(\Gate::allows('notice_edit'), 403);
        $validatedData = $request->validate([
            'title'=>'required',
            'discription'=>'required',
        ]);

        $data = $request->all();
        $data['updated_by'] = Auth::id();
        $updatepost = Updatepost::where('notice_id', $notice->id)->first();
        if($updatepost == NULL || $updatepost == '' || $updatepost == null)
        {
            $category = NoticeCatagory::findOrFail($notice->catagories_id);
            if($data['title']){
                $updatepost['title'] = request()->input('title');
            }else {
                $updatepost['title'] = $notice->title;
            }
            $updatepost['type'] = $category->title;
            $updatepost['notice_id'] = $notice->id;
            $updatepost['created_by'] = Auth::id();
            Updatepost::create($updatepost);
        } else {
            $updatepost->title = $notice->title;
            $updatepost->updated_by = Auth::id();
            $updatepost->update();
        }
            
        if ($request->hasFile('file')){
            if(file_exists(public_path(trans('global.links.notice')).$notice->file))
            {
                unlink(public_path(trans('global.links.notice')).$notice->file);
            }
            $imgName = now()->format('Y-m-d-H-i-s').'-'.request()->file->getClientOriginalName();
            $data['file'] = $imgName;

            request()->file->move(public_path(trans('global.links.notice')), $imgName);

        }

        if($notice->update($data))
        {
        return redirect()->route('notice.index')->with('success', 'Update Successfull');
        } else 
        {
            return redirect()->route('notice.index')->with(['error', 'Somthing was wrong']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Notice $notice)
    {
        abort_unless(\Gate::allows('notice_delete'), 403);
        // DB::table('notices')->where('id', '=', $id)->delete();
        // $var = Notice::findOrFail($id);
        // $var->delete();
        // return back();
        $file= $notice->file;
        if($notice->delete())
        {
            $filename = public_path(trans('global.links.notice')).$file;
             if(\File::exists($filename)){
                 \File::delete($filename);
            }
           return redirect()->route('notice.index')->with('delete', 'Delete Successfull');
        }else {
            return redirect()->route('notice.index')->with('error', 'Not Deleted, Somtheing waswrong');
        }
    }

    public function addcategory()
    {
        return view('notice::addcategory');
    }

    public function storecatagory(Request $request)
    {
         // dd($request);
        $validatedData = $request->validate([
            'title'=>'required',
            // 'slag'=>'required|regex:/^[\pL\s\-]+$/u',
        ]);
        
        // echo str_replace(' ', '', request()->input('slag'));
        $catagory = new NoticeCatagory();
        // $catagory->title = request()->input('slag');
        $catagory->title = request()->input('title');
        $catagory->created_by = Auth::id();
        $data = $catagory -> save();
        // return view('notice::addcatagory');
        // return redirect()->back()->with('msg', 'Success');
        if($data){
            return redirect()->route('notice.index')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('notice.index')->with('error', 'Not Created, Somtheing waswrong');
        }

    }

    public function download($id)
    {
        $notice = Notice::find($id);
        //dd($notice[0]->file);
        $name = $notice->title;
        $file = $notice->file;
        //dd(substr($notice[0]->file, -3));
        $extention = substr($notice->file, -3);
        // dd($name.'.'.$extention);
        $pathToFile = public_path(trans('global.links.notice')).$file;
        return response()->download($pathToFile, $name.'.'.$extention);
    }
}
