<?php

namespace Modules\Notice\Entities;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
