<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::prefix('notice')->group(function() {
Route::prefix('admin')->group(function() {

    Route::resource('notice', 'NoticeController');

    // Route::get('/', 'NoticeController@index');
    Route::get('notices/catagory',['as'=>'notice.category','uses'=>'NoticeController@addcategory']);
    Route::post('notices/storecatagory',['as'=>'notice.storecatagory','uses'=>'NoticeController@storecatagory']);
    Route::get('notices/download/{id}',['as'=>'notice.download','uses'=>'NoticeController@download']);

    /*
    Route::delete('destroy/{id}',['as'=>'notice.destroy','uses'=>'NoticeController@destroy']);
    Route::get('/create',['as'=>'notice.create','uses'=>'NoticeController@create']);
    Route::post('store',['as'=>'notice.store','uses'=>'NoticeController@store']);
    Route::post('update',['as'=>'notice.update','uses'=>'NoticeController@update']);
    Route::get('show/{id}',['as'=>'notice.show','uses'=>'NoticeController@show']);
    Route::get('edit/{id}',['as'=>'notice.edit','uses'=>'NoticeController@edit']);
    */

});
