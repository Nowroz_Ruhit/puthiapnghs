<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longtext('discription');
            $table->string('file');
            // $table->integer('catagories_id');
            // $table->foreign('catagories_id')->references('id')->on('notice_catagories')->onDelete('cascade');
            $table->timestamps();
            // $table->engine='InnoDB';
        });

        Schema::table('notices', function (Blueprint $table) {
            $table->unsignedBigInteger('catagories_id');

            $table->foreign('catagories_id')->references('id')->on('notice_catagories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
