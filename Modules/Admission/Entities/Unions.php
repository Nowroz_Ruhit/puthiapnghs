<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;

class Unions extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
