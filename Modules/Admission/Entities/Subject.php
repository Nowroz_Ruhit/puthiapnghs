<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admission\Entities\SubjectList;
class Subject extends Model
{
	use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function subjecName()
    {
        return $this->belongsTo(SubjectList::class, 'subject_list_id', 'id');
        // return $this->hasMany(Subject::class, 'class_id', 'id');
        
    }

    public function subjecsName()
    {
        return $this->belongsTo(SubjectList::class, 'subject_list_id', 'id');
        // return $this->hasMany(Subject::class, 'class_id', 'id');
        
    }
}
