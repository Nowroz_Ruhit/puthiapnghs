<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admission\Entities\PaymentComplete;
use Modules\Admission\Entities\AdmissionMarkInput;
class AdmissionResult extends Model
{
    use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function complete()
    {
        return $this->belongsTo(PaymentComplete::class, 'admission_id', 'id');
    }

    public function inputMark()
    {
        return $this->belongsTo(AdmissionMarkInput::class, 'input_mark_id', 'id');
    }

    
}
