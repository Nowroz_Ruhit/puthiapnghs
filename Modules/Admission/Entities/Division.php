<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
