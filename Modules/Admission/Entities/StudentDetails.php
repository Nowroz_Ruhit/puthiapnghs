<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Admission\Entities\PaymentComplete;
use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\District;
use Modules\Admission\Entities\Upazila;

class StudentDetails extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at'];

    public function student_id()
    {
        return $this->hasOne(PaymentComplete::class, 'student_details_id', 'id');
    }

    public function className()
    {
        return $this->belongsTo(Paymentamount::class, 'paymentamounts_id', 'id');
    }
    public function psnt_district()
    {
        return $this->belongsTo(District::class, 'present_district', 'id');
    }
    public function per_district()
    {
        return $this->belongsTo(District::class, 'permananent_district', 'id');
    }
    public function loc_district()
    {
        return $this->belongsTo(District::class, 'localGurdian_district', 'id');
    }
    public function psnt_thana()
    {
        return $this->belongsTo(Upazila::class, 'present_thana', 'id');
    }
    public function per_thana()
    {
        return $this->belongsTo(Upazila::class, 'permananent_thana', 'id');
    }
    public function loc_thana()
    {
        return $this->belongsTo(Upazila::class, 'localGurdian_thana', 'id');
    }
}
