<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admission\Entities\Subject;

class SubjectList extends Model
{
	use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function subjectLists()
    {
        // return $this->belongsTo(Subject::class, 'class_id', 'id');
        return $this->hasMany(Subject::class, 'class_id', 'id')->orderby('subject_list_id', 'asc');
        
    }
    
}
