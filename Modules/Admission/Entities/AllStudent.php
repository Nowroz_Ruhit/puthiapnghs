<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AllStudent extends Model
{
	use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
