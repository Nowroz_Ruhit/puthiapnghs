<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    // protected $fillable = [];
    use SoftDeletes;
    // protected $fillable = [
    //     'tran_id','val_id','amount','card_type','store_amount','card_no','bank_tran_id','status','tran_date','card_issuer','card_brand','card_issuer_country','card_issuer_country_code','store_id','verify_sign','verify_sign_sha2','currency_type','currency_amount','currency_rate','base_fair','risk_level','risk_level'
    // ];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
