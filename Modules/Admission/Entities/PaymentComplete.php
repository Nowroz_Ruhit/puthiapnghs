<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\StudentDetails;
use Modules\Admission\Entities\Transaction;

use Modules\Admission\Entities\AdmissionMarkInput;
use Modules\Admission\Entities\AdmissionResult;
use Modules\Admission\Entities\Subject;

class PaymentComplete extends Model
{
	use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function payFor()
    {
        return $this->belongsTo(Paymentamount::class, 'paymentamounts_id', 'id');
    }

    public function student()
    {
        return $this->belongsTo(StudentDetails::class, 'student_details_id', 'id');
    }

    public function tran()
    {
        return $this->belongsTo(Transaction::class, 'transactions_id', 'id');
    }

    public function marks()
    {
        return $this->belongsTo(AdmissionMarkInput::class, 'id', 'admission_id');
    }

    public function position()
    {
        return $this->belongsTo(AdmissionResult::class, 'id', 'admission_id');
    }
    public function subjectMark()
    {
        // return $this->belongsTo(Subject::class, 'paymentamounts_id', 'class_id');
        return $this->hasMany(Subject::class, 'class_id', 'paymentamounts_id');
    }

}
