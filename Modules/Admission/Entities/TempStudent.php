<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TempStudent extends Model
{
	use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    public function student_id()
    {
        return $this->hasOne(PaymentComplete::class, 'student_details_id', 'id');
    }

    public function className()
    {
        return $this->belongsTo(Paymentamount::class, 'paymentamounts_id', 'id');
    }
}
