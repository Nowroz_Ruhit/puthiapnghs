<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Modules\Admission\Entities\PaymentComplete;

class AdmissionMarkInput extends Model
{
    use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function complete()
    {
        return $this->belongsTo(PaymentComplete::class, 'admission_id', 'id');
    }
}
