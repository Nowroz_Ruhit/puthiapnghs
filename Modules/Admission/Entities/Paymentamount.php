<?php

namespace Modules\Admission\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admission\Entities\SubjectList;
use Modules\Admission\Entities\Subject;
use Modules\Admission\Entities\PaymentComplete;

class Paymentamount extends Model
{
	use SoftDeletes;
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function classNames()
    {
        return $this->belongsTo(SubjectList::class, 'id', 'id');
    }

    public function subjectsList()
    {
    	// return $this->belongsTo(Subject::class, 'id', 'class_id');
    	return $this->hasMany(Subject::class, 'class_id', 'id')->orderby('subject_list_id', 'asc');
    }

    public function studentCount()
    {
        // return $this->belongsTo(Subject::class, 'id', 'class_id');
        return $this->hasMany(PaymentComplete::class, 'paymentamounts_id', 'id')->where('created_at','<', '2019-12-29 23:00:29');
    }

    public function studentCount1()
    {
        // return $this->belongsTo(Subject::class, 'id', 'class_id');
        return $this->hasMany(PaymentComplete::class, 'paymentamounts_id', 'id')
        ->where('created_at','>', '2019-12-29 23:00:28');
    }
    
}
