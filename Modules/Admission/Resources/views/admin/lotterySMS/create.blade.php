@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
<link href="{{ asset('css/loader.css') }}" rel="stylesheet" />
@endpush
@section('content')


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.result.lottery.select')}}">Lottery Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.sms.index')}}">SMS</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create</li>
    </ol>
</nav>


<div class="card">
    <div class="card-header">
        Send Lottery Result SMS
    </div>

    <div class="ajax_loader">
        <img src="{{ url('loading.gif') }}" class="img-responsive" />
    </div>
    <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Class <span class="requiredStar" style="color: red"> * </span></label>
                <div class="col-sm-3">
                    <select class="form-control" name="class_name" id="class_name" required>
                        <option value="">Select Class...</option>
                        @foreach($classCollection as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-2  mt-1 d-none" id="select-all-div">
                    <div class="float-right">
                        <input type="checkbox" name="check-all" id="check-all"> &nbsp; <span><b>Select All</b></span>
                    </div>
                </div>
                 
            </div>
       

        <form action="{{ route('admin.admission.result.lottery.sendSMS') }}" method="post">
            @csrf
            <div class="table-responsive table table-bordered d-none" id="student-table">
                <table id="order-listing" class="table">
                    <thead>
                    <tr>
                        <th>Serial</th>
                        <th>Student Name</th>
                        <th>Admission Lottery ID</th>
                        <th>Contact</th>
                        <th>Father Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody id="student-table-body">
                    </tbody>
                </table>
            </div>   
    </div>
            <div class="card-footer">
                <div class="col-sm-5 mt-2 d-none" id="submit-button">
                    <button class="btn btn-primary" type="submit">Send</button>
                </div>    
            </div>
        </form>
</div>

@section('scripts')
@parent
<script>
    let id ='';
    let APP_URL = {!! json_encode(url('/')) !!};
   $('#class_name').change(function(){
       id = $(this).val();
       if(id){
            $.ajax({
                type: 'get',
                url: APP_URL+"/admin/admission/result/lottery/getLotteryResult/"+id,
                beforeSend: function(){
                    $('.ajax_loader').css("visibility", "visible");
                },
                success: function(data) {
                    // console.log(data);
                    let count = 1;
                    let html = '';
                    data.forEach(element => {
                        // console.log(element);
                        html+='<tr>';
                        html+='<td>'+count+'</td>';
                        html+='<td>'+element.student_name+'</td>';
                        html+='<td class="d-none"><input type="hidden" name="lottery_id[]" value='+element.id+'></td>';
                        html+='<td>'+element.admission_lottery_id+'</td>';
                        html+='<td>'+element.contact+'</td>';
                        html+='<td>'+element.student.father_name+'</td>';
                        html+='<td><input type="hidden" name="status[]" value="0"><input type="checkbox" class="status-checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value"></td>';
                        html+='</tr>'
                        count++;
                    });
                    
                    $('#student-table-body').html(html);
                    $("#student-table").removeClass('d-none');
                    $("#submit-button").removeClass('d-none');
                    $("#select-all-div").removeClass('d-none');
                    $('#check-all').change(function () {
                        $('.status-checkbox').prop("checked" , this.checked);
                        $('.status-checkbox').prev().val(1- $('.status-checkbox').prev().val());
                    });

                    $('.status-checkbox').change(function (){
                        let ischecked= $(this).is(':checked');
                        if(!ischecked){
                            $('#check-all').prop("checked" , 0);
                        }
                    });
                },
                complete: function(){
                    $('.ajax_loader').css("visibility", "hidden");
                },
            });
       }else{
        $('#student-table-body').html('');
        $("#student-table").addClass('d-none');
        $("#submit-button").addClass('d-none');
        $("#select-all-div").addClass('d-none');
       }
       
   }); 
</script>
@endsection
@endsection