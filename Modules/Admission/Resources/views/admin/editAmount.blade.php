@extends('layouts.admin')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/aire.css') }}">
@endpush

@section('content')



<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.classAmount')}}">Class</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Class</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        Edit Class
    </div>

    <div class="card-body">
        {{ 
            Aire::open()
            ->route('admin.admission.classAmount.update', $result)
            ->bind($result)
            ->rules([
            'class' => 'required',
            'amount' => 'required',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
        }}

        {{ 
            Aire::input()
            ->label('Class *')
            ->id('class')
            ->name('class')
            ->class('form-control') 
        }}

        {{ 
            Aire::input()
            ->label('Class name *')
            ->id('name')
            ->name('name')
            ->class('form-control') 
            ->placeholder('Class 8')
        }}
        

        {{ 
            Aire::input()
            ->label('Amount/TK *')
            ->id('amount')
            ->name('amount')
            ->class('form-control') 
        }}

        {{
            Aire::input()
            ->label('Admission Date & Time *')
            ->id('admission_date')
            ->name('admission_date')
            ->class('form-control') 
            ->placeholder('YYYY-MM-DD HH:MM')
        }}


        {{ 
            Aire::button()
            ->labelHtml('<strong>Save</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>


@endsection

