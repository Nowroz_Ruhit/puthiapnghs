@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Result</li>
    </ol>
</nav>
<div class="card">
    {{--<nav class="navbar navbar-light bg-light justify-content-between">
        Result
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.xm.create') }}">
                <i class="fas fa-edit nav-icon"></i> Make Result
            </a>
        </form>

    </nav>--}}
    <div class="card-header">Result Publish</div>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Class</th>
                        <th>Total Students</th>
                        <th>Mark Status</th>
                        <th>Result Status</th>
                        <th>1st Time</th>
                        <th>2nd Time</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($classlist as $key => $class)
                    <tr data-entry-id="">
                        <td>{{$class->name}}</td>
                        <td>{{count($class->studentCount)}}</td>
                        <td>
                            {!! $class->input_mark ? '<span class="text-success font-weight-bold">Mark Input Complete</span>' : '<span class="text-danger">Pending</span>' ?? '' !!}
                        </td>
                        <td>
                            {!! $class->publish ? '<span class="text-success font-weight-bold">Result Published</span>' : '<span class="text-danger">Unpublished</span>' ?? '' !!}
                        </td>
                        <td>
                            @if($class->subject_set==1 && $class->input_mark==1 && $class->make_result==1)
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.result.show', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> View
                            </a>
                            @if($class->publish==0 )
                            <a class="btn btn-xs btn-success" href="{{ route('admin.admission.result.publish.class.result', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> Publish Now
                            </a>
                            @endif
                            @endif
                            </td>
                            <td>
                            @if($class->subject_set==1 && $class->input_mark1==1 && $class->make_result1==1)
                            <a class="btn btn-xs btn-info" href="{{ route('admin.admission.result.show1', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> View
                            </a>
                            @if($class->publish1==0 )
                            <a class="btn btn-xs btn-warning" href="{{ route('admin.admission.result.publish.class.result1', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> Publish Now
                            </a>
                            @endif
                            @endif

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
