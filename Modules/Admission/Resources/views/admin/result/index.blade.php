@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Result</li>
    </ol>
</nav>
<div class="card">
    {{--<nav class="navbar navbar-light bg-light justify-content-between">
        Result
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.xm.create') }}">
                <i class="fas fa-edit nav-icon"></i> Make Result
            </a>
        </form>

    </nav>--}}
    <div class="card-header">Result</div>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Class</th>
                        <th>Total Students</th>
                        <th>Total Students 2nd Time</th>
                        <th>Mark Input Status</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($classlist as $key => $class)
                    <tr data-entry-id="">
                        <td>{{$class->name}}</td>
                        <td>{{count($class->studentCount)}}</td>
                        <td>{{count($class->studentCount1)}}</td>
                        <td>
                            {{--$class->make_result ? 'Result Create Complete' : 'Pending'--}}
                            {!! $class->input_mark ? '<span class="text-success font-weight-bold">Mark Input Completed</span>' : '<span class="text-danger">Pending</span>' ?? '' !!}
                        </td>
                        <td>
                            @if($class->subject_set==1 && $class->input_mark==1)
                            @if($class->make_result==1)
                            <span class="text-success font-weight-bold">Merit List Completed</span>
                            @else
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.xm.show', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> View
                            </a>
                            <form action="{{ route('admin.admission.result.store', $class) }}" method="POST" onsubmit="return confirm('Are You Sure ? \nOnly First Time You Can Change This..');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{$class}}">

                                
                                <button type="submit" class="btn btn-xs btn-success">
                                    <i class="fas fa-edit nav-icon"></i> Make Result
                                </button>
                            </form>
                            @endif
                            @endif
<!-- 2nd Time -->
                            @if($class->subject_set==1 && $class->input_mark1==1)
                            @if($class->make_result1==1)
                            <span class="text-warning font-weight-bold"> 2nd Merit List Completed</span>
                            @else
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.xm.show1', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> 2nd Time View
                            </a>
                            <form action="{{ route('admin.admission.result.store1', $class) }}" method="POST" onsubmit="return confirm('Are You Sure ? \nOnly First Time You Can Change This..');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="id" value="{{$class}}">

                                
                                <button type="submit" class="btn btn-xs btn-warning">
                                    <i class="fas fa-edit nav-icon"></i> 2nd Time Make Result
                                </button>
                            </form>
                            @endif                      
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
