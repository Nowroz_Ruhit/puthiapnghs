@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Applicant Report</li>
    </ol>
</nav>
<div class="card">
    {{--<nav class="navbar navbar-light bg-light justify-content-between">
        Result
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.xm.create') }}">
                <i class="fas fa-edit nav-icon"></i> Make Result
            </a>
        </form>

    </nav>--}}
    <div class="card-header">Result
      <div class="float-right">
        <a class="btn btn-outline-dark mr-1" id="print-btn">
            <i class="fas fa-print nav-icon"></i> Print 
        </a>
      </div>
    </div>
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>SL #</th>
                        <th>Image</th>
                        <th>Admission Lottery</th>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Father's Name</th>
                        <th>Primary Contact</th>
                        @if($qoutaStatus)
                          <th>Qouta</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach ($qoutaReports as $key => $qoutaReport)
                    <tr data-entry-id="">
                        <td>{{ ++$key }}</td>
                        <td><img style="width: 80px; height:93px" src="{{asset('public/uplodefile/admission/'.$qoutaReport->image)}}" alt="" srcset=""></td>
                        <td>{{ $qoutaReport->student_id->admission_id }}</td>
                        <td>{{ $qoutaReport->name }}</td>
                        <td>{{ $qoutaReport->className->name }}</td>
                        <td>{{ $qoutaReport->father_name }}</td>
                        @if ($qoutaReport->PrimaryContact == 2)
                            <td>{{$qoutaReport->mother_phone}}</td>
                        @elseif($qoutaReport->PrimaryContact == 1) 
                            <td>{{$qoutaReport->father_phone}}</td>
                        @endif
                        @if($qoutaStatus)
                          <td>{{ $qoutaReport->qoutaName }}</td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>

<div class="printDiv" style="visibility: hidden; display:inline;">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="table-responsive table table-bordered print-container p-3" id="div-id-name"><br>
              <div class="row justify-content-center align-items-center mb-3">
                <div class="float-left mr-4">
                  <img id='img' src="{{asset(trans('global.links.general_show').$bootGeneral->logo)}}" class="">
                </div>
                <div class="float-right mb-4">
                    <span class="font-weight-bold"
                        style="font-size: 25px; margin-top:150px">{{$bootGeneral->title}}</span>
                    <br>
                   
                </div>
            </div>
                <h3  style="text-align:center">{{($qoutaStatus) ? 'Qouta' : 'Non Qouta'}} Student List</h3>
                <br>
                <table id="order-listing" class="table">
                  <thead>
                    <tr>
                      <th style="font-size: 20px;">SL #</th>
                      <th style="font-size: 20px;">Image</th>
                      <th style="font-size: 20px;">Admission Lottery</th>
                      <th style="font-size: 20px;">Name</th>
                      <th style="font-size: 20px;">Class</th>
                      <th style="font-size: 20px;">Father's Name</th>
                      <th style="font-size: 20px;">Primary Contact</th>
                      @if($qoutaStatus)
                        <th style="font-size: 20px;">Qouta</th>
                      @endif  
                    </tr>
                  </thead>
                  @php $i=0; @endphp
                  <tbody>
                    @foreach ($qoutaReports as $key=>$qoutaReport)
                    
                        <tr>
                          <td style="font-size:18px;">{{ ++$key}}</td>
                          <td><img style="width: 80px; height:93px" src="{{asset('public/uplodefile/admission/'.$qoutaReport->image)}}" alt="" srcset=""></td>
                          <td style="font-size:18px;">{{$qoutaReport->student_id->admission_id}}</td>
                          <td style="font-size:18px;">{{$qoutaReport->name}}</td>
                          <td style="font-size:18px;">{{$qoutaReport->className->name}}</td>
                          <td style="font-size:18px;">{{$qoutaReport->father_name}}</td>
                            @if ($qoutaReport->PrimaryContact == 2)
                                <td style="font-size:18px;">{{$qoutaReport->mother_phone}}</td>
                            @elseif($qoutaReport->PrimaryContact == 1) 
                                <td style="font-size:18px;">{{$qoutaReport->father_phone}}</td>
                            @endif
                            @if($qoutaStatus)
                              <td style="font-size:18px;">{{ $qoutaReport->qoutaName}}</td>
                            @endif
                        </tr>
                      
                    @endforeach
                  </tbody>
                </table>
                <br>
                <div class="float-left ml-2">
                  <p>Powered By : Desktopit.com.bd</p>
                </div>
                <div class="float-right">
                  <p>Date: {{date('d-m-Y')}}</p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})
  $('#print-btn').click( function(){
        $('.print-container').printThis();
  })
</script>
@endsection
@endsection
