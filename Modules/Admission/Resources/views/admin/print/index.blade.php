@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Print</li>
    </ol>
</nav>
<div class="card">

    <div class="card-header">Print</div>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Class</th>
                        <th>1st Time Total</th>
                        <th>2nd  Time Total</th>
                        <th>1st Time publish Status</th>
                        <th>2nd Time publish Status</th>
                        <th>1st Time</th>
                        <th>2nd Time</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($classlist as $key => $class)
                    <tr data-entry-id="">
                        <td>{{$class->name}}</td>
                        <td>{{count($class->studentCount)}}</td>
                        <td>{{count($class->studentCount1)}}</td>
                        <td>
                            {!! $class->publish ? '<span class="text-success font-weight-bold">Result Published</span>' : '<span class="text-danger">Pending</span>' ?? '' !!}
                        </td>
                        <td>
                            {!! $class->publish1 ? '<span class="text-success font-weight-bold">Result Published</span>' : '<span class="text-danger">Pending</span>' ?? '' !!}
                        </td>
                        <td>
                            @if($class->publish==1)
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.print.studentDetails', $class) }}">
                               <i class="fas fa-print nav-icon"></i> Print Student Info
                            </a>
                            @endif
                        </td>
                        <td>
                            @if($class->publish1==1)
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.print.studentDetails1', $class) }}">
                               <i class="fas fa-print nav-icon"></i> Print 2nd Time Student Info
                            </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
