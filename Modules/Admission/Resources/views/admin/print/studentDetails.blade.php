<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        @page {
            size: A4;
            /*margin: 25mm 25mm 25mm 25mm;*/
            margin: 25mm 20mm 15mm 20mm;
        }
    </style>
</head>
<body>
@foreach ($results as $key => $result)
        <div style="width: 100%; clear: both;">
            <div style="width: 100%; text-align: center; font-size: 23px;">
                Shahid Mamun Mahmud Police Lines School and College
            </div>
            <div style="width: 16%; float: left;">
                <img src="{{asset('public/uplodefile/general/'.$bootGeneral->logo)}}" width="80px" style="margin-top: 5px;">
            </div>
            <div style="width: 60%; float: left; text-align: center;">

                <div style="width: 100%; text-align: center; font-size: 20px;">
                    Police Lines, Rajshahi-6000<br>Established-1959
                </div>
                <div style="width: 100%; text-align: center;">
                    <div style="width: 60%; text-align: center; font-size: 16px; border: 2px solid black; padding: 15px; margin-right: auto; margin-left: auto;">{{$bootAdmissionStart->title}}</div>
                </div>
            </div>
            <div style="width: 24%; float: left;">
                <img src="{{  asset('public/uplodefile/admission/'.$result->complete->student->image) }}" width="120px" style="margin-top: 5px;">
            </div>
        </div>
        <!-- Header End -->

        <!-- Body Start -->
        <div style="width: 100%; margin-top: 15px; clear: both;">
            <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;">Student Information</div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Name : {{$result->complete->student->name}}
            </div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Birth Cirtificate No. : {{$result->complete->student->birth_certificate}}
            </div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Birth Day : {{$result->complete->student->birth}}
            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Nationality : Bangladeshi
                </div>
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Religion : {{$result->complete->student->religion ?? ''}}
                </div>
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Gender : {{$result->complete->student->gender ?? ''}}
                </div>
            </div>
            <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                Previous Institute Name : {{$result->complete->student->previousInstretute ? $result->complete->student->previousInstretute : ""}}
            </div>
   <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;">Parents</div>
            <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                Father's Name : {{$result->complete->student->father_name}}
            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                    NID No. : {{$result->complete->student->father_nid}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Occupation : {{$result->complete->student->father_profession}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Annual income : 
                </div>
            </div>
            <div style="width: 100%; clear: both;">

                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px;">
                    Email : {{$result->complete->student->father_email}}
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Mobile No. : {{$result->complete->student->father_phone}}
                </div>
            </div>
            <!-- M -->
            <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Mothers's Name : {{$result->complete->student->mother_name}}
            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    NID No. : {{$result->complete->student->mother_nid}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Occupation : {{$result->complete->student->mother_profession ?? ''}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Annual income : 
                </div>
            </div>
            <div style="width: 100%; clear: both;">

                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Email : {{$result->complete->student->mother_email ?? ''}}
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Mobile No. : {{$result->complete->student->mother_phone ?? ''}}
                </div>
            </div>
{{--
            <div style="width: 100%; clear: both;">
                <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold; ">Address</div>
                            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                Present Address : {{$result->complete->student->present_address }}
                            </div>
                            <div style="width: 100%; clear: both;">
                                <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                    Post Code: {{$result->complete->student->present_post_code}}
                                </div>
                                <div style="width: 33.33%; float: left; padding-top:3px;">
                                    Thana : {{$result->complete->student->psnt_thana->name}}
                                </div>
                                <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                    District : {{$result->complete->student->psnt_district->name}}
                                </div>
                            </div>
                                    <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                        Abc Permanent : {{ $result->complete->student->permananent_address ? $result->complete->student->permananent_address : $result->complete->student->present_address}}
                                    </div>
                                    <div style="width: 100%; clear: both;">
                                        <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                            Post Code: {{$result->complete->student->permananent_post_code ? $result->complete->student->permananent_post_code : $result->complete->student->present_post_code}}
                                        </div>
                                        <div style="width: 33.33%; float: left; padding-top:3px;">
                                            Thana : {{$result->complete->student->permananent_thana ? $result->complete->student->per_thana->name : $result->complete->student->psnt_thana->name}}
                                        </div>
                                        <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                            District : {{$result->complete->student->permananent_district ? $result->complete->student->per_district->name : $result->complete->student->psnt_district->name}}
                                        </div>
                                    </div>
                        </div>

                <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6;">

                    <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;"> Local Gurdian</div>
                        <div style="width: 70%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                            Name : {{$result->complete->student->localGurdian_name ? $result->complete->student->localGurdian_name : ""}}
                        </div>
                        <div style="width: 30%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                Relation : {{$result->complete->student->localGurdian_relation ? $result->complete->student->localGurdian_relation : ""}}
                            </div>
                        <div style="width: 100%; clear: both;">
                            
                            <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                Phone No : {{$result->complete->student->localGurdian_phone ? $result->complete->student->localGurdian_phone : ""}}
                            </div>
                            <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                Email  : {{ $result->complete->student->localGurdian_email ? $result->complete->student->localGurdian_email : ""}}
                            </div>
                        </div>
                        <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                            Address : {{$result->complete->student->localGurdian_address ? $result->complete->student->localGurdian_address : ""}}
                        </div>
                        <div style="width: 100%; clear: both;">
                            
                            <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                Post Code: {{$result->complete->student->localGurdian_postCode ? $result->complete->student->localGurdian_postCode : ""}}
                            </div>
                            <div style="width: 33.33%; float: left; padding-top:3px;">
                                Thana : {{$result->complete->student->localGurdian_thana ? $result->complete->student->loc_thana->name : ""}}
                            </div>
                            <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                District : {{$result->complete->student->localGurdian_district ? $result->complete->student->loc_district->name : ""}}
                            </div>
                        </div>
                </div>  

--}}
            <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6;">
                <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;"> Others Information</div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Admission Class : {{$result->complete->payFor->name}}
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    
                    Pament: {{$result->complete->tran->card_issuer}}
                </div>
                </div>
                <div style="width: 100%; clear: both; ">
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Admission <b>Lottery</b> ID : {{$result->complete->admission_id}}
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Pay Day : {{$result->complete->tran->tran_date}}
                </div>
            </div>
        </div>

        <!-- Body End -->

        <!-- Footer Start -->

            <div style="width: 100%; clear: both; page-break-after: always; text-align: center; margin-top: 30px; padding-top: 10px;" >Powered By : DesktopIT<br>
                <span style="font-size: 14px">https//:desktopit.com.bd</span>
            </div>
@endforeach
    </body>
    </html>