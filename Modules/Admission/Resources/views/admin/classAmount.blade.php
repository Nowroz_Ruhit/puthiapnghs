@extends('layouts.admin')

@section('styles')
@parent

@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
    <li class="breadcrumb-item active" aria-current="page">Amount Per Class</li>
  </ol>
</nav>

<div class="card">


    <nav class="navbar navbar-light bg-light justify-content-between">
      Amount Per Class
    {{--@can('academic_create')--}}
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{ route('admin.admission.classAmount.create') }}">
          <i class="fas fa-edit nav-icon"></i> Add Class
        </a>
      </form>
    {{--@endcan--}}
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>
                            Class
                        </th>
                        <th>
                            Class name
                        </th>
                        <th>
                            Taka
                        </th>
                        <th>
                            Date & Time
                        </th>
                        <th>
                            &nbsp;Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($amounts as $key => $amount)
                    <tr data-entry-id="{{ $amount->id }}">
                            <td>
                              {{ $amount->class ?? '' }}
                            </td>
                            <td>
                                {{ $amount->name ?? '' }}
                            </td>
                            <td>
                                {{ $amount->amount ?? '' }} Tk
                            </td>
                            <td>
                                {{ $amount->admission_date ? date('jS M Y \a\t g:i a', strtotime($amount->admission_date)) :''}}
                            </td>
                            <td>
                                {{--@can('product_edit')--}}
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.admission.classAmount.edit', $amount->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                {{--@endcan--}}
                                {{--@can('product_delete')--}}
                                    <form action="{{ route('admin.admission.classAmount.delete', $amount) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                {{--@endcan--}}
                                
                            </td>

                        </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
