@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">SMS</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
        SMS <a class="btn bg-success text-white mr-2">Available SMS : {{ $sms_count['SMSINFO']['BALANCE'] }}</a>
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.sms.create') }}">
                <i class="fas fa-edit nav-icon"></i> Create New
            </a>
        </form>

    </nav>
    <!-- <div class="card-header">SMS</div> -->
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Subject</th>
                        <th>message</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sms as $key => $message)
                    <tr data-entry-id="">
                        <td>{{++$key}}</td>
                        <td>{{$message->subject}}</td>
                        <td>{{$message->message}}</td>
                        <td>
                            <a class="btn btn-xs btn-success" href="{{ route('admin.admission.sms.sendSMS', $message) }}">
                                <i class="fas fa-send nav-icon"></i> Send SMS
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
