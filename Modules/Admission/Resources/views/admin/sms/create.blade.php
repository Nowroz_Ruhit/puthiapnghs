@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush
@section('content')


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.sms.index')}}">SMS</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create</li>
    </ol>
</nav>


<div class="card">
    <div class="card-header">
        Create
    </div>

    <div class="card-body">

            {{ 
                Aire::open()
                ->route('admin.admission.sms.store')
                ->enctype('multipart/form-data')
                ->rules([
                    'subject' => 'required',
                    'subject' => 'required',
                ])
            }}

            {{ Aire::input('input', 'SMS Subject:*')
              ->id('sms_subject')
              ->name('sms_subject')
              ->placeholder('Site Name')
              ->value('')
              ->class('form-control mb-2') 
            }}



              {{ 
                  Aire::textarea()
                  ->label("SMS Body : *")
                  ->id('message')
                  ->name('message')
                  ->class('form-control')
              }}



            {{ Aire::submit('Save SMS')->class('btn btn-success') }}

            <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="{{ route('admin.admission.sms.index') }}">Cancel</a>

            {{ Aire::close() }}
          
    </div>
</div>




@endsection