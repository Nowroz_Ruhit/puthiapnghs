@extends('layouts.admin')
@section('styles')
@parent
<!-- <link href="{{ asset('css/aire.css') }}" rel="stylesheet" /> -->
@endsection

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.sms.index')}}">SMS</a></li>
        <li class="breadcrumb-item active" aria-current="page">Send SMS</li>
    </ol>
</nav>
<div class="card">
    <div class="card-header">
        Send SMS
    </div>
    
    <div class="card-body">
        <div class="row ">
            <div class="col-lg-5 ">
                <div class="text-right pt-2">Select Class </div> 
            </div>
            <div class="col-lg-3">
                {{ Aire::open() 
                    ->route('admin.admission.sms.sms_send')
                    ->bind($sms)
                    ->rules([
                    
                    ])
                    
                    ->enctype('multipart/form-data')
                }}
                {{
                    Aire::select($classList)
                    ->name('class_id')
                    ->id('class_id')
                    ->required()
                    ->class('form-control')

                }}
                
            </div>
        </div>

        {{ Aire::input('input', 'SMS Subject:*')
              ->id('sms_subject')
              ->name('sms_subject')
              ->placeholder('SMS Subject')
              ->class('form-control mb-2') 
            }}



              {{ 
                  Aire::textarea()
                  ->label("SMS Body : *")
                  ->id('message')
                  ->name('message')
                  ->class('form-control')
              }}



            {{ Aire::submit('Save SMS')->class('btn btn-success') }}

            <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="{{ route('admin.admission.sms.index') }}">Cancel</a>

        
        {{ Aire::close() }}
    </div>
</div>
@section('scripts')
@parent

<script type="text/javascript">
   /* var selector = document.getElementById('class_id');
    console.log(selector[selector.selectedIndex].value);
*/
document.getElementById("subBtn").disabled  = true;
$("#class_id").change(function() {
    // console.log($( "#class_id option:selected" ).val());
    // var selector = document.getElementById('class_id');
    if($( "#class_id option:selected" ).val() != 0)
    {
        document.getElementById("subBtn").disabled  = false;
    }else {
        document.getElementById("subBtn").disabled  = true;
    }
});



    let table = document.getElementById("subject-table");

    for(var i = 1; i < table.rows.length; i++)
    {
        document.getElementById("total"+i).disabled  = true;
        document.getElementById("pass"+i).disabled  = true;
        // $("#total"+i).prop('disabled', true);
        // $("#pass"+i).prop('disabled', true);
        // document.getElementById("checkbox"+i).onclick = function() { myfunction("checkbox"+i); };
    }
    function myfunction(id) {
        id=parseInt(id+1);
        if(jQuery("#checkbox"+id).is(":checked"))
        {
            document.getElementById("total"+id).disabled  = false;
            document.getElementById("pass"+id).disabled  = false;
            document.getElementById("subject_id"+id).disabled  = false;
            document.getElementById("subject_d"+id).value  = parseInt(id);
        }else {
            document.getElementById("total"+id).disabled  = true;
            document.getElementById("pass"+id).disabled  = true;
            document.getElementById("total"+id).value  = "";
            document.getElementById("pass"+id).value  = "";
            document.getElementById("subject_d"+id).value  = "";
            document.getElementById("subject_id"+id).disabled  = true;
        }

    }
</script>
@endsection
@endsection
<!-- $("#permananent_address " ).val("").prop('disabled', false); -->
