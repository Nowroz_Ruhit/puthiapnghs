@extends('layouts.admin')
@section('styles')
@parent
@endsection
@section('scripts')
@parent
@endsection
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Admission</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
        Admission Dashboard
        {{--@can('academic_create')--}}
        <form class="form-inline">
            <a class="btn btn-outline-dark mr-3" href="{{ route('admin.admission.xm.index') }}">
                <i class="fas fa-cog nav-icon"></i> Mark Input
            </a>
            <a class="btn btn-outline-dark mr-3" href="{{ route('admin.admission.subjectSettings.index') }}">
                <i class="fas fa-list nav-icon"></i> Subject Settings
            </a>
            <a class="btn btn-outline-dark mr-3" href="{{ route('admin.admission.classAmount') }}">
                <i class="fas fa-list nav-icon"></i> Class List
            </a>
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.paymentStatus') }}">
                <i class="fas fa-list nav-icon"></i> View List
            </a>
        </form>
        {{--@endcan--}}
    </nav>
    <!-- <div class="card border-left-primary shadow py-2">
<div class="card-body">
<div class="row no-gutters align-items-center">
<div class="col mr-2">
<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Earnings (Monthly)</div>
<div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
</div>
<div class="col-auto">
<i class="fas fa-calendar fa-2x text-gray-300"></i>
</div>
</div>
</div>
</div> -->
    <!-- </div> -->
    <!-- <i class="fas fa-calendar fa-2x text-gray-300"></i>
<i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
<i class="fas fa-clipboard-list fa-2x text-gray-300"></i> 
<i class="fas fa-comments fa-2x text-gray-300"></i> -->
    <!--  admin dashboard  -->
    <!--     <div class="card-body">
        <div class="row ">
            <div class="col-lg-3 d-flex justify-content-center mt-3 mb-3">
                <a class="text-decoration-none text-reset" href="#" style="max-width: 15rem; min-width: 15rem;max-height: 20px;min-height: 20px;">
                    <div class="card bg-light">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Transaction (Monthly)</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">40,000 tk</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-box-open fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex justify-content-center mt-3 mb-3">
                <a class="text-decoration-none text-reset" href="#" style="max-width: 15rem; min-width: 15rem;max-height: 20px;min-height: 20px;">
                    <div class="card bg-info">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Registration (Monthly)</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-plus fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex justify-content-center mt-3 mb-3">
                <a class="text-decoration-none text-reset " href="#" style="max-width: 15rem; min-width: 15rem;max-height: 20px;min-height: 20px;">
                    <div class="card bg-danger">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Number Of Student</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-user-plus fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex justify-content-center mt-3 mb-3">
                <a class="text-decoration-none text-reset " href="#" style="max-width: 15rem; min-width: 15rem;max-height: 20px;min-height: 20px;">
                    <div class="card bg-warning">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Number Of Course</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-balance-scale fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex justify-content-center mt-3 mb-3">
                <a class="text-decoration-none text-reset " href="#" style="max-width: 15rem; min-width: 15rem;max-height: 20px;min-height: 20px;">
                    <div class="card bg-success">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Registration</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 d-flex justify-content-center mt-3 mb-3">
                <a class="text-decoration-none text-reset " href="#" style="max-width: 15rem; min-width: 15rem;max-height: 20px;min-height: 20px;">
                    <div class="card bg-primary">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Vendor</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">$40,000</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div> -->
    <!-- end admin dashboard -->
    <div class="card-columns ml-3 mr-3 mt-3">
        <div class="card bg-primary">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1" style="min-height: 140px;">
                    <p class="h5 card-text border-bottom pb-1 font-weight-bold">Transaction</p>
                    <div class=" mb-0 text-gray-800 ">Total Transaction : <span class="font-weight-bold">{{$transaction_count}}</span><br>
                    Success : <span class="font-weight-bold">{{$success_transaction_count}}</span><br>
                    Fail : <span class="font-weight-bold">{{$failed_transaction_count}}</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="card bg-success">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1" style="min-height: 140px;">
                    <p class="h5 card-text border-bottom pb-1 font-weight-bold">StudentS</p>
                    <div class="mb-0 text-gray-800">
                      Payed : <span class="font-weight-bold">{{$number_of_student}}</span><br>
                      Unayed : <span class="font-weight-bold">{{$number_of_student_panding}}</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="card bg-info">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1" style="min-height: 140px;">
                    <p class="h5 card-text border-bottom pb-1 font-weight-bold"> Amount</p>
                    <div class=" mb-0  text-gray-800">
                    Pay Ammount : <span class="font-weight-bold">{{$total_transcation_amount}}</span><br>
                    Store Ammount : <span class="font-weight-bold">{{$total_store_amount}}</span><br>
                    </div>
                </div>
            </a>
        </div>
      </div>
      <div class="card-columns ml-3 mr-3 ">
        <div class="card bg-success">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1" style="min-height: 140px;">
                    <p class="h5 card-text border-bottom pb-1 font-weight-bold">Class</p>
                    <div class="mb-0 text-gray-800">
                      Ready For Payment : <span class="font-weight-bold">{{$total_class}}</span><br>
                    </div>
                </div>
            </a>
        </div>

<!--         <div class="card bg-warning">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1">
                    <p class="card-text">Success Transaction</p>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$success_transaction_count}}</div>
                </div>
            </a>
        </div> -->
        <!-- <div class="card bg-primary">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1">
                    <p class="card-text">Cancel Transaction</p>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$cancelled_transaction_count}}</div>
                </div>
            </a>
        </div> -->
        <!-- <div class="card bg-warning">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1">
                    <p class="card-text">Fail Transaction</p>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$failed_transaction_count}}</div>
                </div>
            </a>
        </div> -->
        
        <!-- <div class="card bg-warning">
            <a class="text-decoration-none text-reset " href="#" style="max-width: 14rem; min-width: 14rem;">
                <div class="card-body text-center text-uppercase mb-1">
                    <p class="card-text">Total Payment Complete</p>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">{{$payment_complete_transcation}}</div>
                </div>
            </a>
        </div> -->
        
    </div>
    @endsection
