@extends('layouts.admin')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/aire.css') }}">
@endpush

@section('content')



<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.xm.index')}}">Marks</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.xm.show', $mark->class_id)}}">View</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit</li>
</ol>
</nav>
<div class="card">
    <div class="card-header">
        Edit Mark of *
    </div>

    <div class="card-body">
        <div class="container">
            {{ 
                Aire::open()
                ->route('admin.admission.xm.update', $mark)
                ->bind($mark)
                ->rules([
                'class' => 'required',
                'amount' => 'required',
                ])
                ->messages([
                'title' => 'You must accept the terms',
                'photo' => 'Cover Photo is Rrequired',
                ]) 
                ->enctype('multipart/form-data')
            }}
            <!-- <table class="table  table-borderless"> -->
                <table class="table  table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Admission ID</th>
                            <th>Class</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$mark->complete->student->name }}</td>
                            <td>{{$mark->complete->admission_id }}</td>
                            <td>{{$mark->complete->payFor->name}}</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>

                @if($mark->subject_1)
                {{ 
                    Aire::input()
                    ->label('Bangla')
                    ->id('name')
                    ->name('subject_1')
                    ->class('form-control') 
                }}
                @endif
                @if($mark->subject_2)
                {{ 
                    Aire::input()
                    ->label('English')
                    ->id('name')
                    ->name('subject_2')
                    ->class('form-control') 

                }}

                @endif
                @if($mark->subject_3)
                {{ 
                    Aire::input()
                    ->label('Mathematics')
                    ->id('name')
                    ->name('subject_3')
                    ->class('form-control') 

                }}
                @endif
                @if($mark->subject_4)
                {{ 
                    Aire::input()
                    ->label('Viva')
                    ->id('name')
                    ->name('subject_4')
                    ->class('form-control') 
                }}
                @endif
            </div>
            <br>
            <div class="d-flex justify-content-center" >
                <div class="row" id="apply_button_responsive">
                  {{ 
                    Aire::button()
                    ->labelHtml('Update')
                    ->id('subBtn')
                    ->class(' btn btn-primary') 
                }}


            </div>
        </div>


        {{ Aire::close() }}
        <br>
    </div>
</div>


@endsection

