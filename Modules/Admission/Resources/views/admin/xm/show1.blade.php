@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.xm.index')}}">Marks</a></li>
        <li class="breadcrumb-item active" aria-current="page">View</li>
    </ol>
</nav>
<div class="card">
    {{--<nav class="navbar navbar-light bg-light justify-content-between">
        View
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.xm.create') }}">
                <i class="fas fa-edit nav-icon"></i> Make Result
            </a>
        </form>

    </nav>--}}
    <div class="card-header">{{$cla_id->name}} Admission Marks</div>
    
    <div class="card-body">
        <div class="alert alert-success" role="alert">
        <div class="text-center h4 ">
             <span class="font-weight-bold text-uppercase text-danger">Marks Of {{$cla_id->name}}</span>
        </div>

         
        </div>
        <div class="table-responsive ">
            <br>
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <!-- <th class="text-center">&nbsp;</th> -->
                        <th class="text-center">Admission ID</th>
                        <th class="text-center">Name</th>
                        <th class="text-center">Bangla</th>
                        <th class="text-center">English</th>
                        <th class="text-center">Mathematics</th>
                        <th class="text-center">Viva</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($marks as $key => $mark)
                    <tr data-entry-id="{{$mark->id}}">
                        <td class="text-center">{{$mark->complete->admission_id }}</td>
                        <td class="text-center">{{$mark->complete->student->name }}</td>
                        <td class="text-center">{{$mark->subject_1}}</td>
                        <td class="text-center">{{$mark->subject_2}}</td>
                        <td class="text-center">{{$mark->subject_3}}</td>
                        <td class="text-center">{{$mark->subject_4}}</td>
                        <td class="text-center">
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.xm.edit', $mark) }}">
                               <i class="fas fa-edit nav-icon"></i> Edit
                            </a>
                        </td>
                        <!-- <td>&nbsp;</td> -->
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
