@extends('layouts.admin')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/aire.css') }}">
@endpush
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
         <li class="breadcrumb-item"><a href="{{route('admin.admission.xm.index')}}">Marks</a></li>
        <li class="breadcrumb-item active" aria-current="page">Make Input</li>
    </ol>
</nav>
<div class="card">
    {{--<nav class="navbar navbar-light bg-light justify-content-between">
        Make Input
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.subjectSettings.subject') }}">
                <i class="fas fa-list nav-icon"></i> Make Result
            </a>
        </form>

    </nav>--}}
    <div class="card-header">Make Input</div>
    <div class="card-body">
        <div class="text-center h4 text-success">Make Input For <span class="font-weight-bold text-uppercase"> {{$classData->name}}</span></div>

        <div class="container" id="sub-body">
            <br>
            {{-- 
                Aire::open()
                ->route('admin.admission.xm.store1')
                ->enctype('multipart/form-data')
                ->method('POST')
            --}}
            <form action="{{route('admin.admission.xm.store1')}}" method="POST" enctype="multipart/form-data">
               @csrf
            <table class="table table-bordered" id="subject-table">
                <thead>
                    <tr>
                        <!-- <th class="text-center"> Class </th> -->
                        <th class="text-center">Student ID</th>
                        <th class="text-center">Student Name</th>
                        @foreach ($classData->subjectsList as $i => $subject)
                        <th class="text-center">{{$subject->subjecName->subject_name}}</th>
                        @endforeach
                        <!-- <th class="text-center">&nbsp;</th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach ($classData->subjectsList as $i => $subject)
                        <input type="hidden" name="subject_id[]" value="{{$subject->subjecName->id}}">

                    @endforeach
                    @foreach ($classData->studentCount1 as $i => $student)
                    <tr>
                        <!-- <td>
                            Class - 1
                            <input type="hidden" name="class_id[]">
                        </td> -->
                        <td>
                            {{$student->admission_id}}
                            <input type="hidden" name="admission_id[]" value="{{$student->id}}">
                            <input type="hidden" name="class_id[]" value="{{$classData->id}}">
                            <input type="hidden" name="student[]" value="{{$student->student->id}}">
                            <input type="hidden" name="emp[]" value="">

                        </td>
                        <td>
                            {{$student->student->name}}
                        </td>
                        @foreach ($classData->subjectsList as $i => $subject)
                        <td>
                            <input type="number" name="{{$subject->subjecName->subject_name.'[]'}}" required="required" class="form-control" min="0" max="{{$subject->subject_marks}}" placeholder="">
                            {{--<input type="hidden" name="subject_id[]" value="{{$subject->subjecName->id}}">--}}
                        </td>
                        @endforeach
                        <!-- <td>&nbsp;</td> -->
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center" >
                <div class="row" id="apply_button_responsive">
                  {{-- 
                    Aire::button()
                    ->labelHtml('Save')
                    ->id('subBtn')
                    ->class(' btn btn-primary') 
                --}}
                <button class="btn btn-primary" type="submit">Save</button>

            </div>
        </div>
        </div>
        </form>
        {{-- Aire::close() --}}
    </div>
</div>
@endsection
