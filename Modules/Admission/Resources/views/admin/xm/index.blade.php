@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mark</li>
    </ol>
</nav>
<div class="card">
    {{--<nav class="navbar navbar-light bg-light justify-content-between">
        Mark
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.xm.create') }}">
                <i class="fas fa-edit nav-icon"></i> Make Input
            </a>
        </form>

    </nav>--}}
    <div class="card-header">Mark Input</div>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Class</th>
                        <th>Total Students</th>
                        <th>Total Students 2nd Time</th>
                        <th>Mark Input Status</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($classlist as $key => $class)
                    <tr data-entry-id="">
                        <td>{{$class->name}}</td>
                        <td>{{count($class->studentCount)}}</td>
                        <td>{{count($class->studentCount1)}}</td>
                        <td>
                            {{--$class->make_result ? 'Mark Input Complete' : 'Pending'--}}
                            {!! $class->input_mark ? '<span class="text-success font-weight-bold">Mark Input Complete</span>' : '<span class="text-danger">Pending</span>' ?? '' !!}
                        </td>
                        <td class="text-center">
                            @if($class->subject_set==1)
                            @if($class->make_result==0)
                            @if($class->input_mark==1)
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.xm.show', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> View
                            </a>
                            @else
                            <a class="btn btn-xs btn-success" href="{{ route('admin.admission.xm.make', $class) }}">
                                <i class="fas fa-edit nav-icon"></i> Make Input
                            </a>
                            @endif
                            @else
                            <span class="text-success font-weight-bold ml-3"><i class="fa fa-check nav-icon"></i> 1st Time Marit List Complete</span>

                            @if($class->make_result1==0)
                            @if($class->input_mark1==1)
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.xm.show1', $class) }}">
                               <i class="fas fa-eye nav-icon"></i> 2nd Time View
                            </a>
                            @else
                            <a class="btn btn-xs btn-warning" href="{{ route('admin.admission.xm.make1', $class) }}">
                                <i class="fas fa-edit nav-icon"></i>2nd Time Make Input
                            </a>
                            @endif
                            @else
                            <span class="text-success font-weight-bold"><i class="fa fa-check nav-icon"></i> 2nd Time Marit List Complete</span>
                            @endif

                            @endif
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
