@extends('layouts.admin')

@section('styles')
@parent

@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Academic</li>
  </ol>
</nav>

<div class="card">


    <nav class="navbar navbar-light bg-light justify-content-between">
      Payment Complete
    {{--@can('academic_create')
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{ route("academic.create") }}">
          <i class="fas fa-edit nav-icon"></i>   {{ trans('global.add') }} {{ trans('global.academic.fields.post') }}
        </a>
      </form>
      @endcan--}}
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>
                            Name
                        </th>
                        <th>
                            Class
                        </th>
                        <th>
                            Payment Amount
                        </th>
                        <th>
                            Transaction
                        </th>
                        <th>
                            Payment Date
                        </th>
                        <!-- <th>
                            &nbsp;Action
                        </th> -->
                    </tr>
                </thead>
                <tbody>
                  @foreach ($students as $key => $student)
                    <tr data-entry-id="{{ $student->id }}">
                            <td>
                              {{ $student->student->name }}
                            </td>
                            <td>
                                {{ $student->payFor->class ?? '' }}
                            </td>
                            <td>
                                {{ $student->amount ?? '' }}
                            </td>
                            <td>
                                {{ $student->tran_id ?? '' }}
                            </td>
                            <td>
                                {{ $student->tran_date ?? '' }}
                            </td>
                          {{--  <td>
                                @can('product_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.products.show', $product->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('product_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.products.edit', $product->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('product_delete')
                                    <form action="{{ route('admin.products.destroy', $product->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                                
                            </td>--}}

                        </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
