<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="{{asset('css/css/https _stackpath.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css')}}">
    <!-- Bootstrap CSS -->
<!--     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css"> -->
    <title>{{$admissionFormPrint->name}}</title>
    <style>
 
    th{
        font-size: 13px;
    }
    td{
        font-size: 15px;
    }
    table.no-spacing {
        border-spacing:0; /* Removes the cell spacing via CSS */
        border-collapse: collapse;  /* Optional - if you don't want to have double border where cells touch */
    }
    </style>
</head>

<body>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <ul class="nav nav-tabs" role="tablist">
                    </ul>
                    <div class="tab-content" id="demo">
                        <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">
                            <div class="col">
                                <div class="table-responsive">
                                    <div class="row">
                                        <div class="col-6 col-md-8">
                                            <table class="table table-borderless">
                                                <thead>
                                                    <th scope="col">Student Name</th>
                                                    <th scope="col">Class</th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>{{$admissionFormPrint->name}}</td>
                                                        <td>{{$admissionFormPrint->gender}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-6 col-md-4">
                                            <table class="table table-borderless">
                                                <thead>
                                                    <!-- <th scope="col">Student Imag</th> -->
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td id="image">
                                                            <img id="previewHolder" alt="Student Image" src="{{ $admissionFormPrint->image ? asset('public/uplodefile/admission/'.$admissionFormPrint->image) : asset('public/uplodefile/defult/user.png') }}" class="circle border mx-auto" width="100" />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <table class="table table-borderless">
                                        <thead>
                                            <!-- <label>Student Information</label> -->
                                            <button type="button" class="btn btn-secondary btn-sm btn-block">Student Information</button>
                                            <tr>
                                                <th scope="col">Gender</th>
                                                <th scope="col">Religion</th>
                                                <th scope="col">Date Of Birth</th>
                                                <th scope="col">Birth Certificet</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->gender }}</td>
                                                <td>{{ $admissionFormPrint->religion }}</td>
                                                <td>{{ $admissionFormPrint->birth }}</td>
                                                <td>{{ $admissionFormPrint->birth_certificate}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Blood</th>
                                                <th scope="col">Nationality</th>
                                                <th scope="col">Previous Instretute</th>
                                                <th scope="col">Previous Roll</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->blood }}</td>
                                                <td>{{ $admissionFormPrint->nationality }}</td>
                                                <td>{{ $admissionFormPrint->previousInstretute }}</td>
                                                <td>{{ $admissionFormPrint->previousRoll }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <button type="button" class="btn btn-secondary btn-sm btn-block">Gurdian Information</button>
                                            <tr>
                                                <th scope="col">Father Name </th>
                                                <th scope="col">Father Profession </th>
                                                <th scope="col">Mother Name</th>
                                                <th scope="col">Mother Profession</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$admissionFormPrint->father_name }}</td>
                                                <td>{{$admissionFormPrint->father_profession}}</td>
                                                <td>{{$admissionFormPrint->mother_name}}</td>
                                                <td>{{$admissionFormPrint->mother_profession}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Father Nid </th>
                                                <th scope="col">Father Contact No </th>
                                                <th scope="col">Mother Nid</th>
                                                <th scope="col">Mother Contact No</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$admissionFormPrint->father_nid }}</td>
                                                <td>{{$admissionFormPrint->father_phone}}</td>
                                                <td>{{$admissionFormPrint->mother_nid}}</td>
                                                <td>{{$admissionFormPrint->mother_phone}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Address </th>
                                                <th scope="col">Post Office</th>
                                                <th scope="col">Thana</th>
                                                <th scope="col">Post Code</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->present_address }}</td>
                                                <td>{{ $admissionFormPrint->present_post_office }}</td>
                                                <td>{{ $admissionFormPrint->present_thana }}</td>
                                                <td>{{ $admissionFormPrint->present_post_code }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Father E-Mail </th>
                                                <th scope="col">Mother E-Mail</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{$admissionFormPrint->father_email }}</td>
                                                <td>{{$admissionFormPrint->mother_email}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <!-- <label>Permananent Address</label> -->
                                            <button type="button" class="btn btn-secondary btn-sm btn-block">Present Address</button>
                                            <tr>
                                                <th scope="col">Address </th>
                                                <th scope="col">Post Office</th>
                                                <th scope="col">Thana</th>
                                                <th scope="col">Post Code</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->present_address }}</td>
                                                <td>{{ $admissionFormPrint->present_post_office}}</td>
                                                <td>{{ $admissionFormPrint->present_thana }}</td>
                                                <td>{{ $admissionFormPrint->present_post_code }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <!-- <label>Permananent Address</label> -->
                                            <button type="button" class="btn btn-secondary btn-sm btn-block">Permananent Address</button>
                                            <tr>
                                                <th scope="col">Address </th>
                                                <th scope="col">Post Office</th>
                                                <th scope="col">Thana</th>
                                                <th scope="col">Post Code</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->permananent_address }}</td>
                                                <td>{{ $admissionFormPrint->permananent_post_office}}</td>
                                                <td>{{ $admissionFormPrint->permananent_thana }}</td>
                                                <td>{{ $admissionFormPrint->permananent_post_code }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <!-- <label>Permananent Address</label> -->
                                            <button type="button" class="btn btn-secondary btn-sm btn-block">Local Gurdian</button>
                                            <tr>
                                                <th scope="col">Name</th>
                                                <th scope="col">Relation</th>
                                                <th scope="col">Contact Number</th>
                                                <th scope="col">Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->localGurdian_name }}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_relation}}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_phone }}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_email }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Address</th>
                                                <th scope="col">Village</th>
                                                <th scope="col">Road</th>
                                                <th scope="col">Post Office</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->localGurdian_address }}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_village }}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_road }}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_post_office }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <tr>
                                                <th scope="col">Post Code</th>
                                                <th scope="col">Thana</th>
                                                <th scope="col">District</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->localGurdian_postCode }}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_thana }}</td>
                                                <td>{{ $admissionFormPrint->localGurdian_district }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="table table-borderless">
                                        <thead>
                                            <button type="button" class="btn btn-secondary btn-sm btn-block">Bank Information</button>
                                            <tr>
                                                <th scope="col">Payment Amounts Id </th>
                                                <th scope="col">Transactions Id</th>
                                                <th scope="col">Payment By</th>
                                                <th scope="col">Bank Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $admissionFormPrint->paymentamounts_id }}</td>
                                                <td>{{ $admissionFormPrint->transactions_id }}</td>
                                                <td>Rocket</td>
                                                <td>Bangladesh Bank</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--table-responsive-->
                        </div>
                        <!--tab-->
                    </div>
                    <!--tab-content-->
                </div>
                <!--col-->
            </div>
            <!--row-->
        </div>
        <!--card-body-->
        <!-- <a href="#basic" id="printKitten">print</a> -->
        <div class="col-sm-3">
            <button href="#basic" id="printKitten" type="button" class="btn btn-primary btn-sm btn-block" onclick="printDiv('demo');">Print</button>
        </div>
 <script src="{{ asset('modules/admission/Resources/assets/js/print.min.js') }}"></script>     
<script type="text/javascript">
    function printDiv(divName) {

    printJS({
        printable: divName,
        type: 'html',
        style: '@page { size: A4; }',
        css: "{{asset('css/css/https _stackpath.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css')}}",
        header: false,
        scanStyles: true,
        documentTitle : '',
        modalMessage : '',
    });

  // $("#viewammount").css("display", "none");

}
</script>

</body>

</html>