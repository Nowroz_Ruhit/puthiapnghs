@extends('layouts.admin')
@section('styles')
@parent
@endsection
@section('scripts')
@parent
@endsection
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Subject Settings</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
        Subject Settings
        {{--@can('academic_create')--}}
        <form class="form-inline">
            <a class="btn btn-outline-dark mr-3" href="{{ route('admin.admission.subjectSettings.subject') }}">
                <i class="fas fa-list nav-icon"></i> Subject List
            </a>
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.subjectSettings.create') }}">
                <i class="fas fa-edit nav-icon"></i> Set Subjects Marks
            </a>
            {{--<a class="btn btn-outline-dark mr-3" href="{{ route('admin.admission.classAmount') }}">
                <i class="fas fa-list nav-icon"></i> Class List
            </a>
            <a class="btn btn-outline-dark" href="{{ route('admin.admission.paymentStatus') }}">
                <i class="fas fa-list nav-icon"></i> View List
            </a>--}}
        </form>
        {{--@endcan--}}
    </nav>
    
    <div class="card-body">
        <div class="row">
            @foreach ($classList as $key => $class)

            <div class="col-lg-4">
                <div class="card">
                    <nav class="navbar navbar-light bg-light justify-content-between border border-bottom-0">
                        {{$class->name}}
                        <form class="form-inline">
                            <a class="" href="{{ route('admin.admission.subjectSettings.edit', $class)}}">
                                <i class="fas fa-edit nav-icon"></i>
                            </a>
                        </form>
                    </nav>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>বিষয়</th>
                                <th>নম্বর</th>
                                <th>পাশ নাম্বার</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($class->subjectsList as $key => $val)
                            <tr>
                                <td>{{$val->subjecName->subject_name}}</td>
                                <td>{{$val->subject_marks}}</td>
                                <td>{{$val->subject_pass_marks}}</td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>

            @endforeach
            <!-- End -->
        </div>
    </div>
</div>
@endsection
