@extends('layouts.admin')

@section('styles')
@parent

@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.subjectSettings.index')}}">Subject Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Subject List</li>
  </ol>
</nav>

<div class="card">


    <nav class="navbar navbar-light bg-light justify-content-between">
      Subject List
    {{--@can('academic_create')--}}
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{route('admin.admission.subjectSettings.subjects.create')}}">
          <i class="fas fa-edit nav-icon"></i> Add Subject
        </a>
      </form>
    {{--@endcan--}}
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>
                            Subject
                        </th>
                        <!-- <th>
                            Class name
                        </th>
                        <th>
                            Taka
                        </th> -->
                        <th>
                            &nbsp;Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($subjectList as $key => $subject)
                  <tr>
                      <td data-entry-id="{{ $subject->id }}">
                                {{ $subject->subject_name ?? '' }}
                            </td>
                            <td>

                                    <a class="btn btn-xs btn-info" href="{{ route('admin.admission.subjectSettings.subjects.edit', $subject) }}">
                                        {{ trans('global.edit') }}
                                    </a>

                                    <form action="{{ route('admin.admission.subjectSettings.subjects.delete', $subject) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                
                            </td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
