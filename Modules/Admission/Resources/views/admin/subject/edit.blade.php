@extends('layouts.admin')
@section('styles')
@parent
<!-- <link href="{{ asset('css/aire.css') }}" rel="stylesheet" /> -->
@endsection

@section('content')

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.subjectSettings.index')}}">Subject Settings</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create</li>
    </ol>
</nav>
<div class="card">
    <div class="card-header">
        Create
    </div>
    
    <div class="card-body">
        <div class="row border-bottom">
            <div class="col-lg-5 ">
                <div class="text-right pt-2">Select Class </div> 
            </div>
            <div class="col-lg-3">
                {{ Aire::open() 
                    ->route('admin.admission.subjectSettings.update', $data)
                    ->bind($result)
                    ->enctype('multipart/form-data')
                }}
                {{
                    Aire::select([$data->id => $data->name])
                    ->name('class_id')
                    ->id('class_id')
                    ->class('form-control')
                    ->value($data->id)
                }}
                    <!-- ->value($data->id) -->

 
         
            </div>
        </div>
        <div class="container" id="sub-body">
            <br>
            <table class="table table-borderless" id="subject-table">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Subject</th>
                        <th class="text-center">Marks</th>
                        <th class="text-center">Pass Marks</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $x=0; ?>

                    @foreach ($result as $i => $subject)
                    <tr >
                        <td>
                            <input type="checkbox" name="checkbox[]" id="checkbox{{$i+1}}" onclick="myfunction({{$i}});" class="mt-2" 
                            
                            @if($i<count($result))
                            {{$result[$i]->checkbox ? 'checked' : ''}}
                            @endif>
                            
                        </td>
                        <td>
                            <div class="mt-2">{{$subject->subject_name ?? ''}}</div>
                            <input type="hidden" name="subject_list_id[]" id="subject_id{{$i+1}}" value="{{$subject->sub_id}}">
                            <input type="hidden" name="subject_list_d[]" id="subject_d{{$i+1}}" >
                            <input type="hidden" name="checkbox_d[]" id="checkbox_d{{$i+1}}" >
                        </td>
                        <td>
                            {{
                                Aire::number()
                                ->class('form-control')
                                ->id('total'.($i+1))
                                ->name('subject_marks[]')
                                ->value($i<count($result) ? $result[$i]->subject_marks : "")
                            }}
                        </td>
                        <td>
                            {{
                                Aire::number()
                                ->class('form-control')
                                ->id('pass'.($i+1))
                                ->name('subject_pass_marks[]')
                                ->value($i<count($result) ? $result[$i]->subject_pass_marks : "")
                            }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center" >
                <div class="row" id="apply_button_responsive">
                  {{ 
                    Aire::button()
                    ->labelHtml('Update')
                    ->id('subBtn')
                    ->class(' btn btn-primary') 
                }}

            </div>
        </div>
        </div>
        {{ Aire::close() }}
    </div>
</div>
@section('scripts')
@parent

<script type="text/javascript">
    let table = document.getElementById("subject-table");

    for(var i = 1; i < table.rows.length; i++)
    {
        // if(document.getElementById("total"+id).value == "")
        if(jQuery("#checkbox"+i).is(":checked"))
        {
            document.getElementById("total"+i).disabled  = false;
            document.getElementById("pass"+i).disabled  = false;
            document.getElementById("subject_d"+i).value  = parseInt(i);
            // document.getElementById("#checkbox_d"+i).value = document.getElementById("subject_id"+i).value;
        }else{ 
            document.getElementById("total"+i).disabled  = true;
            document.getElementById("pass"+i).disabled  = true;
            /*document.getElementById("total"+i).value  = "";
            document.getElementById("pass"+i).value  = "";
            document.getElementById("subject_d"+i).value  ="";*/
            // document.getElementById("checkbox_d"+i).removeAttr( "value" );
        }
    }
    function myfunction(id) {
        id=parseInt(id+1);
        if(jQuery("#checkbox"+id).is(":checked"))
        {
            document.getElementById("total"+id).disabled  = false;
            document.getElementById("pass"+id).disabled  = false;
            // document.getElementById("subject_id"+id).disabled  = false;
            document.getElementById("subject_d"+id).value  = parseInt(id);
            document.getElementById("checkbox_d"+id).value = ""
        }else {
            document.getElementById("checkbox_d"+id).value = document.getElementById("subject_id"+id).value;
            /*document.getElementById("total"+id).value  = "";
            document.getElementById("pass"+id).value  = "";*/
            document.getElementById("total"+id).disabled  = true;
            document.getElementById("pass"+id).disabled  = true;
            // document.getElementById("subject_id"+id).disabled  = true;
            document.getElementById("subject_d"+id).value  = "";
            
        }

    }
</script>
@endsection
@endsection

