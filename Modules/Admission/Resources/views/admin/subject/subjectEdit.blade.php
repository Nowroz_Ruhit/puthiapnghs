@extends('layouts.admin')
@section('styles')
@parent
<!-- <link href="{{ asset('css/aire.css') }}" rel="stylesheet" /> -->
@endsection
@section('scripts')
@parent
@endsection
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.subjectSettings.index')}}">Subject Settings</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.subjectSettings.subject')}}">Subjects</a></li>
        <li class="breadcrumb-item active" aria-current="page">Update</li>
    </ol>
</nav>
<div class="card">
    <div class="card-header">
        Update
    </div>
    
    <div class="card-body">

        {{ Aire::open() 
            ->route('admin.admission.subjectSettings.subjects.update', $subjectList)
            ->bind($subjectList)
            ->rules([
                'subject_name' => 'required',
                ])
            ->enctype('multipart/form-data')
        }}

        {{
            Aire::input()
            ->class('form-control')
            ->id('')
            ->label('Subject Code (optional)')
            ->name('subject_code')
            ->placeholder('')
        }}

        {{
            Aire::input()
            ->class('form-control')
            ->id('')
            ->label('Subject')
            ->name('subject_name')
            ->placeholder('Bangla')
        }}

        <div class="d-flex justify-content-center" >
            <div class="row" id="apply_button_responsive">
              {{ 
                Aire::button()
                ->labelHtml('Update')
                ->id('subBtn')
                ->class(' btn btn-info') 
            }}

        </div>
    </div>
</div>
{{ Aire::close() }}

</div>

@endsection

