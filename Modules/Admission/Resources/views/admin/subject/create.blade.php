@extends('layouts.admin')
@section('styles')
@parent
<!-- <link href="{{ asset('css/aire.css') }}" rel="stylesheet" /> -->
@endsection

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.subjectSettings.index')}}">Subject Settings</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create</li>
    </ol>
</nav>
<div class="card">
    <div class="card-header">
        Create
    </div>
    
    <div class="card-body">
        <div class="row border-bottom">
            <div class="col-lg-5 ">
                <div class="text-right pt-2">Select Class </div> 
            </div>
            <div class="col-lg-3">
                {{ Aire::open() 
                    ->route('admin.admission.subjectSettings.store')
                    ->rules([
                    'subject_pass_marks' => 'same:subject_marks',
                    ])
                    ->messages([
                    'accepted' => 'You must accept the terms',
                    ])
                    ->enctype('multipart/form-data')
                }}
                {{
                    Aire::select($classList)
                    ->name('class_id')
                    ->id('class_id')
                    ->class('form-control')

                }}
                
            </div>
        </div>
        <div class="container" id="sub-body">
            <br>
            <table class="table table-borderless" id="subject-table">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Subject</th>
                        <th class="text-center">Marks</th>
                        <th class="text-center">Pass Marks</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subjectList as $i => $subject)
                    <tr >
                        <td>
                            <input type="checkbox" name="checkbox[]" id="checkbox{{$i+1}}" onclick="myfunction({{$i}});" class="mt-2">
                        </td>
                        <td>
                            <div class="mt-2">{{$subject->subject_name ?? ''}}</div>
                            <input type="hidden" name="subject_list_id[]" id="subject_id{{$i+1}}" value="{{$subject->id}}">
                            <input type="hidden" name="subject_list_d[]" id="subject_d{{$i+1}}" >
                        </td>
                        <td>
                            {{
                                Aire::number()
                                ->class('form-control')
                                ->id('total'.($i+1))
                                ->name('subject_marks[]')
                            }}
                        </td>
                        <td>
                            {{
                                Aire::number()
                                ->class('form-control')
                                ->id('pass'.($i+1))
                                ->name('subject_pass_marks[]')
                            }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center" >
                <div class="row" id="apply_button_responsive">
                  {{--<a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Cancel</a>
                  <a class="btn btn-warning inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Clear</a>--}}
                  {{ 
                    Aire::button()
                    ->labelHtml('Save')
                    ->id('subBtn')
                    ->class(' btn btn-primary') 
                }}

            </div>
        </div>
        </div>
        {{ Aire::close() }}
    </div>
</div>
@section('scripts')
@parent

<script type="text/javascript">
   /* var selector = document.getElementById('class_id');
    console.log(selector[selector.selectedIndex].value);
*/
document.getElementById("subBtn").disabled  = true;
$("#class_id").change(function() {
    // console.log($( "#class_id option:selected" ).val());
    // var selector = document.getElementById('class_id');
    if($( "#class_id option:selected" ).val() != 0)
    {
        document.getElementById("subBtn").disabled  = false;
    }else {
        document.getElementById("subBtn").disabled  = true;
    }
});



    let table = document.getElementById("subject-table");

    for(var i = 1; i < table.rows.length; i++)
    {
        document.getElementById("total"+i).disabled  = true;
        document.getElementById("pass"+i).disabled  = true;
        // $("#total"+i).prop('disabled', true);
        // $("#pass"+i).prop('disabled', true);
        // document.getElementById("checkbox"+i).onclick = function() { myfunction("checkbox"+i); };
    }
    function myfunction(id) {
        id=parseInt(id+1);
        if(jQuery("#checkbox"+id).is(":checked"))
        {
            document.getElementById("total"+id).disabled  = false;
            document.getElementById("pass"+id).disabled  = false;
            document.getElementById("subject_id"+id).disabled  = false;
            document.getElementById("subject_d"+id).value  = parseInt(id);
        }else {
            document.getElementById("total"+id).disabled  = true;
            document.getElementById("pass"+id).disabled  = true;
            document.getElementById("total"+id).value  = "";
            document.getElementById("pass"+id).value  = "";
            document.getElementById("subject_d"+id).value  = "";
            document.getElementById("subject_id"+id).disabled  = true;
        }

    }
</script>
@endsection
@endsection
<!-- $("#permananent_address " ).val("").prop('disabled', false); -->
