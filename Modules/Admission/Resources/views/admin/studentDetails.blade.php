@extends('layouts.admin')
@section('styles')
@parent
@endsection
@section('scripts')
@parent
@endsection
@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('frontend.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.paymentStatus')}}">Status</a></li>
        <li class="breadcrumb-item active" aria-current="page">Student Info</li>
    </ol>
</nav>
<div class="accordion" id="accordionExample">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Student Information
                </button>
            </h2>
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="row">
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Name :{{ $admissionFormDetails->name }}
                        </p>
                        <p class="justify-content-sd-start">
                            Date Of Birth :{{ $admissionFormDetails->birth }}
                        </p>
                        <p class="justify-content-sd-start">
                            Gender :{{ $admissionFormDetails->gender }}
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Blood :{{ $admissionFormDetails->blood }}
                        </p>
                        <p class="justify-content-sd-start">
                            Religion :{{ $admissionFormDetails->religion }}
                        </p>
                        <p class="justify-content-sd-start">
                            Birth Certificate : 
                            {{ $admissionFormDetails->birth_certificate }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Nationality :{{ $admissionFormDetails->nationality }}
                        </p>
                        <p class="justify-content-sd-start">Previous Instretute :{{ $admissionFormDetails->previousInstretute }}
                        </p>
                        <p class="justify-content-sd-start">
                            Previous Roll :{{ $admissionFormDetails->previousRoll }}
                        </p>
                    </div>
                </div>
                <div class="col-md-2 mt-2">
                    <img id="previewHolder" alt="Student Image" src="{{ $admissionFormDetails->image ? asset('public/uplodefile/admission/'.$admissionFormDetails->image) : asset('public/uplodefile/defult/user.png') }}" class="circle mx-auto" width="120" />
                    {{--<h4>{{ $admissionFormDetails->name }}</h4>--}}
                </div>
                {{--<div class="col-md-4">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Payment Amounts Id :{{ $admissionFormDetails->paymentamounts_id }}
                        </p>
                        <p class="justify-content-sd-start">
                            Transactions Id :{{ $admissionFormDetails->transactions_id }}
                        </p>
                        <p class="justify-content-sd-start">
                            Birth Certificate :{{ $admissionFormDetails->birth_certificate }}
                        </p>
                    </div>
                </div>--}}
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Gurdian Information
                </button>
            </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="row">
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Father Name :{{ $admissionFormDetails->father_name }}
                        </p>
                        <p class="justify-content-sd-start">
                            Father Nid :{{ $admissionFormDetails->father_nid }}
                        </p>
                        <p class="justify-content-sd-start">
                            Father Phone Number:{{ $admissionFormDetails->father_phone }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Father Profession :{{ $admissionFormDetails->father_profession }}
                        </p>
                        <p class="justify-content-sd-start">
                            Father Email :{{ $admissionFormDetails->father_email }}
                        </p>
                        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Mother Name :{{ $admissionFormDetails->mother_name }}
                        </p>
                        <p class="justify-content-sd-start">
                            Mother Nid :{{ $admissionFormDetails->mother_nid }}
                        </p>
                        <p class="justify-content-sd-start">
                            Mother Phone :{{ $admissionFormDetails->mother_phone }}
                        </p>
                        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Mother Profession :{{ $admissionFormDetails->mother_profession }}
                        </p>
                        <p class="justify-content-sd-start">
                            Mother Email :{{ $admissionFormDetails->mother_email }}
                        </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingThree">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Present Address
                </button>
            </h2>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Present Address :{{ $admissionFormDetails->present_address }}
                        </p>
                        <!--<p class="justify-content-sd-start">
                            Present Village :{{-- $admissionFormDetails->present_village --}}
                        </p>
                        <p class="justify-content-sd-start">
                            Present Road :{{-- $admissionFormDetails->present_road --}}
                        </p>
                        <p class="justify-content-sd-start">
                            Present Post Office :{{-- $admissionFormDetails->present_post_office --}}
                        </p>-->
                        <p class="justify-content-sd-start">
                            Present Post Code :{{ $admissionFormDetails->present_post_code }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present Thana :{{ $admissionFormDetails->psnt_thana->name }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present District :{{ $admissionFormDetails->psnt_district->name }}
                        </p>

                        <!-- <p class="justify-content-sd-start">
                            Mother Nid :{{ $admissionFormDetails->mother_nid }}
                        </p> -->
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="card-body">
                        
                    </div>
                </div> -->
                <div class="col-md-6">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Permananent Address :{{ $admissionFormDetails->permananent_address ?
                            $admissionFormDetails->permananent_address :
                            $admissionFormDetails->present_address }}
                        </p>
                        <!--<p class="justify-content-sd-start">
                            Permananent Village :{{-- $admissionFormDetails->permananent_village --}}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Road :{{-- $admissionFormDetails->permananent_road --}}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Post Office :{{-- $admissionFormDetails->permananent_post_office --}}
                        </p>-->
                        <p class="justify-content-sd-start">
                            Permananent Post Code :{{ $admissionFormDetails->permananent_post_code ? 
                            $admissionFormDetails->permananent_post_code : 
                            $admissionFormDetails->present_post_code
                            }}

                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Thana :{{ $admissionFormDetails->permananent_thana ?
                            $admissionFormDetails->per_thana->name : 
                            $admissionFormDetails->psnt_thana->name
                            }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent District :{{ $admissionFormDetails->permananent_district ?
                            $admissionFormDetails->per_district->name : 
                            $admissionFormDetails->psnt_district->name
                            }}
                        </p>
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="card-body">
                        
                    </div>
                </div> -->
                @if($admissionFormDetails->PrimaryContact==3)
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Name :{{ $admissionFormDetails->localGurdian_name }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Relation :{{ $admissionFormDetails->localGurdian_relation }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Phone Number:{{ $admissionFormDetails->localGurdian_phone }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Email :{{ $admissionFormDetails->localGurdian_email }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Address :{{ $admissionFormDetails->localGurdian_address }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village :{{ $admissionFormDetails->localGurdian_village }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village Road :{{ $admissionFormDetails->localGurdian_road }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Post Office :{{ $admissionFormDetails->localGurdian_post_office }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian PostCode :{{ $admissionFormDetails->localGurdian_postCode }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Thana :{{ $admissionFormDetails->localGurdian_thana }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian District :{{ $admissionFormDetails->localGurdian_district }}
                        </p>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingFour">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Primary Contact
                </button>
            </h2>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
            <div class="row">
                {{--<div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Payment Status : {{ $PaymentComplete->complete ? 'Paid' : 'unpaid' }}
                        </p>
                        <p class="justify-content-sd-start">
                            Transaction ID : $data->present_village 
                        </p>
                        <p class="justify-content-sd-start">
                            Present Road :{{ $admissionFormDetails->present_road }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present Post Office :{{ $admissionFormDetails->present_post_office }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Present Post Code :{{ $admissionFormDetails->present_post_code }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present Thana :{{ $admissionFormDetails->present_thana }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present District :{{ $admissionFormDetails->present_district }}
                        </p>
                        <p class="justify-content-sd-start">
                            Mother Nid :{{ $admissionFormDetails->mother_nid }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Permananent Address :{{ $admissionFormDetails->permananent_address }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Village :{{ $admissionFormDetails->permananent_village }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Road :{{ $admissionFormDetails->permananent_road }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Post Office :{{ $admissionFormDetails->permananent_post_office }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Permananent Post Code :{{ $admissionFormDetails->permananent_post_code }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Thana :{{ $admissionFormDetails->permananent_thana }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent District :{{ $admissionFormDetails->permananent_district }}
                        </p>
                    </div>
                </div>--}}
                @if($admissionFormDetails->PrimaryContact == 3)
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Name :{{ $admissionFormDetails->localGurdian_name }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Relation :{{ $admissionFormDetails->localGurdian_relation }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Phone Number:{{ $admissionFormDetails->localGurdian_phone }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Email :{{ $admissionFormDetails->localGurdian_email }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Address :{{ $admissionFormDetails->localGurdian_address }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village :{{ $admissionFormDetails->localGurdian_village }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village Road :{{ $admissionFormDetails->localGurdian_road }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Post Office :{{ $admissionFormDetails->localGurdian_post_office }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian PostCode :{{ $admissionFormDetails->localGurdian_postCode }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Thana :{{ $admissionFormDetails->localGurdian_thana }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian District :{{ $admissionFormDetails->localGurdian_district }}
                        </p>
                    </div>
                </div>
                @elseif($admissionFormDetails->PrimaryContact == 2)
                <div class="col-lg-12">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Primary Contact Is Set As Mother
                        </p>
                        <p class="justify-content-sd-start">
                            All Information about Admission will resived By {{$admissionFormDetails->name}}'s Mother By SMS & Email
                        </p>
                        {{--<p class="justify-content-sd-start">
                            Local Gurdian District :{{ $admissionFormDetails->localGurdian_district }}
                        </p>--}}
                    </div>
                </div>
                @elseif($admissionFormDetails->PrimaryContact == 1)
                <div class="col-lg-12">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Primary Contact Is Set As Father
                        </p>
                        <p class="justify-content-sd-start">
                            All Information about Admission will resived By {{$admissionFormDetails->name}}'s Father By SMS & Email
                        </p>
                        {{--<p class="justify-content-sd-start">
                            Local Gurdian District :{{ $admissionFormDetails->localGurdian_district }}
                        </p>--}}
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    {{--<div class="card">
        <div class="card-header" id="headingFive">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                    Payment Status
                </button>
            </h2>
        </div>
        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
            <div class="row">
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Payment Status : {{ $PaymentComplete->complete ? 'Paid' : 'unpaid' }}
                        </p>
                        <p class="justify-content-sd-start">
                            Transaction ID : $data->present_village 
                        </p>
                        <p class="justify-content-sd-start">
                            Present Road :{{ $admissionFormDetails->present_road }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present Post Office :{{ $admissionFormDetails->present_post_office }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Present Post Code :{{ $admissionFormDetails->present_post_code }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present Thana :{{ $admissionFormDetails->present_thana }}
                        </p>
                        <p class="justify-content-sd-start">
                            Present District :{{ $admissionFormDetails->present_district }}
                        </p>
                        <p class="justify-content-sd-start">
                            Mother Nid :{{ $admissionFormDetails->mother_nid }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Permananent Address :{{ $admissionFormDetails->permananent_address }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Village :{{ $admissionFormDetails->permananent_village }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Road :{{ $admissionFormDetails->permananent_road }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Post Office :{{ $admissionFormDetails->permananent_post_office }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Permananent Post Code :{{ $admissionFormDetails->permananent_post_code }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Thana :{{ $admissionFormDetails->permananent_thana }}
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent District :{{ $admissionFormDetails->permananent_district }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Name :{{ $admissionFormDetails->localGurdian_name }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Relation :{{ $admissionFormDetails->localGurdian_relation }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Phone Number:{{ $admissionFormDetails->localGurdian_phone }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Email :{{ $admissionFormDetails->localGurdian_email }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Address :{{ $admissionFormDetails->localGurdian_address }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village :{{ $admissionFormDetails->localGurdian_village }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village Road :{{ $admissionFormDetails->localGurdian_road }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Post Office :{{ $admissionFormDetails->localGurdian_post_office }}
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian PostCode :{{ $admissionFormDetails->localGurdian_postCode }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Thana :{{ $admissionFormDetails->localGurdian_thana }}
                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian District :{{ $admissionFormDetails->localGurdian_district }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
</div>
@endsection
