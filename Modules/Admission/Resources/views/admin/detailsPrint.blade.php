<!-- detailsPrint.blade.php -->

{{--<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ asset('admission/css/details-print.css') }}">
</head>
<body>
<!-- <button onclick="printDiv('student-full-info')">Print</button> -->
	<div id="student-full-info">
		<div id="custom-print-header" class="row custom-print-header">
			<div id="custom-print-img" style="width: 20%; float: left;" class="col-sm-2">
				<img src="{{asset('public/uplodefile/general/'.$bootGeneral->logo)}}" class="float-right" width="80px">
			</div>
			<div id="custom-print-name" style="width: 80%; float: left;" class="col-sm-10">
				Shahid Mamun Mahmud Police Lines<br> School and College, Rajshahi
			</div>
		</div>

		<div id="main-body">
			<div id="personal-info" class="parofbody">
				<div class="div_title">Student Information</div>
				<table id="name_table">
					<tr>
						<th>Name :</th>
						<td colspan="3">{{$complete->student->name}}</td>
						<td rowspan="5" style="border: none !important;" align="center">
							<img src="{{asset('public/uplodefile/admission/'.$complete->student->image)}}" class="float-right " width="100px" style="margin-left: 10px;">
						</td>
					</tr>
					<tr>
						<th>Class :</th>
						<td>{{$complete->payFor->name}}</td>

						<th>Gender :</th>
						<td>{{$complete->student->gender}}</td>
					</tr>
					<tr>
						<th>Admiddion ID :</th>
						<td>{{$complete->admission_id}}</td>
						<th>Religion :</th>
						<td>{{$complete->student->religion}}</td>
					</tr>
					<tr>
						<th>Birth Certificet :</th>
						<td>{{$complete->student->birth_certificate}}</td>
						<th>Date Of Birth :</th>
						<td>{{$complete->student->birth}}</td>
					</tr>
					<tr>
						<th>Blood :</th>
						<td>{{$complete->student->blood}}</td>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
				<!-- <div id="user_image_div">
					<img src="{{asset('public/uplodefile/general/2019-09-23-12-20-19-Edit-Monogram_001.png')}}" class="float-right" width="80px">
				</div> -->
				<!-- <br><br><br> -->
			</div>
			<!-- <div style="clear: both;"></div> -->
<!-- <hr> -->
			<div class="parofbody">
				<!-- <br><br> -->
				<div class="div_title">Gurdian Information</div>
				<br>
				<table>
					<tr>
						<th>&nbsp;</th>
						<th class="center">Father</th>
						<th class="center">Mother</th>
					</tr>
					<tr>
						<th>Name</th>
						<td>{{$complete->student->father_name}}</td>
						<td>{{$complete->student->mother_name}}</td>
					</tr>
					<tr>
						<th>Phone No</th>
						<td>{{$complete->student->father_phone}}</td>
						<td>{{$complete->student->mother_phone}}</td>
					</tr>
					<tr>
						<th>Email</th>
						<td>{{$complete->student->father_email}}</td>
						<td>{{$complete->student->mother_email}}</td>
					</tr>
					<tr>
						<th>NID No.</th>
						<td>{{$complete->student->father_nid}}</td>
						<td>{{$complete->student->mother_nid}}</td>
					</tr>
					<tr>
						<th>Occupation</th>
						<td>{{$complete->student->father_profession}}</td>
						<td>{{$complete->student->mother_profession}}</td>
					</tr>
				</table>
			</div>
<br>
			<div class="parofbody">
				<div class="div_title">Address Information</div>
				<br>
				<table>
					<tr>
						<th>&nbsp;</th>
						<th class="center">Present Address</th>
						<th class="center">Permanent Address</th>
					</tr>
					<tr>
						<th>Address</th>
						<td>{{$complete->student->present_address}}</td>
						<td>{{$complete->student->permananent_address ? $complete->student->permananent_address :  $complete->student->present_address}}</td>
					</tr>
					<tr>
						<th>Post Code</th>
						<td>{{$complete->student->present_post_code}}</td>
						<td>{{$complete->student->permananent_post_code ? $complete->student->permananent_post_code : $complete->student->present_post_code }}</td>
					</tr>
					<tr>
						<th>Thana </th>
						<td>{{$complete->student->psnt_thana->name}}</td>
						<td>{{$complete->student->permananent_thana ? $complete->student->per_thana->name : $complete->student->psnt_thana->name}}</td>
					</tr>
					<tr>
						<th>District</th>
						<td>{{$complete->student->psnt_district->name}}</td>
						<td>{{$complete->student->permananent_district ? $complete->student->per_district->name : $complete->student->psnt_district->name}}</td>
					</tr>
					
					
				</table>
			</div>
			<br>
			<div class="parofbody">
				<div class="div_title">Primary Contact Information</div>
				@if($complete->student->PrimaryContact == 1)
					<br><table><tr><td><span>Primary Contact sat as Father</span></td></tr></table>
				@elseif($complete->student->PrimaryContact == 2)
					<br><table><tr><td><span>Primary Contact sat as Mother</span></td></tr></table>
				@elseif($complete->student->PrimaryContact == 3)
					<span>Primary Contact sat as Local Gurdian</span>
				<table>
					<tr>
						<th>Name</th>
						<td>{{$complete->student->localGurdian_name}}</td>
						<th>Relation</th>
						<td>{{$complete->student->localGurdian_relation}}</td>
						<th>Phoner No.</th>
						<td>{{$complete->student->localGurdian_phone}}</td>
					</tr>
					<tr>
						<th>Email</th>
						<td>{{$complete->student->localGurdian_email}}</td>
						<th>Address</th>
						<td colspan="3">{{$complete->student->localGurdian_address}}</td>
					</tr>
					<tr>
						<!-- <th>Village</th>
						<td>{{$complete->student->localGurdian_village}}</td> -->
						<th>Post Code</th>
						<td>{{$complete->student->localGurdian_postCode}}</td>
						<th>Thana</th>
						<td>{{$complete->student->loc_thana->name}}</td>
						<th>District</th>
						<td>{{$complete->student->loc_district->name}}</td>
					</tr>
					<!-- <tr>
						<th>Road</th>
						<td>{{$complete->student->localGurdian_road}}</td>
						<th>District</th>
						<td>{{$complete->student->loc_district->name}}</td>
						<th>Post Code</th>
						<td>{{$complete->student->blood}}</td>
						<td colspan="2">&nbsp;</td>
					</tr> -->
				</table>
				@endif
			</div>
			<br>
			<div class="parofbody">
				<div class="div_title">Others Information</div>
				<table>
					<tr>
						<th>Payed By</th>
						<td>{{$complete->tran->card_type}}</td>
						<th>Ammount</th>
						<td>{{$complete->tran->amount}}</td>
						<th>Payment Date</th>
						<td>{{$complete->tran->tran_date}}</td>
					</tr>
					<tr>
						<th>Previous Instretute</th>
						<td colspan="3">{{$complete->student->previousInstretute}}</td>
						<th>Previous Roll</th>
						<td>{{$complete->student->previousRoll}}</td>
					</tr>
				</table>
			</div>

		</div>
		<div class="divFooter">Powerd By : desktopit.com.bd</div>
	</div>
	<script src="{{ asset('admission/js/print.min.js') }}"></script>
	<!-- <script type="text/javascript">
		function printDiv(divName) {
	    printJS({
	        printable: divName,
	        type: 'html',
	        style: '@page { size: A4; }',
	        css: "{{ asset('modules/admission/Resources/assets/css/details-print.css') }}",
	        header: false,
	        scanStyles: true,
	        documentTitle : '',
	        modalMessage : '',
	    });

	  // $("#viewammount").css("display", "none");

	}
	</script> -->
</body>
</html>--}}

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        @page {
            size: A4;
            /*margin: 25mm 25mm 25mm 25mm;*/
            margin: 25mm 20mm 15mm 20mm;
        }
    </style>
</head>
<body>

        <div style="width: 100%;">
            <div style="width: 100%; text-align: center; font-size: 23px;">
                Puthia P.N Government High School
            </div>
            <div style="width: 16%; float: left;">
                <img src="{{asset('public/LOGO-SCHOOL.png') }}" width="100px" style="margin-top: 5px;">
            </div>
            <div style="width: 60%; float: left; text-align: center;">

                <div style="width: 100%; text-align: center; font-size: 20px;">
                    Puthia, Rajshahi-6260<br>Established-1865
				</div>
				<br>
                <div style="width: 100%; text-align: center;">
                    <div style="width: 60%; text-align: center; font-size: 16px; border: 2px solid black; padding: 15px; margin-right: auto; margin-left: auto;">{{$bootAdmissionStart->title}}</div>
                </div>
            </div>
            <div style="width: 24%; float: left;">
                <img src="{{  asset('public/uplodefile/admission/'.$complete->student->image) }}" width="120px" style="margin-top: 5px;">
            </div>
        </div>
        <!-- Header End -->

        <!-- Body Start -->
        <div style="width: 100%; margin-top: 15px; clear: both;">
            <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;">Student Information</div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Name : {{$complete->student->name}}
            </div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Birth Cirtificate No. : {{$complete->student->birth_certificate}}
            </div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Birth Day : {{$complete->student->birth}}
            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Nationality : Bangladeshi
                </div>
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Religion : {{$complete->student->religion}}
                </div>
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Gender : {{$complete->student->gender}}
                </div>
            </div>
            <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                Previous Institute Name : {{$complete->student->previousInstretute ? $complete->student->previousInstretute : ""}}
            </div>
   <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;">Parents</div>
            <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                Father's Name : {{$complete->student->father_name}}
            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                    NID No. : {{$complete->student->father_nid}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Occupation : {{$complete->student->father_profession}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Annual income : 
                </div>
            </div>
            <div style="width: 100%; clear: both;">

                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px;">
                    Email : {{$complete->student->father_email}}
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Mobile No. : {{$complete->student->father_phone}}
                </div>
            </div>
            <!-- M -->
            <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Mothers's Name : {{$complete->student->mother_name}}
            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    NID No. : {{$complete->student->mother_nid}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Occupation : {{$complete->student->mother_profession}}
                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Annual income : 
                </div>
            </div>
            <div style="width: 100%; clear: both;">

                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Email : {{$complete->student->mother_email}}
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Mobile No. : {{$complete->student->mother_phone}}
                </div>
            </div>

            <div style="width: 100%; clear: both;">
                <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold; ">Address</div>
                            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                Present Address : {{$complete->student->present_address}}
                            </div>
                            <div style="width: 100%; clear: both;">
                                <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                    Post Code: {{$complete->student->present_post_code}}
                                </div>
                                <div style="width: 33.33%; float: left; padding-top:3px;">
                                    Thana : {{$complete->student->psnt_thana->name}}
                                </div>
                                <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                    District : {{$complete->student->psnt_district->name}}
                                </div>
                            </div>
                                    <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                        Permanent Address: {{ $complete->student->permananent_address ? $complete->student->permananent_address : $complete->student->present_address}}
                                    </div>
                                    <div style="width: 100%; clear: both;">
                                        <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                            Post Code: {{$complete->student->permananent_post_code ? $complete->student->permananent_post_code : $complete->student->present_post_code}}
                                        </div>
                                        <div style="width: 33.33%; float: left; padding-top:3px;">
                                            Thana : {{$complete->student->permananent_thana ? $complete->student->per_thana->name : $complete->student->psnt_thana->name}}
                                        </div>
                                        <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                            District : {{$complete->student->permananent_district ? $complete->student->per_district->name : $complete->student->psnt_district->name}}
                                        </div>
                                    </div>
                        </div>

                <!--<div style="width: 100%; clear: both; border-top: 1px solid #dee2e6;">-->

                <!--    <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;"> Local Gurdian</div>-->
                <!--        <div style="width: 70%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--            Name : {{$complete->student->localGurdian_name ? $complete->student->localGurdian_name : ""}}-->
                <!--        </div>-->
                <!--        <div style="width: 30%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--                Relation : {{$complete->student->localGurdian_relation ? $complete->student->localGurdian_relation : ""}}-->
                <!--            </div>-->
                <!--        <div style="width: 100%; clear: both;">-->
                            
                <!--            <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--                Phone No : {{$complete->student->localGurdian_phone ? $complete->student->localGurdian_phone : ""}}-->
                <!--            </div>-->
                <!--            <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--                Email  : {{ $complete->student->localGurdian_email ? $complete->student->localGurdian_email : ""}}-->
                <!--            </div>-->
                <!--        </div>-->
                <!--        <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--            Address : {{$complete->student->localGurdian_address ? $complete->student->localGurdian_address : ""}}-->
                <!--        </div>-->
                <!--        <div style="width: 100%; clear: both;">-->
                            
                <!--            <div style="width: 33.33%; float: left; padding-top:3px;"> -->
                <!--                Post Code: {{$complete->student->localGurdian_postCode ? $complete->student->localGurdian_postCode : ""}}-->
                <!--            </div>-->
                <!--            <div style="width: 33.33%; float: left; padding-top:3px;">-->
                <!--                Thana : {{$complete->student->localGurdian_thana ? $complete->student->loc_thana->name : ""}}-->
                <!--            </div>-->
                <!--            <div style="width: 33.33%; float: left; padding-top:3px;"> -->
                <!--                District : {{$complete->student->localGurdian_district ? $complete->student->loc_district->name : ""}}-->
                <!--            </div>-->
                <!--        </div>-->
                <!--</div>  -->


            <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6;">
                <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;"> Others Information</div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Admission Class : {{$complete->payFor->name}}
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    
                    Payment: {{$complete->tran->card_issuer}}
                </div>
                </div>
                <div style="width: 100%; clear: both; ">
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    <b>Admission Lottery ID</b> : <b>{{$complete->admission_id}}</b>
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Pay Day : {{$complete->tran->tran_date}}
				</div>
				
			</div>
			<div style="width: 100%; clear: both; ">
				@if($complete->student->hasQouta)
					<div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
						Qouta : {{$complete->student->qoutaName}}
					</div>
				@endif
			</div>
        </div>

        <!-- Body End -->

        <!-- Footer Start -->
            <div style="width: 100%; text-align: center; margin-top: 85px;" >Powered By : DesktopIT<br>
                <span style="font-size: 14px">http//:desktopit.com.bd</span>
            </div>

    </body>
    </html>