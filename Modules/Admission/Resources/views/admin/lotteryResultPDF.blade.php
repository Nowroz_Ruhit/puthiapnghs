<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style>
            @page {
                /* size: A4; */
                /*margin: 25mm 25mm 25mm 25mm;*/
                /* margin: 10mm 10mm 10mm 10mm;  */
            }
            .table {
width: 100%;
margin-bottom: 1rem;
color: #212529;
}

.table th,
.table td {
padding: 0.75rem;
vertical-align: top;
border-top: 1px solid #dee2e6;
}

.table thead th {
vertical-align: bottom;
border-bottom: 2px solid #dee2e6;
}

.table tbody + tbody {
border-top: 2px solid #dee2e6;
}

.table-sm th,
.table-sm td {
padding: 0.3rem;
}

.table-bordered {
border: 1px solid #dee2e6;
}

.table-bordered th,
.table-bordered td {
border: 1px solid #dee2e6;
}

.table-bordered thead th,
.table-bordered thead td {
border-bottom-width: 2px;
}

.table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
border: 0;
}

.table-striped tbody tr:nth-of-type(odd) {
background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
color: #212529;
background-color: rgba(0, 0, 0, 0.075);
}

.table-primary,
.table-primary > th,
.table-primary > td {
background-color: #b8daff;
}

.table-primary th,
.table-primary td,
.table-primary thead th,
.table-primary tbody + tbody {
border-color: #7abaff;
}

.table-hover .table-primary:hover {
background-color: #9fcdff;
}

.table-hover .table-primary:hover > td,
.table-hover .table-primary:hover > th {
background-color: #9fcdff;
}

.table-secondary,
.table-secondary > th,
.table-secondary > td {
background-color: #d6d8db;
}

.table-secondary th,
.table-secondary td,
.table-secondary thead th,
.table-secondary tbody + tbody {
border-color: #b3b7bb;
}

.table-hover .table-secondary:hover {
background-color: #c8cbcf;
}

.table-hover .table-secondary:hover > td,
.table-hover .table-secondary:hover > th {
background-color: #c8cbcf;
}

.table-success,
.table-success > th,
.table-success > td {
background-color: #c3e6cb;
}

.table-success th,
.table-success td,
.table-success thead th,
.table-success tbody + tbody {
border-color: #8fd19e;
}

.table-hover .table-success:hover {
background-color: #b1dfbb;
}

.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
background-color: #b1dfbb;
}

.table-info,
.table-info > th,
.table-info > td {
background-color: #bee5eb;
}

.table-info th,
.table-info td,
.table-info thead th,
.table-info tbody + tbody {
border-color: #86cfda;
}

.table-hover .table-info:hover {
background-color: #abdde5;
}

.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
background-color: #abdde5;
}

.table-warning,
.table-warning > th,
.table-warning > td {
background-color: #ffeeba;
}

.table-warning th,
.table-warning td,
.table-warning thead th,
.table-warning tbody + tbody {
border-color: #ffdf7e;
}

.table-hover .table-warning:hover {
background-color: #ffe8a1;
}

.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
background-color: #ffe8a1;
}

.table-danger,
.table-danger > th,
.table-danger > td {
background-color: #f5c6cb;
}

.table-danger th,
.table-danger td,
.table-danger thead th,
.table-danger tbody + tbody {
border-color: #ed969e;
}

.table-hover .table-danger:hover {
background-color: #f1b0b7;
}

.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
background-color: #f1b0b7;
}

.table-light,
.table-light > th,
.table-light > td {
background-color: #fdfdfe;
}

.table-light th,
.table-light td,
.table-light thead th,
.table-light tbody + tbody {
border-color: #fbfcfc;
}

.table-hover .table-light:hover {
background-color: #ececf6;
}

.table-hover .table-light:hover > td,
.table-hover .table-light:hover > th {
background-color: #ececf6;
}

.table-dark,
.table-dark > th,
.table-dark > td {
background-color: #c6c8ca;
}

.table-dark th,
.table-dark td,
.table-dark thead th,
.table-dark tbody + tbody {
border-color: #95999c;
}

.table-hover .table-dark:hover {
background-color: #b9bbbe;
}

.table-hover .table-dark:hover > td,
.table-hover .table-dark:hover > th {
background-color: #b9bbbe;
}

.table-active,
.table-active > th,
.table-active > td {
background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
background-color: rgba(0, 0, 0, 0.075);
}

.table .thead-dark th {
color: #fff;
background-color: #343a40;
border-color: #454d55;
}

.table .thead-light th {
color: #495057;
background-color: #e9ecef;
border-color: #dee2e6;
}

.table-dark {
color: #fff;
background-color: #343a40;
}

.table-dark th,
.table-dark td,
.table-dark thead th {
border-color: #454d55;
}

.table-dark.table-bordered {
border: 0;
}

.table-dark.table-striped tbody tr:nth-of-type(odd) {
background-color: rgba(255, 255, 255, 0.05);
}

.table-dark.table-hover tbody tr:hover {
color: #fff;
background-color: rgba(255, 255, 255, 0.075);
}

@media (max-width: 575.98px) {
.table-responsive-sm {
display: block;
width: 100%;
overflow-x: auto;
-webkit-overflow-scrolling: touch;
}
.table-responsive-sm > .table-bordered {
border: 0;
}
}

@media (max-width: 767.98px) {
.table-responsive-md {
display: block;
width: 100%;
overflow-x: auto;
-webkit-overflow-scrolling: touch;
}
.table-responsive-md > .table-bordered {
border: 0;
}
}

@media (max-width: 991.98px) {
.table-responsive-lg {
display: block;
width: 100%;
overflow-x: auto;
-webkit-overflow-scrolling: touch;
}
.table-responsive-lg > .table-bordered {
border: 0;
}
}

@media (max-width: 1199.98px) {
.table-responsive-xl {
display: block;
width: 100%;
overflow-x: auto;
-webkit-overflow-scrolling: touch;
}
.table-responsive-xl > .table-bordered {
border: 0;
}
}

.table-responsive {
display: block;
width: 100%;
overflow-x: auto;
-webkit-overflow-scrolling: touch;
}

.table-responsive > .table-bordered {
border: 0;
}

.table-responsive {
display: block;
width: 100%;
overflow-x: auto;
-webkit-overflow-scrolling: touch;
}

.table-responsive > .table-bordered {
border: 0;
}

.flex-container {
  display: flex;
  flex-wrap: wrap;
}

.flex-item-left {
  flex: 50%;
}

.flex-item-right {
  flex: 50%;
}
        </style>
        {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/css/https _stackpath.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css')}}"> --}}
        {{-- <link href="{{ asset('css/custom.css') }}" rel="stylesheet" /> --}}
        
    </head>
    <body>
        <div class="printDiv">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-12">
                    <div class="table-responsive table table-bordered print-container" id="div-id-name"><br>
                      <div class="row justify-content-center align-items-center mb-3">
                        <div class="" style="width: 20%; margin: 0 auto;">
                          <img id='img' style="width:150px;height:150px" src="{{asset(trans('global.links.general_show').$bootGeneral->logo)}}" class="">
                        </div>
                        <br>
                        {{-- <div class="" style="margin: 0 auto;">
                            <span class="font-weight-bold"
                                style="font-size: 20px;margin-top:150px;">Puthia P.N Government High School</span>
                            <br>
                            <span class="font-weight-bold" style="font-size: 20px">{{ $lotteryResults->first()->paymentAmount->name ?? '' }}</span>
                        </div> --}}
                        <p class="font-weight-bold" style="font-size: 20px;margin-top:20px;text-align:center">Puthia P.N Government High School</p>
                        <p class="font-weight-bold" style="font-size: 20px;text-align:center">
                            {{ $lotteryResults->first()->paymentAmount->name ?? '' }}</p>
                        </div>
                        <h3  style="text-align:center">Admission Lottery Result</h3>
                        <br>
                        <table id="order-listing" class="table">
                          <thead>
                            <tr>
                              <th style="font-size: 15px;">Serial</th>
                              <th style="font-size: 15px;">Image</th>
                              <th style="font-size: 15px;">Student Name</th>
                              <th style="font-size: 15px;">Admission Lottery ID</th>
                              <th style="font-size: 15px;">Contact</th>
                              <th style="font-size: 15px;">Father Name</th>
                              <th style="font-size: 15px;">Status</th>
                            </tr>
                          </thead>
                          @php $i=0; @endphp
                          <tbody>
                            @foreach ($lotteryResults as $key=>$lotteryResult)
                              @if($lotteryResult->status)
                                <tr>
                                  <td style="font-size:12px;">{{++$i}}</td>
                                  <td><img style="width: 80px; height:80px" src="{{asset('public/uplodefile/admission/'.$lotteryResult->student->image)}}" alt="" srcset=""></td>
                                  <td style="font-size:12px;">{{$lotteryResult->student_name}}</td>
                                  <td style="font-size:12px;">{{$lotteryResult->admission_lottery_id}}</td>
                                  <td style="font-size:12px;">{{$lotteryResult->contact}}</td>
                                  <td style="font-size:12px;">{{$lotteryResult->student->father_name}}</td>
                                  <td style="font-size:12px;">{{($lotteryResult->status) ? 'Selected' : 'Not Selected'}}</td>
                                </tr>
                              @endif
                            @endforeach
                          </tbody>
                        </table>
                        <br>
                        <div>
                            <span style="float:right">{{date('d-m-Y')}}</span><span style="float:left">Powered By : Desktopit.com.bd</span>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </body>
</html>