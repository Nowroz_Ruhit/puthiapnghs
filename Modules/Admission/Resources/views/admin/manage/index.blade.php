@extends('layouts.admin')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
        <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Manasge</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      Manage
    @can('admission_reset')
      <form class="form-inline">
        <a class="btn btn-info mr-3" href="{{ route('admin.admission.manage.resetbackup') }}">
          <i class="fas fa-file-upload"></i> Backup & Reset
        </a>
        <a class="btn btn-outline-dark" href="{{ route('admin.admission.manage.reset') }}">
          <i class="fas fa-undo nav-icon"></i> Reset
        </a>
      </form>
    @endcan
    </nav>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Admission</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($admission as $key => $class)
                    <tr data-entry-id="">
                        <td>{{$class->title}}</td>
                        <td>{{$class->start}}</td>
                        <td>{{$class->end}}</td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="{{ route('admin.admission.manage.edit', $class) }}">
                               <i class="fas fa-edit nav-icon"></i> Edit
                            </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
