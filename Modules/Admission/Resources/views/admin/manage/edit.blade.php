@extends('layouts.admin')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/aire.css') }}">
@endpush

@section('content')



<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.index')}}">Admission</a></li>
    <li class="breadcrumb-item"><a href="{{route('admin.admission.classAmount')}}">Class</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Class</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        Edit Class
    </div>

    <div class="card-body">
        {{ 
            Aire::open()
            ->route('admin.admission.manage.update', $admission)
            ->bind($admission)
            ->rules([
            'title' => 'required'


            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
        }}

        {{ 
            Aire::input()
            ->label('Title *')
            ->id('title')
            ->name('title')
            ->class('form-control') 
            ->placeholder('Admission - 2019') 
        }}

        {{ 
            Aire::input()
            ->label('Admission Open *')
            ->id('start')
            ->name('start')
            ->class('form-control')
            ->placeholder('YYYY-MM-DD HH:MM') 
        }}

        {{ 
            Aire::input()
            ->label('Admission Close *')
            ->id('end')
            ->name('end')
            ->class('form-control') 
            ->placeholder('YYYY-MM-DD HH:MM')

        }}
        

        {{ 
            Aire::button()
            ->labelHtml('<strong>Save</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>


@endsection

