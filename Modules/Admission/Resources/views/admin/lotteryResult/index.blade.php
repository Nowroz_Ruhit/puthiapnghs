@extends('layouts.admin')

@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item" aria-current="page">Admission</li>
    <li class="breadcrumb-item">Result</li>
    <li class="breadcrumb-item active" aria-current="page">Lottery Results</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      Lottery Result List {{($lotteryResults->isNotEmpty()) ? 'for '.$lotteryResults->first()->paymentAmount->name : ''}}
    {{-- @can('notice_create') --}}
      <form class="form-inline">
            @if($lotteryResults->isNotEmpty())
                <a class="btn btn-outline-dark mr-1" id="download-pdf" href="{{ route('admin.admission.result.lottery.pdf', $lotteryResults->first()->payment_amount_id) }}">
                  <i class="fas fa-file-pdf nav-icon"></i> Download PDF 
                </a>
            @endif
            <a class="btn btn-outline-dark mr-1" id="print-btn">
                <i class="fas fa-print nav-icon"></i> Print 
            </a>
            <a class="btn btn-outline-dark mr-1" href="{{  route('admin.admission.result.lottery.choose') }}">
                <i class="fas fa-edit nav-icon"></i> Create Lottery Result
            </a>
      </form>
      {{-- @endcan --}}
    </nav>



    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">
                            Serial
                        </th>
                        <th>
                            Student Name
                        </th>
                        <th>
                            Admission Lottery ID
                        </th>
                        <th>
                            Contact
                        </th>
                        <th>
                            Class
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                           Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($lotteryResults as $key=>$lotteryResult)
                        <tr>
                          <td>{{++$key}}</td>
                          <td>{{$lotteryResult->student_name}}</td>
                          <td>{{$lotteryResult->admission_lottery_id}}</td>
                          <td>{{$lotteryResult->contact}}</td>
                          <td>{{$lotteryResult->paymentAmount->name}}</td>
                          <td>{{($lotteryResult->status) ? 'Selected' : 'Not Selected'}}</td>
                          <td class="text-center">
                            <a href="{{ route("admin.admission.result.lottery.editlotteryResult", $lotteryResult)}}"
                              class="btn btn-primary btn-sm"
                              title="Edit"
                              role="button">
                                <i class="fa fa-edit"></i>
                            </a>
                          </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="printDiv" style="visibility: hidden; display:inline;">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <div class="table-responsive table table-bordered print-container p-3" id="div-id-name"><br>
                <div class="row justify-content-center align-items-center mb-3">
                  <div class="float-left mr-4">
                    <img id='img' src="{{asset(trans('global.links.general_show').$bootGeneral->logo)}}" class="">
                  </div>
                  <div class="float-right mb-4">
                      <span class="font-weight-bold"
                          style="font-size: 25px; margin-top:150px">{{$bootGeneral->title}}</span>
                      <br>
                      <span class="font-weight-bold" style="font-size: 25px">{{ $lotteryResults->first()->paymentAmount->name ?? '' }}</span>
                  </div>
              </div>
                  <h3  style="text-align:center">Admission Lottery Result</h3>
                  <br>
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                        <th style="font-size: 20px;">Serial</th>
                        <th style="font-size: 20px;">Student Name</th>
                        <th style="font-size: 20px;">Admission Lottery ID</th>
                        <th style="font-size: 20px;">Contact</th>
                        <th style="font-size: 20px;">Father Name</th>
                        <th style="font-size: 20px;">Status</th>
                      </tr>
                    </thead>
                    @php $i=0; @endphp
                    <tbody>
                      @foreach ($lotteryResults as $key=>$lotteryResult)
                        @if($lotteryResult->status)
                          <tr>
                            <td style="font-size:18px;">{{++$i}}</td>
                            <td style="font-size:18px;">{{$lotteryResult->student_name}}</td>
                            <td style="font-size:18px;">{{$lotteryResult->admission_lottery_id}}</td>
                            <td style="font-size:18px;">{{$lotteryResult->contact}}</td>
                            <td style="font-size:18px;">{{$lotteryResult->student->father_name}}</td>
                            <td style="font-size:18px;">{{($lotteryResult->status) ? 'Selected' : 'Not Selected'}}</td>
                          </tr>
                        @endif
                      @endforeach
                    </tbody>
                  </table>
                  <br>
                  <div class="float-left ml-2">
                    <p>Powered By : Desktopit.com.bd</p>
                  </div>
                  <div class="float-right">
                    <p>Date: {{date('d-m-Y')}}</p>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.products.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})
  $('#print-btn').click( function(){
        $('.print-container').printThis();
  })

  

</script>
@endsection
@endsection
