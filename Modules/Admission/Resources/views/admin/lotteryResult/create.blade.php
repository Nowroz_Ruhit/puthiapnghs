@extends('layouts.admin')

@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item" aria-current="page">Admission</li>
    <li class="breadcrumb-item" aria-current="page">Result</li>
    <li class="breadcrumb-item active" aria-current="page">Create Lottery Result</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      Create Lottery Result {{($students->isNotEmpty()) ? 'for '.$students->first()->payFor->name : ''}}
    </nav>

    <div class="card-body">
        <form action="{{ route('admin.admission.result.lottery.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="table-responsive col-9">
                <table class=" table table-bordered table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <tr>
                                <th>SL No.</th>
                                <th>Admission Lottery ID</th>
                                <th>Name</th>
                                <th>Father Name</th>
                                {{-- @permission('attendance_edit') --}}
                                <th>Action</th>
                                {{-- @endpermission --}}
                            </tr>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($students as $key=>$student)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$student->admission_id}}</td>
                                <td class="d-none"><input type="hidden" name="student_id[]" value="{{$student->student->id}}"></td>
                                <td>{{$student->student->name}}</td>
                                <td>{{$student->student->father_name}}</td>
                                <td>
                                  {{-- <input class="status-checkbox" name="status[]" type="checkbox"> --}}
                                  <input type="hidden" name="status[]" value="0"><input type="checkbox" class="status-checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value">
                                  {{-- here two checkboxes are used as a hack to force the checkbox to submit value even when it is not checked to help organize request data in controller --}}
                                </td>
                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                
            </div>
            <div class="col-3">
              <div class="card" style="position:fixed;">
                <div class="card-body">
                  <span id="checkbox-count">0</span>
                  <span> Students Selected</span>
                  <br>
                  <button class="btn btn-primary mt-2" type="submit">Create</button>
                </div>
              </div>
            </div>
          </div>  
        </form>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.products.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

$('input[type="checkbox"]').click(function(){
    $('#checkbox-count').text($('.status-checkbox:checked').length);
});

</script>
@endsection
@endsection
