@extends('layouts.admin')

@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item" aria-current="page">Admission</li>
    <li class="breadcrumb-item" aria-current="page">Result</li>
    <li class="breadcrumb-item active" aria-current="page">Choose Class</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      Choose Class to Prepare Lottery Result
 </nav>



    <div class="card-body">
        <form id="lottery" action="{{route('admin.admission.result.lottery.create')}}" method="POST">
          @csrf
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Class <span class="requiredStar" style="color: red"> * </span></label>
                <div class="col-sm-5">
                    <select class="form-control" name="class_name" id="class_name" required>
                        <option value="">Select Class...</option>
                          @foreach($classCollection as $key => $value)
                            <option value="{{$key}}">{{$value}}</option>
                          @endforeach
                    </select>
                </div>
                <div class="w-100"></div>
                <div class="col-sm-2"></div>
                <div class="col-sm-5 mt-2">
                  <button class="btn btn-primary" type="submit">Create</button>
                </div>  
            </div>
        </form>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.products.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
