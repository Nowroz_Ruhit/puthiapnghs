@extends('layouts.admin')

@section('styles')
@parent

@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Academic</li>
  </ol>
</nav>

<div class="card">


    <nav class="navbar navbar-light bg-light justify-content-between">
      Panding Process
    {{--@can('academic_create')
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{ route("academic.create") }}">
          <i class="fas fa-edit nav-icon"></i>   {{ trans('global.add') }} {{ trans('global.academic.fields.post') }}
        </a>
      </form>
      @endcan--}}
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>
                            Name
                        </th>
                        <th>
                            Father
                        </th>
                        <th>
                            Mother
                        </th>

                        <th>
                            Class
                        </th>
                        <th>
                            Amount
                        </th>
                        <th>
                            &nbsp;Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($students as $key => $student)
                    <tr data-entry-id="{{ $student->id }}">
                            <td>
                              {{ $student->name }}
                            </td>
                            <td>
                                {{ $student->father_name ?? '' }}
                            </td>
                            <td>
                                {{ $student->mother_name ?? '' }}
                            </td>
                            <td>
                                {{ $student->class ?? '' }}
                            </td>
                            <td>
                                {{ $student->amount ?? '' }}
                            </td>
                            
                            <td class="text-center">
                                <!-- @can('product_show') -->
                                    <a class="btn btn-xs btn-primary" href=" route('admin.products.show', $student->id) ">
                                        Show
                                    </a>
                               <!--  @endcan
                                @can('product_edit') -->
                                    <a class="btn btn-xs btn-info" href=" route('admin.products.edit', $student->id) ">
                                        Edit
                                    </a>
                                <!-- @endcan -->
                            {{--    @can('product_delete')
                                    <form action="{{ route('admin.products.destroy', $product->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan--}}
                                
                            </td>

                        </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
