@extends('admission::layouts.guest')
@push('css')
-{{-<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/aire.css') }}">--}}
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush
@section('content')
  <section class="site-content mt-3">
      <div class="row">
        <div class="col-sm-12">
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              Application Form
          </div>
          <div class="card-body text-justify">       
<!--                 ->route('academic.store') -->
              {{ 
                Aire::open()
                ->route('frontend.admission.store')
                ->rules([
                'name' => 'required',
                'birth' => 'required',
                'birth_certificate' => 'required',
                'gender' => 'required',
                'religion' => 'required',
                'nationality' => 'required',
                'father_name' => 'required',
                'father_nid' => 'required',
                'father_phone' => 'required',
                'father_profession' => 'required',
                'mother_name' => 'required',
                'mother_nid' => 'required',

                'mother_profession' => 'required',
                'present_address' => 'required',
                'permananent_address' => 'required',
                ])
                ->messages([
                'title' => 'You must accept the terms',
                'photo' => 'Cover Photo is Rrequired',
                ]) 
                ->enctype('multipart/form-data')
                ->method('POST')
            }}
            <div id="personal-info" class="tab">
                <h2><p class="text-center">Student Information</p></h2>
                 
                {{ 
                    Aire::input()
                    ->label('Full Name Of Student *')
                    ->id('name')
                    ->name('name')
                    ->class('form-control') 
                }}
                <div class="row">
                    <div class="col">
                {{ 
                    Aire::input()
                    ->label('Date Of Birth *')
                    ->id('birth')
                    ->name('birth')
                    ->class('form-control') 
                    ->placeholder('DD/MM/YYYY')
                }}
                    </div>
                    <div class="col">
                {{ 
                    Aire::input()
                    ->label('Birth Certificate No. *')
                    ->id('birth_certificate')
                    ->name('birth_certificate')
                    ->class('form-control') 
                }}
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                {{ 
                    Aire::select(['none' => 'Select Gender', 
                    'male'=>'Male', 'female'=>'Female', 'others'=>'Others'])
                    ->label('Gender *')
                    ->id('gender')
                    ->name('gender')
                    ->class('form-control') 
                }}
                    </div>
                    <div class="col">
                {{ 
                    Aire::select(['none' => 'Select Blood Group', 
                    'a(+ve)'=>'A(+ve)', 'a(-ve)'=>'A(-ve)',
                    'b(+ve)'=>'B(+ve)', 'b(-ve)'=>'B(+ve)',
                    'o(+ve)'=>'O(+ve)', 'o(-ve)'=>'O(-ve)',
                    'ab(+ve)'=>'AB(+ve)', 'ab(-ve)'=>'AB(-ve)',])
                    ->label('Blood Group *')
                    ->id('blood')
                    ->name('blood')
                    ->class('form-control') 
                }}
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                {{ 
                    Aire::input()
                    ->label('Religion *')
                    ->id('religion')
                    ->name('religion')
                    ->class('form-control') 
                }}
                    </div>
                    <div class="col">
                {{ 
                    Aire::input()
                    ->label('Nationality *')
                    ->id('nationality')
                    ->name('nationality')
                    ->class('form-control') 
                }}
                    </div>
                </div>
            </div>
            <div id="guardian" class="tab">
                <h2><p class="text-center">Guardian's Information</p></h2>
                {{ 
                    Aire::input()
                    ->label('Full Name Of Father *')
                    ->id('father_name')
                    ->name('father_name')
                    ->class('form-control') 
                }}

                <div class="row">
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('NID No *')
                            ->id('father_nid')
                            ->name('father_nid')
                            ->class('form-control') 
                        }}
                    </div>
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('Phone No. *')
                            ->id('father_phone')
                            ->name('father_phone')
                            ->class('form-control') 
                        }}
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('Profession *')
                            ->id('father_profession')
                            ->name('father_profession')
                            ->class('form-control') 
                        }}
                    </div>
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('Email ')
                            ->id('father_email')
                            ->name('father_email')
                            ->class('form-control') 
                        }}
                    </div>
                </div>

                {{ 
                    Aire::input()
                    ->label('Full Name Of Mother *')
                    ->id('mother_name')
                    ->name('mother_name')
                    ->class('form-control') 
                }}

                <div class="row">
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('NID No. *')
                            ->id('mother_nid')
                            ->name('mother_nid')
                            ->class('form-control') 
                        }}
                    </div>
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('Phone No. *')
                            ->id('mother_phone')
                            ->name('mother_phone')
                            ->class('form-control') 
                        }}
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('Profession *')
                            ->id('mother_profession')
                            ->name('mother_profession')
                            ->class('form-control') 
                        }}
                    </div>
                    <div class="col">
                        {{ 
                            Aire::input()
                            ->label('Email ')
                            ->id('mother_email')
                            ->name('mother_email')
                            ->class('form-control') 
                        }}
                    </div>
                </div>

                
            </div>
            <div id="address" class="tab">
                <div>
                    <h2><p class="text-center">Presint Address</p></h2>
                    {{ 
                        Aire::input()
                        ->label('Present Address *')
                        ->id('present_address')
                        ->name('present_address')
                        ->class('form-control') 
                    }}
                        <div class="row">
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Village/House ')
                                    ->id('present_village')
                                    ->name('present_village')
                                    ->class('form-control') 
                                }}
                            </div>
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Road/Block/Sector ')
                                    ->id('present_road')
                                    ->name('present_road')
                                    ->class('form-control') 
                                }}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                
                                {{ 
                                    Aire::input()
                                    ->label('Post Office ')
                                    ->id('present_post_office')
                                    ->name('present_post_office')
                                    ->class('form-control') 
                                }}
                            </div>
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Post Code ')
                                    ->id('present_post_code')
                                    ->name('present_post_code')
                                    ->class('form-control') 
                                }}
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Police Station ')
                                    ->id('present_thana')
                                    ->name('present_thana')
                                    ->class('form-control') 
                                }}
                            </div>
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('District ')
                                    ->id('present_district')
                                    ->name('present_district')
                                    ->class('form-control') 
                                }}
                            </div>
                        </div>
                        

                </div>

                <div>
                    <h2><p class="text-center">Permanent Address</p></h2>
                    {{
                        Aire::checkbox()
                        ->label('Same As Present Address')
                        ->id('same_as_present')
                        ->name('same_as_present')

                    }}
                
                    {{ 
                        Aire::input()
                        ->label('Permanent Address *')
                        ->id('permananent_address')
                        ->name('permananent_address')
                        ->class('form-control') 
                    }}
                        <div class="row">
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Village/House ')
                                    ->id('permananent_village')
                                    ->name('permananent_village')
                                    ->class('form-control') 
                                }}
                            </div>
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Road/Block/Sector ')
                                    ->id('permananent_road')
                                    ->name('permananent_road')
                                    ->class('form-control') 
                                }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                
                                {{ 
                                    Aire::input()
                                    ->label('Post Office ')
                                    ->id('permananent_post_office')
                                    ->name('permananent_post_office')
                                    ->class('form-control') 
                                }}
                            </div>
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Post Code ')
                                    ->id('permananent_post_code')
                                    ->name('permananent_post_code')
                                    ->class('form-control') 
                                }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('Police Station ')
                                    ->id('permananent_thana')
                                    ->name('permananent_thana')
                                    ->class('form-control') 
                                }}
                            </div>
                            <div class="col">
                                {{ 
                                    Aire::input()
                                    ->label('District ')
                                    ->id('permananent_district')
                                    ->name('permananent_district')
                                    ->class('form-control') 
                                }}
                            </div>
                        </div>
                </div>


            </div>
           
            

            <div class="d-flex justify-content-center" >
                <div class="row" id="apply_button_responsive">
<!--                   <a class="btn btn-info inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#" id="prevBtn" onclick="nextPrev(-1)">Previous</a> -->
                  <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Cancel</a>
                  <a class="btn btn-warning inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Clear</a>
<!--                   <a class="btn theme-bg theme-border-color inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#" id="nextBtn" onclick="nextPrev(1)">Next</a> -->
                  {{ 
                    Aire::button()
                    ->labelHtml('Submit')
                    ->id('subBtn')
                    ->class(' btn theme-bg theme-border-color') 
                }}

            </div>
        </div>
        {{ Aire::close() }}

    </div>

</div>

</div>
</div>
</section>
@section('scripts')
    
    <script src="{{ asset('modules/admission/Resources/assets/js/admission.js') }}"></script>
    
    <script type="text/javascript">


        $( "#same_as_present" ).change(function() {
            // console.log('ok');
            if(jQuery('#same_as_present').is(":checked"))
            {
                // alert();
                $("#permananent_address").val(document.getElementById('present_address').value).prop('disabled', true);

                $("#permananent_village").val(document.getElementById('present_village').value).prop('disabled', true);
                $("#permananent_road").val(document.getElementById('present_road').value).prop('disabled', true);
                $("#permananent_post_office").val(document.getElementById('present_post_office').value).prop('disabled', true);
                $("#permananent_post_code").val(document.getElementById('present_post_code').value).prop('disabled', true);
                $("#permananent_thana").val(document.getElementById('present_thana').value).prop('disabled', true);
                $("#permananent_district").val(document.getElementById('present_district').value).prop('disabled', true);

            }else{
                $("#permananent_address " ).val("").prop('disabled', false);

                $("#permananent_village " ).val("").prop('disabled', false);
                $("#permananent_road " ).val("").prop('disabled', false);
                $("#permananent_post_office " ).val("").prop('disabled', false);
                $("#permananent_post_code " ).val("").prop('disabled', false);
                $("#permananent_thana " ).val("").prop('disabled', false);
                $("#permananent_district " ).val("").prop('disabled', false);
            }
        });
    </script>
@endsection
@endsection