@extends('admission::layouts.guest')
@push('css')

@endpush
@section('content')
<section class="site-content mt-3">
  <div class="row">
    <div class="col-sm-12">
      <div class="card rounded-0 theme-border theme-shadow">
        <div class="card-header theme-border-color rounded-0 theme-bg">
          Application Form
      </div>
      <div class="card-body text-justify">       
          {{ 
            Aire::open()
            ->route('admission.imageUpload', $data)
            ->enctype('multipart/form-data')
            ->bind($data)
            ->rules([
            'image' => 'required',
            ])
            ->messages([
            'image' => 'Image max width 300px and max height 350px',
            ]) 

        }}
        
        <!-- <div class="row"> -->
            <h3><p class="text-center ">Application Information</p></h3>
            <div class="row">

                <div class="col-lg-6 mx-auto">

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <!-- <tr>
                                    <td colspan="2">
                                        <h4><p class="text-center ">Student Information</p></h4>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td>Name</td>
                                    <td>{{$data->name}}</td>
                                </tr>
                                <tr>
                                    <td>Father</td>
                                    <td>{{$data->father_name}}</td>
                                </tr>
                                <tr>
                                    <td>Mother</td>
                                    <td>{{$data->mother_name}}</td>
                                </tr>
                                <tr>
                                    <td>Date Of Birth</td>
                                    <td>{{$data->birth}}</td>
                                </tr>
                                <tr>
                                    <td>Birth Certificate No</td>
                                    <td>{{$data->birth_certificate}}</td>
                                </tr>

                                <tr>
                                    <td>Gender</td>
                                    <td>{{$data->gender}}</td>
                                </tr>
                                <tr>
                                    <td>Blood Group</td>
                                    <td>{{$data->blood}}</td>
                                </tr>
                                <tr>
                                    <td>Religion</td>
                                    <td>{{$data->religion}}</td>
                                </tr>
                                <tr>
                                    <td>Nationality</td>
                                    <td>{{$data->nationality}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    
                </div>

                <div class="col-lg-4">
                    <div class="col-lg-12 mx-auto">
                        <div class="row mb-2 mr-3 ml-2">
                            <img id="previewHolder" alt="Student Image" src="{{ $data->image ? asset('public/uplodefile/admission/'.$data->image) : asset('public/uplodefile/defult/user.png') }}" class="border mx-auto" height="220" width="200" />
                        </div>

                    </div>

            </div>
        </div>
    </div>

    {{ Aire::close() }}

</div>

</div>

</div>
<!-- </div> -->
</section>
@section('scripts')

@endsection

@endsection