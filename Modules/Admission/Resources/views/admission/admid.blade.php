<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Admid Card - {{$data->name}}</title>
    <!-- <link rel="stylesheet"href="{{asset('css/css/https _stackpath.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css')}}"> -->
<!--     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
    <!-- <img src="{{asset('images/logo1.png')}}" alt="Image" class="rounded-circle" height="100" width="100">
    <h2 class="text-center">School Name With School Location</h2> -->
    <style>
    @page {
        size: A4;
        margin: 25mm 25mm 25mm 25mm;
    }

    body {
    	width: 595px;
    	height: 842px;
    	margin: 25mm 25mm 25mm 25mm;
    }

/*    h2,
    h3,
    h4 {
        font-size: 20pt;
        margin-top: 5px;
    }
    .col{
        padding-bottom: -15px;
        margin-top: 10px;
    }

    .col-sm-6 {
        padding-left: 445px;
        margin-top: -145px;
    }
    .col-sm-5{
        padding-left: 278px;
        margin: 0px;
    }
    .col-6.col-md-4 {
    padding-left: 50px;
    margin: 5px;
    padding-top: 20px;
    }
    h3.text-center {
    padding-top: 14px;
}*/
.h2 {
	width: 100%;
	font-size: 25px;
	font-weight: bold;
}
.h3 {
	width: 100%;
	font-size: 18px;
}
#logo {
	width: 100%;
	/*border: 1px solid #ddd;*/
	margin: auto;
}
#logo img {
  display: block;
  margin-left: auto;
  margin-right: auto;
  /*width: 40%;*/
}
#title {
	width: 100%;
	/*margin-top: 10mm;*/
	/*border: 1px solid blue;*/
}
#top {
	border: 1px solid red;
}
.row {
	width: 100%;
	margin-top: 3mm;
}
.col {
	float: left;
}
.text-center {
	width: 100%;
	text-align: center;
}
#admit {
	width: 100%;
	text-align: center;
}
#info {
	float: left;
	width: 65%;
	/*border: 1px solid #bbb;*/
}
#image {
	float: left;
	width: 20%;
	}
/*#image img{
	margin-top: 2mm;
}*/

    </style>

</head>

<body>
    
        <div class="col" id="logo">
            <img src="{{asset('public/uplodefile/general/2019-09-23-12-20-19-Edit-Monogram_001.png')}}" alt="Image" class="rounded-circle" height="80" width="80">
        </div>

    	<div id="title" class="text-center">
    		<span class="text-center h2">
    			Hojbrolo High School And Collage</span><br>
    		<span class="text-center h3">
    			This section we can use school slogan or address
    		</span>

    	</div>


<h3 id="admit">Admit Card</h3>

	<div class="row">
		<div id="info">
			<p>Admission ID : {{$data->student_id->admission_id}}</p>
			<p>Student's Name : {{$data->name}}</p>
			<p>Father's name : {{$data->father_name}}</p>
			<p>Mother's Name : {{$data->mother_name}}</p>
			<p>Admission Time : 00:00 AM</p>
			<p>Place : </p>
		</div>

		<div id="image">
			<img src="{{asset('public/uplodefile/admission/'.$data->image)}}" alt="" height="200" width="200" /><br>
		</div> 
	</div>


</body>
</html>
