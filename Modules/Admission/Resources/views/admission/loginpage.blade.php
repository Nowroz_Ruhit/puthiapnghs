@extends('admission::layouts.guest')
@push('css')

@endpush
@section('content')
<section class="site-content mt-3">
  <div class="row">
    <div class="col-sm-12">
      <div class="card rounded-0 theme-border theme-shadow">
        <div class="card-header theme-border-color rounded-0 theme-bg">
          Application Form
          {{-- <span class="float-right"><a class="btn btn-xs btn-success" href="{{route('admin.admission.studentDownload', $data->student_id->admission_id)}}" >Download Form</a></span> --}}
      </div>
      
      <div class="card-body text-justify">       


        <!-- <div class="row"> -->
            <h3><p class="text-center ">Application Information</p></h3>
            <div class="row">

                <div class="col-lg-6 mx-auto">

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <!-- <tr>
                                    <td colspan="2">
                                        <h4><p class="text-center ">Student Information</p></h4>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td>Admission Lottery ID</td>
                                    <td>{{$data->student_id->admission_id ?? ''}}</td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>{{$data->name}}</td>
                                </tr>
                                <tr>
                                    <td>Father</td>
                                    <td>{{$data->father_name}}</td>
                                </tr>
                                <tr>
                                    <td>Mother</td>
                                    <td>{{$data->mother_name}}</td>
                                </tr>
                                <tr>
                                    <td>Date Of Birth</td>
                                    <td>{{$data->birth}}</td>
                                </tr>
                                <tr>
                                    <td>Birth Certificate No</td>
                                    <td>{{$data->birth_certificate}}</td>
                                </tr>

                                <tr>
                                    <td>Gender</td>
                                    <td>{{$data->gender}}</td>
                                </tr>
                                <tr>
                                    <td>Blood Group</td>
                                    <td>{{$data->blood}}</td>
                                </tr>
                                <tr>
                                    <td>Religion</td>
                                    <td>{{$data->religion}}</td>
                                </tr>
                                @if($data->hasQouta)
                                    <tr>
                                        <td>Qouta</td>
                                        <td>{{$data->qoutaName}}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>

                    
                </div>
                @php
                    $date = new DateTime('now', new DateTimeZone('Asia/Dhaka'));
                    //echo $date->format("Y/m/d");
                    $start = date('Y/m/d', strtotime($bootAdmissionStart->start));
                    $end = date('Y/m/d', strtotime($bootAdmissionStart->end));
                    $today = $date->format("Y/m/d");
                    $create = date('Y/m/d', strtotime($data->created_at));
                @endphp

                @if($create <= $end)
                <div class="col-lg-4">
                    @if($data->className->publish == 0 || $data->className->publish1 == 0)
                    <div class="col-lg-12 text-center mb-3 border p-3"> 
                        @if($data->student_id->admission_id)
                        {{-- <p class="h3 theme-color">Filled Form </p> --}}
                        <a href="{{route('admission.studentDownload',$data->student_id->admission_id)}}" class="btn btn-info text-center">Form Download</a>
                        
                        @endif    
                    </div>
                    @endif
                    <div class="col-lg-12 mx-auto">
                        <div class="row mb-2 mr-3 ml-2">
                            <img id="previewHolder" alt="Student Image" src="{{ $data->image ? asset('public/uplodefile/admission/'.$data->image) : asset('public/uplodefile/defult/user.png') }}" class="border mx-auto" height="220" width="200" />
                        </div>

                    </div>
                    
                    @if($data->className->publish == 0 || $data->className->publish1 == 0)
                    <div class="col-lg-12 mx-auto">
                        <div class="table-responsive">
                            <table class="table table-bordered text-center">
                                <thead class="theme-bg">
                                    <tr>
                                        <th>Class</th>
                                        <th>Payment Status</th>
                                        <!-- <th>Admit Card</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    <td>{{$data->className->name}}</td>
                                    <td>
                                        {!! $data->complete ? '<span class="text-success font-weight-bold">Paid</span>' : '<span class="text-warning font-weight-bold">Panding</span>' ?? '' !!}
                                    </td>
                                </tbody>
                            </table>

                        </div>
                    </div>
                    @endif
                    

                </div>
                @endif
            </div>
        </div>


    </div>

</div>

</div>
<!-- </div> -->
</section>
@section('scripts')

@endsection

@endsection