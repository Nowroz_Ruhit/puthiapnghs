@extends('admission::layouts.guest')
@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/aire.css') }}">
@endpush
@section('content')
<section class="site-content mt-3">
  <div class="row">
    <div class="col-sm-12">
      <div class="card rounded-0 theme-border theme-shadow">
        <div class="card-header theme-border-color rounded-0 theme-bg">
          Application Form
      </div>
      <div class="card-body text-justify">       
          {{ 
            Aire::open()
            ->route('frontend.admission.imageUpload', $data)
            ->enctype('multipart/form-data')
            ->bind($data)
            ->rules([
            'image' => 'required',
            ])
            ->messages([
            'image' => 'Image max width 300px and max height 350px',
            ]) 

        }}
        
        <!-- <div class="row"> -->
            <h3><p class="text-center ">Application Information</p></h3>
            <div class="row">

                <div class="col-lg-6 mx-auto">

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <!-- <tr>
                                    <td colspan="2">
                                        <h4><p class="text-center ">Student Information</p></h4>
                                    </td>
                                </tr> -->
                                <tr>
                                    <td>Name</td>
                                    <td>{{$data->name}}</td>
                                </tr>
                                <tr>
                                    <td>Father</td>
                                    <td>{{$data->father_name}}</td>
                                </tr>
                                <tr>
                                    <td>Mother</td>
                                    <td>{{$data->mother_name}}</td>
                                </tr>
                                <tr>
                                    <td>Date Of Birth</td>
                                    <td>{{$data->birth}}</td>
                                </tr>
                                <tr>
                                    <td>Birth Certificate No</td>
                                    <td>{{$data->birth_certificate}}</td>
                                </tr>

                                <tr>
                                    <td>Gender</td>
                                    <td>{{$data->gender}}</td>
                                </tr>
                                <tr>
                                    <td>Blood Group</td>
                                    <td>{{$data->blood}}</td>
                                </tr>
                                <tr>
                                    <td>Religion</td>
                                    <td>{{$data->religion}}</td>
                                </tr>
                                <tr>
                                    <td>Nationality</td>
                                    <td>{{$data->nationality}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    
                </div>

                <div class="col-lg-4">
                    <div class="col-lg-12 mx-auto">
                        <div class="row mb-2 mr-3 ml-2">
                            <img id="previewHolder" alt="Student Image" src="{{ $data->image ? asset('public/uplodefile/admission/'.$data->image) : asset('public/uplodefile/defult/user.png') }}" class="border mx-auto" height="220" width="200" />
                        </div>

                        {{
                            Aire::input()
                            ->type('file')
                            ->label("Student's Image *")
                            ->id('image')
                            ->name('image')
                            ->class('col-12 border')

                        }}
                    </div>
                    <div class="d-flex justify-content-center" >
                        <div class="row" id="apply_button_responsive">
                          <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Cancel</a>
                          <!--               <a class="btn btn-warning inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Clear</a> -->
                          {{ 
                            Aire::button()
                            ->labelHtml('Submit')
                            ->id('subBtn')
                            ->class(' btn theme-bg theme-border-color') 
                        }}

                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Aire::close() }}

</div>

</div>

</div>
<!-- </div> -->
</section>
@section('scripts')
<script type="text/javascript">
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $('#previewHolder').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#image").change(function() {
    readURL(this);
});

$( "#same_as_present" ).change(function() {
// console.log('ok');
if(jQuery('#same_as_present').is(":checked"))
{
        // alert();
        $("#permananent_address").val(document.getElementById('present_address').value).prop('disabled', true);

        $("#permananent_village").val(document.getElementById('present_village').value).prop('disabled', true);
        $("#permananent_road").val(document.getElementById('present_road').value).prop('disabled', true);
        $("#permananent_post_office").val(document.getElementById('present_post_office').value).prop('disabled', true);
        $("#permananent_post_code").val(document.getElementById('present_post_code').value).prop('disabled', true);
        $("#permananent_thana").val(document.getElementById('present_thana').value).prop('disabled', true);
        $("#permananent_district").val(document.getElementById('present_district').value).prop('disabled', true);

    }else{
        $("#permananent_address " ).val("").prop('disabled', false);

        $("#permananent_village " ).val("").prop('disabled', false);
        $("#permananent_road " ).val("").prop('disabled', false);
        $("#permananent_post_office " ).val("").prop('disabled', false);
        $("#permananent_post_code " ).val("").prop('disabled', false);
        $("#permananent_thana " ).val("").prop('disabled', false);
        $("#permananent_district " ).val("").prop('disabled', false);
    }
});

if(jQuery('#same_as_present').is(":checked"))
{
    $("#permananent_address").val(document.getElementById('present_address').value).prop('disabled', true);

    $("#permananent_village").val(document.getElementById('present_village').value).prop('disabled', true);
    $("#permananent_road").val(document.getElementById('present_road').value).prop('disabled', true);
    $("#permananent_post_office").val(document.getElementById('present_post_office').value).prop('disabled', true);
    $("#permananent_post_code").val(document.getElementById('present_post_code').value).prop('disabled', true);
    $("#permananent_thana").val(document.getElementById('present_thana').value).prop('disabled', true);
    $("#permananent_district").val(document.getElementById('present_district').value).prop('disabled', true);
}
</script>
@endsection

@endsection