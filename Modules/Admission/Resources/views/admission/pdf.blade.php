<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Admid Card - {{$data->name}}</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="{{asset('public/guest-user/css/bootstrap.min.css')}}">
    <script src="{{asset('public/guest-user/js/bootstrap.min.js')}}"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> -->
    <!-- <img src="{{asset('Puthia_PN_school_logo.png')}}" alt="Image" class="rounded-circle" height="100" width="100">
    <h2 class="text-center">School Name With School Location</h2> -->
    <style>
    @page {
        size: A4;
        margin: 25mm 25mm 25mm 25mm;
    }

/*    body {}

    h2,
    h3,
    h4 {
        font-size: 20pt;
        margin-top: 5px;
    }
    .col{
        padding-bottom: -15px;
        margin-top: 10px;
    }

    .col-sm-6 {
        padding-left: 445px;
        margin-top: -145px;
    }
    .col-sm-5{
        padding-left: 278px;
        margin: 0px;
    }
    .col-6.col-md-4 {
    padding-left: 50px;
    margin: 5px;
    padding-top: 20px;
    }
    h3.text-center {
    padding-top: 14px;
}*/

    </style>
</head>

<body>
    <div class="row">
        <div class="col-sm-5">
            <img src="{{asset('/Puthia_PN_school_logo.png')}}" alt="Image" class="rounded-circle" height="80" width="80">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 class="text-center">
                Shahid Mamun Mahamud Police Line<br> School and College, Rajshahi
            </h3>
            <p class="text-center">
            <!-- পুলিশ লাইনস রাজশাহী পোস্ট- জিপিও-৬০০০, থানা-রাজপাড়া, জেলা- রাজশ -->
            Police Line Rajshahi, GPO - 6000, Thana - Rajpara, Rajshahi
        </p>
        <h3 class="text-center">Admit Card</h3>
        </div>
    </div>
<!-- <div class="row">
    <div class="col">
        <h3 class="text-center">Admit Card</h3>
    </div>
</div> -->
<div class="row">
    <div class="col order-first" style="width: 50%; float: left;">
        <p>Admission ID : {{$data->student_id->admission_id}}</p>
        <p>Student's Name : {{$data->name}}</p>
        <p>Father's name : {{$data->father_name}}</p>
        <p>Mother's Name : {{$data->mother_name}}</p>
        <p>Admission Time : 00:00 AM</p>
        <p>Place : </p>
    </div>
<!--     </div>

    <div class="row"> -->
        <div class="col order-last" style="width: 50%; float: left;">
            <img src="{{asset('public/uplodefile/admission/'.$data->image)}}" alt=""  width="120" class="m-3" />
        </div> 
    </div>
    <!-- <div class="row">
        <div class="col-6 col-md-4">
            <span class="text-center">Developed By Desktopit</span>
        </div>
    </div> -->
<!-- <span class="bel" id="bel">Powered By : desktopit.com.bd</span> -->
</body>
</html>
