@extends('admission::layouts.guest')
@push('css')
{{--<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/aire.css') }}">--}}
<link href="{{ asset('css/admission/aire.css') }}" rel="stylesheet" />
@endpush
@section('content')
<!-- Start Breadcrumb -->
    <!-- <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item active" aria-current="page">আমাদের সম্পর্কে</li>
      </ol>
  </nav> -->
  <!-- End Breadcrumb -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

  <section class="site-content mt-3">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              Application Form
          </div>

          <div class="card-body text-justify">  
            {{ 
                Aire::open()
                ->route('frontend.admission.store')
                ->rules([
                'name' => 'required',
                'birth' => 'required',
                'birth_certificate' => 'required',
                'gender' => 'required',
                'religion' => 'required',
                'nationality' => 'required',
                'father_name' => 'required',
                'father_nid' => 'required',
                'father_phone' => 'required',
                'father_profession' => 'required',
                'mother_name' => 'required',
                'mother_nid' => 'required',
                'mother_profession' => 'required',

                'present_address' => 'required',
                'present_post_code' => 'required',
                'present_district' => 'required',
                'present_thana' => 'required',

                'permananent_address' => 'required',
                'permananent_post_code' => 'required',
                'permananent_district' => 'required',
                'permananent_thana' => 'required',

                'PrimaryContact' => 'required',

                'localGurdian_name' => 'required',
                'localGurdian_relation' => 'required',
                'localGurdian_phone' => 'required',
                'localGurdian_email' => 'required',
                'localGurdian_address' => 'required',
                'localGurdian_postCode' => 'required',
                'localGurdian_district' => 'required',
                'localGurdian_thana' => 'required',

                'image'=> 'required|file',
                
                'father_email' => 'email',
                'mother_email' => 'email',
                'localGurdian_email' => 'email',
                ])
                ->messages([
                'title' => 'You must accept the terms',
                'photo' => 'Cover Photo is Rrequired',
                ]) 
                ->enctype('multipart/form-data')
                ->method('POST')
                ->id('fm')
            }}

            <fieldset>
                <legend class="theme-color">Student Information:</legend>

                <div class="row">
                    <div class="col-lg-6">
                        {{ 
                            Aire::input()
                            ->label("Student's Full Name *")
                            ->id('name')
                            ->name('name')
                            ->class('form-control')
                            ->placeholder('Full Name')
                            ->required() 

                        }}

                        {{ 
                            Aire::input()
                            ->label('Date Of Birth *')
                            ->id('birth')
                            ->name('birth')
                            ->class('form-control') 
                            ->placeholder('DD/MM/YYYY')
                            ->required()
                            
                        }}

                        <script>
                            $('#birth').datepicker({
                                uiLibrary: 'bootstrap4'
                            });
                        </script>

                        {{ 
                            Aire::input()
                            ->label('Birth Certificate No. *')
                            ->id('birth_certificate')
                            ->name('birth_certificate')
                            ->class('form-control')
                            ->required() 
                        }}

                        {{ 
                            Aire::select([NULL => 'Select Gender', 
                            'male'=>'Male', 'female'=>'Female', 'others'=>'Others'])
                            ->label('Gender *')
                            ->id('gender')
                            ->name('gender')
                            ->class('form-control')
                            ->required()  
                        }}

                        {{ 
                            Aire::select([NULL => 'Select Blood Group', 
                            'a(+ve)'=>'A(+ve)', 'a(-ve)'=>'A(-ve)',
                            'b(+ve)'=>'B(+ve)', 'b(-ve)'=>'B(+ve)',
                            'o(+ve)'=>'O(+ve)', 'o(-ve)'=>'O(-ve)',
                            'ab(+ve)'=>'AB(+ve)', 'ab(-ve)'=>'AB(-ve)',])
                            ->label('Blood Group ')
                            ->id('blood')
                            ->name('blood')
                            ->class('form-control') 
                        }}

                        {{ 
                            Aire::input()
                            ->label('Religion *')
                            ->id('religion')
                            ->name('religion')
                            ->class('form-control') 
                        }}

<!--                         {{ 
                            Aire::input()
                            ->label('Nationality *')
                            ->id('nationality')
                            ->name('nationality')
                            ->class('form-control') 
                        }} -->
                    </div>
                    <div class="col-lg-6">

                        <div class="col-lg-12 mx-auto">
                            <div class="row mb-2 mr-3 ml-2">
                                <img id="previewHolder" alt="Student Image" src="{{  asset('public/uplodefile/defult/user.png') }}" class="border mx-auto" height="220" width="200" />
                            </div>
                            <p id="error1" style="display:none; color:#FF0000;" class="alert alert-danger">
                                Invalid Image Format! Image Format Must Be JPG, JPEG, PNG.
                            </p>
                            <p id="error2" style="display:none; color:#FF0000;" class="alert alert-danger">
                                Image maximum File Size Limit is 300 kb.
                            </p>

                            <p id="error3" style="display:none; color:#FF0000;" class="alert alert-danger text-center">
                                Please upload exact size Image <br>( maximum hight 350px and maximum width 300px )
                                
                            </p>
                                <input type="hidden" id="pass">
                            {{
                                Aire::image()
                                ->type('file')
                                ->label("Student's Image *")
                                ->id('image')
                                ->name('image')
                                ->required()
                              

                            }}
                            <label class="text-info text-center col-12">Image maximum hight 350px and maximum width 300px  </label>
                        </div>


                        {{ 
                            Aire::input()
                            ->label('Previous Institute Name ')
                            ->id('previousInstretute')
                            ->name('previousInstretute')
                            ->class('form-control') 
                        }}

                        {{ 
                            Aire::input()
                            ->label('PSC/JSC Roll No. (If Aplicable) ')
                            ->id('previousRoll')
                            ->name('previousRoll')
                            ->class('form-control') 
                        }}

                    </div>
                </div>
            </fieldset>



            <fieldset>
                <legend class="theme-color">Family Information:</legend>
                <div class="row">
                    <div class="col-lg-6">
                        {{ 
                            Aire::input()
                            ->label("Father's Name *")
                            ->id('father_name')
                            ->name('father_name')
                            ->class('form-control') 
                            ->required() 
                        }}

                        {{ 
                            Aire::input()
                            ->label("Father's Phone No. *")
                            ->id('father_phone')
                            ->name('father_phone')
                            ->class('form-control')
                            ->required()  
                        }}

                        {{ 
                            Aire::email()
                            ->label("Father's Email ")
                            ->id('father_email')
                            ->name('father_email')
                            ->class('form-control') 
                        }}

                    </div>
                    <div class="col-lg-6">
                        {{ 
                            Aire::input()
                            ->label("Father's Occupation *")
                            ->id('father_profession')
                            ->name('father_profession')
                            ->class('form-control') 
                            ->required() 
                        }}

                        {{ 
                            Aire::input()
                            ->label("Father's NID No *")
                            ->id('father_nid')
                            ->name('father_nid')
                            ->class('form-control') 
                            ->required() 
                        }}

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        {{ 
                            Aire::input()
                            ->label("Mother's Name  *")
                            ->id('mother_name')
                            ->name('mother_name')
                            ->class('form-control') 
                            ->required() 
                        }}

                        {{ 
                            Aire::input()
                            ->label("Mother's Phone No. ")
                            ->id('mother_phone')
                            ->name('mother_phone')
                            ->class('form-control') 
                        }}

                        {{ 
                            Aire::email()
                            ->label("Mother's Email ")
                            ->id('mother_email')
                            ->name('mother_email')
                            ->class('form-control') 
                        }}
                    </div>
                    <div class="col-lg-6">
                        {{ 
                            Aire::input()
                            ->label("Mother's Occupation *")
                            ->id('mother_profession')
                            ->name('mother_profession')
                            ->class('form-control') 
                            ->required() 
                        }}

                        {{ 
                            Aire::input()
                            ->label('NID No. *')
                            ->id('mother_nid')
                            ->name('mother_nid')
                            ->class('form-control')
                            ->required()  
                        }}
                    </div>
                </div>
            </fieldset>


            <fieldset>
                <legend class="theme-color">Address Information:</legend>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row border-bottom m-2">
                            <h5 class=""><p class="text-info">Present Address</p></h5> 
                        </div>
                        {{ 
                            Aire::textArea()
                            ->label('Present Address *')
                            ->id('present_address')
                            ->name('present_address')
                            ->placeholder('House/Rode: , Village/Area/Word: , Post Office: ')
                            ->class('form-control') 
                            ->required() 
                        }}

                        <!-- {{ 
                            Aire::input()
                            ->label('Post Office *')
                            ->id('present_post_office')
                            ->name('present_post_office')
                            ->class('form-control') 
                        }} -->

                        {{ 
                            Aire::input()
                            ->label('Post Code *')
                            ->id('present_post_code')
                            ->name('present_post_code')
                            ->class('form-control') 
                            ->required() 
                        }}

                        {{ 
                            Aire::select($district)
                            ->label('District *')
                            ->id('present_district')
                            ->name('present_district')
                            ->class('form-control') 
                            ->required() 
                        }}
                        

                        {{ 
                            Aire::select($thana)
                            ->label('Thana *')
                            ->id('present_thana')
                            ->name('present_thana')
                            ->class('form-control') 
                            ->required() 
                        }}
                        
                        
                    </div>
                    <div class="col-lg-6">
                        <div class="row border-bottom m-2">
                            <h5 class=""><p class="text-info">Permanent Address</p></h5>
                            {{
                                Aire::checkbox()
                                ->label('Same As Present Address')
                                ->id('same_as_present')
                                ->name('same_as_present')

                            }}
                        </div>
                        {{ 
                    Aire::textArea()
                    ->label('Permanent Address *')
                    ->id('permananent_address')
                    ->name('permananent_address')
                    ->placeholder('House/Rode: , Village/Area/Word: , Post Office: ')
                    ->class('form-control') 
                    ->required()
                }}

                        <!-- {{ 
                            Aire::input()
                            ->label('Post Office *')
                            ->id('permananent_post_office')
                            ->name('permananent_post_office')
                            ->class('form-control') 
                        }} -->
                        {{ 
                            Aire::input()
                            ->label('Post Code *')
                            ->id('permananent_post_code')
                            ->name('permananent_post_code')
                            ->class('form-control') 
                        }}

                        {{ 
                            Aire::select($district)
                            ->label('District *')
                            ->id('permananent_district')
                            ->name('permananent_district')
                            ->class('form-control') 
                        }}

                        {{ 
                            Aire::select($thana)
                            ->label('Thana *')
                            ->id('permananent_thana')
                            ->name('permananent_thana')
                            ->class('form-control') 
                        }}
                        
                    </div>
                </div>



            </fieldset>

            <fieldset>
                <legend class="theme-color">Primary Contact Information:</legend>
                <label class="text-danger "><strong>Note:</strong> Primary Contact person will receive admission related all information ( via Email and SMS)</label>
                        {{ 
                        Aire::select([NULL => 'Select Primary Contact', 
                        '1'=>'Father', '2'=>'Mother', '3'=>'Others'])
                        ->label('Primary Contact *')
                        ->id('PrimaryContact')
                        ->name('PrimaryContact')
                        ->class('form-control')
                        ->required()  
                    }}
                <fieldset id="LocalGurdian">
                <legend class="theme-color">Local Gurdian's Information:</legend>
                <div class="row">
                    <div class="col-lg-6">
                        {{ 
                            Aire::input()
                            ->label("Name *")
                            ->id('localGurdian_name')
                            ->name('localGurdian_name')
                            ->class('form-control') 
                            
                        }}
                        {{ 
                            Aire::input()
                            ->label('Relation *')
                            ->id('localGurdian_relation')
                            ->name('localGurdian_relation')
                            ->class('form-control') 
                            
                        }}
                        {{ 
                            Aire::input()
                            ->label('Phone No *')
                            ->id('localGurdian_phone')
                            ->name('localGurdian_phone')
                            ->class('form-control')
                            
                        }}
                        {{ 
                            Aire::email()
                            ->label('Email *')
                            ->id('localGurdian_email')
                            ->name('localGurdian_email')
                            ->class('form-control')
                            
                        }}

                        
                        

                    </div>
                    <div class="col-lg-6">
                        
                        {{ 
                            Aire::textArea()
                            ->label('Address *')
                            ->id('localGurdian_address')
                            ->name('localGurdian_address')
                            ->rows(4)
                            ->placeholder('House/Rode: , Village/Area/Word: , Post Office: , Post Code: ')
                            ->class('form-control') 
                            
                        }}
                        {{ 
                            Aire::input()
                            ->label('Post Code *')
                            ->id('localGurdian_postCode')
                            ->name('localGurdian_postCode')
                            ->class('form-control') 
                            
                        }}

                        <!-- {{ 
                            Aire::input()
                            ->label('Post Office *')
                            ->id('')
                            ->name('')
                            ->class('form-control') 
                        }}-->

                         
                        {{ 
                            Aire::select($district)
                            ->label('District *')
                            ->id('localGurdian_district')
                            ->name('localGurdian_district')
                            ->class('form-control')
                            
                        }}
                        {{ 
                            Aire::select($thana)
                            ->label('Thana *')
                            ->id('localGurdian_thana')
                            ->name('localGurdian_thana')
                            ->class('form-control') 
                            
                        }}
                        
                    </div>
                </div>
                </fieldset>
            </fieldset>
            <div class="d-flex justify-content-center" >
            <div class="row" id="apply_button_responsive">
               
                <!-- <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Cancel</a>
                <a class="btn btn-warning inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Clear</a> -->
                
                {{ 
                    Aire::button()
                    ->labelHtml('Submit')
                    ->id('subBtn')
                    ->class(' btn theme-bg theme-border-color') 
                }}

            </div>
        </div>

            {{ Aire::close() }}

        </div>
    </div>
    <!-- End Welcome or About Text -->
</div>
</div>
</section>


@section('scripts')

<script src="{{ asset('modules/admission/Resources/assets/js/admission.js') }}"></script>
<script type="text/javascript">
 function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#previewHolder').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
}
}
//---------------start
/*var _URL = window.URL || window.webkitURL;
var orWidth, orHeight;
    $("#image").change(function (e) {

        var image, file;

        if ((file = this.files[0])) {

            image = new Image();

            image.onload = function () {
                orWidth = this.width;
                orHeight = this.height;
                // alert("The image width is " + orWidth + " and image height is " + orHeight);
            };

            image.src = _URL.createObjectURL(file);

        }

    });*/
    //--------------end
$("#image").change(function() {
    readURL(this);
});

$('#subBtn').prop("disabled", true);
var a=0;
//binds to onchange event of your input field
$('#image').bind('change', function() {
    if ($('input:submit').attr('disabled',false)){
        $('input:submit').attr('disabled',true);
    }
    var ext = $('#image').val().split('.').pop().toLowerCase();
    if ($.inArray(ext, [ 'png','jpg','jpeg']) == -1){
        $('#error1').slideDown("slow");
        $('#error2').slideUp("slow");
        $('#error3').slideUp("slow");
        a=0;
    }else{
        var picsize = (this.files[0].size);
        if (picsize > 300000){
            $('#error2').slideDown("slow");
            document.getElementById("pass").value  = 0;
            a=0;
        }
        else{

            $('#error2').slideUp("slow");
            var _URL = window.URL || window.webkitURL;
            var orWidth, orHeight;
            var image, file;
            if ((file = this.files[0])) {
                image = new Image();
                image.onload = function () {
                    orWidth = this.width;
                    orHeight = this.height;
                    // alert("The image width is " + orWidth + " and image height is " + this.height);

                    // console.log("NO "+orWidth+' '+orHeight);
                    if(orWidth > 300 || orHeight > 350)
                    {
                        $('#error3').slideDown("slow");
                        // console.log("YES");
                        document.getElementById("pass").value  = 0;
                        a=0;
                    }else{
                        a=1;
                        $('#error3').slideUp("slow");
                        // $('#pass').value =1;
                        document.getElementById("pass").value  = 1;
                        // console.log("NO *");
                        $('#subBtn').attr('disabled',false);
                    }
                };
                image.src = _URL.createObjectURL(file);
            }

        }
        console.log(a+'PX='+document.getElementById("pass").value);
        if((document.getElementById("pass").value == 1) || (document.getElementById("pass").value == '1') ){
            a=1;
            // console.log(a+'X='+document.getElementById("pass").value);
            $('#subBtn').attr('disabled',false);
        }else {
            a=0;
            // console.log(a+'XY');
            $('#subBtn').attr('disabled',true);
        }


        // console.log($('#previewHolder').width() + " " + $('#image').width());
       
        $('#error1').slideUp("slow");
        if (a==1){
            $('#subBtn').attr('disabled',false);
        }
        //$('#subBtn').attr('disabled',false);

    }
});
</script>

    <script type="text/javascript">
    // $("#id").css("display", "none");
    // $("#id").css("display", "block");
    $("#LocalGurdian").css("display", "none");
    $( "#PrimaryContact" ).change(function() {
        if(jQuery('#PrimaryContact').val() == '3')
        {
            $("#LocalGurdian").css("display", "block"); 
        } else 
        {
            $("#LocalGurdian").css("display", "none");
        }
    });

    
    $( "#permananent_district" ).change(function() {
        if(jQuery('#permananent_district').val() == '0')
        {
            // var = "<option>Select </option>";
            $('#permananent_thana').html("<option>Select </option>")
        } else 
        {
            district('permananent_district', 'permananent_thana')
        }
    });
    $( "#present_district" ).change(function() {
        if(jQuery('#present_district').val() == '0')
        {
            // var = "<option>Select </option>";
            $('#present_thana').html("<option>Select </option>")
        } else 
        {
            district('present_district', 'present_thana')
        }
    });
    $( "#localGurdian_district" ).change(function() {
        if(jQuery('#localGurdian_district').val() == '0')
        {
            // var = "<option>Select </option>";
            $('#localGurdian_thana').html("<option>Select </option>")
        } else 
        {
            district('localGurdian_district', 'localGurdian_thana')
        }
    });

// ----------------------------------------------------
// $( "#permananent_thana" ).change(function() {
//         if(jQuery('#permananent_district').val() == '0')
//         {
//             // var = "<option>Select </option>";
//             $('#permananent_post_code').html("<option>Select </option>")
//         } else 
//         {
//             thana('permananent_thana', 'permananent_post_code')
//         }
//     });
//     $( "#present_thana" ).change(function() {
//         if(jQuery('#present_thana').val() == '0')
//         {
//             // var = "<option>Select </option>";
//             $('#present_post_code').html("<option>Select </option>")
//         } else 
//         {
//             thana('present_thana', 'present_post_code')
//         }
//     });
//     $( "#localGurdian_thana" ).change(function() {
//         if(jQuery('#localGurdian_thana').val() == '0')
//         {
//             // var = "<option>Select </option>";
//             $('#localGurdian_postCode').html("<option>Select </option>")
//         } else 
//         {
//             thana('localGurdian_thana', 'localGurdian_postCode')
//         }
//     });



    function district (to, form){
     var value1 = $('#'+to).val();
     var value = value1.trim()
     var _token = $('input[name="_token"]').val();
     $.ajax({
        url:"{{ route('frontend.district') }}",
        method:"POST",
        data:{value:value, _token:_token},
        success:function(result){
            $('#'+form).html(result);
            // var x = "<option>Select </option>";
            // $('#designation_id').html(x);
        }
    })
 }

 // function thana (to, form){
 //     var value1 = $('#'+to).val();
 //     var value = value1.trim()
 //     var _token = $('input[name="_token"]').val();
 //     $.ajax({
 //        url:"{{ route('frontend.thana') }}",
 //        method:"POST",
 //        data:{value:value, _token:_token},
 //        success:function(result){
 //            $('#'+form).html(result);
 //            // var x = "<option>Select </option>";
 //            // $('#designation_id').html(x);
 //        }
 //    })
 // }

    // $('#employee_type_id').change(function(event){
    //     var x = "<option>Select </option>";
    //     $('#designation_id').html(x);
    //     var value = this.value;
    //     // console.log('Value = '+v);
    //     var _token = $('input[name="_token"]').val();
    //     $.ajax({
    //         url:"{{ route('employee.employee_designation') }}",
    //         method:"POST",
    //         data:{value:value, _token:_token},
    //         success:function(result){
    //             $('#designation_id').html(result);
    //             console.log(result);
    //         }
    //     })
    // });
</script>

<script type="text/javascript">
    $( "#same_as_present" ).change(function() {
            // console.log('ok');
            if(jQuery('#same_as_present').is(":checked"))
            {
                // alert();
                $("#permananent_address").val(document.getElementById('present_address').value).prop('disabled', true);

                // $("#permananent_village").val(document.getElementById('present_village').value).prop('disabled', true);
                // $("#permananent_road").val(document.getElementById('present_road').value).prop('disabled', true);
                // $("#permananent_post_office").val(document.getElementById('present_post_office').value).prop('disabled', true);
                $("#permananent_post_code").val(document.getElementById('present_post_code').value).prop('disabled', true);
                console.log(document.getElementById('present_thana').value);
                $("#permananent_thana").val(document.getElementById('present_thana').value).prop('disabled', true);
                $("#permananent_district").val(document.getElementById('present_district').value).prop('disabled', true);

            }else{
                $("#permananent_address " ).val("").prop('disabled', false);

                // $("#permananent_village " ).val("").prop('disabled', false);
                // $("#permananent_road " ).val("").prop('disabled', false);
                // $("#permananent_post_office " ).val("").prop('disabled', false);
                $("#permananent_post_code " ).val("").prop('disabled', false);
                $("#permananent_thana " ).val("").prop('disabled', false);
                $("#permananent_district " ).val("").prop('disabled', false);
            }
        });
    </script>
    <script type="text/javascript">
        window.firstTime();
        function firstTime() {
            if(jQuery('#same_as_present').is(":checked"))
            {
                $("#permananent_address").val(document.getElementById('present_address').value).prop('disabled', true);
                $("#permananent_post_code").val(document.getElementById('present_post_code').value).prop('disabled', true);
                console.log(document.getElementById('present_thana').value);
                $("#permananent_thana").val(document.getElementById('present_thana').value).prop('disabled', true);
                $("#permananent_district").val(document.getElementById('present_district').value).prop('disabled', true);

            }

            if(jQuery('#PrimaryContact').val() == '3')
            {
                $("#LocalGurdian").css("display", "block"); 
            }

        }
    </script>
    <script type="text/javascript">
    /*    if (validation.fails();) {
  alert("ABC");

}*/



    </script>
    @endsection
@endsection