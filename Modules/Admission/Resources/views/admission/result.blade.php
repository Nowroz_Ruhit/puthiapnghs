@extends('admission::layouts.guest')
@section('content')
<section class="site-content mt-2">
    <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              ভর্তি ফলাফল
            </div>
            <div class="card-body text-justify">

    <div class="row">
        <div class="col-lg-10 col-sm-12 mx-auto">
            <div class="row">
                <div class="col-md-6">
            <div class="card rounded-0 theme-border theme-shadow">
                <div class="card-header theme-border-color rounded-0 theme-bg">
                    Search By Certificate No
                </div>
                <div class="card-body text-justify">     
                    <div>
                        {{ 
                            Aire::open()
                            ->route('admission.student.datachack')
                            ->rules([
                            'user' => 'required',
                            'password' => 'required',
                            ])
                            ->messages([
                            'title' => 'You must accept the terms',
                            'photo' => 'Cover Photo is Rrequired',
                            ]) 
                            ->enctype('multipart/form-data')
                            ->method('POST')
                        }}
                        {{ 
                            Aire::input()
                            ->label("Student's Birth Certificate No")
                            ->id('birth_certificate')
                            ->name('birth_certificate')
                            ->class('form-control') 
                        }}
                        
                        {{ 
                            Aire::select($class)
                            ->label('Select Class *')
                            ->id('class_id')
                            ->name('class_id')
                            ->class('form-control') 
                        }}

                        
                    {{

                        Aire::button()
                        ->labelHtml('Search')
                        ->class(' btn theme-bg theme-border-color') 
                    }}

                    {{ Aire::close() }}
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-6">
            <div class="card rounded-0 theme-border theme-shadow">
                <div class="card-header theme-border-color rounded-0 theme-bg">
                    Search By Admission ID
                </div>
                <div class="card-body text-justify">     
                    <div>
                        {{ 
                            Aire::open()
                            ->route('admission.student.datachack')
                            ->rules([
                            'user' => 'required',
                            'password' => 'required',
                            ])
                            ->messages([
                            'title' => 'You must accept the terms',
                            'photo' => 'Cover Photo is Rrequired',
                            ]) 
                            ->enctype('multipart/form-data')
                            ->method('POST')
                        }}
                        {{ 
                            Aire::input()
                            ->label("Admission ID")
                            ->id('admission_id')
                            ->name('admission_id')
                            ->class('form-control') 
                        }}

                        {{ 
                            Aire::select($class)
                            ->label('Select Class *')
                            ->id('class_id')
                            ->name('class_id')
                            ->class('form-control') 
                        }}

                        
                    {{

                        Aire::button()
                        ->labelHtml('Search')
                        ->class(' btn theme-bg theme-border-color') 
                    }}

                    {{ Aire::close() }}
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>

<br>

<div class="card rounded-0 theme-border theme-shadow">
    <div class="card-header theme-border-color rounded-0 theme-bg">
        ভর্তির জন্য যা যা লাগছে
    </div>
    <div class="card-body text-justify">
        বিঃ দ্রঃ ভর্তি পরীক্ষায় উত্তীর্ণ ছাত্র-ছাত্রীদের নিম্নে তালিকাকৃত শ্রেণি শিক্ষকগণের সাথে যোগাযোগ করে  ৩০/১২/২০১৯ খ্রি. সোমবার  হতে ০৭/০১/২০২০ খ্রি. মঙ্গলবার  পর্যন্ত সকাল ১০:০০ থেকে ০১:০০ পর্যন্ত ভর্তি হওয়ার জন্য অভিভাবকদের অনুরোধ করা হলোঃ-


        <table class="table table-bordered text-center mt-3">
            <thead class="thead-light">
                <tr>
                    <th colspan="5" class="text-center">শিশু থেকে দ্বিতীয় শ্রেণি </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center">শ্রেণির নাম</th>
                    <th class="text-center">শ্রেণি শিক্ষকের নাম মোবাইল নং</th>
                    <th class="text-center">প্রয়োজনীয় কাগজপত্রাদি</th>
                    <th class="text-center">শ্রেণি কক্ষ</th>
                    <th class="text-center">মোট টাকার পরিমান</th>
                </tr>

                <tr>
                    <td  class=" text-left">শিশু</td>
                    <td  class=" text-left">
                        জনাব মাজেদা খাতুন <br>
                        (০১৭৭৯-৩৬১১২৪)
                    </td>
                    <td class=" text-left" rowspan="3">
                        ১। জন্ম নিবন্ধন সনদের সত্যায়িত      ফটোকপি।<br>
                        ২। ০২ (দুই) কপি পাসপোর্ট            সাইজের ছবি (স্কুল ড্রেস)<br>
                        ৩। মূল প্রবেশ পত্র<br>
                        ৪। পিতা মাতার  NID কার্ডের ফটোকপি
                    </td>
                    <td rowspan="3">নজরুল ভবন নীচতলা</td>
                    <td class=" text-left" rowspan="3">
                        সেসান=      ২,৮৭৫/-<br>
                        বেতন (জানু)=৩০০/-<br>
                        বার্ষিকভোজ   =২০০/-<br>
                        <b>সর্বমোট=     ৩,৩৭৫/-</b>
                    </td>
                </tr>
                <tr>
                    <td  class=" text-left">প্রথম</td>
                    <td  class=" text-left">
                        আব্দুল ওয়াদুদ <br>
                        (০১৭১৩-৯৩৭৭৪৭)
                    </td>
                </tr>
                <tr>
                    <td  class=" text-left">দ্বিতীয়</td>
                    <td  class=" text-left">
                        জনাব তাহেরা খাতুন<br>
                        (০১৭৩৭-২১৪২৭৭)
                    </td>
                </tr>

            </tbody>
        </table>

        <table class="table table-bordered text-center mt-3">
            <thead class="thead-light">
                <tr>
                    <th colspan="5" class="text-center">তৃতীয় থেকে পঞ্চম শ্রেণি </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center">শ্রেণির নাম</th>
                    <th class="text-center">শ্রেণি শিক্ষকের নাম মোবাইল নং</th>
                    <th class="text-center">প্রয়োজনীয় কাগজপত্রাদি</th>
                    <th class="text-center">শ্রেণি কক্ষ</th>
                    <th class="text-center">মোট টাকার পরিমান</th>
                </tr>

                <tr>
                    <td  class=" text-left">তৃতীয়</td>
                    <td  class=" text-left">
                        জনাব ফারজানা রহমান (মিতু)<br>
                        (০১৯৮৯-৫৬৩৮২৮)
                    </td>
                    <td class=" text-left" rowspan="3">
                        ১। জন্ম নিবন্ধন সনদের সত্যায়িত      ফটোকপি।<br>
                        ২। ০২ (দুই) কপি পাসপোর্ট            সাইজের ছবি (স্কুল ড্রেস)<br>
                        ৩। মূল প্রবেশ পত্র<br>
                        ৪। টিসি
                        ৫। তথ্য ফরম
                        ৬। পিতা মাতার  NID কার্ডের ফটোকপি
                    </td>
                    <td rowspan="3">নজরুল ভবন নীচতলা</td>
                    <td class=" text-left" rowspan="2">
                        সেসান=      ২,৮৭৫/-<br>
                        বেতন (জানু)=৩০০/-<br>
                        বার্ষিকভোজ   =২০০/-<br>
                        <b>সর্বমোট=     ৩,৩৭৫/-</b>
                    </td>
                </tr>
                <tr>
                    <td  class=" text-left">চতুর্থ</td>
                    <td  class=" text-left">
                        জনাব তামান্না সুলতানা<br>
                        (০১৭২৩-২৪৮৪৪৬)
                    </td>
                </tr>
                <tr>
                    <td  class=" text-left">পঞ্চম</td>
                    <td  class=" text-left">
                        জনাব জাহিদুল হক ভূঞা<br>
                        (০১৭৫৪-৭৮২০৩৭)
                    </td>
                    <td class=" text-left" >
                        সেসান=      ৩,৩৭৫/-<br>
                        বেতন (জানু)=৩০০/-<br>
                        বার্ষিকভোজ   =২০০/-<br>
                        <b>সর্বমোট=     ৩,৮৭৫/-</b>
                    </td>
                </tr>
                
            </tbody>
        </table>

        <table class="table table-bordered text-center mt-3">
            <thead class="thead-light">
                <tr>
                    <th colspan="5" class="text-center">৬ষ্ঠ থেকে নবম শ্রেণি </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th class="text-center">শ্রেণির নাম</th>
                    <th class="text-center">শাখার নাম</th>
                    <th class="text-center">শ্রেণি শিক্ষকের নাম মোবাইল নং</th>
                    <th class="text-center">প্রয়োজনীয় কাগজপত্রাদি</th>
                    <th class="text-center">মোট টাকার পরিমান</th>
                </tr>

                <tr>
                    <td  class=" text-left" rowspan="2">৬ষ্ঠ</td>
                    <td>পদ্মা</td>
                    <td  class=" text-left">
                        জনাব মো:মাহফুজুর রহমান<br>
                        (০১৭২২-০৩৭৯৪৭)
                    </td>
                    <td class=" text-left" rowspan="9">
                        ১। জন্ম নিবন্ধন সনদের সত্যায়িত      ফটোকপি।<br>
                        ২। ০২ (দুই) কপি পাসপোর্ট            সাইজের ছবি (স্কুল ড্রেস)<br>
                        ৩। মূল প্রবেশ পত্র<br>
                        ৪।  টিসি জমা দিতে হবে।<br>
                        ৫। সমাপনী পরীক্ষা  পাসের নম্বর পত্র।<br>
                        ৬। তথ্য ফরম<br>
                        ৭। পিতা মাতার  NID কার্ডের ফটোকপি
                    </td>
                    <td class=" text-left" rowspan="4">
                        সেসান=      ৩,৭৭৫/-<br>
                        বেতন (জানু)=৫০০/-<br>
                        বার্ষিকভোজ   =২০০/-<br>
                        <b>সর্বমোট=     ৪,৪৭৫/-</b>
                    </td>
                </tr>
                <tr>
                    <td >মেঘনা  </td>
                    <td  class=" text-left">
                        জনাব আখতারুজ্জামান <br>
                        (০১৭২৯-৪৫২৯৩৯)
                    </td>
                </tr>
                <tr>
                    <td  class=" text-left" rowspan="2">সপ্তম</td>
                    <td>তিস্তা </td>
                    <td  class=" text-left">
                        কাজী হাসিবুল হাসান <br>
                        (০১৭৩৬-০৭২৬১৩)
                    </td>
                </tr>
                <tr>
                    <td>তিতাস</td>
                    <td  class=" text-left">
                        ফিরোজ আহম্মেদ<br>
                        (০১৯১৭-১১৮০৯০)
                    </td>
                </tr>
                <tr>
                    <td  class=" text-left" rowspan="2">অষ্টম</td>
                    <td>রুপসা   </td>
                    <td  class=" text-left">
                        জনাব মো: আব্দুল ওহাব<br>
                        ০১৭২৭-৮১৪১২১
                    </td>
                    <td class=" text-left" rowspan="5">
                        সেসান=      ৪,২৭৫/-<br>
                        বেতন (জানু)=৫০০/-<br>
                        বার্ষিকভোজ   =২০০/-<br>
                        <b>সর্বমোট=     ৪,৯৭৫/-</b>
                    </td>
                </tr>
                <tr>
                    <td>সুরমা</td>
                    <td  class=" text-left">
                        জনাব নূরমোহাম্মদ (রাজ) <br>
                        (০১৭৩৫-৫৯৫৯৭৭)
                    </td>
                </tr>

                <tr>
                    <td  class=" text-left" rowspan="3">নবম</td>
                    <td>পলাশ (বিজ্ঞান)</td>
                    <td  class=" text-left">
                        মো: শাহিনুজ্জামান<br>
                        (০১৫৫৬-৩১৫০৯২)
                    </td>
                </tr>
                <tr>
                    <td>ডালিয়া (বাণিজ্য)</td>
                    <td  class=" text-left">
                        জনাব মনোজ কুমার মন্ডল<br>
                        (০১৭১৬-৬৯৫৫৪১)
                    </td>
                </tr>
                <tr>
                    <td>শিমুল (মানবিক)</td>
                    <td  class=" text-left">
                        মোঃ জাকির হোসাইন<br>
                        (০১৭৭৩-৬৩৯৮৬১)
                    </td>
                </tr>
                

            </tbody>
        </table>

        <div class="text-center mt-3 mb-3">
                   <h5> ড.মোঃ গোলাম মাওলা  </h5>
  অধ্যক্ষ<br>
  মোবাইল - ০১৭১১-৩৮০০৮৩
  <br><b>E-mail:</b> smmplsc21@yahoo.com
            </div>
    </div>

</div>
</div>
</section>
@section('scripts')
@endsection
@endsection