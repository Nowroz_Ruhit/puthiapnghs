<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <style type="text/css">
    	.main-wrap {
			border: 1px solid #ddd;
			box-shadow: 0 2px 2px 2px #999;
			display: block;
			margin: 25px auto;
			max-width: 480px;
			padding: 20px 30px;
			font-family: 'Roboto', sans-serif;
		}
		.main-wrap p{
			line-height: 20px;
		}
		.mtb-5{
			margin-bottom: 5px;
			margin-top: 5px;
		}
		.font-18{
			font-size: 18px;
		}

		@media (max-width: 500px) {
			.main-wrap p{
				font-size: 12px;
			}
			.font-18{
				font-size: 14px !important;
			}

		}
		a {
			text-decoration: none;
		}
    </style>
<script type="text/javascript"></script></head>
<body>

    <div class="main-wrap">
		<div style="width: 100%; text-align: center; font-size: 23px;">
			Shahid Mamun Mahmud Police Lines <br>School and College
		</div>
		<div style="width: 100%; text-align: center; font-size: 20px;">
			Police Lines, Rajshahi-6000
		</div>
		<div style="width: 100%; text-align: center; margin-top: 5px; margin-bottom: 5px;">
			<div style="width: 150px; text-align: center; font-size: 16px; border: 2px solid black; padding: 15px; margin-right: auto; margin-left: auto;">{{$bootAdmissionStart->title}}</div>
		</div>
		<br><br>
	    <p class="mtb-5">
			Dear {{$s_name}},<br>
			Thanks for your Payment in {{$class}}.<br>
			Your Admission ID is {{$student_id}}.<br>
			Please Download Your Admid Card.
			<br><br><br>
			<div style="text-align: center;">
				For more info please visit <a href="https://smmplsc.edu.bd/admission">https://smmplsc.edu.bd/admission</a><br>
				<strong>Cell Us : 0721-771943, 01711380083</strong><br>
				<strong>E-mail Us : smmplsc21@yahoo.com</strong>
			</div>
	    </p>
	    <br>
<p style="text-align: center;">Powered By : <a href="https//:desktopit.com.bd">Desktop IT</a></p>
		<!-- <p class="mtb-5 font-18" style="margin-top: 30px;"><strong>--190 Celebration Committee </strong> </p> -->
		{{--<p ><img src="{{ asset('public/uplodefile/defult/logo.png') }}" alt="RCS-190-logo" width="36"> <img style="float: right;" src="{{ asset('public/uplodefile/defult/dit-36x36.png') }}" alt="dit-logo" width="36"> </p>--}}
	</div>

   
</body>
</html>