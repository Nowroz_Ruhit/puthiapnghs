<?php
// use Modules\Frontend\Http\Controllers\FrontendController;
// use Modules\Visitor\Entities\Visitor;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $bootGeneral->title }}</title>
    <meta name="author" content="Puthia P.N Government High School, Puthia, Rajshahi">
    <meta name="keywords" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi, puthia p.n government high school puthia rajshahi, puthia p.n government high school admission form, admission form puthia p.n government high school, online admission form puthia p.n government high school">
    <meta name="description" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi, puthia p.n government high school puthia rajshahi, puthia p.n government high school admission form, admission form puthia p.n government high school, online admission form puthia p.n government high school">

    <meta property="og:title" content="PUTHIAPNGHS| puthia p.n government high school, puthia, rajshahi| পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় | PUTHIAPNGHS Admission" />
    <meta property="og:site_name" content="পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়" />
    <meta property="og:description" content="PUTHIAPNGHS, puthia p.n government high school puthia rajshahi, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় , puthia p.n government high school puthia rajshahi" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://puthiapnghs.edu.bd/admission"/>
	
	<meta property="og:image" content="http://smmplsc.edu.bd/public/admission.jpg" />
	<meta property="og:type" content="website" /> 
	<meta property="og:url" content="https://puthiapnghs.edu.bd/admission"/>
	<meta property="og:title" content="online admission puthia p.n government high school" />
  <meta property="og:description" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi" />
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-YSYG2CN2XW"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-YSYG2CN2XW');
    </script>

    @stack('css')

  


<!--     <link rel="shortcut icon" href="{{asset('public/guest-user/images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('public/guest-user/images/favicon.png')}}" type="image/x-icon"> -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset(trans('global.links.general_show').$bootGeneral->fab )}}">
    
    <link rel="stylesheet" href="{{asset('public/guest-user/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="{{asset('public/guest-user/assets/fontawesome-5.10.2/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/guest-user/assets/themify-icons/themify-icons.css')}}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('public/guest-user/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('public/guest-user/css/admission.css')}}">
    <link rel="stylesheet" href="{{asset('public/guest-user/css/responsive.css')}}">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" />
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('modules/admission/Resources/assets/css/admission.css') }}">--}}
    <link href="{{ asset('css/admission/admission.css') }}" rel="stylesheet" />


    @yield('styles')



</head>

<body>
    <div class="container main-container pt-1">




<!--Section NavBar -->
    {{--<section id="mainNav">
        <nav class="navbar navbar-expand-lg navbar-light">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav  py-4 py-md-0 nav">
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link " href="{{route('frontend.admission.index')}}">Home</a>
              </li>                          
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="{{route('frontend.admission.apply')}}">Apply Now</a>
              </li>
               <!-- <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="#">Admission Circular</a>
              </li>
               <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="#">Admission Procedure</a>
              </li>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="#">Payment Procedure</a>
              </li>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="#">Payment Acknowledgement Check</a>
              </li>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="#">Result</a>
              </li> -->
              
              <!-- <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="{{route('admission.student.login')}}">
                  <i class="fas fa-user nav-icon"></i> 
                Login</a>
              </li> -->
            </ul>
          </div>
        </nav>
    </section>--}}
    <!-- End Section NavBar-->
@yield('content')



    <section class="copyright-power mt-3">
      <div class="row">
        <div class="col-sm-6">
          <p>Copyright &copy; {{date('Y')}} | {{ $bootGeneral->title }}</p>
        </div>
        <div class="col-sm-6 text-right">
          <p>Powered by <a href="http://desktopit.com.bd">Desktop IT</a>  </p>
        </div>
      </div>
    </section>
    </div>
    <!-- </div> -->


    <!-- Bootstrap JS -->
    <script src="{{asset('public/guest-user/js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{ asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_jquery_3.3.1_jquery.min.js') }}"></script>
    <script src="{{asset('public/guest-user/js/popper.min.js')}}"></script>
    <script src="{{asset('public/guest-user/js/bootstrap.min.js')}}"></script>

    <!-- Custom JS -->
    <script src="{{asset('public/guest-user/js/custom.js')}}"></script>
    
    
    @yield('scripts')

<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>
<script type="text/javascript">
  var hulla = new hullabaloo();
  @if(Session::has('success')) 
          hulla.send("{{ Session::get('success') }}", 'success') 
     @php
       Session::forget('success');
     @endphp
  @endif

  @if(Session::has('danger')) 
          hulla.send("{{ Session::get('danger') }}", 'danger') 
     @php
       Session::forget('danger');
     @endphp
  @endif

  @if(Session::has('warning')) 
          hulla.send("{{ Session::get('warning') }}", 'warning') 
     @php
       Session::forget('warning');
     @endphp
  @endif
  </script>

</body>
</html>
