<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('input_mark_id')->unsigned()->nullable();
            $table->bigInteger('class_id')->unsigned()->nullable();
            $table->bigInteger('admission_id')->unsigned()->nullable();
            $table->bigInteger('total_mark')->unsigned()->nullable();
            
            $table->string('position')->nullable();


            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('class_id')
            ->references('id')->on('paymentamounts')
            ->onUpdate('cascade')->onDelete('cascade'); 
            $table->foreign('admission_id')
            ->references('id')->on('payment_completes')
            ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('input_mark_id')
            ->references('id')->on('admission_mark_inputs')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_results');
    }
}
