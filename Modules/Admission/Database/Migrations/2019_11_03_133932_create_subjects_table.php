<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('class_id')->unsigned()->nullable();
            $table->bigInteger('subject_list_id')->unsigned()->nullable();
            $table->string('checkbox')->nullable();
            // $table->string('subject_code')->nullable();
            // $table->string('subject_name')->nullable();
            $table->string('subject_marks')->nullable();
            $table->string('subject_pass_marks')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('class_id')
            ->references('id')->on('paymentamounts')
            ->onUpdate('cascade')->onDelete('cascade'); 
            $table->foreign('subject_list_id')
            ->references('id')->on('subject_lists')
            ->onUpdate('cascade')->onDelete('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
