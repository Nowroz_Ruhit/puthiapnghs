<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentCompletesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_completes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->biginteger('student_details_id')->unsigned()->nullable();
            $table->biginteger('paymentamounts_id')->unsigned()->nullable();
            $table->biginteger('transactions_id')->unsigned()->nullable();
            $table->string('admission_id')->nullable();
            $table->boolean('complete')->default(0)->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_completes');
    }
}
