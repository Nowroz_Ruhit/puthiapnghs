<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_students', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('educationType',35);
            $table->string('name');
            $table->string('birth');
            $table->string('birth_certificate');
            $table->string('gender');
            $table->string('blood')->nullable();
            $table->string('religion');
            $table->string('nationality')->nullable();
            $table->string('image')->nullable();
            $table->string('previousInstretute')->nullable();
            $table->string('previousRoll')->nullable();
            $table->string('hasQouta',1)->nullable();
            $table->string('qoutaName',35)->nullable();
            $table->string('father_name');
            $table->string('father_nid')->nullable();
            $table->string('father_phone');
            $table->string('father_profession');
            $table->string('father_email')->nullable();

            $table->string('mother_name');
            $table->string('mother_nid')->nullable();
            $table->string('mother_phone')->nullable();
            $table->string('mother_profession');
            $table->string('mother_email')->nullable();

            $table->string('present_address');
            $table->string('present_village')->nullable();
            $table->string('present_road')->nullable();
            $table->string('present_post_office')->nullable();
            $table->string('present_post_code')->nullable();
            $table->string('present_thana')->nullable();
            $table->string('present_district')->nullable();

            $table->boolean('same_as_present')->default(0);

            $table->string('permananent_address')->nullable();
            $table->string('permananent_village')->nullable();
            $table->string('permananent_road')->nullable();
            $table->string('permananent_post_office')->nullable();
            $table->string('permananent_post_code')->nullable();
            $table->string('permananent_thana')->nullable();
            $table->string('permananent_district')->nullable();

            $table->boolean('PrimaryContact')->default(0);

            $table->string('localGurdian_name')->nullable();
            $table->string('localGurdian_relation')->nullable();
            $table->string('localGurdian_phone')->nullable();
            $table->string('localGurdian_email')->nullable();

            $table->string('localGurdian_address')->nullable();
            $table->string('localGurdian_village')->nullable();
            $table->string('localGurdian_road')->nullable();
            $table->string('localGurdian_post_office')->nullable();
            $table->string('localGurdian_postCode')->nullable();
            $table->string('localGurdian_thana')->nullable();
            $table->string('localGurdian_district')->nullable();

            $table->biginteger('paymentamounts_id')->unsigned()->nullable();
            $table->biginteger('transactions_id')->unsigned()->nullable();
            $table->boolean('complete')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_students');
    }
}
