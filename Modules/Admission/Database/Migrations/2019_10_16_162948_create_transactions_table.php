<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            // $table->string('tran_id',100)->unique();
            // $table->string('val_id')->nullable();
            // $table->float('amount',16,2)->unsigned();
            // $table->string('card_type')->nullable();
            // $table->float('store_amount',16,2)->unsigned()->nullable();
            // $table->string('card_no')->nullable();
            // $table->string('bank_tran_id')->nullable();
            // $table->string('status')->nullable();
            // $table->string('tran_date')->nullable();
            // $table->string('card_issuer')->nullable();
            // $table->string('card_brand')->nullable();
            // $table->string('card_issuer_country')->nullable();
            // $table->string('card_issuer_country_code')->nullable();
            // $table->string('store_id')->nullable();
            // $table->string('verify_sign')->nullable();
            // $table->string('verify_sign_sha2')->nullable();
            // $table->string('currency_type')->nullable();
            // $table->string('currency_amount')->nullable();
            // $table->string('currency_rate')->nullable();
            // $table->string('base_fair')->nullable();
            // $table->string('risk_level')->nullable();
            // $table->string('risk_title')->nullable();
            // $table->string('error')->nullable();
            $table->string('tran_id',30)->unique()->nullable();
            $table->string('val_id',50)->nullable();
            $table->float('amount',16,2)->unsigned()->nullable();
            $table->string('card_type',50)->nullable();
            $table->float('store_amount',16,2)->unsigned()->nullable();
            $table->string('card_no',30)->nullable();
            $table->string('bank_tran_id',50)->nullable();
            $table->string('status',255)->nullable();
            $table->string('tran_date',255)->nullable();
            $table->string('currency',3)->nullable();
            $table->string('card_issuer',50)->nullable();
            $table->string('card_brand',30)->nullable();
            $table->string('card_issuer_country',50)->nullable();
            $table->string('card_issuer_country_code',2)->nullable();
            $table->string('store_id')->nullable();
            $table->string('verify_sign',255)->nullable();
            $table->longText('verify_key')->nullable();
            $table->string('verify_sign_sha2',255)->nullable();
            $table->string('currency_type',3)->nullable();
            $table->float('currency_amount',16,2)->unsigned()->nullable();
            $table->float('currency_rate',8,4)->unsigned()->nullable();
            $table->float('base_fair',8,4)->unsigned()->nullable();
            $table->string('value_a',255)->nullable();
            $table->string('value_b',255)->nullable();
            $table->string('value_c',255)->nullable();
            $table->string('value_d',255)->nullable();
            $table->integer('risk_level')->nullable();
            $table->string('risk_title',50)->nullable();
            $table->string('error')->nullable();
            $table->longText('key')->nullable();
            $table->string('pass')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
