<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('all_students', function (Blueprint $table) {
            $table->bigIncrements('id');
            /*Admission Table Start*/
            $table->bigInteger('admission_starts_id')->unsigned()->nullable(); 
            $table->string('title')->nullable();
            /*Admission Table End*/

            /*Student Details Table Start*/
            $table->string('name');
            $table->string('birth');
            $table->string('birth_certificate');
            $table->string('gender');
            $table->string('blood')->nullable();
            $table->string('religion');
            $table->string('nationality')->nullable();
            $table->string('image')->nullable();

            $table->string('previousInstretute')->nullable();
            $table->string('previousRoll')->nullable();

            $table->string('father_name');
            $table->string('father_nid')->nullable();
            $table->string('father_phone');
            $table->string('father_profession');
            $table->string('father_email')->nullable();

            $table->string('mother_name');
            $table->string('mother_nid')->nullable();
            $table->string('mother_phone')->nullable();
            $table->string('mother_profession');
            $table->string('mother_email')->nullable();

            $table->string('present_address');
            $table->string('present_village')->nullable();
            $table->string('present_road')->nullable();
            $table->string('present_post_office')->nullable();
            $table->string('present_post_code')->nullable();
            $table->string('present_thana')->nullable();
            $table->string('present_district')->nullable();

            $table->boolean('same_as_present')->default(0);

            $table->string('permananent_address')->nullable();
            $table->string('permananent_village')->nullable();
            $table->string('permananent_road')->nullable();
            $table->string('permananent_post_office')->nullable();
            $table->string('permananent_post_code')->nullable();
            $table->string('permananent_thana')->nullable();
            $table->string('permananent_district')->nullable();

            $table->tinyInteger('PrimaryContact')->default(0);

            $table->string('localGurdian_name')->nullable();
            $table->string('localGurdian_relation')->nullable();
            $table->string('localGurdian_phone')->nullable();
            $table->string('localGurdian_email')->nullable();

            $table->string('localGurdian_address')->nullable();
            $table->string('localGurdian_village')->nullable();
            $table->string('localGurdian_road')->nullable();
            $table->string('localGurdian_post_office')->nullable();
            $table->string('localGurdian_postCode')->nullable();
            $table->string('localGurdian_thana')->nullable();
            $table->string('localGurdian_district')->nullable();
            /*Student Details Table End*/

            /*Payment  Complete Table Start*/
            $table->biginteger('paymentamounts_id')->unsigned()->nullable();
            $table->string('admission_id')->nullable();
            /*Payment  Complete Table End*/

            /*Result Table Start*/
            $table->bigInteger('total_mark')->unsigned()->nullable();
            $table->string('position')->nullable();
            $table->integer('part')->unsigned()->nullable();
            /*Result Table End*/

            /*Mark Table Start*/
            $table->integer('subject_1')->unsigned()->nullable();
            $table->integer('subject_2')->unsigned()->nullable();
            $table->integer('subject_3')->unsigned()->nullable();
            $table->integer('subject_4')->unsigned()->nullable();
            /*Mark Table End*/

            /*Transactions Table Start*/
            $table->string('tran_id',30)->unique()->nullable();
            $table->string('val_id',50)->nullable();
            $table->float('amount',16,2)->unsigned()->nullable();
            $table->string('card_type',50)->nullable();
            $table->float('store_amount',16,2)->unsigned()->nullable();
            $table->string('card_no',30)->nullable();
            $table->string('bank_tran_id',50)->nullable();
            $table->string('status',255)->nullable();
            $table->string('tran_date',255)->nullable();
            $table->string('currency',3)->nullable();
            $table->string('card_issuer',50)->nullable();
            $table->string('card_brand',30)->nullable();
            $table->string('card_issuer_country',50)->nullable();
            $table->string('card_issuer_country_code',2)->nullable();
            $table->string('store_id')->nullable();
            $table->string('verify_sign',255)->nullable();
            $table->longText('verify_key')->nullable();
            $table->string('verify_sign_sha2',255)->nullable();
            $table->string('currency_type',3)->nullable();
            $table->float('currency_amount',16,2)->unsigned()->nullable();
            $table->float('currency_rate',8,4)->unsigned()->nullable();
            $table->float('base_fair',8,4)->unsigned()->nullable();
            $table->string('value_a',255)->nullable();
            $table->string('value_b',255)->nullable();
            $table->string('value_c',255)->nullable();
            $table->string('value_d',255)->nullable();
            $table->integer('risk_level')->nullable();
            $table->string('risk_title',50)->nullable();
            $table->string('error')->nullable();
            $table->longText('key')->nullable();
            $table->string('pass')->nullable();
            /*Transactions Table End*/
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('all_students');
    }
}
