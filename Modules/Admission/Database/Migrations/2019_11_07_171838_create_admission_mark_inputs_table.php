<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionMarkInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_mark_inputs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('class_id')->unsigned()->nullable();
            $table->bigInteger('admission_id')->unsigned()->nullable();

            $table->integer('subject_1')->unsigned()->nullable();
            $table->integer('subject_2')->unsigned()->nullable();
            $table->integer('subject_3')->unsigned()->nullable();
            $table->integer('subject_4')->unsigned()->nullable();

            $table->string('extra')->nullable();


            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('class_id')
            ->references('id')->on('paymentamounts')
            ->onUpdate('cascade')->onDelete('cascade'); 
            $table->foreign('admission_id')
            ->references('id')->on('payment_completes')
            ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_mark_inputs');
    }
}
