<?php

namespace Modules\Admission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Admission\Entities\AdmissionStart;
use Modules\Admission\Entities\AdmissionResult;
use Modules\Admission\Entities\AdmissionMarkInput;

use Modules\Admission\Entities\Transaction;
use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\StudentDetails;
use Modules\Admission\Entities\PaymentComplete;
use Modules\Admission\Entities\TempStudent;
use DB;
use Modules\Admission\Entities\SubjectList;
use Modules\Admission\Entities\Subject;
use Modules\Admission\Entities\AllStudent;
class ManageContollerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('admission_start'), 403);
        $admission = AdmissionStart::all();
        return view('admission::admin.manage.index', compact('admission'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admission::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admission::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(AdmissionStart $admission)
    {
        abort_unless(\Gate::allows('admission_start'), 403);
        // dd($admission);
        return view('admission::admin.manage.edit', compact('admission'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, AdmissionStart $admission)
    {
        abort_unless(\Gate::allows('admission_start'), 403);
        if($admission->update($request->all())){
            return redirect()->route('admin.admission.manage.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('admin.admission.manage.index')->with('error', 'Not Updated, Somtheing was wrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function reset()
    {
        abort_unless(\Gate::allows('admission_reset'), 403);
        DB::table('admission_results')->delete();
        DB::table('admission_mark_inputs')->delete();
        DB::table('temp_students')->delete();
        DB::table('payment_completes')->delete();
        DB::table('transactions')->delete();
        DB::table('student_details')->delete();

        DB::select("ALTER TABLE admission_results AUTO_INCREMENT = 1");
        DB::select("ALTER TABLE admission_mark_inputs AUTO_INCREMENT = 1");
        DB::select("ALTER TABLE temp_students AUTO_INCREMENT = 1");
        DB::select("ALTER TABLE payment_completes AUTO_INCREMENT = 1");
        DB::select("ALTER TABLE transactions AUTO_INCREMENT = 1");
        DB::select("ALTER TABLE student_details AUTO_INCREMENT = 1");
        DB::select("UPDATE `paymentamounts` set `count` = 0, `input_mark` =0, `make_result` =0, `publish`=0, `input_mark1` =0, `make_result1` =0, `publish1`=0");

        return back();
    }

    public function resetbackup()
    {
        $admission = AdmissionStart::orderBy('id', 'desc')->first();
        $students = StudentDetails::all();

        foreach ($students as $key => $student) {
            $data = array();
            /*Admission Table Start*/
            $data['admission_starts_id'] = $admission->id;
            $data['title'] = $admission->title;
            /*Admission Table End*/

            /*Student Details Table Start*/
            $data['name'] = $student->name;
            $data['birth'] = $student->birth;
            $data['birth_certificate'] = $student->birth_certificate;
            $data['gender'] = $student->gender;
            $data['blood'] = $student->blood;
            $data['religion'] = $student->religion;
            $data['nationality'] = $student->nationality;
            $data['image'] = $student->image;

            $data['previousInstretute'] = $student->previousInstretute;
            $data['previousRoll'] = $student->previousRoll;

            $data['father_name'] = $student->father_name;
            $data['father_nid'] = $student->father_nid;
            $data['father_phone'] = $student->father_phone;
            $data['father_profession'] = $student->father_profession;
            $data['father_email'] = $student->father_email;

            $data['mother_name'] = $student->mother_name;
            $data['mother_nid'] = $student->mother_nid;
            $data['mother_phone'] = $student->mother_phone;
            $data['mother_profession'] = $student->mother_profession;
            $data['mother_email'] = $student->mother_email;

            $data['present_address'] = $student->present_address;
            $data['present_village'] = $student->present_village;
            $data['present_road'] = $student->present_road;
            $data['present_post_office'] = $student->present_post_office;

            $data['present_post_code'] = $student->present_post_code;
            $data['present_thana'] = $student->present_thana;
            $data['present_district'] = $student->present_district;

            $data['same_as_present'] = $student->same_as_present;

            $data['permananent_address'] = $student->permananent_address;
            $data['permananent_village'] = $student->permananent_village;
            $data['permananent_road'] = $student->permananent_road;
            $data['permananent_post_office'] = $student->permananent_post_office;
            $data['permananent_post_code'] = $student->permananent_post_code;
            $data['permananent_thana'] = $student->permananent_thana;
            $data['permananent_district'] = $student->permananent_district;

            $data['PrimaryContact'] = $student->PrimaryContact;

            $data['localGurdian_name'] = $student->localGurdian_name;
            $data['localGurdian_relation'] = $student->localGurdian_relation;
            $data['localGurdian_phone'] = $student->localGurdian_phone;
            $data['localGurdian_email'] = $student->localGurdian_email;

            $data['localGurdian_address'] = $student->localGurdian_address;
            $data['localGurdian_village'] = $student->localGurdian_village;
            $data['localGurdian_road'] = $student->localGurdian_road;
            $data['localGurdian_post_office'] = $student->localGurdian_post_office;

            $data['localGurdian_postCode'] = $student->localGurdian_postCode;
            $data['localGurdian_thana'] = $student->localGurdian_thana;
            $data['localGurdian_district'] = $student->localGurdian_district;
            /*Student Details Table End*/

            /*Payment  Complete Table Start*/
            $data['paymentamounts_id'] = $student->student_id->paymentamounts_id;
            $data['admission_id'] = $student->student_id->admission_id;
            /*Payment  Complete Table End*/

            /*Result Table Start*/
            $data['total_mark'] = $student->student_id->position->total_mark ?? 0;
            $data['position'] = $student->student_id->position->position ?? '';
            $data['part'] = $student->student_id->position->part ?? 1;
            /*Result Table End*/

            /*Mark Table Start*/
            $data['subject_1'] = $student->student_id->marks->subject_1 ?? 0;
            $data['subject_2'] = $student->student_id->marks->subject_2 ?? 0;
            $data['subject_3'] = $student->student_id->marks->subject_3 ?? 0;
            $data['subject_4'] = $student->student_id->marks->subject_4 ?? 0;
            /*Mark Table End*/

            /*Transactions Table Start*/
            $data['tran_id'] = $student->student_id->tran->tran_id;
            $data['val_id'] = $student->student_id->tran->val_id;
            $data['amount'] = $student->student_id->tran->amount;
            $data['card_type'] = $student->student_id->tran->card_type;
            $data['store_amount'] = $student->student_id->tran->store_amount;
            $data['card_no'] = $student->student_id->tran->card_no;
            $data['bank_tran_id'] = $student->student_id->tran->bank_tran_id;
            $data['status'] = $student->student_id->tran->status;
            $data['tran_date'] = $student->student_id->tran->tran_date;
            $data['currency'] = $student->student_id->tran->currency;

            $data['card_issuer'] = $student->student_id->tran->card_issuer;
            $data['card_brand'] = $student->student_id->tran->card_brand;
            $data['card_issuer_country'] = $student->student_id->tran->card_issuer_country;
            $data['card_issuer_country_code'] = $student->student_id->tran->card_issuer_country_code;
            $data['store_id'] = $student->student_id->tran->store_id;
            $data['verify_sign'] = $student->student_id->tran->verify_sign;
            $data['verify_key'] = $student->student_id->tran->verify_key;
            $data['verify_sign_sha2'] = $student->student_id->tran->verify_sign_sha2;
            $data['currency_type'] = $student->student_id->tran->currency_type;
            $data['currency_amount'] = $student->student_id->tran->currency_amount;

            $data['currency_rate'] = $student->student_id->tran->currency_rate;
            $data['base_fair'] = $student->student_id->tran->base_fair;
            $data['value_a'] = $student->student_id->tran->value_a;
            $data['value_b'] = $student->student_id->tran->value_b;
            $data['value_c'] = $student->student_id->tran->value_c;
            $data['value_d'] = $student->student_id->tran->value_d;
            $data['risk_level'] = $student->student_id->tran->risk_level;
            $data['risk_title'] = $student->student_id->tran->risk_title;
            $data['error'] = $student->student_id->tran->error;
            $data['key'] = $student->student_id->tran->key;
            $data['pass'] = $student->student_id->tran->pass;
            /*Transactions Table End*/

            AllStudent::create($data);
            // DB::table('all_students')->insert($data);

        }
            
        return redirect()->route('admin.admission.manage.reset');
    }
}
