<?php

namespace Modules\Admission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use PDF;
use Modules\Admission\Entities\Transaction;
use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\StudentDetails;
use Modules\Admission\Entities\PaymentComplete;
use Modules\Admission\Entities\TempStudent;
use DB;
use Modules\Admission\Entities\SubjectList;
use Modules\Admission\Entities\Subject;
use Modules\SMS\Entities\Sms;
use Modules\SMS\Entities\smsStore;
use Modules\Admission\Entities\AdmissionResult;

class AdmissionAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    // {
    //     // abort_unless(\Gate::allows('academic_create'), 403);
    //     return view('admission::admin.index');
    // }
    {
        abort_unless(\Gate::allows('admission'), 403);
        // To Count Total transcation including success,canclled,failed
        $transaction_count           = Transaction::count();
        $success_transaction_count   = Transaction::where('status','=','VALID')
        ->count();
        $cancelled_transaction_count = Transaction::where('status','=','CANCELLED')
        ->count();
        $failed_transaction_count    = Transaction::where('status','=','FAILED')
        ->count();

        $total_transcation_amount    = Transaction::sum('amount');
        $total_store_amount    = Transaction::sum('store_amount');
        $number_of_student           = StudentDetails::count();
        $number_of_student_panding   = TempStudent::count();
        $payment_complete_transcation= PaymentComplete::sum('complete');
        $total_class = Paymentamount::count();


        // dd($payment_complete_transcation); 
        
        return view('admission::admin.index',
            compact('transaction_count',
                'success_transaction_count',
                'cancelled_transaction_count',
                'failed_transaction_count',
                'total_transcation_amount',
                'number_of_student',
                'payment_complete_transcation', 'total_store_amount','number_of_student_panding','total_class'
            ));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('admission'), 403);
        return view('admission::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('admission'), 403);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        return view('admission::admin.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        return view('admission::admin.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        abort_unless(\Gate::allows('admission'), 403);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        abort_unless(\Gate::allows('admission'), 403);
    }

    public function paymentComplete()
    {
        abort_unless(\Gate::allows('admission'), 403);
        $students = PaymentComplete::orderBy('paymentamounts_id', 'ASC')->get();
        return view('admission::admin.paymentComplete', compact('students'));
    }

    public function paymentPending() 
    {
        abort_unless(\Gate::allows('admission'), 403);
        $students = TempStudent::where('complete', false)->get();
        return view('admission::admin.paymentPending', compact('students'));
    }

    public function paymentStatus()
    {
        abort_unless(\Gate::allows('admission'), 403);
        // return view('admission::admin.paymentStatus');
        $ammounts = Transaction::sum('store_amount');
        $classes = Paymentamount::all();
        return view('admission::admin.paymentStatus', compact('classes', 'ammounts'));
    }

    public function transactions() 
    {
        abort_unless(\Gate::allows('admission'), 403);
        $transactions = Transaction::all();
        return view('admission::admin.transactions', compact('transactions'));
    }

    public function classAmount() 
    {
        abort_unless(\Gate::allows('admission'), 403);
        $amounts = Paymentamount::orderby('class', 'asc')->get();
        return view('admission::admin.classAmount', compact('amounts'));
    }

    public function classAmountCreate()
    {
        abort_unless(\Gate::allows('admission'), 403);
        return view('admission::admin.createAmount');
    }

    public function classAmountStore(Request $request)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $validatedData = $request->validate([
            'class'=>'required',
            'amount'=>'required',
        ]);
        $amount = $request->all();
        $amount['created_by'] = Auth::id();
        $data = Paymentamount::create($amount);
        if($data){
            return redirect()->route('admin.admission.classAmount')->with('success', 'Add Successfull');
        }else {
            return redirect()->route('admin.admission.classAmount')->with('error', 'Not Added, Somtheing waswrong');
        }
    }

    public function classAmountEdit(Paymentamount $result)
    {
        abort_unless(\Gate::allows('admission'), 403);
        return view('admission::admin.editAmount', compact('result'));
    }

    public function classAmountUpdate(Request $request, Paymentamount $result)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $validatedData = $request->validate([
            'class'=>'required',
            'amount'=>'required',
        ]);
        $amount = $request->all();
        $amount['updated_by'] = Auth::id();
        // $data = Paymentamount::create($amount);

        if($result->update($amount)){
            return redirect()->route('admin.admission.classAmount')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('admin.admission.classAmount')->with('error', 'Not Updated, Somtheing waswrong');
        }
    }

    public function classAmountDelete(Paymentamount $result)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $user['deleted_by'] = Auth::id();
        $result->update($user);
        if($result->delete()){
            return redirect()->route('admin.admission.classAmount')->with('delete', 'Delete Successfull');
        }else {
            return redirect()->route('admin.admission.classAmount')->with('error', 'Not Deleted, Somtheing waswrong');
        }
    }

    public function datafilter(Request $request)
    {
        abort_unless(\Gate::allows('admission'), 403);
        // dd($request->all());
        if(request()->ajax())
        {
            $gender = $request->filter_gender;
            $class = $request->filter_class;
            $form = $request->filter_date_from;
            $to = $request->filter_date_to;

            if((!empty($request->filter_gender)) && (!empty($request->filter_class)) && (!empty($request->filter_date_from)) && (!empty($request->filter_date_to)))
            {
             $datas = DB::table('payment_completes')
             ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
             ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
             ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
             ->select('payment_completes.admission_id','student_details.name as student_name', 'student_details.father_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount','student_details.image')
             ->where([['student_details.gender', $request->filter_gender], ['paymentamounts.class', $request->filter_class]
         ])
             ->whereBetween('transactions.tran_date', [$request->filter_date_from, $request->filter_date_to])
             ->orderBy('payment_completes.admission_id', 'ASC')
             ->get();
         }
         else if((!empty($request->filter_gender)) && (!empty($request->filter_class)) && (empty($request->filter_date_from)) && (empty($request->filter_date_to)))
         {
             $datas = DB::table('payment_completes')
             ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
             ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
             ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
             ->select('payment_completes.admission_id','student_details.name as student_name', 'student_details.father_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount','student_details.image')
             ->where([['student_details.gender', $request->filter_gender], ['paymentamounts.class', $request->filter_class]
         ])
             ->orderBy('payment_completes.admission_id', 'ASC')
             ->get();
         }
         else if((!empty($request->filter_gender)) && (empty($request->filter_class)) && (!empty($request->filter_date_from)) && (!empty($request->filter_date_to)))
         {
             $datas = DB::table('payment_completes')
             ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
             ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
             ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
             ->select('payment_completes.admission_id','student_details.name as student_name', 'student_details.father_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount','student_details.image')
             ->where('student_details.gender', $request->filter_gender)
             ->whereBetween('transactions.tran_date', [$request->filter_date_from, $request->filter_date_to])
             ->orderBy('payment_completes.admission_id', 'ASC')
             ->get();
         }
         else if((empty($request->filter_gender)) && (!empty($request->filter_class)) && (!empty($request->filter_date_from)) && (!empty($request->filter_date_to)))
         {
             $datas = DB::table('payment_completes')
             ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
             ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
             ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
             ->select('payment_completes.admission_id','student_details.name as student_name', 'student_details.father_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount','student_details.image')
             ->where('paymentamounts.class', $request->filter_class)
             ->whereBetween('transactions.tran_date', [$request->filter_date_from, $request->filter_date_to])
             ->orderBy('payment_completes.admission_id', 'ASC')
             ->get();
         }
         else if((empty($request->filter_gender)) && (empty($request->filter_class)) && (!empty($request->filter_date_from)) && (!empty($request->filter_date_to)))
         {
             $datas = DB::table('payment_completes')
             ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
             ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
             ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
             ->select('payment_completes.admission_id','student_details.name as student_name', 'student_details.father_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount','student_details.image')
             ->whereBetween('transactions.tran_date', [$request->filter_date_from, $request->filter_date_to])
             ->orderBy('payment_completes.admission_id', 'ASC')
             ->get();
         }
         else if((empty($request->filter_gender)) && (!empty($request->filter_class)) && (empty($request->filter_date_from)) && (empty($request->filter_date_to)))
         {
                // $data = DB::table('tbl_customer')
                // ->select('CustomerName', 'Gender', 'Address', 'City', 'PostalCode', 'Country')
                // ->where('Gender', $request->filter_gender)
                // ->where('Country', $request->filter_country)
                // ->get();
                // $data = StudentDetails::select('name', 'gender')->where('gender', $request->filter_gender)->get();
             $datas = DB::table('payment_completes')
             ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
             ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
             ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
             ->select('payment_completes.admission_id','student_details.name as student_name', 'student_details.father_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount','student_details.image')
             ->where('paymentamounts.class', $request->filter_class)
             ->orderBy('payment_completes.admission_id', 'ASC')
             ->get();

         }
         else if((!empty($request->filter_gender)) && (empty($request->filter_class)) && (empty($request->filter_date_from)) && (empty($request->filter_date_to)))
         {
                // $data = DB::table('tbl_customer')
                // ->select('CustomerName', 'Gender', 'Address', 'City', 'PostalCode', 'Country')
                // ->where('Gender', $request->filter_gender)
                // ->where('Country', $request->filter_country)
                // ->get();
                // $data = StudentDetails::select('name', 'gender')->where('gender', $request->filter_gender)->get();
             $datas = DB::table('payment_completes')
             ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
             ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
             ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
             ->select('payment_completes.admission_id','student_details.name as student_name', 'student_details.father_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount','student_details.image')
             ->where('student_details.gender', $request->filter_gender)
             ->orderBy('payment_completes.admission_id', 'ASC')
             ->get();

         }
         else
         {
                // $data = StudentDetails::select('name', 'gender')->get();
            $datas = DB::table('payment_completes')
            ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
            ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
            ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
            ->select('payment_completes.admission_id',
                'student_details.name as student_name',
                'student_details.father_name', 
                'paymentamounts.name as class_name', 
                'student_details.gender',
                'transactions.tran_date',
                'transactions.store_amount',
                'student_details.image'
            )
            ->orderBy('payment_completes.admission_id', 'ASC')
            ->get();
        }
            // $datas = array();
        $tk = 0;
        $k =0;
        if(!empty($datas[0]->admission_id)){
                // '<img id="previewHolder" alt="Student Image" src="'. asset('public/uplodefile/admission/'.$data->image).'" class="border mx-auto" width="80" />'
            foreach ($datas as $key => $value) {
                 // $data[$key]['image'] = '<img id="previewHolder" alt="Student Image" src="'. asset('public/uplodefile/admission/'.$value->image).'" class="border mx-auto" width="80" />';
                $data[$key]['image'] = $value->image;
                $data[$key]['admission_id'] =  $value->admission_id;
                $data[$key]['student_name'] = $value->student_name;
                $data[$key]['father_name'] = $value->father_name;
                $data[$key]['class_name'] = $value->class_name;
                $data[$key]['gender'] = $value->gender;
                $data[$key]['tran_date'] = $value->tran_date;
                $data[$key]['store_amount'] = $value->store_amount;

                $tk = $tk+(int)$value->store_amount;
                // $data[$key]['ax'] = 100;
                $data[$key]['action'] = '<a class="btn btn-xs btn-primary" href="'.route('admin.admission.studentView', $value->admission_id).'">
                View
                </a>
                <a class="btn btn-xs btn-info" href="'.route('admin.admission.studentDownload', $value->admission_id).'" >
                Print
                </a>';
                // <a class="btn btn-xs btn-success" href="'.route('admin.admission.print.admit', $value->admission_id).'" >Admit Card</a> 
//admin.admission.studentDownload
//onclick="divSelectionforPrint('.$value->admission_id.')"
// <a class="btn btn-xs btn-success" href="'.route('admin.admission.studentAdmitDownload', $value->admission_id).'" onclick="printstudent('.'student-full-info'.')">Admit Card</a> 
// <a class="btn btn-xs btn-info" href="#" onclick="printstudent('."'student-full-info'".')"> Print</a>
// ,'."'student-full-print'".'
                
                $k++;
                
            }
            // $data[$k]['amount'] = $tk;
            // session()->put('amount',$tk);
            return datatables()->of($data)->make(true);
        }else{
            $data = $datas;
            return datatables()->of($data)->make(true);
        }
    }

}

public function joinFunction() 
{
    abort_unless(\Gate::allows('admission'), 403);
    $filter_gender = 'male';
    $filter_class = '2';
    $from = '2019-10-25';
    $to = '2019-10-28';

    $data = DB::table('payment_completes')
    ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
    ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
    ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
    ->select('payment_completes.admission_id','student_details.name as student_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.tran_date', 'transactions.store_amount')
    ->where([['student_details.gender', $filter_gender], ['paymentamounts.class', $filter_class]
])
    ->whereBetween('transactions.tran_date', [$from, $to])
            // ->wheredate('transactions.tran_date', $filter_date)
    ->get();
    dd($data);
}

public function studentView($id) 
{
    abort_unless(\Gate::allows('admission'), 403);
    $PaymentComplete = PaymentComplete::where('admission_id','=', $id)->firstOrFail();
        // dd($admission_id);
    $admissionFormDetails = StudentDetails::findOrFail($PaymentComplete->student_details_id);

    $data = DB::table('payment_completes')
    ->join('student_details', 'payment_completes.student_details_id', '=', 'student_details.id')
    ->join('paymentamounts', 'payment_completes.paymentamounts_id', '=', 'paymentamounts.id')
    ->join('transactions', 'payment_completes.transactions_id', '=', 'transactions.id')
    ->select('payment_completes.admission_id','student_details.name as student_name', 'paymentamounts.name as class_name', 'student_details.gender','transactions.*')
    ->where( 'payment_completes.admission_id', $id)
    ->get();
    return view('admission::admin.studentDetails',  compact('admissionFormDetails', 'PaymentComplete', 'data'));
}

public function studentDownload($id)
{
    abort_unless(\Gate::allows('admission'), 403);
        // echo "Download ".$id;
        // return view('admission::admin.detailsPrint');

    // $admission_id = $request->value;
    $complete = PaymentComplete::where('admission_id','=', $id)->firstOrFail();
    $view = view("admission::admin.detailsPrint",compact('complete'))->render();
    // return response()->json(['html'=>$view]);
    $pdf = PDF::loadView("admission::admin.detailsPrint",compact('complete'));
        return $pdf->download($complete->student->name.' Student Information.pdf');
}

public function downloadCard($id)
{
    abort_unless(\Gate::allows('admission'), 403);
    $admission_id = PaymentComplete::where('admission_id','=', $id)->firstOrFail();
        // dd($admission_id);
    $data = StudentDetails::findOrFail($admission_id->student_details_id);
        // dd($admission_id->student);
        // $data = StudentDetails::findOrFail($id);
    $pdf = PDF::loadView('admission::admission.pdf', compact('data'));  
    return $pdf->download('Admit Card - '.$data->name.'.pdf');
}

public function downloadDetails ($id) 
{
    abort_unless(\Gate::allows('admission'), 403);
    $admissionFormPrint = StudentDetails::findOrFail($id);
    return view('admission::admin.studentDetailsPrint', compact('admissionFormPrint'));
}

public function subjectSettings()
{
    abort_unless(\Gate::allows('admission'), 403);
    $classList = Paymentamount::where('subject_set','=','1')->orderby('class', 'asc')->get();
    // $classList = Paymentamount::all();
    /*foreach ($classList as $key => $value) {
        # code...
    
        // echo $value->subjectsList;
        foreach ($value->subjectsList as $key => $val) {
            echo $val->subjecName->subject_name.' '.$val->subject_marks.' '.$val->subject_pass_marks;
            echo "<br>";
        }
        echo "<br><br>";
    }*/
    // dd($classList[3]->subjectsList);
    //     exit;
        // dd($classList[3]->classNames->subjectLists[1]->subjecName->subject_name);
    // dd($classList[4]->subjectsList[4]);
    return view('admission::admin.subject.index',compact('classList'));

}

public function qoutaReport(Request $request)
{
    $qoutaStatus = $request->qouta_status;
    if($qoutaStatus){
        $qoutaReports = StudentDetails::where('hasQouta',1)->where('paymentamounts_id',$request->class_name)->get();
    }else{
        $qoutaReports = StudentDetails::whereNull('hasQouta')->where('paymentamounts_id',$request->class_name)->get();
    }
    return view('admission::admin.result.qoutaReport', compact('qoutaReports','qoutaStatus'));
}

public function qoutaSelect()
{
    $classCollection = Paymentamount::get()->pluck('name', 'id');
    return view('admission::admin.result.qoutaSelect', compact('classCollection'));
}

public function subjectSettingsCreate()
{
    abort_unless(\Gate::allows('admission'), 403);
    $subjectList = SubjectList::all();
    $classData = Paymentamount::where('subject_set','=','0')->orderBy('class', 'asc')
    ->get();
    $classList['0'] = "Select Class";
    foreach ($classData as $key => $value) {
        $classList[$value['id']] = $value['name'];
    }
        // dd($subjectList[0]->subject_name);
    return view('admission::admin.subject.create', compact('classList', 'subjectList'));
}

public function subjectSettingsStore(Request $request)
{
    abort_unless(\Gate::allows('admission'), 403);
    $validatedData = $request->validate([
            // foreach ($request->checkbox as $key => $value) {
                 // 'total' => 'required',
                 // 'pass' => 'required',
            // }
    ]);
    $key = 0;
            // foreach ($request->subject_list_id as $x => $value) {
    for($x=0; $x<count($request->subject_list_id); $x++){
        if(!empty($request->subject_list_d[$x]) ){

            $data = ['class_id' => $request->class_id,
            'subject_list_id' => $request->subject_list_id[$x],
            'checkbox' => $request->checkbox[$key],
            'subject_marks' => $request->subject_marks[$key],
            'subject_pass_marks' => $request->subject_pass_marks[$key]
        ];

        $d = Subject::create($data);
        $key++;
    }
}
            // dd($data);
if($d){
    $class = Paymentamount::findOrFail($request->class_id);
    $class->update(['subject_set'=>'1']);
    return redirect()->route('admin.admission.subjectSettings.index')->with('success', 'Create Successfull');
}else {
    return redirect()->route('admin.admission.subjectSettings.index')->with('error', 'Not Created, Somtheing waswrong');
}
}

public function subjectSettingsEdit(Paymentamount $data)
{    
    abort_unless(\Gate::allows('admission'), 403);
    $subjectLists = SubjectList::all();
    $classData = Paymentamount::orderBy('class', 'asc')
    ->get();
    $classList['0'] = "Select Class";
    foreach ($classData as $key => $value) {
        $classList[$value['id']] = $value['name'];
    }

// dd($data->id);

    $result = DB::select("
        SELECT 
        `subject_lists`.`id` as `sub_id`, 
        `subject_lists`.`subject_name`, 
        x.* 
        FROM `subject_lists` 
        LEFT JOIN (
        SELECT * FROM `subjects` WHERE `subjects`.`class_id`= $data->id and `subjects`.`deleted_at` IS NULL
        ) as x 
        ON `subject_lists`.`id` = x.`subject_list_id` 
        WHERE `subject_lists`.`deleted_at` IS NULL
        ORDER by `subject_lists`.`id` ASC ");

        // dd($result);
    return view('admission::admin.subject.edit', compact('classList', 'subjectLists', 'result', 'data'));
}

public function subjectSettingsUpdate(Request $request, Paymentamount $data)
{
        // dd($request->all());
        // dd($data);
    abort_unless(\Gate::allows('admission'), 403);
    $validatedData = $request->validate([
            // foreach ($request->checkbox as $key => $value) {
                 // 'total' => 'required',
                 // 'pass' => 'required',
            // }
    ]);
    $key = 0;
    for($x=0; $x<count($request->subject_list_id); $x++){
        // echo 'x = '.$x.' ';
        if(!empty($request->checkbox_d[$x])){

            $cha = Subject::where([
                ['class_id','=', $request->class_id], ['subject_list_id', '=', $request->checkbox_d[$x]]
            ])->first();
            // dd($cha);
            //exit;
            $cha->delete();
            // $empty++;
        }
        if(!empty($request->subject_list_d[$x]) ){

            $data = ['class_id' => $request->class_id,
                'subject_list_id' => $request->subject_list_id[$x],
                'checkbox' => $request->checkbox[$key],
                'subject_marks' => $request->subject_marks[$key],
                'subject_pass_marks' => $request->subject_pass_marks[$key]
            ];
            $ch = Subject::where([
            ['class_id','=', $request->class_id], ['subject_list_id', '=', $request->subject_list_id[$x]]
            ])->first();
            if($ch){
            // if(empty($request->checkbox[$key])){
            //     $ch->delete();
            // }else{
                $d = $ch->update($data);
            // }
            } else {
             // dd($data);
                $d = Subject::Create($data);
            }
            $key++;
        }
    }

    if($d){
        $class = Paymentamount::findOrFail($request->class_id);
        $class->update(['subject_set'=>'1']);
                // $class->update(['subject_set'=>1]);
        return redirect()->route('admin.admission.subjectSettings.index')->with('success', 'Update Successfull');
    }else {
        return redirect()->route('admin.admission.subjectSettings.index')->with('error', 'Not Update, Somtheing waswrong');
    }
}

public function subjectSettingsSubject()
{
    abort_unless(\Gate::allows('admission'), 403);
    $subjectList = SubjectList::all();
    return view('admission::admin.subject.subject', compact('subjectList'));
}

public function subjectSettingsSubjectsCreate()
{
    abort_unless(\Gate::allows('admission'), 403);
    return view('admission::admin.subject.subjectCreate');
}

public function subjectSettingsSubjectsStore(Request $request)
{
        // dd($request->all());
    abort_unless(\Gate::allows('admission'), 403);
    $validatedData = $request->validate([
        'subject_name' => 'required',
    ]);
    $d = $request->all();
    $data = SubjectList::create($d);
    if($data){
        return redirect()->route('admin.admission.subjectSettings.subject')->with('success', 'Create Successfull');
    }else {
        return redirect()->route('admin.admission.subjectSettings.subject')->with('error', 'Not Created, Somtheing waswrong');
    }
}

public function subjectSettingsSubjectsEdit(SubjectList $subjectList)
{
    abort_unless(\Gate::allows('admission'), 403);
    return view('admission::admin.subject.subjectEdit', compact('subjectList'));
}

public function subjectSettingsSubjectsUpdate(Request $request, SubjectList $subjectList)
{
    abort_unless(\Gate::allows('admission'), 403);
    $validatedData = $request->validate([
        'subject_name' => 'required',
    ]);
    $d = $request->all();
        // $data = SubjectList::create($d);
    $data = $subjectList->update($d);
    if($data){
        return redirect()->route('admin.admission.subjectSettings.subject')->with('success', 'Update Successfull');
    }else {
        return redirect()->route('admin.admission.subjectSettings.subject')->with('error', 'Not Update, Somtheing waswrong');
    }
}

public function subjectSettingsSubjectsDelete(SubjectList $subjectList)
{
    abort_unless(\Gate::allows('admission'), 403);
    $subjectList_id = $subjectList->id;
    if($subjectList->delete()){
        $subjects = Subject::where('subject_list_id', '=', $subjectList_id)->get();
        if($subjects){
            foreach ($subjects as $key => $value) {
                $value->delete();
            }
        }
        return redirect()->route('admin.admission.subjectSettings.subject')->with('delete', 'Delete Successfull');
    }else {
        return redirect()->route('admin.admission.subjectSettings.subject')->with('error', 'Not Update, Somtheing waswrong');
    }
}


public function smsIndex()
{
    abort_unless(\Gate::allows('admission'), 403);
    $sms_count = $this->getAvailableSMSCount();
    // dd($sms_count['SMSINFO']['BALANCE']);
    $sms = Sms:: all();
    return view('admission::admin.sms.index', compact('sms_count', 'sms'));
}

public function smsCreate()
{
    abort_unless(\Gate::allows('admission'), 403);
    return view('admission::admin.sms.create');
}

public function smsStore(Request $request)
{
    abort_unless(\Gate::allows('admission'), 403);
    $sms = $request->all();
    $data = Sms::create($sms);
    if($data){
        return redirect()->route('admin.admission.sms.index')->with('success', 'Add Successfull');
    }else {
        return redirect()->route('admin.admission.sms.index')->with('error', 'Not Added, Somtheing was wrong');
    }
}

public function sendSMS(Sms $sms)
{
    abort_unless(\Gate::allows('admission'), 403);
    $classData = Paymentamount::where('make_result', 1)->orderBy('class', 'asc')->get();
    $classList[''] = "Select Class";
    foreach ($classData as $key => $value) {
        $classList[$value['id']] = $value['name'];
    }
    return view('admission::admin.sms.sendSMS', compact('classList', 'sms'));
}

public function sms_send (Request $request)
{
    abort_unless(\Gate::allows('admission'), 403);

    $clas_id = $request->class_id;
    $message = $request->message;
    $subject = $request->sms_subject;
    $results = AdmissionResult::where([['status', 'Pass'],['class_id', $clas_id]])->get();

    foreach ($results as $key => $result) {
        $primaryContact = $result->complete->student->PrimaryContact;
        $phone = "";
        $email = "";
        $admission_id = $result->complete->admission_id;
        $class = $result->complete->payFor->name;
        if($primaryContact==1)
        {
            $phone = $result->complete->student->father_phone;
            $email = $result->complete->student->father_email;
        }else if($primaryContact==2)
        {
            $phone = $result->complete->student->mother_phone;
            $email = $result->complete->student->mother_email;
        }else
        {
            $phone = $result->complete->student->localGurdian_phone;
            $email = $result->complete->student->localGurdian_email;
        }
        echo $class.' '.$result->position.' '.$admission_id.' '.$phone.' '.$email.' '.$message.'<br>';

        /*if ($phone) {
            $user = "SMMPLSC";
            $pass = "69G569v>";
            $sid = "SMMPLSCENG";
            $url="http://sms.sslwireless.com/pushapi/dynamic/server.php"; 
            $time = time();
            $param="user=$user&pass=$pass&sms[0][0]= $phone&sms[0][1]=".urlencode($message)."&sms[0][2]=$time&sid=$sid";
            $crl = curl_init(); curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE); 
            curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2); 
            curl_setopt($crl,CURLOPT_URL,$url); 
            curl_setopt($crl,CURLOPT_HEADER,0); 
            curl_setopt($crl,CURLOPT_RETURNTRANSFER,1); 
            curl_setopt($crl,CURLOPT_POST,1);
            curl_setopt($crl,CURLOPT_POSTFIELDS,$param);
            $response = curl_exec($crl); 
            curl_close($crl); 
            
         }*/
         // Sms Store
         $sms_data['subject'] = $subject;
         $sms_data['message'] = $message;
         $sms_data['number'] = $phone;
         $sms_data['catagory'] = 'Admission Selecton SMS';
         $sms_data['created_by'] = Auth::id();
         smsStore::create($sms_data);
         

    }
    // if($data){
        return redirect()->route('admin.admission.sms.index')->with('success', 'Send Successfull');
    // }else {
    //     return redirect()->route('admin.admission.sms.index')->with('error', 'Not Added, Somtheing was wrong');
    // }
}

  public function getAvailableSMSCount()
  {
    // $user = "rcs190";
    // $pass = "i@49D321";
    // $sid = "RajCollegiate190";

    $user = "SMMPLSC";
    $pass = "69G569v>";
    $sid = "SMMPLSCENG";
    // $url="http://sms.sslwireless.com/pushapi/dynamic/server.php";

    $request = "smsbalance";
    $url="http://sms.sslwireless.com/pushapi/dynamicplus/server.php";
    $param="user=$user&pass=$pass&sid=$sid&request=$request";

    $crl = curl_init();
    curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2);
    curl_setopt($crl,CURLOPT_URL,$url);
    curl_setopt($crl,CURLOPT_HEADER,0);
    curl_setopt($crl,CURLOPT_RETURNTRANSFER,1);
    curl_setopt($crl,CURLOPT_POST,1);
    curl_setopt($crl,CURLOPT_POSTFIELDS,$param);
    $response = curl_exec($crl);
    curl_close($crl);

    // Convert xml string into an object
    $new = simplexml_load_string($response);

    // Convert into json
    $con = json_encode($new);

    // Convert into associative array
    $newArr = json_decode($con, true);

    return $newArr;

  }


}
// }
// }
