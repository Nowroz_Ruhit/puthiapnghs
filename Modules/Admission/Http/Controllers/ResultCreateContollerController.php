<?php

namespace Modules\Admission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use PDF;

use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\AdmissionResult;
use Modules\Admission\Entities\AdmissionMarkInput;
use Modules\Admission\Entities\LotteryResult;
use Modules\Admission\Entities\PaymentComplete;
use Modules\SMS\Entities\Sms;
use Modules\SMS\Entities\smsStore;
use Modules\Admission\Entities\StudentDetails;
use Modules\General\Entities\General;
class ResultCreateContollerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classlist = Paymentamount::orderby('class', 'asc')->get();
        return view('admission::admin.result.index', compact('classlist'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admission::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request, Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        // dd($request->all());
        $marks = AdmissionMarkInput::where([['class_id', '=', $cla_id->id], ['total', '!=', '0']])
        ->orderBY('total', 'desc')
        ->orderBY('subject_3', 'desc')
        ->orderBY('subject_2', 'desc')
        ->orderBY('subject_1', 'desc')
        ->get();
        foreach ($marks as $key => $value) {
            $status = '';
            if($value['status']==0)
            {
                $status = 'Pass';
            }
            else if($value['status']==1)
            {
                $status = 'Fail';
            }
            $resultData = [
                'input_mark_id' => $value['id'],
                'class_id' => $value['class_id'],
                'admission_id' => $value['admission_id'],
                'total_mark' => $value['total'],
                'status' => $status,
                'position' => ($key+1)
            ];
            AdmissionResult::create($resultData);
        }
        
        /*$fail = AdmissionMarkInput::where([['class_id', '=', $cla_id->id],['status','=', 1]])->get();
        foreach ($fail->values() as $key => $value) {
            $failtData = [
                'input_mark_id' => $value['id'],
                'class_id' => $value['class_id'],
                'admission_id' => $value['admission_id'],
                'total_mark' => $value['total'],
                'position' => 'Fail',
            ];
            AdmissionResult::create($failtData);
        }*/
        $cla_id->update(['make_result'=>'1']);
        return redirect()->route('admin.admission.result.show', $cla_id);
    }

    public function store1(Request $request, Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        // dd($request->all());
        $marks = AdmissionMarkInput::where([['class_id', '=', $cla_id->id], ['total', '!=', '0'], ['part','=', 2]])
        ->orderBY('total', 'desc')
        ->orderBY('subject_3', 'desc')
        ->orderBY('subject_2', 'desc')
        ->orderBY('subject_1', 'desc')
        ->get();
        foreach ($marks as $key => $value) {
            $status = '';
            if($value['status']==0)
            {
                $status = 'Pass';
            }
            else if($value['status']==1)
            {
                $status = 'Fail';
            }
            $resultData = [
                'input_mark_id' => $value['id'],
                'class_id' => $value['class_id'],
                'admission_id' => $value['admission_id'],
                'total_mark' => $value['total'],
                'status' => $status,
                'position' => ($key+1),
                'part' => 2,
            ];
            AdmissionResult::create($resultData);
        }
        $cla_id->update(['make_result1'=>'1']);
        return redirect()->route('admin.admission.result.show1', $cla_id);
    }

    public function result(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        if($cla_id->make_result==0){
            $marks = AdmissionMarkInput::where('class_id', '=', $cla_id->id)->get();
            $mark_array = array();
            foreach ($marks as $key => $value) {
                $mark_array[] = [
                    "id" => $value->id,
                    "class_id" => $value->class_id,
                    "admission_id" => $value->admission_id,
                    'total_mark' => (
                        $value->subject_1 + $value->subject_2 + $value->subject_3 + $value->subject_4
                    )
                ];
            }
            $collection = collect($mark_array);
            $sorted = $collection->sortByDesc('total_mark');
            foreach ($sorted->values() as $key => $value) {
                $resultData = [
                    'input_mark_id' => $value['id'],
                    'class_id' => $value['class_id'],
                    'admission_id' => $value['admission_id'],
                    'total_mark' => $value['total_mark'],
                    'position' => ($key+1)
                ];
                AdmissionResult::create($resultData);
            }
            $cla_id->update(['make_result'=>'1']);
        }
    }

    public function result1(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        if($cla_id->make_result==0){
            $marks = AdmissionMarkInput::where([['class_id', '=', $cla_id->id],['part', '=', 2]])->get();
            $mark_array = array();
            foreach ($marks as $key => $value) {
                $mark_array[] = [
                    "id" => $value->id,
                    "class_id" => $value->class_id,
                    "admission_id" => $value->admission_id,
                    'total_mark' => (
                        $value->subject_1 + $value->subject_2 + $value->subject_3 + $value->subject_4
                    )
                ];
            }
            $collection = collect($mark_array);
            $sorted = $collection->sortByDesc('total_mark');
            foreach ($sorted->values() as $key => $value) {
                $resultData = [
                    'input_mark_id' => $value['id'],
                    'class_id' => $value['class_id'],
                    'admission_id' => $value['admission_id'],
                    'total_mark' => $value['total_mark'],
                    'position' => ($key+1),
                    'part' => 2,
                ];
                AdmissionResult::create($resultData);
            }
            $cla_id->update(['make_result1'=>'1']);
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $results = AdmissionResult::where([['class_id', '=', $cla_id->id], ['part', '=', 1]])->orderBy('admission_id', 'asc')->get();
        return view('admission::admin.result.show', compact('cla_id', 'results'));
        // return view('admission::show');
    }

    public function show1(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $results = AdmissionResult::where([['class_id', '=', $cla_id->id],['part', '=', 2]])->orderBy('admission_id', 'asc')->get();
        return view('admission::admin.result.show', compact('cla_id', 'results'));
        // return view('admission::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admission::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function publish()
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classlist = Paymentamount::orderby('class', 'asc')->get();
        return view('admission::admin.result.publish', compact('classlist'));
    }
    
    public function publishClassResult(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $cla_id->update(['publish'=>'1']); // will be open
        $result = AdmissionResult::where([['class_id', '=', $cla_id->id],['status','=', 'Pass']])->get();
        // dd($result[0]->complete);
        foreach ($result as $key => $data) {
            $email = "";
            $phone = "";
            $class = $cla_id->name;
            $s_name = $data->complete->student->name;
            $student_id = $data->complete->admission_id;
            $p = $data->position;
            $position = "";
            if($p==1)
            {
                $position = "1st";
            }elseif ($p==2) {
                $position = "2nd";
            }elseif ($p==3) {
                $position = "3rd";
            }else{
                $position = $p."th";
            }

            if($data->complete->student->PrimaryContact==3){
                $email = $data->complete->student->localGurdian_email;
                $phone = $data->complete->student->localGurdian_phone;
            }elseif($data->complete->student->PrimaryContact==2){
                $email = $data->complete->student->mother_email;
                $phone = $data->complete->student->mother_phone;
            }elseif ($data->complete->student->PrimaryContact==1) {
                $email = $data->complete->student->father_email;
                $phone = $data->complete->student->father_phone;
            }
            // echo $email." ".$phone." ".$class." ".$s_name." ".$student_id."<br>";
        
        
        if ($email) {

        $subject = "Admission Result - 2020";
        $message = view('admission::admission.email.confirm', compact('class', 's_name', 'student_id', 'position'));

        $headers = 'From: admission@smmplsc.edu.bd'. "\r\n" .
        'Reply-To: admission@smmplsc.edu.bb'. "\r\n" .
        'Content-Type: text/html; charset=ISO-8859-1\r\n'.
        'X-Mailer: PHP/' . phpversion();
        $mail = mail($email, $subject, $message, $headers);
        
        }
        if ($phone) {
            /*$user = "rcs190 ";
            $pass = "i@49D321";
            $sid = "RajCollegiate190"; */
            if(strlen($phone) == 11){
                $phone = '88'.$phone;
            }
            $user = "SMMPLSC";
            $pass = "69G569v>";
            $sid = "SMMPLSCENG";
            $url="http://sms.sslwireless.com/pushapi/dynamic/server.php"; 
            $time = time();
            $msg = "Congratulations'".$s_name."', You are passed id '".$student_id."' and position '".$position."'. visit  for details http://smmplsc.edu.bd/admission/student/result";

            $param="user=$user&pass=$pass&sms[0][0]= $phone&sms[0][1]=".urlencode($msg)."&sms[0][2]=$time&sid=$sid";
            $crl = curl_init(); curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE); 
            curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2); 
            curl_setopt($crl,CURLOPT_URL,$url); 
            curl_setopt($crl,CURLOPT_HEADER,0); 
            curl_setopt($crl,CURLOPT_RETURNTRANSFER,1); 
            curl_setopt($crl,CURLOPT_POST,1);
            curl_setopt($crl,CURLOPT_POSTFIELDS,$param);
            $response = curl_exec($crl); 
            curl_close($crl); 

            // Sms Store
         $sms_data['subject'] = '';
         $sms_data['message'] = $msg;
         $sms_data['number'] = $phone;
         $sms_data['catagory'] = 'After Admission SMS';
         $sms_data['created_by'] = Auth::id();
         $sms_data['status'] = $response;
         smsStore::create($sms_data);
            
         }
        }

        return back();
    }

    public function publishClassResult1(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $cla_id->update(['publish1'=>'1']); // will be open
        $result = AdmissionResult::where([['class_id', '=', $cla_id->id],['status','=', 'Pass'], ['part', '=', 2]])->get();
        // dd($result[0]->complete);
        foreach ($result as $key => $data) {
            $email = "";
            $phone = "";
            $class = $cla_id->name;
            $s_name = $data->complete->student->name;
            $student_id = $data->complete->admission_id;
            $p = $data->position;
            $position = "";
            if($p==1)
            {
                $position = "1st";
            }elseif ($p==2) {
                $position = "2nd";
            }elseif ($p==3) {
                $position = "3rd";
            }else{
                $position = $p."th";
            }

            if($data->complete->student->PrimaryContact==3){
                $email = $data->complete->student->localGurdian_email;
                $phone = $data->complete->student->localGurdian_phone;
            }elseif($data->complete->student->PrimaryContact==2){
                $email = $data->complete->student->mother_email;
                $phone = $data->complete->student->mother_phone;
            }elseif ($data->complete->student->PrimaryContact==1) {
                $email = $data->complete->student->father_email;
                $phone = $data->complete->student->father_phone;
            }
            // echo $email." ".$phone." ".$class." ".$s_name." ".$student_id."<br>";
        
        
        if ($email) {

        $subject = "Admission Result - 2020";
        $message = view('admission::admission.email.confirm', compact('class', 's_name', 'student_id', 'position'));

        $headers = 'From: admission@smmplsc.edu.bd'. "\r\n" .
        'Reply-To: admission@smmplsc.edu.bb'. "\r\n" .
        'Content-Type: text/html; charset=ISO-8859-1\r\n'.
        'X-Mailer: PHP/' . phpversion();
        $mail = mail($email, $subject, $message, $headers);
        
        }
        if ($phone) {
            if(strlen($phone) == 11){
                $phone = '88'.$phone;
            }
            $user = "SMMPLSC";
            $pass = "69G569v>";
            $sid = "SMMPLSCENG";
            $url="http://sms.sslwireless.com/pushapi/dynamic/server.php"; 
            $time = time();
            $msg = "Congratulations'".$s_name."', You are passed id '".$student_id."' and position '".$position."'. visit  for details http://smmplsc.edu.bd/admission/student/result";

            $param="user=$user&pass=$pass&sms[0][0]= $phone&sms[0][1]=".urlencode($msg)."&sms[0][2]=$time&sid=$sid";
            $crl = curl_init(); curl_setopt($crl,CURLOPT_SSL_VERIFYPEER,FALSE); 
            curl_setopt($crl,CURLOPT_SSL_VERIFYHOST,2); 
            curl_setopt($crl,CURLOPT_URL,$url); 
            curl_setopt($crl,CURLOPT_HEADER,0); 
            curl_setopt($crl,CURLOPT_RETURNTRANSFER,1); 
            curl_setopt($crl,CURLOPT_POST,1);
            curl_setopt($crl,CURLOPT_POSTFIELDS,$param);
            $response = curl_exec($crl); 
            curl_close($crl); 

            // Sms Store
         $sms_data['subject'] = $cla_id->name;
         $sms_data['message'] = $msg;
         $sms_data['number'] = $phone;
         $sms_data['catagory'] = 'After 2nd Time Admission SMS';
         $sms_data['created_by'] = Auth::id();
         $sms_data['status'] = $response;
         smsStore::create($sms_data);
            
         }
         

        }

        return back();
    }

    public function test()
    {
        abort_unless(\Gate::allows('admission'), 403);
        // "SELECT * FROM `admission_mark_inputs` WHERE `status`=0 && `total`!=0 ORDER BY `total` DESC, `subject_3` DESC, `subject_2` DESC,`subject_1` DESC";
        //->orderBY([['total', 'desc'],['subject_3', 'desc'], ['subject_2', 'desc'], ['subject_1', 'desc']])->get();
        $result = AdmissionMarkInput::where([['class_id', '=', $cla_id->id],['status', '=', '0'], ['total', '!=', '0']])
        ->orderBY('total', 'desc')
        ->orderBY('subject_3', 'desc')
        ->orderBY('subject_2', 'desc')
        ->orderBY('subject_1', 'desc')
        ->get();
        // orderBy('admission_id', 'asc')
        foreach ($result as $key => $value) {
            echo "id = ".$value->id." Math = ".$value->subject_3." English = ".$value->subject_2." Bangla = ".$value->subject_1.' Total = '.$value->total.'<br>';
        }
    }
    
    public function selectClass()
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classCollection = Paymentamount::get()->pluck('name', 'id');
        return view('admission::admin.lotteryResult.selectClass', compact('classCollection'));
    }

    public function lotteryResultIndex(Request $request)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $lotteryResults = LotteryResult::where('payment_amount_id',$request->class_name)->get();
        if($lotteryResults->isEmpty()){
            return redirect()->route('admin.admission.result.lottery.select')->with('warning', 'No Lottery Result Exists for the selected class. Please create one.');
        }else{
            return view('admission::admin.lotteryResult.index', compact('lotteryResults'));
        }
    }

    public function editlotteryResult(LotteryResult $lotteryResult)
    {
        // return $lotteryResult;
        return view('admission::admin.lotteryResult.edit', compact('lotteryResult'));
    }

    public function chooseClass()
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classCollection = Paymentamount::get()->pluck('name', 'id');
        return view('admission::admin.lotteryResult.chooseClass', compact('classCollection'));
    }

    public function createLotteryResult(Request $request)
    {
        abort_unless(\Gate::allows('admission'), 403);
        // dd($request);
        $checkDuplicate = LotteryResult::where('payment_amount_id', $request->class_name)->get();
        if($checkDuplicate->isEmpty()){
            $students = PaymentComplete::with(['student'])->where('paymentamounts_id', $request->class_name)->get();
            return view('admission::admin.lotteryResult.create', compact('students'));
        }else{
            return redirect()->route('admin.admission.result.lottery.choose')->with('error', 'Lottery Result has already been created for the selected class');
        }
    }

    public function storeLotteryResult(Request $request)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $status_collection = collect($request->status);
        $student_id_collection = collect($request->student_id);
        for ($i=0; $i<($status_collection->count()); $i++) { 
            $lotteryResult = new LotteryResult();
            $student = StudentDetails::findorFail($student_id_collection[$i]);
            $lotteryResult->student_name = $student->name;
            $lotteryResult->admission_lottery_id = $student->student_id->admission_id;
            $lotteryResult->student_details_id = $student_id_collection[$i];
            $lotteryResult->status = $status_collection[$i];
            if($student->PrimaryContact==2){
                $lotteryResult->contact = $student->mother_phone;
            }else {
                $lotteryResult->contact = $student->father_phone;
            }
            $lotteryResult->payment_amount_id = $student->student_id->paymentamounts_id;
            $lotteryResult->save();
        }
        return redirect()->route('admin.admission.result.lottery.choose')->with('success', 'Lottery Result created successfully');
    }

    public function updateLotteryResult(LotteryResult $lotteryResult, Request $request)
    {
        $lotteryResult->status = $request->status;
        $lotteryResult->save();
        return redirect()->route('admin.admission.result.lottery.select')->with('success', 'Lottery Result updated successfully');
    }

    public function createLotteryResultsms()
    {
        $classCollection = Paymentamount::get()->pluck('name', 'id');
        return view('admission::admin.lotterySMS.create', compact('classCollection'));
    }

    public function getLotteryResult($id)
    {
        $students = LotteryResult::with(['student'])->where('payment_amount_id', $id)->where('status',1)->get();
        return response()->json($students);
    }

    public function sendLotteryResultsms(Request $request)
    {
        $institute_name = General::firstorFail()->name;
        $institute_noticeboard_url = General::firstorFail()->url.'notice';
        $status_collection = collect($request->status);
        $lottery_id_collection = collect($request->lottery_id);
        for ($i=0; $i<($status_collection->count()) ; $i++) {
            if($status_collection[$i] == 1){
                $lotteryResult = LotteryResult::findorFail($lottery_id_collection[$i]);
                $text = "অভিনন্দন, আপনার সন্তান ".$lotteryResult->student_name.", ".$lotteryResult->paymentAmount->name." এ, ".$institute_name." এ ভর্তির জন্যে নির্বাচিত হয়েছে। বিস্তারিত তথ্যের জন্যে স্কুলের নোটিশবোর্ড ".$institute_noticeboard_url." ভিজিট করুন।";
                $phone = $lotteryResult->contact;
                // $phone = '01782618015';
                $this->sendsms($phone,$text); 
            } 
        }
        return redirect()->route('admin.admission.result.lottery.createSMS')->with('success', 'Lottery Result SMS has been sent successfully');
    }

    public function createLotteryResultPDF(Request $request)
    {
        $lotteryResults = LotteryResult::where('payment_amount_id',$request->id)->get();
        $className = Paymentamount::findorFail($request->id)->name;
        $view = view("admission::admin.lotteryResultPDF",compact('lotteryResults'))->render();
        $pdf = PDF::loadView("admission::admin.lotteryResultPDF",compact('lotteryResults'));
            return $pdf->download($className.' Lottery Result.pdf');
        return $request;
    }

    private function sendsms($number,$text)
    {       
      
        $DOMAIN = "https://smsplus.sslwireless.com";
        $SID = "PUTHIAPNGHS";
        $API_TOKEN = "PUTHIAPNGHS-ccebb495-ca55-4fb2-8d0d-bc18887e6402";

        $messageData = [
            [
                "msisdn" => $number,
                "text" => $text,
                "csms_id" => uniqid(),
            ]
        ];

        $params = [
            "api_token" => $API_TOKEN,
            "sid" => $SID,
            "sms" => $messageData,
        ];

        $params = json_encode($params);
        $url = trim($DOMAIN, '/') . "/api/v3/send-sms/dynamic";

        $ch = curl_init(); // Initialize cURL
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params),
            'accept:application/json'
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
