<?php

namespace Modules\Admission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

use PDF;
use Modules\Admission\Entities\Transaction;
use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\StudentDetails;
use Modules\Admission\Entities\PaymentComplete;
use Modules\Admission\Entities\TempStudent;
use DB;
use Modules\Admission\Entities\SubjectList;
use Modules\Admission\Entities\Subject;
use Modules\Admission\Entities\AdmissionResult;
use Modules\Admission\Entities\AdmissionMarkInput;
class ExamCreateContollerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classlist = Paymentamount::orderby('class', 'asc')->get();
        //  dd($classlist[4]->name);
        // dd(count($classlist[4]->studentCount));
        return view('admission::admin.xm.index', compact('classlist'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classData = Paymentamount::where('subject_set','=','0')->orderBy('class', 'asc')->get();
        $subjectList = SubjectList::all();
        $classList['0'] = "Select Class";
        foreach ($classData as $key => $value) {
            $classList[$value['id']] = $value['name'];
        }
        return view('admission::admin.xm.create', compact('classList', 'subjectList'));
    }


    public function make($id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classData = Paymentamount::findOrFail($id);
        $subjectList = SubjectList::all();
        // dd($classData->subjectsList);
        // dd($classData->subjectsList[0]->subjecName);
        return view('admission::admin.xm.create', compact('classData', 'subjectList'));
    }

    public function make1($id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $classData = Paymentamount::findOrFail($id);
        $subjectList = SubjectList::all();
        // dd($classData->subjectsList);
        // dd($classData->subjectsList[0]->subjecName);
        return view('admission::admin.xm.create1', compact('classData', 'subjectList'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    // public function store()
    {
        abort_unless(\Gate::allows('admission'), 403);
        // $validatedData = $request->validate([
        //     'Bangla' => 'required',
        //     'Bangla.*'=>'max:5',
        // ]);
        // dd($request->all());
        $calss_id = $request->class_id[0];
        $bangla = $request->Bangla;
        $english = $request->English;
        $math = $request->Mathematics;
        $viva = $request->Viva;

        if(empty($request->Bangla))
        {
            $bangla = $request->emp;
        }
        if(empty($request->English))
        {
            $english = $request->emp;
        }
        if(empty($request->Mathematics))
        {
            $math = $request->emp;
        }
        if(empty($request->Viva))
        {
            $viva = $request->emp;
        }

        $classData = Paymentamount::findOrFail($calss_id);
        foreach ($request->admission_id as $key => $value) {
            $status = 0;
        if(0<=$classData->class && $classData->class<=2)
        {
            if($viva[$key]<$classData->subjectsList[0]->subject_pass_marks)
            {
                $status = 1;
            }
            $data = ['admission_id' => $request->admission_id[$key], 
            'class_id' => $request->class_id[$key],
            'subject_4' => $viva[$key],
            'total' => $viva[$key],
            'status' => $status,
            'part' => 1,
            ];

        }
        elseif (3<=$classData->class && $classData->class<=9) {
            if($bangla[$key]<$classData->subjectsList[0]->subject_pass_marks)
            {
                $status = 1;
            }
            if($english[$key]<$classData->subjectsList[1]->subject_pass_marks)
            {
                $status = 1;
            }
            if($math[$key]<$classData->subjectsList[2]->subject_pass_marks)
            {
                $status = 1;
            }
            $data = ['admission_id' => $request->admission_id[$key], 
            'class_id' => $request->class_id[$key],

            'subject_1' => $bangla[$key],
            'subject_2' => $english[$key],
            'subject_3' => $math[$key],
            'total' => ($bangla[$key]+$english[$key]+$math[$key]+$viva[$key]),
            'status' => $status,
            'part' => 1,
            ];
        }



            //print_r($data);
            $d = AdmissionMarkInput::create($data);
        }

        if($d){
            $class = Paymentamount::findOrFail($request->class_id[0]);
            $class->update(['input_mark'=>'1']);
            return redirect()->route('admin.admission.xm.index')->with('success', 'Insert Successfull');
        }else {
            return redirect()->route('admin.admission.xm.index')->with('error', 'Not Inserted, Somtheing waswrong');
        }
    }

    public function store1(Request $request)
    // public function store()
    {
        abort_unless(\Gate::allows('admission'), 403);

        $calss_id = $request->class_id[0];
        $bangla = $request->Bangla;
        $english = $request->English;
        $math = $request->Mathematics;
        $viva = $request->Viva;

        if(empty($request->Bangla))
        {
            $bangla = $request->emp;
        }
        if(empty($request->English))
        {
            $english = $request->emp;
        }
        if(empty($request->Mathematics))
        {
            $math = $request->emp;
        }
        if(empty($request->Viva))
        {
            $viva = $request->emp;
        }

        $classData = Paymentamount::findOrFail($calss_id);
        foreach ($request->admission_id as $key => $value) {
            $status = 0;
        if(0<=$classData->class && $classData->class<=2)
        {
            if($viva[$key]<$classData->subjectsList[0]->subject_pass_marks)
            {
                $status = 1;
            }
            $data = ['admission_id' => $request->admission_id[$key], 
            'class_id' => $request->class_id[$key],
            'subject_4' => $viva[$key],
            'total' => $viva[$key],
            'status' => $status,
            'part' => 2,
            ];

        }
        elseif (3<=$classData->class && $classData->class<=9) {
            if($bangla[$key]<$classData->subjectsList[0]->subject_pass_marks)
            {
                $status = 1;
            }
            if($english[$key]<$classData->subjectsList[1]->subject_pass_marks)
            {
                $status = 1;
            }
            if($math[$key]<$classData->subjectsList[2]->subject_pass_marks)
            {
                $status = 1;
            }
            $data = ['admission_id' => $request->admission_id[$key], 
            'class_id' => $request->class_id[$key],

            'subject_1' => $bangla[$key],
            'subject_2' => $english[$key],
            'subject_3' => $math[$key],
            'total' => ($bangla[$key]+$english[$key]+$math[$key]+$viva[$key]),
            'status' => $status,
            'part' => 2,
            ];
        }



            //print_r($data);
           $d = AdmissionMarkInput::create($data);
        }

        if($d){
            $class = Paymentamount::findOrFail($request->class_id[0]);
            $class->update(['input_mark1'=>'1']);
            return redirect()->route('admin.admission.xm.index')->with('success', 'Insert Successfull');
        }else {
            return redirect()->route('admin.admission.xm.index')->with('error', 'Not Inserted, Somtheing waswrong');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     **/
    public function show(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $marks = AdmissionMarkInput::where([['class_id', '=', $cla_id->id],['part','=', 1]])->get();
        // DD($marks[0]->complete->admission_id);
        return view('admission::admin.xm.show', compact('marks', 'cla_id'));
    }
    public function show1(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $marks = AdmissionMarkInput::where([['class_id', '=', $cla_id->id],['part','=', 2]])->get();
        // dd($marks);
        // DD($marks[0]->complete->admission_id);
        return view('admission::admin.xm.show1', compact('marks', 'cla_id'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(AdmissionMarkInput $mark)
    {
        abort_unless(\Gate::allows('admission'), 403);
        // dd($mark);
        return view('admission::admin.xm.edit', compact('mark'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, AdmissionMarkInput $mark)
    {
        abort_unless(\Gate::allows('admission'), 403);
        // dd($request->all());
        if($mark->update($request->all())){
            return redirect()->route('admin.admission.xm.show', $mark->class_id)->with('success', 'Update Successfull');
        }else {
            return redirect()->route('admin.admission.xm.show', $mark->class_id)->with('error', 'Not Updated, Somtheing waswrong');
        }

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        abort_unless(\Gate::allows('admission'), 403);
    }


}
