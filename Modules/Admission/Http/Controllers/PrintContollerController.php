<?php

namespace Modules\Admission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use PDF;
use Modules\Admission\Entities\StudentDetails;
use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\AdmissionResult;
use Modules\Admission\Entities\PaymentComplete;
class PrintContollerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('admission'), 403);
        // return view('admission::index');
        $classlist = Paymentamount::orderby('class', 'asc')->get();
        return view('admission::admin.print.index', compact('classlist'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admission::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admission::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admission::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function studentDetails(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $results = AdmissionResult::where([['class_id', '=', $cla_id->id],['position', '!=', 'Fail'],['part','=', 1]])->orderBy('admission_id', 'asc')->get();
        $pdf = PDF::loadView('admission::admin.print.studentDetails', compact('results'));
        return $pdf->download($cla_id->name.' Student Information.pdf');
    }

    public function studentDetails1(Paymentamount $cla_id)
    {
        abort_unless(\Gate::allows('admission'), 403);
        $results = AdmissionResult::where([['class_id', '=', $cla_id->id],['position', '!=', 'Fail'],['part','=', 2]])->orderBy('admission_id', 'asc')->get();
        
        $pdf = PDF::loadView('admission::admin.print.studentDetails', compact('results'));
        return $pdf->download($cla_id->name.' Student Information.pdf');
    }

    public function admit($id)
    {
        //abort_unless(\Gate::allows('admission'), 403);
        $admission_id = PaymentComplete::where('admission_id','=', $id)->firstOrFail();
       // dd($admission_id);
        $data = StudentDetails::findOrFail($admission_id->student_details_id);
        $pdf = PDF::loadView('admission::admin.print.admit', compact('data'));  
        return $pdf->download('Admit Card - '.$data->name.'.pdf');

        // return view('admission::admin.print.admit', compact('data')); 
    }

    public function email()
    {
        //abort_unless(\Gate::allows('admission'), 403);
        $class = 02;
        $s_name = "Md. abc Abc Abc";
        $student_id = 20190222;
        $position = '1st';
        return view('admission::admission.email.confirm', compact('class', 's_name', 'student_id', 'position'));
    } 
}
