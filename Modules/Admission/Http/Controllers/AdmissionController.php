<?php

namespace Modules\Admission\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use PDF;

use Modules\Admission\Entities\Transaction;
use Modules\Admission\Entities\Paymentamount;
use Modules\Admission\Entities\StudentDetails;
use Modules\Admission\Entities\PaymentComplete;
use Modules\Admission\Entities\District;
use Modules\Admission\Entities\Upazila;
use Modules\Admission\Entities\TempStudent;
use Modules\Admission\Entities\AdmissionStart;
class AdmissionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('admission::admission.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    /*
    @ && (date("Y/m/d")<=date('Y/m/d', strtotime($bootAdmissionStart->end))))
    */
    public function create()
    {
        date_default_timezone_set("Asia/Dhaka");
        $admissionStart = AdmissionStart::latest()->first();
        if(date("Y/m/d") <= date('Y/m/d', strtotime($admissionStart->end))){
            if(date('Y/m/d', strtotime($admissionStart->start)) <= date("Y/m/d")  ){
        $districtData = District::orderBy('name', 'asc')->distinct('district')->get();
        $district = array();
        $district[NULL] = "Select District";
        foreach ($districtData as $key => $value) {
            $district[$value['id']] = $value['name'];
        }


        $thanaData = Upazila::distinct('name')->get();
        $thana = array();
        $thana[NULL] = "Select Upazila";
        foreach ($thanaData as $key => $value) {
            $thana[$value['id']] = $value['name'];
        }

        // 
        // $thana = array();
        // $thana['0'] = "Select Thana";
        
        // $postcode=array();
        // $postcode[0] = "Select Post Code";
        // $postcodeData = District::select('postcode')->distinct('postcode')->get();
        // foreach ($postcodeData as $key => $value) {
        //     $postcode[$value['postcode']] = $value['thana'];
        // }

        // dd($district);
        return view('admission::admission.create', compact('district', 'thana'));
        } else {
            return '<div class="alert alert-warning p-4" role="alert">
  Admission is not open yet <br>
</div>';
        }
    } else {
        return "Admission Time is Over";
    }
    }

    public function apply()
    {
        return view('admission::admission.apply');
    }

    public function image($id)
    {
        $data = TempStudent::findOrFail($id);
        return view('admission::admission.image', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request);
        if($request->same_as_present ==1){
            /*$validatedData = $request->validate([
                'permananent_address' => 'required',
                'permananent_post_code' => 'required',
                'permananent_district' => 'required',
                'permananent_thana' => 'required',
            ]);*/
            $validatedData = $request->validate([
            'name' => 'required',
            'birth' => 'required',
            'birth_certificate' => 'required|unique:student_details,birth_certificate',
            'gender' => 'required',
            'religion' => 'required',
            // 'nationality' => 'required',
            'father_name' => 'required',
            'father_nid' => 'required',
            'father_phone' => 'required',
            'father_profession' => 'required',
            'mother_name' => 'required',
            'mother_nid' => 'required',
            'mother_profession' => 'required',
            'present_address' => 'required',
            'present_post_code' => 'required',
            'present_district' => 'required',
            'present_thana' => 'required',
            'PrimaryContact' => 'required',
            'image' => 'required',
            'image'  => 'dimensions:max_width=300,max_height=350',
            
        ]);
        }else {
            $validatedData = $request->validate([
            'name' => 'required',
            'birth' => 'required',
            'birth_certificate' => 'required|unique:student_details,birth_certificate',
            'gender' => 'required',
            'religion' => 'required',
            // 'nationality' => 'required',
            'father_name' => 'required',
            'father_nid' => 'required',
            'father_phone' => 'required',
            'father_profession' => 'required',
            'mother_name' => 'required',
            'mother_nid' => 'required',
            'mother_profession' => 'required',
            'present_address' => 'required',
            'present_post_code' => 'required',
            'present_district' => 'required',
            'present_thana' => 'required',

            'permananent_address' => 'required',
            'permananent_post_code' => 'required',
            'permananent_district' => 'required',
            'permananent_thana' => 'required',

            'PrimaryContact' => 'required',

            'image' => 'required',
            'image'  => 'dimensions:max_width=300,max_height=350',
        ]);
        }

        if($request->PrimaryContact ==3){
            $validatedData = $request->validate([
                'localGurdian_name' => 'required',
                'localGurdian_relation' => 'required',
                'localGurdian_phone' => 'required',
                'localGurdian_email' => 'required',
                'localGurdian_address' => 'required',

                'localGurdian_postCode' => 'required',
                'localGurdian_district' => 'required',
                'localGurdian_thana' => 'required',
            ]);
        }
              
        

        $data = $request->all();
        // dd($data);
        if ($request->hasFile('image')){
            // if((file_exists(public_path(trans('global.links.admission').$student->image)))&&($student->image != NULL) && (!empty($student->image)))
            // {
            //     unlink(public_path(trans('global.links.admission')).$student->image);
            // } 
            $imageName = now()->format('Y-m-d-H-i-s').'.'.request()->file('image')->getClientOriginalExtension();
            request()->image->move(public_path(trans('global.links.admission')), $imageName);
            $data['image'] = $imageName;

        }
        
        $student = TempStudent::create([
            'educationType' => 'General',
            'name' => $request->name,
            'birth' => $request->birth,
            'birth_certificate' => $request->birth_certificate,
            'gender' => $request->gender,
            'blood' => $request->blood,
            'religion' => $request->religion,
            'previousInstretute' => $request->previousInstretute,
            'previousRoll' => $request->previousRoll,
            'previousRoll' => $request->previousRoll,
            'hasQouta' => $request->hasQouta,
            'qoutaName' => $request->qoutaName,
            'father_name' => $request->father_name,
            'father_phone' => $request->father_phone,
            'father_email' => $request->father_email,
            'father_profession' => $request->father_profession,
            'father_nid' => $request->father_nid,
            'mother_name' => $request->mother_name,
            'mother_phone' => $request->mother_phone,
            'mother_email' => $request->mother_email,
            'mother_profession' => $request->mother_profession,
            'mother_nid' => $request->mother_nid,
            'present_address' => $request->present_address,
            'present_post_code' => $request->present_post_code,
            'present_district' => $request->present_district,
            'present_thana' => $request->present_thana,
            'same_as_present' => $request->same_as_present,
            'permananent_address' => $request->permananent_address,
            'permananent_post_code' => $request->permananent_post_code,
            'permananent_district' => $request->permananent_district,
            'permananent_thana' => $request->permananent_thana,
            'PrimaryContact' => $request->PrimaryContact,
            'localGurdian_name' => $request->localGurdian_name,
            'localGurdian_relation' => $request->localGurdian_relation,
            'localGurdian_phone' => $request->localGurdian_phone,
            'localGurdian_email' => $request->localGurdian_email,
            'localGurdian_address' => $request->localGurdian_address,
            'localGurdian_postCode' => $request->localGurdian_postCode,
            'localGurdian_district' => $request->localGurdian_district,
            'localGurdian_thana' => $request->localGurdian_thana,
            'image' => $imageName,
        ]);

        // $id = $student->id;
        //$data['id'] = $student->id;
       // return view('admission::admission.image', compact('data'));
        $data = $student;
        return redirect()->route('frontend.admission.selection', $data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admission::admission.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admission::admission.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, TempStudent $student)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'birth' => 'required',
            'birth_certificate' => 'required',
            'gender' => 'required',
            'religion' => 'required',
            // 'nationality' => 'required',
            'father_name' => 'required',
            'father_nid' => 'required',
            'father_phone' => 'required',
            'father_profession' => 'required',
            'mother_name' => 'required',
            'mother_nid' => 'required',
            'mother_profession' => 'required',
            'present_address' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            "image"  => "dimensions:max_width=300,max_height=350",
        ]);

        $data = $request->all();
        if ($request->hasFile('image')){
            if((file_exists(public_path(trans('global.links.admission').$student->image)))&&($student->image != NULL) && (!empty($student->image)))
            {
                // unlink(public_path(trans('global.links.admission').$student->image));
                unlink(public_path(trans('global.links.admission')).$student->image);
            } 
            $imageName = now()->format('Y-m-d-H-i-s').'.'.request()->file('image')->getClientOriginalExtension();
            request()->image->move(public_path(trans('global.links.admission')), $imageName);
            $data['image'] = $imageName;
            $student->update($data);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function imageUploge(Request $request, TempStudent $student)
    {
        $validatedData = $request->validate([
            'image' => 'required',
            "image"  => "dimensions:max_width=300,max_height=350",
        ]);

        $data = $request->all();
        if ($request->hasFile('image')){
            if((file_exists(public_path(trans('global.links.admission').$student->image)))&&($student->image != NULL) && (!empty($student->image)))
            {
                unlink(public_path(trans('global.links.admission')).$student->image);
            } 
            $imageName = now()->format('Y-m-d-H-i-s').'.'.request()->file('image')->getClientOriginalExtension();
            request()->image->move(public_path(trans('global.links.admission')), $imageName);
            $data['image'] = $imageName;

        }
        if($student->update($data)){
            $data = $student;
            return redirect()->route('admission.selection', $data);
        }

    }

    public function selection(TempStudent $student) 
    {
        $amounts = Paymentamount::all();
        return view('admission::admission.classSelection', compact('student', 'amounts'));
    }

    public function login ()
    {
        return view('admission::admission.login');
    }

    public function admissionResult ()
    {
        $classData = Paymentamount::where('publish',1)->get();
        
        $class = array();
        $class[NULL] = "Select Class";
        foreach ($classData as $key => $value) {
            $class[$value['id']] = $value['name'];
        }
        return view('admission::admission.result', compact('class'));
    }

    public function admitdata(Request $request)
    {
        $validatedData = $request->validate([
            'birth' => 'required',
            'birth_certificate' => 'required',
        ]);
        $birth = $request->birth;
        $birth_certificate = $request->birth_certificate;
        $data = StudentDetails::where('birth_certificate',  $birth_certificate)->first();
        if($data)
        {
            // return redirect()->route('admission.student.loginpage', $data);
            return view('admission::admission.loginpage', compact('data'));
        }
        else 
        {
            return redirect()->back()->with('watning', 'Not match, Please Try Again.');
        }
    }

    public function datachack (Request $request)
    {
        // $validatedData = $request->validate([
        //     'birth' => 'required',
        //     'birth_certificate' => 'required',
        // ]);
        $notPublish = 0;
        if(!empty($request->admission_id) && !empty($request->class_id)){
            $admission_id = $request->admission_id;
            $class_id = $request->class_id;
            $data = PaymentComplete::where([['admission_id', $admission_id], ['paymentamounts_id', $class_id]])->first();

            if(!empty($data) && $data->payFor->publish == 1)
            {
                $student = $data->student;
                if($student)
                {
                    return redirect()->route('admission.student.home', $student);
                }
                else 
                {
                    return redirect()->back()->with('watning', 'Not match, Please Try Again.');
                }
            }
            else {
                return redirect()->back()->with('watning', 'Result Not Publish');
            }
        }
        if(!empty($request->birth_certificate) && !empty($request->class_id)){
            $birth = $request->birth;
            $birth_certificate = $request->birth_certificate;
            $student = StudentDetails::where('birth_certificate',  $birth_certificate)->first();
            if(!empty($student) && $student->className->publish == 1){
                if($student)
                {
                    return redirect()->route('admission.student.home', $student);
                }
                else 
                {
                    return redirect()->back()->with('watning', 'Not match, Please Try Again.');
                }
            }
            else {
                return redirect()->back()->with('watning', 'Result Not Publish');
            }


        }

        // if($student)
        // {
        //     return redirect()->route('admission.student.home', $student);
        // }
        // else 
        // {
        //     return redirect()->back()->with('watning', 'Not match, Please Try Again.');
        // }
    }

    public function studentDownload($id)
    {
            // echo "Download ".$id;
            // return view('admission::admin.detailsPrint');

        // $admission_id = $request->value;
        $complete = PaymentComplete::where('admission_id','=', $id)->firstOrFail();
        $view = view("admission::admin.detailsPrint",compact('complete'))->render();
        // return response()->json(['html'=>$view]);
        $pdf = PDF::loadView("admission::admin.detailsPrint",compact('complete'));
            return $pdf->download($complete->student->name.' Student Information.pdf');
    }

    public function home (StudentDetails $student)
    {
        // pass parameter of student
        $data = StudentDetails::findOrFail($student->id);
        // dd($data->student_id->subjectMark);
        // dd($data->student_id);
        return view('admission::admission.home', compact('data'));
    }

    public function confirm() {
        return 0;
    }

    public function modalBlade(Request $request)
    {

// I will insert class id into student details, whenever, i pass another blade, I will retrive the info be student info table.
        $class_id = $request->position;
        $student_id = $request->position2;
        // $registration = Studentregistration::where('student_details_id', $student_id)->firstOrFail();
        // $data['paymentamounts_id'] = $class_id;
        // $registration->update($data);
        $student = TempStudent::findOrFail($student_id);
        $data['paymentamounts_id'] = $class_id;
        $student->update($data);
        $amounts = Paymentamount::findOrFail($class_id);
        $fee = $amounts->amount;
        // $charge = ceil(($fee*2.5)/100);
        // $total = ceil($fee + $charge);
         $modal = "Application For ".$amounts->name."<br>Application Fee ".$fee." TK. Here Admission Fee is 110 TK and Service Charge is 5 TK";
        // $modal = "Application Fee ".$fee." TK + Charge ".$charge." TK<br> Total ".$total." TK";
        return $modal;
    }

    public function district(Request $request)
    // public function district()
    {
        $data = $request->get('value');
        // $data = "Dhaka";
        $data2 = Upazila::where('district_id', '=', $data)->distinct('name')->orderBy('name', 'asc')->get();
        $result= "<option value='NULL'>Select Thana</option>";
        foreach ($data2 as $key => $value) {
            // $type[$value['id']] = $value['type'];
            $result.="<option value=".$value['id'].">".$value['name']."</option>";
        }
        // dd($result);
        return $result;
    }

   /* public function thana(Request $request)
    {
        $data = $request->get('value');
        // $data = "Dhaka";
        $data2 = District::select('postcode', 'postoffice')->where('thana', '=', $data)->orderBy('postcode', 'asc')->get();
        $result= "<option value='NULL'>Select Post Code</option>";
        foreach ($data2 as $key => $value) {
            // $type[$value['id']] = $value['type'];
            $result.="<option value=".$value['postcode'].">".$value['postoffice']." - ".$value['postcode']."</option>";
        }
        // dd($result);
        return $result;
    }
*/

    public function downloadCard ($id)
    {
        $data = StudentDetails::findOrFail($id);
        $pdf = PDF::loadView('admission::admission.pdf', compact('data'));  
        return $pdf->download('Admit Card - '.$data->name.'.pdf');

        // return view('admission::admission.admid', compact('data'));
    }
}
