<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Admission Frontend Start */
// Route::resource('admission', 'AdmissionController');


// Route::post('/foo', function () {
//     echo 1;
//     exit('hh');
// })->name('foo');
// Route::post('/foo', 'PaymentController@paymentSuccess')->name('foo');



Route::prefix('admission')->group(function() {
    Route::get('/',[
        'as'=>'frontend.admission.index',
        'uses'=>'AdmissionController@index'
    ]);
    Route::get('apply',[
        'as'=>'frontend.admission.apply',
        'uses'=>'AdmissionController@create'
    ]);
    // Route::get('create',[
    //     'as'=>'frontend.admission.create',
    //     'uses'=>'AdmissionController@create'
    // ]);
    // Route::get('apply',[
    //     'as'=>'frontend.admission.apply',
    //     'uses'=>'AdmissionController@apply'
    // ]);
    Route::post('store',[
        'as'=>'frontend.admission.store',
        'uses'=>'AdmissionController@store'
    ]);
    Route::get('image/{id}',[
        'as'=>'frontend.admission.image',
        'uses'=>'AdmissionController@image'
    ]);
    Route::post('update/{student}',[
        'as'=>'frontend.admission.update',
        'uses'=>'AdmissionController@update'
    ]);
    Route::post('image/{student}/upload',[
        'as'=>'frontend.admission.imageUpload',
        'uses'=>'AdmissionController@imageUploge'
    ]);
    Route::get('selection/{student}',[
        'as'=>'frontend.admission.selection',
        'uses'=>'AdmissionController@selection'
    ]);
    Route::get('confirm/{student}',[
        'as'=>'frontend.admission.confirm',
        'uses'=>'AdmissionController@confirm'
    ]);
    Route::post('confirm/modal',[
        'as'=>'frontend.admission.modal',
        'uses'=>'AdmissionController@modalBlade'
    ]);

    Route::get('payment/{student}',[
        'as'=>'payment.create',
        'uses'=>'PaymentController@paymentByStudent'
    ]);
    Route::post('district',[
        'as'=>'frontend.district',
        'uses'=>'AdmissionController@district'
    ]);
    Route::post('thana',[
        'as'=>'frontend.thana',
        'uses'=>'AdmissionController@thana'
    ]);
    Route::prefix('payment')->group(function() {        
        Route::post('success/',[
            'as'=>'payment.success',
            'uses'=>'PaymentController@paymentSuccess'
        ]);
        Route::post('fail',[
            'as'=>'payment.fail',
            'uses'=>'PaymentController@paymentFail'
        ]);
        Route::post('cancel',[
            'as'=>'payment.cancel',
            'uses'=>'PaymentController@paymentCancel'
        ]);
        Route::post('ipn/',[
            'as'=>'payment.ipn',
            'uses'=>'PaymentController@paymentIPN'
        ]);
        Route::get('email/payment-status',[
            'as'=>'payment.emailStatus',
            'uses'=>'PaymentController@emailStatus'
        ]);
    });

    Route::get('student/home/{student}',[
            'as'=>'admission.student.home',
            'uses'=>'AdmissionController@home'
        ]);
    Route::prefix('student')->group(function() {
        Route::get('login',[
            'as'=>'admission.student.login',
            'uses'=>'AdmissionController@login'
        ]);
        Route::get('result',[
            'as'=>'admission.student.result',
            'uses'=>'AdmissionController@admissionResult'
        ]);
        Route::post('datachack',[
            'as'=>'admission.student.datachack',
            'uses'=>'AdmissionController@datachack'
        ]);
        Route::post('student-admit',[
            'as'=>'admission.student.loginpage',
            'uses'=>'AdmissionController@admitdata'
        ]);
        Route::get('download/{id}',[
            'as'=>'admission.student.download',
            'uses'=>'AdmissionController@downloadCard'
        ]);

        Route::get('stadunt/download/{id}',[
            'as'=>'admission.studentDownload',
            'uses'=>'AdmissionController@studentDownload'
        ]);
    });  

});

Route::prefix('admin')->group(function() {

    // Route::resource('admissions', 'AdmissionAdminController');
    Route::get('admission',[
        'as'=>'admin.admission.index',
        'uses'=>'AdmissionAdminController@index'
    ]);

    Route::prefix('admission')->group(function() {

        Route::get('/sms',[
            'as'=>'admin.admission.sms.index',
            'uses'=>'AdmissionAdminController@smsIndex'
        ]);
        Route::get('/select-qouta',[
            'as'=>'admin.admission.qouta.select',
            'uses'=>'AdmissionAdminController@qoutaSelect'
        ]);

        Route::post('/qouta',[
            'as'=>'admin.admission.qouta.index',
            'uses'=>'AdmissionAdminController@qoutaReport'
        ]);
        Route::get('/sms/create',[
            'as'=>'admin.admission.sms.create',
            'uses'=>'AdmissionAdminController@smsCreate'
        ]);
        Route::post('/sms/store',[
            'as'=>'admin.admission.sms.store',
            'uses'=>'AdmissionAdminController@smsStore'
        ]);
        Route::get('/sms/{sms}/send',[
            'as'=>'admin.admission.sms.sendSMS',
            'uses'=>'AdmissionAdminController@sendSMS'
        ]);
        Route::post('/sms/sms-send',[
            'as'=>'admin.admission.sms.sms_send',
            'uses'=>'AdmissionAdminController@sms_send'
        ]);


        Route::get('payment-complete',[
            'as'=>'admin.admission.paymentComplete',
            'uses'=>'AdmissionAdminController@paymentComplete'
        ]);
        Route::get('payment/filter',[
            'as'=>'admin.admission.paymentFilter',
            'uses'=>'AdmissionAdminController@datafilter'
        ]);
        Route::get('payment-pending',[
            'as'=>'admin.admission.paymentPending',
            'uses'=>'AdmissionAdminController@paymentPending'
        ]);
        Route::get('status',[
            'as'=>'admin.admission.paymentStatus',
            'uses'=>'AdmissionAdminController@paymentStatus'
        ]);
        Route::get('transactions',[
            'as'=>'admin.admission.transactions',
            'uses'=>'AdmissionAdminController@transactions'
        ]);
        Route::get('class-amount',[
            'as'=>'admin.admission.classAmount',
            'uses'=>'AdmissionAdminController@classAmount'
        ]);
        Route::get('class-amount/create',[
            'as'=>'admin.admission.classAmount.create',
            'uses'=>'AdmissionAdminController@classAmountCreate'
        ]);
        Route::post('class-amount/store',[
            'as'=>'admin.admission.classAmount.store',
            'uses'=>'AdmissionAdminController@classAmountStore'
        ]);
        Route::get('class-amount/edit/{result}',[
            'as'=>'admin.admission.classAmount.edit',
            'uses'=>'AdmissionAdminController@classAmountEdit'
        ]);
        Route::post('class-amount/update/{result}',[
            'as'=>'admin.admission.classAmount.update',
            'uses'=>'AdmissionAdminController@classAmountUpdate'
        ]);
        Route::delete('class-amount/delete/{result}',[
            'as'=>'admin.admission.classAmount.delete',
            'uses'=>'AdmissionAdminController@classAmountDelete'
        ]);

        Route::get('data',[
            'as'=>'admin.admission.joinData',
            'uses'=>'AdmissionAdminController@joinFunction'
        ]);

        Route::get('stadunt/view/{id}',[
            'as'=>'admin.admission.studentView',
            'uses'=>'AdmissionAdminController@studentView'
        ]);
        Route::get('stadunt/download/{id}',[
            'as'=>'admin.admission.studentDownload',
            'uses'=>'AdmissionAdminController@studentDownload'
        ]);
        Route::get('stadunt/download-admit/{id}',[
            'as'=>'admin.admission.studentAdmitDownload',
            'uses'=>'AdmissionAdminController@downloadCard'
        ]);

        Route::get('stadunt/download-details/{id}',[
            'as'=>'admin.admission.studentDetailsPrint',
            'uses'=>'AdmissionAdminController@downloadDetails'
        ]);

        Route::get('subject-settings',[
            'as'=>'admin.admission.subjectSettings.index',
            'uses'=>'AdmissionAdminController@subjectSettings'
        ]);
        Route::prefix('subject-settings')->group(function() {
            Route::get('create',[
                'as'=>'admin.admission.subjectSettings.create',
                'uses'=>'AdmissionAdminController@subjectSettingsCreate'
            ]);
            Route::post('store',[
                'as'=>'admin.admission.subjectSettings.store',
                'uses'=>'AdmissionAdminController@subjectSettingsStore'
            ]);
            Route::get('edit/{data}',[
                'as'=>'admin.admission.subjectSettings.edit',
                'uses'=>'AdmissionAdminController@subjectSettingsEdit'
            ]);
            Route::post('update/{data}',[
                'as'=>'admin.admission.subjectSettings.update',
                'uses'=>'AdmissionAdminController@subjectSettingsUpdate'
            ]);

            Route::get('subjects',[
                'as'=>'admin.admission.subjectSettings.subject',
                'uses'=>'AdmissionAdminController@subjectSettingsSubject'
            ]);
            Route::get('subjects/create',[
                'as'=>'admin.admission.subjectSettings.subjects.create',
                'uses'=>'AdmissionAdminController@subjectSettingsSubjectsCreate'
            ]);
            Route::post('subjects/store',[
                'as'=>'admin.admission.subjectSettings.subjects.store',
                'uses'=>'AdmissionAdminController@subjectSettingsSubjectsStore'
            ]);
            Route::get('subjects/edit/{subjectList}',[
                'as'=>'admin.admission.subjectSettings.subjects.edit',
                'uses'=>'AdmissionAdminController@subjectSettingsSubjectsEdit'
            ]);
            Route::post('subjects/update/{subjectList}',[
                'as'=>'admin.admission.subjectSettings.subjects.update',
                'uses'=>'AdmissionAdminController@subjectSettingsSubjectsUpdate'
            ]);
            Route::delete('subjects/delete/{subjectList}',[
                'as'=>'admin.admission.subjectSettings.subjects.delete',
                'uses'=>'AdmissionAdminController@subjectSettingsSubjectsDelete'
            ]);
        });

        Route::get('xm',[
            'as'=>'admin.admission.xm.index',
            'uses'=>'ExamCreateContollerController@index'
        ]);
        Route::prefix('xm')->group(function() {
            Route::get('create',[
                'as'=>'admin.admission.xm.create',
                'uses'=>'ExamCreateContollerController@create'
            ]);
            Route::get('make/{id}',[
                'as'=>'admin.admission.xm.make',
                'uses'=>'ExamCreateContollerController@make'
            ]);
            Route::get('make-2nd-time/{id}',[
                'as'=>'admin.admission.xm.make1',
                'uses'=>'ExamCreateContollerController@make1'
            ]);
            Route::post('store',[
                'as'=>'admin.admission.xm.store',
                'uses'=>'ExamCreateContollerController@store'
            ]);
            Route::post('store1',[
                'as'=>'admin.admission.xm.store1',
                'uses'=>'ExamCreateContollerController@store1'
            ]);

            Route::get('show/{cla_id}',[
                'as'=>'admin.admission.xm.show',
                'uses'=>'ExamCreateContollerController@show'
            ]);
            Route::get('show-2nd-time/{cla_id}',[
                'as'=>'admin.admission.xm.show1',
                'uses'=>'ExamCreateContollerController@show1'
            ]);
            Route::get('edit/{mark}',[
                'as'=>'admin.admission.xm.edit',
                'uses'=>'ExamCreateContollerController@edit'
            ]);
            Route::post('update/{mark}',[
                'as'=>'admin.admission.xm.update',
                'uses'=>'ExamCreateContollerController@update'
            ]);
        });
        Route::get('result',[
            'as'=>'admin.admission.result.index',
            'uses'=>'ResultCreateContollerController@index'
        ]);
        Route::get('result/test',[
            'as'=>'admin.admission.result.test',
            'uses'=>'ResultCreateContollerController@test'
        ]);
        Route::prefix('result')->group(function() {
            Route::get('create',[
                'as'=>'admin.admission.result.create',
                'uses'=>'ResultCreateContollerController@create'
            ]);
            Route::get('show/{cla_id}',[
                'as'=>'admin.admission.result.show',
                'uses'=>'ResultCreateContollerController@show'
            ]);
            Route::get('show-2nd-time/{cla_id}',[
                'as'=>'admin.admission.result.show1',
                'uses'=>'ResultCreateContollerController@show1'
            ]);
            Route::get('marks/{cla_id}',[
                'as'=>'admin.admission.result.result',
                'uses'=>'ResultCreateContollerController@result'
            ]);
            Route::post('store/{cla_id}',[
                'as'=>'admin.admission.result.store',
                'uses'=>'ResultCreateContollerController@store'
            ]);
            Route::post('store-2nd-time/{cla_id}',[
                'as'=>'admin.admission.result.store1',
                'uses'=>'ResultCreateContollerController@store1'
            ]);
            Route::get('publish',[
                'as'=>'admin.admission.result.publish',
                'uses'=>'ResultCreateContollerController@publish'
            ]);
            Route::get('publish/class/result/{cla_id}',[
                'as'=>'admin.admission.result.publish.class.result',
                'uses'=>'ResultCreateContollerController@publishClassResult'
            ]);
            Route::get('publish/class/result-2nd-time/{cla_id}',[
                'as'=>'admin.admission.result.publish.class.result1',
                'uses'=>'ResultCreateContollerController@publishClassResult1'
            ]);
            Route::get('lottery/select-class',[
                'as'=>'admin.admission.result.lottery.select',
                'uses'=>'ResultCreateContollerController@selectClass'
            ]);

            Route::post('lottery',[
            'as'=>'admin.admission.result.lottery',
            'uses'=>'ResultCreateContollerController@lotteryResultIndex'
            ]);

            Route::get('lottery/choose-class',[
            'as'=>'admin.admission.result.lottery.choose',
            'uses'=>'ResultCreateContollerController@chooseClass'
            ]);

            Route::post('lottery/create',[
            'as'=>'admin.admission.result.lottery.create',
            'uses'=>'ResultCreateContollerController@createLotteryResult'
            ]);

            Route::get('lottery/resultPDF/{id}',[
            'as'=>'admin.admission.result.lottery.pdf',
            'uses'=>'ResultCreateContollerController@createLotteryResultPDF'
            ]);

            Route::post('lottery/store',[
            'as'=>'admin.admission.result.lottery.store',
            'uses'=>'ResultCreateContollerController@storeLotteryResult'
            ]);

            Route::get('lottery/{lotteryResult}/edit',[
            'as'=>'admin.admission.result.lottery.editlotteryResult',
            'uses'=>'ResultCreateContollerController@editlotteryResult'
            ]);

            Route::post('lottery/{lotteryResult}/update',[
            'as'=>'admin.admission.result.lottery.updatelotteryResult',
            'uses'=>'ResultCreateContollerController@updateLotteryResult'
            ]);
            
            Route::get('lottery/sms/create',[
            'as'=>'admin.admission.result.lottery.createSMS',
            'uses'=>'ResultCreateContollerController@createLotteryResultsms'
            ]);

            Route::get('lottery/getLotteryResult/{id}',[
            'as'=>'admin.admission.result.getLotteryResult',
            'uses'=>'ResultCreateContollerController@getLotteryResult'
            ]);

            Route::post('lottery/sms/send',[
            'as'=>'admin.admission.result.lottery.sendSMS',
            'uses'=>'ResultCreateContollerController@sendLotteryResultsms'
            ]);

        });
        Route::get('print',[
                'as'=>'admin.admission.print.index',
                'uses'=>'PrintContollerController@index'
            ]);
        Route::prefix('print')->group(function() {
            Route::get('student/{cla_id}',[
                'as'=>'admin.admission.print.studentDetails',
                'uses'=>'PrintContollerController@studentDetails'
            ]);
            Route::get('student-2nd-time/{cla_id}',[
                'as'=>'admin.admission.print.studentDetails1',
                'uses'=>'PrintContollerController@studentDetails1'
            ]);
            Route::get('admit/{id}',[
                'as'=>'admin.admission.print.admit',
                'uses'=>'PrintContollerController@admit'
            ]);
            Route::get('email',[
                'as'=>'admin.admission.print.email',
                'uses'=>'PrintContollerController@email'
            ]);
        });
        Route::get('manage',[
            'as'=>'admin.admission.manage.index',
            'uses'=>'ManageContollerController@index'
        ]); 
        Route::get('manage/edit/{admission}',[
            'as'=>'admin.admission.manage.edit',
            'uses'=>'ManageContollerController@edit'
        ]);
        Route::post('manage/update/{admission}',[
            'as'=>'admin.admission.manage.update',
            'uses'=>'ManageContollerController@update'
        ]);
        Route::get('manage/reset',[
            'as'=>'admin.admission.manage.reset',
            'uses'=>'ManageContollerController@reset'
        ]);
        Route::get('manage/resetbackup',[
            'as'=>'admin.admission.manage.resetbackup',
            'uses'=>'ManageContollerController@resetbackup'
        ]);  
    });

});

