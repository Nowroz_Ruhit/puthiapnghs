<?php

namespace Modules\PhotoGallery\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Modules\PhotoGallery\Entities\Gallery;
use Modules\PhotoGallery\Entities\Photos;
// use Intervention\Image\Facades\Image;
use Image;

class PhotoGalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $album = Gallery::all();
        return view('photogallery::index', compact('album'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('photogallery::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request, Gallery $gallery)
    {
        //dd($request->all());
        abort_unless(\Gate::allows('photo_uplode'), 403);
        $validatedData = $request->validate([
            'title'=>'required',
            // 'photo'=>'required',
        ]);
        // dd(request()->all());
        $gallery = $request->all();
        $gallery['created_by'] = Auth::id();
        if ($request->hasFile('photo')){ 
            // $imageName = request()->logo->getClientOriginalName();
            $imageName = request()->input('name').'.'.request()->file('photo')->getClientOriginalExtension();
            request()->file('photo')->move(public_path(trans('global.links.gallery')), $imageName);
            $image = Image::make(public_path(trans('global.links.gallery').$imageName))->resize(450, 450);
            $image->save();

            $gallery['photo'] = $imageName;
        }
        $data = Gallery::create($gallery);
        $id = $data->id;
        if($data){
            return redirect()->route('photogallery.imgs',$id)->with('success', 'Album Create Successfull');
        }else {
            return redirect()->route('photogallery.index')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        // dd($id);
        $gallery = Gallery::find($id)->photos;
        // dd($gallery);
        return view('photogallery::view', compact('gallery','id'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('photogallery::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function imgs($id)
    {
        $gallery = Gallery::findOrFail($id);
        // dd($gallery);
        return view('photogallery::imgInput',compact('gallery'));
    }

    public function uploadImages(Request $request)
    {   
        abort_unless(\Gate::allows('photo_uplode'), 403);
        $imgName = now()->format('Y-m-d-H-i-s').'-'.request()->file->getClientOriginalName();
        $photos = new Photos();
        $photos->gallery_id  = request()->input('gallery');
        $photos->photo = $imgName;
        $photos->created_by = Auth::id();
        $photos->save();

        request()->file->move(public_path(trans('global.links.photo')), $imgName);
        return response()->json(['uploaded' => '/images/'.$imgName]);

    }
}
