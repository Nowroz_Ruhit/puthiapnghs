<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function() {

	Route::resource('photogallery', 'PhotoGalleryController');
	Route::resource('photogallery/album/uplode', 'PhotoGalleryController@imgs');
    /*Route::get('/', 'PhotoGalleryController@index');
    // Route::get('view/{id}', 'PhotoGalleryController@show')->name('show');
    Route::get('show/{id}',['as'=>'gallery.show','uses'=>'PhotoGalleryController@show']);*/

    Route::get('photogallery/album/{id}/uplode',['as'=>'photogallery.imgs','uses'=>'PhotoGalleryController@imgs']);
    Route::post('photogallery/album/uplode/image',['as'=>'photogallery.upload','uses'=>'PhotoGalleryController@uploadImages']);
});

