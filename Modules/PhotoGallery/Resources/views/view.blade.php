@extends('layouts.admin')

@section('styles')
@parent
<link href="{{ asset('Modules/PhotoGallery/Resources/assets/css/jquery.bsPhotoGallery.css')}}" rel="stylesheet">
@endsection

@section('scripts')
@parent
<script src="{{ asset('Modules/PhotoGallery/Resources/assets/js/jquery.bsPhotoGallery.js')}}"></script>	
<script>
	$(document).ready(function(){
		$('ul.first').bsPhotoGallery({
			"classes" : "col-xl-3 col-lg-2 col-md-4 col-sm-4",
			"hasModal" : true,
			"shortText" : false  
		});
	});
</script>
@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('photogallery.index')}}">Photo Gallery</a></li>
    <li class="breadcrumb-item active" aria-current="page">Gallery Photoes</li>
  </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      Photo Gallery
    @can('photo_uplode')
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{ route('photogallery.imgs',$id) }}">
          <i class="fas fa-edit nav-icon"></i>   Add More Picture
        </a>
      </form>
      @endcan
    </nav>
        <div class="card-body">
  	<div class="container">
  		<div>
  			<!-- Hedder -->
  		</div>
  		<ul class="row first">
  			@foreach ($gallery as $key => $photo) 
        <li>    
            <img alt="Rocking the night away"  src="{{ asset(trans('global.links.photo_show').$photo->photo)}}">
            <!-- <p>photo->id</p> -->
        </li>
        @endforeach

  		</ul>
  	</div> 
  </div>
  </div>
  @endsection
