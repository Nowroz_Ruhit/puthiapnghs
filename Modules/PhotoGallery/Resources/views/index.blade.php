@extends('layouts.admin')

@section('styles')
@parent
<link href="{{ asset('Modules/PhotoGallery/Resources/assets/css/jquery.bsPhotoGallery.css')}}" rel="stylesheet">
@endsection

@section('scripts')
@parent
<script src="{{ asset('Modules/PhotoGallery/Resources/assets/js/jquery.bsPhotoGallery.js')}}"></script>	
<script>
	$(document).ready(function(){
		$('ul.first').bsPhotoGallery({
			"classes" : "col-xl-3 col-lg-2 col-md-4 col-sm-4",
			"hasModal" : false,
			"shortText" : false  
		});
	});
</script>
@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Photo Gallery</li>
  </ol>
</nav>


    <nav class="navbar navbar-light bg-light justify-content-between">
      Photo Gallery
    @can('photo_uplode')
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{ route('photogallery.create') }}">
          <i class="fas fa-edit nav-icon"></i>   Create Album
        </a>
      </form>
      @endcan
    </nav>

    <div class="card-body">
  	<div class="container">
  		<div>
  			<!-- Hedder -->
  		</div>
  		<ul class="row first">
  			@foreach ($album as $key => $gallery) 
  			<li>
  				<a href="{{ route('photogallery.show', $gallery->id) }}">
  					@if( ($gallery->title == NULL) && ($gallery->photo == NULL))
  					<img alt="Rocking the night away"  src="{{ asset(trans('global.links.news_show').$gallery->news->photo)}}">
  					<p>{{$gallery->news->title}}</p>
  					@else
  					<img alt="Rocking the night away"  src="{{ asset(trans('global.links.gallery_show').$gallery->photo)}}">
  					<p>{{$gallery->title}}</p>
  					@endif
  				</a>
  			</li>
  			@endforeach

  		</ul>
  	</div>
    </div> 
  @endsection
