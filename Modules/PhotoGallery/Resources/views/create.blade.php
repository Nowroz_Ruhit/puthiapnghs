@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')

@if(session()->get('msg'))
 <a href="{{route('news.index')}}" class="btn btn-success">Back to News List</a>

<div class="alert-success col-12 text-center h1 p-6  mt-5 mb-5">
    Successfully Create your "<i>News</i>". <br>If you want to add mode Photos, then click 
    <strong>
        <a href="{{ route('news.photoes',session()->get('msg')) }}">here</a>
    </strong> 
</div>

<div class="col text-center">
      <!-- <button class="btn btn-default">Centered button</button> -->
      <a href="{{ route('news.photoes',session()->get('msg')) }}" class="btn btn-danger btn-lg mt-6">Add More Photos</a>
</div>
@else
<!-- <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script> -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('photogallery.index')}}">Photo Gallery</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create New Album</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} Album
    </div>

    <div class="card-body">
        {{ 
            Aire::open()
            ->route('photogallery.store')
            ->rules([
            'title' => 'required',
            'photo'=> 'required|file',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
        }}

        {{ 
            Aire::input()
            ->label('Title*')
            ->id('title')
            ->name('title')
            ->class('form-control') 
        }}


<img src="" id="cover-img-tag" width="200px" class="mb-2" />
        {{
            Aire::input()
            ->type('file')
            ->label('Album Cover Photo (image size must be 450 × 450) *')
            ->id('cover-photo')
            ->name('photo')
            
        }}



        {{ 
            Aire::button()
            ->labelHtml('<strong>Create Album</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>


<script src="{{ asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js') }}"></script>

<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#cover-photo").change(function(){
        readURL(this);
    });

</script>
@endif
@endsection

