<?php

namespace Modules\PhotoGallery\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\News\Entities\News;
use Modules\PhotoGallery\Entities\Photos;

class Gallery extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function news()
    {
        return $this->belongsTo(News::class);
    }

    public function photos()
    {
        // return $this->hasMany(Photos::class, 'gallery_idIndex', 'local_key');
        return $this->hasMany(Photos::class, 'gallery_id', 'id');
    }
}
