<?php

namespace Modules\PhotoGallery\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\PhotoGallery\Entities\Gallery;

class Photos extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
