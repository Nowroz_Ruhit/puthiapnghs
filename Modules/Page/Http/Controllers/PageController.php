<?php

namespace Modules\Page\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Page\Entities\Page;
use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('page_access'), 403);
        $pages = Page::orderBy('active', 'ASC')->get();
        return view('page::index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('page_create'), 403);
        return view('page::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
        ]);
        // dd($request->all());
        // $page = NULL;
        $data = $request->all();
        $data['created_by'] = Auth::id();
        if(Page::create($data)){
        // if($page){
            // session()->put('success','Created successfully.');
            return redirect()->route('page.index')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('page.index')->with('error', 'Not Created, Somtheing waswrong');
        }
        // return view('page::create');;   
    }


    public function show(Page $page)
    {
        abort_unless(\Gate::allows('page_access'), 403);
        return view('page::show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Page $page)
    {
        return view('page::edit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Page $page)
    {
        abort_unless(\Gate::allows('page_edit'), 403);
        request()->validate([
            'title' => 'required',
        ]);


        $data = $request->all(); 
        $data['updated_by'] = Auth::id();
        if( !($request->has('active')) ){
            $data['active'] = 0;
            $data['page_oder']=null;         
        }


        $page->update($data);
        if($page){
            // session()->put('success','Updated successfully.');
            return redirect()->route('page.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('page.index')->with('error', 'Can not Update, Somtheing waswrong');
        }
        
        // return redirect()->route('page.edit',compact('page'));
        
    }



    public function updatePositionView()
    {
        abort_unless(\Gate::allows('page_access'), 403);
        $pages = Page::orderBy('page_oder', 'ASC')->where('active',1)->get();
        $pub = Page::where('active',1)->count();
        $unpub = Page::where('active',0)->count();
        return view('page::position',compact('pages','pub','unpub'));
    }





    public function updatePosition(Request $request)
    {
        abort_unless(\Gate::allows('page_edit'), 403);


        $position = $request->position;
        $i=1;
        foreach($position as $k=>$v){
            Page::where('id',$v)->update(array('page_oder'=>$i++));
            $i++;
        }
        return response()->json($position);
    }



    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Page $page)
    {
        $page->delete();
        if($page){
            session()->put('success','Deleted successfully.');
        }
        return redirect()->route('page.index');
    }

    public function uplodeCkeditor(Request $request)

    {

        if($request->hasFile('upload')) {

            $originName = $request->file('upload')->getClientOriginalName();

            $fileName = pathinfo($originName, PATHINFO_FILENAME);

            $extension = $request->file('upload')->getClientOriginalExtension();

            // $fileName = $fileName.'_'.time().'.'.$extension;
            $fileName = now()->format('Y-m-d-H-i-s').'.'.$extension;

        

            $request->file('upload')->move(public_path(trans('global.links.page')), $fileName);

   

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');

            $url = asset(trans('global.links.page_show').$fileName); 

            $msg = 'Image uploaded successfully'; 

            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

               

            @header('Content-type: text/html; charset=utf-8'); 

            echo $response;

        }

    }
}
