@extends('layouts.admin')
@section('content')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Page</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>All Pages</b></a>

      @can('page_create')
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{ route('page.create') }}"> <i class="fas fa-edit nav-icon"></i> Add New Page</a>
          <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{ route('page.position') }}"><i class="fas fa-edit nav-icon"></i> Change Page Oder</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>

                        <th>
                            Page tittle
                        </th>
                        <!-- <th>
                            Page Link
                        </th> -->
                        <th>
                            Publish
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($pages as $key => $page)
                        <tr id="{{ $page->id }}">

                            <td>
                                {{ $page->title ?? '' }}
                            </td>
                            <!-- <td>
                                 $page->page_link ?? '' 
                            </td> -->
                            <td>

                                {!! $page->active ? '<span class="text-success font-weight-bold">Published</span>' : '<span class="text-warning">Unpublish</span>' ?? '' !!}
                            </td>

                            <td>
                                @can('page_access')
                                    <a class="btn btn-xs btn-info" href="{{ route('page.show',$page) }}">
                                        {{ trans('global.show') }}
                                    </a>
                                @endcan
                                @can('page_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('page.edit',$page) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('page_delete')
                                    <form action="{{ route('page.destroy', $page) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




@section('scripts')

@parent
<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>

<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });

    var hulla = new hullabaloo();
    @if(Session::has('success')) 
        hulla.send("{{ Session::get('success') }}", 'success') 
        @php
           Session::forget('success');
        @endphp
    @endif


</script>

@endsection

@endsection