@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush
@section('content')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Page</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add New Page</li>
  </ol>
</nav>

<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<!-- <script src="{{ asset('public/ck-lib/js/ckeditor.js') }}"></script>
<script src="{{ asset('public/ck-lib/js/sample.js') }}"></script> -->

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Add New Page</b></a>

      @can('page_create')
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-success" href="{{ route('page.index') }}">Back</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">

            {{ 
                Aire::open()
                ->route('page.store')
                ->enctype('multipart/form-data')
                ->rules([
                    'title' => 'required',
                ])->messages([
                    'link' => 'Incorect link url'
                ])
            }}

            {{ Aire::input('input', 'Page Title:*')
              ->id('title')
              ->name('title')
              ->placeholder('Site Name')
              ->value('')
              ->class('form-control mb-2') 
            }}

            <!-- {{ Aire::input('input', 'Page Url:*')
              ->id('page_link')
              ->name('page_link')
              ->placeholder('page/pagename')
              ->value('')
              ->class('form-control' )}} -->

              {{ 
                  Aire::textarea()
                  ->label("Page Data")
                  ->id('editor1')
                  ->name('contant')
                  ->class('form-control')
              }}

<!--               <script>
                  initSample();
              </script> -->


            {{ Aire::submit('Create Page')->class('btn btn-success') }}

            <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="{{ route('page.index') }}">Cancel</a>

            {{ Aire::close() }}
          
    </div>
</div>
<script type="text/javascript">

    CKEDITOR.replace('editor1', {

        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",

        filebrowserUploadMethod: 'form'

    });


</script>



@endsection