<?php
use Modules\Frontend\Http\Controllers\FrontendController;
use Modules\Visitor\Entities\Visitor;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $bootGeneral->title }}</title>

    @stack('css')

    <meta name="viewport" content="width=device-width, initial-scale=1.0">


<!--     <link rel="shortcut icon" href="{{asset('public/guest-user/images/favicon.png')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('public/guest-user/images/favicon.png')}}" type="image/x-icon"> -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset(trans('global.links.general_show').$bootGeneral->fab )}}">
    
    <link rel="stylesheet" href="{{asset('public/guest-user/css/bootstrap.min.css')}}">
    <!-- <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="{{asset('public/guest-user/assets/fontawesome-5.10.2/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/guest-user/assets/themify-icons/themify-icons.css')}}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('public/guest-user/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('public/guest-user/css/responsive.css')}}">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" />
    


    @yield('styles')



</head>

<body>
    <div class="container main-container pt-3">


    <!-- Logo Wrap  -->
    <section class="logo-wrap">
        <div class="row">
          <div class="col-md-12">
            <img class="banner-bg" src="{{asset(trans('global.links.general_show').$bootGeneral->banar)}}" alt="Header Banner">
            <a class="navbar-brand" href="#">
              <!-- <img src="{{asset('public/guest-user/images/logo.png')}}" alt="PiceIntlBD"> -->
              <!-- <span>Government Laboratory High School, Bangladesh</span> -->
              <img src="{{asset(trans('global.links.general_show').$bootGeneral->logo)}}" alt="PiceIntlBD">
              <span>{{$bootGeneral->title}}</span>
            </a>
          </div>
        </div>
    </section>



<section class="site-content mt-3">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              {{$page->title}} 
              <a href="{{route('page.index')}}" class="btn float-right" style="background: #f1f2f3 !important; border: none">Back</a>
              <a href="{{route('page.edit',$page)}}" class="btn float-right mr-1" style="background: #f1f2f3 !important; border: none"><i class="fas fa-edit nav-icon"></i> Edit</a>
            </div>
            <div class="card-body text-justify" id="page-contant">
              {!!$page->contant!!}
            </div>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>



    <footer class="mt-3">
        <div class="row">
          <div class="col-md-4 text-left footer-contact">
            <h4><i class="fas fa-square-full"></i> যোগাযোগ</h4>
            <p>
              <i class="fas fa-phone-square-alt"></i>
              {{$bootGeneral->phone}}
            </p>
            <p>
              <i class="far fa-envelope"></i>
              {{$bootGeneral->email}}
            </p>
            <p>
              <i class="fas fa-map-marker-alt"></i>
              {!!$bootGeneral->address!!}
            </p>
          </div>

          <div class="col-md-4 text-left footer-service">
            <h4><i class="fas fa-square-full"></i> গুরুত্বপূর্ণ লিংক</h4>
            <ul>
              @foreach ($importantlink as $key => $value)
              <li>
                <a href="{{ $value->link }}" target="_blank">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  {{ $value->title }}</a>
              </li>
              @endforeach
            </ul>
          </div>
          <div class="col-md-4 text-left footer-service">
            <h4><i class="fas fa-square-full"></i> ভিজিটর</h4>
            <ul>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Visited User {{ Visitor::distinct('ip')->count('ip')}}</a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Totla Hits {{ Visitor::sum('hits')}}
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Today Visited : {{Visitor::where('date', date('Y-m-d'))->count()}}
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Today's Date : {{date('d MM Y')}}
                </a>
              </li>
            </ul>
          </div>
        </div>
    </footer>
    <!-- End Footer -->
    <section class="copyright-power">
      <div class="row">
        <div class="col-sm-6">
          <p>Copyright &copy; 2019 | {{ $bootGeneral->title }}</p>
        </div>
        <div class="col-sm-6 text-right">
          <p>Powered by <a href="http://desktopit.com.bd">Desktop IT</a>  </p>
        </div>
      </div>
    </section>
    </div>
    <!-- </div> -->


    <!-- Bootstrap JS -->
    <script src="{{asset('public/guest-user/js/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{ asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_jquery_3.3.1_jquery.min.js') }}"></script>
    <script src="{{asset('public/guest-user/js/popper.min.js')}}"></script>
    <script src="{{asset('public/guest-user/js/bootstrap.min.js')}}"></script>

    <!-- Custom JS -->
    <script src="{{asset('public/guest-user/js/custom.js')}}"></script>

</body>
</html>