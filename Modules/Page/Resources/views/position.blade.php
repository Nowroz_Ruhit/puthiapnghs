@extends('layouts.admin')
@section('content')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Page</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>All Pages</b></a>

      @can('page_create')
        <form class="form-inline">
          <div class="btn btn-info mr-2">Total Page: {{ $pub+$unpub }}</div>
          <div class="btn btn-danger mr-2">Unpublished: {{ $unpub }}</div>
          <div class="btn btn-warning mr-5">Published: {{ $pub }}</div>
          <a class="form-control mr-sm-2 btn btn-success" href="{{ route('page.index') }}">Back</a>
        </form>
      @endcan

    </nav>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>
                        <th>
                            
                        </th>
                        <th>
                            Page tittle
                        </th>
                        <th>
                            Page Link
                        </th>
                        <th>
                            Publish
                        </th>
                    </tr>
                </thead>
                <tbody class='row_position'>
                    @foreach($pages as $key => $page)
                        <tr id="{{ $page->id }}">
                            <td  width="1">
                                
                            </td>
                            <td>
                                {{ $page->title ?? '' }}
                            </td>
                            <td>
                                {{ $page->page_link ?? '' }}
                            </td>
                            <td>
                                {{ $page->active ? 'Published' : 'Unpublish' ?? '' }}
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>




@section('scripts')

@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{ asset('public/lib/hullabaloo/js/hullabaloo.js') }}"></script>

<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });

    var hulla = new hullabaloo();

    $( ".row_position" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
        }
    });


    function updateOrder(data) {
        //console.log(data);
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url:"{{ route('postionOder') }}",
            type:'post',
            data:{position:data},
            success:function(){
                hulla.send("Position Updated", 'success')
            }
        })
    }
</script>

@endsection

@endsection