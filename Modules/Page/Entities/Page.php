<?php

namespace Modules\Page\Entities;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    // protected $fillable = [ 'id','title', 'contant','page_oder','page_link','active' ];
    protected $guarded = ['id', 'created_at'];
}
