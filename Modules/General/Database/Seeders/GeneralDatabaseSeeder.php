<?php

namespace Modules\General\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\General\Entities\General;

class GeneralDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
public function run()
    {
        // Model::unguard();

        // $this->call("OthersTableSeeder");
        $general = [
            [
                'id'            => '1',
                'name'          => 'Demo School',
                'title'         => 'Demo School',
                'url'           => 'http://school.com',
                'phone'         => '017',
                'email'         => 'school@school.com',
                'address'       => '203/1 Par-Naogaon Naogaon-6500, Bangladesh',
                'discription'   => '<p>শিক্ষা নিয়ে গড়ব দেশ,</p><p>শেখ হাসিনার বাংলাদেশ।</p><p>শিক্ষা নিয়ে গড়ব দেশ,</p><p>শেখ হাসিনার বাংলাদেশ।</p><p>&nbsp;</p>',
                'banar'         => '2019-09-14-09-27-28-header_bg.jpg',
                'logo'          => '2019-09-14-09-27-28-logo.png',
                'slogan'        => '<p>শিক্ষা নিয়ে গড়ব দেশ,</p><p>শেখ হাসিনার বাংলাদেশ।</p>',
                'fab'        => '2019-09-14-09-27-28-favicon.png', 
                'created_at'    => '2019-04-15 19:14:42',
                'updated_at'    => '2019-04-15 19:14:42',
            ],
        ];
        General::insert($general);
    }
}
