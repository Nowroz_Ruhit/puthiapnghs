@extends('layouts.admin')
@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="">Administration</a></li>
    <li class="breadcrumb-item"><a href="">Home Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">General</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      General Information
      @can('employee_create')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-success" href="{{route('general.edit',$general)}}">
          <i class="fas fa-edit nav-icon"></i>  {{ trans('global.edit') }}
        </a>
      </form>
      @endcan
    </nav>

    <div class="card-body">
    	<table class="table table-borderless table-responsive">
        <tr>
          <th scope="row" >Name</th>
          <td >{{$general->name}}</td>
        </tr>
        <tr>
          <th scope="row">Site Title</th>
          <td>{{$general->title}}</td>
        </tr>
        <tr>
          <th scope="row">Site URL</th>
          <td>{{$general->url}}</td>
        </tr>
        <tr>
          <th scope="row">Telephone</th>
          <td >{{$general->tel_phone}}</td>
        </tr>
        <tr>
          <th scope="row">Phone</th>
          <td >{{$general->phone}}</td>
        </tr>
        <tr>
          <th scope="row">Email</th>
          <td >{{$general->email}}</td>
        </tr>
        <tr>
          <th scope="row" >Address</th>
          <td >{{$general->address}}</td>
        </tr>

        <tr>
          <th scope="row" >Slogan</th>
          <td >{!!$general->slogan!!}</td>
        </tr>
        <tr>
          <th scope="row" >Discription </th>
          <td ><div class="text-justify">{!!$general->discription!!}</div></td>
        </tr>

        

        <tr>
          <th>Banar</th>
         <td colspan="3">
           <img class="col-12"   src="{{ asset(trans('global.links.general_show').$general->banar) }}">
        </td>
      </tr>

      <tr>
        <th>Logo</th>
         <td colspan="2">
          <img class="col-3"   src="{{asset(trans('global.links.general_show').$general->logo)}}">
        </td>
      </tr>
      <tr>
        <th>Fab-icon</th>
        <td colspan="2">
          <img class="col-1"   src="{{asset(trans('global.links.general_show').$general->fab)}}">
        </td>
      </tr>
      <tr>
        <th>Google Map Location</th>
        <td colspan="3">
          {!! $general->map !!}
        </td>
      </tr>
    </table>
    </div>
</div>

@endsection