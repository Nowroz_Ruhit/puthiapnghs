@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')

@if(session()->get('msg'))
 <a href="{{url('notice')}}" class="btn btn-success">Back to Notice List</a>

<div class="alert-success col-12 text-center h1 p-6  mt-5 mb-5">
    "<i>Notice</i>"  {{session()->get('msg')}} Successfully
     
</div>

@else
<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="">Administration</a></li>
    <li class="breadcrumb-item"><a href="">Home Settings</a></li>
    <li class="breadcrumb-item"><a href="{{route('general.index')}}">General</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>
<div class="card">
<!--     <div class="card-header">
        General Inforamtion
    </div> -->
    <nav class="navbar navbar-light bg-light justify-content-between">
      General Inforamtion
      @can('general_update')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-success" href="{{route('general.index')}}">
          <i class="fas  nav-icon"></i>  Back
        </a>
      </form>
      @endcan
    </nav>

    <div class="card-body">
         <!-- old('name', isset($product) ? $product->name : '')
        ->route() 
        ->rules([
            'name' => 'required',
            'title' => 'required',
            'url' => 'required|url',
            'phone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'discription' => 'required',
            'slogan' => 'required',
            'banar' => 'required|image',
            'logo' => 'required|image',
            
            ])           
          -->
        {{ 
            Aire::open()
            ->route('general.update', $general)
            ->bind($general)

            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
        }}

        {{ 
            Aire::input()
            ->label('Schoolo Name')
            ->id('name')
            ->name('name')
            ->class('form-control') 
        }}
        {{ 
            Aire::input()
            ->label('Site Title')
            ->id('title')
            ->name('title')
            ->class('form-control') 
        }}
        {{ 
            Aire::input()
            ->label('School Site Url')
            ->id('url')
            ->name('url')
            ->class('form-control') 
        }}

        {{ 
            Aire::input()
            ->label('School Tele-Phone no.')
            ->id('tel_phone')
            ->name('tel_phone')
            ->class('form-control') 
        }}

        {{ 
            Aire::input()
            ->label('School Phone no.')
            ->id('phone')
            ->name('phone')
            ->class('form-control') 
        }}
        {{ 
            Aire::input()
            ->label('School Email')
            ->id('email')
            ->name('email')
            ->class('form-control') 
        }}
        {{ 
            Aire::input()
            ->label('Address')
            ->id('address')
            ->name('address')
            ->class('form-control') 
        }}

        {{ 
            Aire::textarea()
            ->label('Slogan')
            ->id('slogan')
            ->name('slogan')
            ->class('form-control') 
        }}
        <script>
            CKEDITOR.replace( 'slogan' );
        </script>

        {{ 
            Aire::textarea()
            ->label('Discription')
            ->id('discription')
            ->name('discription')
            ->class('form-control')
        }}
        <script>
            CKEDITOR.replace( 'discription' );
        </script>

    <img src="" id="banar-img-tag" width="200px" class="mb-2" />
        {{
            Aire::input()
            ->type('file')
            ->label('Site Banar Photo (image size must be 1180 × 190) *')
            ->id('banar-photo')
            ->name('banar')
            
        }}

    <img src="" id="logo-img-tag" width="200px" class="mb-2" />
        {{
            Aire::input()
            ->type('file')
            ->label('Logo (image size must be 190 × 190) *')
            ->id('logo-photo')
            ->name('logo')
            
        }}

    <img src="" id="fab-img-tag" width="200px" class="mb-2" />
        {{
            Aire::input()
            ->type('file')
            ->label('Fab-icon (image size must be 36 × 36) *')
            ->id('fab-photo')
            ->name('fab')
            
        }}



        <script>
            CKEDITOR.replace( 'discription' );
        </script>


        {{ 
            Aire::textarea()
            ->label('Google Map Location')
            ->id('map')
            ->name('map')
            ->class('form-control')
        }}


        {{ 
            Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>
<script src="{{ asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js') }}"></script>

<script type="text/javascript">

    function banar(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#banar-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function logo(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#logo-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function fab(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#fab-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#banar-photo").change(function(){
        banar(this);
    });
    $("#logo-photo").change(function(){
        logo(this);
    });
    $("#fab-photo").change(function(){
        fab(this);
    });

</script>
@endif
@endsection

