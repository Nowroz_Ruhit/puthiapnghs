<?php

namespace Modules\General\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

use Modules\General\Entities\General;
use Intervention\Image\Facades\Image;


class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('general_access'), 403);
        // $general = General::orderByRaw('updated_at - created_at DESC')
        $general = General::latest()->first();
        return view('general::index', compact('general'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('general::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request, General $general)
    {
        // 
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('general::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(General $general)
    {
        abort_unless(\Gate::allows('general_update'), 403);
        return view('general::edit', compact('general'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */

    public function update(Request $request, General $general)
    {
        abort_unless(\Gate::allows('general_update'), 403);
        request()->validate([
            'name'          => 'required',
            'title'         => 'required',
            'url'           => 'required',
            'phone'         => 'required',
            'email'         => 'required',
            'address'       => 'required',
            'discription'   => 'required|min:20',
            // 'slogan'        => 'required',
            // 'banar'         => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'logo'          => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            // 'fab'          => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',


        ]);
        
        $data = $request->all();
        $data['updated_by'] = Auth::id();
        if($request->map){
            $data['map'] = str_replace('width="600" height="450"','width="100%" height="286"', $request->map);
        }
        
        if ($request->hasFile('banar')){ 
            if(file_exists(public_path(trans('global.links.general')).$general->banar)){
                unlink(public_path(trans('global.links.general')).$general->banar);
            }


            $imageName = now()->format('Y-m-d-H-i-s').'-'.str_replace(' ', '', request()->banar->getClientOriginalName());

            request()->file('banar')->move(public_path(trans('global.links.general')), $imageName);
            
            $image = Image::make(public_path(trans('global.links.general').$imageName))->resize(1180, 190);
            $image->save();

            $data['banar'] = $imageName;
        }

        if ($request->hasFile('logo')){
            if(file_exists(public_path(trans('global.links.general')).$general->logo)){
                unlink(public_path(trans('global.links.general')).$general->logo);
            }
            $imageName = now()->format('Y-m-d-H-i-s').'-'.str_replace(' ', '', request()->logo->getClientOriginalName());

            request()->file('logo')->move(public_path(trans('global.links.general')), $imageName);
            $image = Image::make(public_path(trans('global.links.general').$imageName))->fit(190);
            $image->save();

            $data['logo'] = $imageName;
        }

        if ($request->hasFile('fab')){
            if(file_exists(public_path(trans('global.links.general')).$general->logo)){
                unlink(public_path(trans('global.links.general')).$general->fab);
            }
 
            $imageName = now()->format('Y-m-d-H-i-s').'-'.str_replace(' ', '', request()->fab->getClientOriginalName());
            request()->file('fab')->move(public_path(trans('global.links.general')), $imageName);

            $image = Image::make(public_path(trans('global.links.general').$imageName))->fit(36);
            $image->save();

            $data['fab'] = $imageName;
        }

        $general->update($data);

        if($general){
            return redirect()->route('general.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('general.index')->with('error', 'Not Update, Somtheing waswrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
