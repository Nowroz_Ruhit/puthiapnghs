<?php

namespace Modules\General\Entities;

use Illuminate\Database\Eloquent\Model;

class General extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
