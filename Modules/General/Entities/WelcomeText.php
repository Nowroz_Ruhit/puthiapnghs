<?php

namespace Modules\General\Entities;

use Illuminate\Database\Eloquent\Model;

class WelcomeText extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
