<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route:: get ('/', function () {
//    return view('frontend::maintenance');
// });
// Route::prefix('frontend')->group(function() {
//     Route::get('/', 'FrontendController@index');
// });

// Route::get('menu','FrontendController@menu')->name('menu');
// Route::get('/', 'FrontendController@maintenance')->name('maintenance');
Route::get('/', 'FrontendController@index')->name('index');
Route::get('about', 'FrontendController@about')->name('about');
Route::get('/gallery', 'FrontendController@gallery')->name('gallery');
Route::get('/contact', 'FrontendController@contact')->name('contact');

Route::get('academic/{type}',[
	'as'=>'frontend.academic',
	'uses'=>'FrontendController@academic'
]);
Route::get('page/{page}',[
	'as'=>'frontend.page',
	'uses'=>'FrontendController@page'
]);
Route::get('/{type}/employee/{id}/',[
	'as'=>'frontend.employee.list',
	'uses'=>'FrontendController@employee'
]);
Route::get('employee/{id}/',[
	'as'=>'frontend.employee',
	'uses'=>'FrontendController@employee'
]);
Route::get('/{in}/employee/{type}/details/{employee}',[
	'as'=>'frontend.employee.details',
	'uses'=>'FrontendController@employeeDetails'
]);
Route::get('notice',[
	'as'=>'frontend.notice',
	'uses'=>'FrontendController@notice'
]);
Route::get('news',[
	'as'=>'frontend.news',
	'uses'=>'FrontendController@news'
]);
Route::get('result',[
	'as'=>'frontend.result',
	'uses'=>'FrontendController@result'
]);
Route::get('academic/{type}/view/{id}',[
	'as'=>'frontend.academic_view',
	'uses'=>'FrontendController@academicShow'
]);
Route::get('notice/show/{id}',[
	'as'=>'frontend.notice.show',
	'uses'=>'FrontendController@noticeShow'
]);
// Route::get('notice/show/{id}','FrontendController@notice_show'
// )->name('notice_show');

Route::get('academic/{type}/download/{id}',[
	'as'=>'frontend.academic.download',
	'uses'=>'FrontendController@academicDownload'
]);

Route::get('notice/download/{id}',[
	'as'=>'frontend.notice_download',
	'uses'=>'FrontendController@noticeDownload'
]);

Route::get('news/{id}/view',[
	'as'=>'frontend.news.view',
	'uses'=>'FrontendController@newsShow'
]);
Route::get('gallery/{id}/view',[
	'as'=>'frontend.gallery.view',
	'uses'=>'FrontendController@galleryShow'
]);
Route::get('speech/{speech}/view',[
	'as'=>'frontend.speech',
	'uses'=>'FrontendController@speech'
]);

// Route::get('test','FrontendController@sideBar');

Route::post('email',[
	'as'=>'frontend.email',
	'uses'=>'FrontendController@emailRequest'
]);

Route::get('/clear-all', function () {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('route:clear');

    return 'cache,view and route is cleared';
});
