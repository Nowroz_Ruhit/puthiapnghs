<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

// use Modules\PhotoGallery\Entities\Gallery;
// use Modules\Academic\Entities\AcademicCategory;
// use Modules\General\Entities\General;
// use Modules\Notice\Entities\Notice;


use Modules\Academic\Entities\Academic;
use Modules\Academic\Entities\AcademicCategory;
use Modules\Employee\Entities\Employee;
use Modules\Email\Entities\Email;
use Modules\General\Entities\General;
use Modules\General\Entities\WelcomeText;
use Modules\Homeslider\Entities\Homeslider;
use Modules\Employee\Entities\Institute;
use Modules\News\Entities\News;
use Modules\News\Entities\Updatepost;
use Modules\Notice\Entities\Notice;

use Modules\PhotoGallery\Entities\Gallery;
use Modules\Speech\Entities\Speech;
use Modules\Visitor\Entities\Visitor;
use Modules\Page\Entities\Page;



class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        Visitor::hit();
        $homesliders = Homeslider::where('active',1)->orderBy('updated_at', 'ASC')->get();
        $updatepost = Updatepost::take(5)->latest()->get();
        // if(empty($updatepost[0]))
        // {
        //     return 'Empty';
        // } else {
        //     return 'Not Empty';
        // }
        $size = count($homesliders);
    // trans('global.links.news')
        return view('frontend::index', compact('homesliders', 'size', 'updatepost'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('frontend::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('frontend::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('frontend::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function about()
    {
        return view('frontend::about');
    }

    public function contact()
    {
        return view('frontend::contact');
    }

    public function gallery()
    {
        // return 'Tasmir';
        $album = Gallery::paginate(3);
        return view('frontend::gallery', compact('album'));
    }

/*    public static function menu()
    {
        $academicCategory = AcademicCategory::all();
        // dd($academicCategory);
        return view('frontend::partials.guestmenu', compact('academicCategory'));
    }
*/
    public function academicShow($type,$id)
    {
        $academic = Academic::findOrFail($id);
        return view('frontend::academicView', compact('type', 'academic'));
    }

    public function noticeShow($id)
    {
        $notice = Notice::findOrFail($id);

        // dd($academic);
        return view('frontend::noticeShow', compact('notice'));
    }

    public function newsShow($id)
    {
        $news = News::findOrFail($id);
        $photos = News::findOrFail($news->id)->album->photos;
        return view('frontend::newsShow',compact('news' , 'photos'));
    }

    public function galleryShow($id)
    {
        $gallery = Gallery::findOrFail($id);
        $photos = Gallery::findOrFail($id)->photos;

        return view('frontend::photosShow' , compact('photos', 'gallery'));
    }

    public function academic($type)
    {
        // return $id;
        $category = AcademicCategory::where('title', $type)->first();
        // $id = $category->id;
        $academics = Academic::where('catagories_id', $category->id)->get();
        return view('frontend::academic', compact('type', 'academics'));
    }

    public function employee($type, $id)
    {
        $employees = Employee::where('institute_id', $id)->orderBy('designation_id', 'asc')->paginate(9);
        $institute = Institute::findOrFail($id);
        return view('frontend::employee', compact('type', 'employees', 'institute'));
    }

    public function employeeDetails($in, $type, Employee $employee)
    {
        return view('frontend::employeeDetails', compact('in', 'type', 'employee'));
    }

    public function notice()
    {
        $notices = Notice::orderBy('id', 'desc')->paginate(9);
        return view('frontend::notice', compact('notices'));
    }

    public function news()
    {
        $newss = News::orderBy('id', 'desc')->paginate(9);
        return view('frontend::news', compact('newss'));
    }

    public function result()
    {
        return view('frontend::result');
    }
    
    public function speech(Speech $speech)
    {
        // $speech = Speech::latest()->first();
        // dd($speech);
        // return $speech;
        return view('frontend::speech' ,compact('speech'));
    }

    public static function sideBar()
    {
        $speechs = Speech::all();
        $notices = Notice::take(5)->latest()->get();
        return view('frontend::partials.rightSide', compact('speechs', 'notices'));
    }

    public function academicDownload($type,$id){
        $academic = Academic::findOrFail($id);
        //dd($notice[0]->file);
        $name = $academic->title;
        $file = $academic->file;
        //dd(substr($notice[0]->file, -3));
        $extention = substr($academic->file, -3);
        // dd($name.'.'.$extention);
        $pathToFile = public_path(trans('global.links.academic')).'/'.$file;
        return response()->download($pathToFile, $name.'.'.$extention);
    }

    public function maintenance ()
    {
        return view('frontend::maintenance');
    }
    

    public function noticeDownload($id)
    {
        $notice = Notice::findOrFail($id);
        //dd($notice[0]->file);
        $name = $notice->title;
        $file = $notice->file;
        //dd(substr($notice[0]->file, -3));
        $extention = substr($notice->file, -3);
        // dd($name.'.'.$extention);
        $pathToFile = public_path(trans('global.links.notice')).$file;
        return response()->download($pathToFile, $name.'.'.$extention);
    }

    public function emailRequest (Request $request) {
        $validatedData = $request->validate([
            'name'=>'required',
            'form'=>'required',
            'phone'=>'required',
            'detail'=>'required',
            'subject'=>'required',
        ]);
        $data = $request->all();
        $to = 'smmplsc21@yahoo.com';
        $data['to'] = $to;
 
        Email::create($data);

        $subject = $data['subject'];
        $message = view('frontend::email', compact('data'));

        $headers = 'From: '.$data['form']. "\r\n" .
        'Reply-To: '.$data['form']. "\r\n" .
        'Content-Type: text/html; charset=ISO-8859-1\r\n'.
        'X-Mailer: PHP/' . phpversion();
       
        $mail = mail($to, $subject, $message, $headers);
        if($mail){      
            session()->put('success','Email Send.');  
        } else {
            session()->put('danger','Email Not Send');
        }
        return redirect()->route('contact');
         // return redirect()->route('contact')->with('success', 'Email Saved');
    }
    public function page (Page $page) {
        
        if($page->active == 1){
            return view('frontend::page', compact('page'));
        } else {
            abort(404);
            // return redirect()->route('index');
        }
    }
}
