@php
use Modules\Frontend\Http\Controllers\FrontendController;
@endphp
@extends('frontend::layouts.guest')
@section('content')
    <!-- Section NewsTicker -->
    <section class="news-ticker mt-3">
      <div class="row">
            <!--Breaking box-->
            <div class="col-3 col-md-3 col-lg-2 pr-md-0 pr-0">
                <div class="p-2 bg-primary text-white text-center breaking-caret"><span class="">Update</span></div>
            </div>
            <!--end breaking box-->
            <!--Breaking content-->

           
            <div class="col-9 col-md-9 col-lg-10 pl-md-4 py-2">
              <div class="breaking-box">
                <div id="carouselbreaking" class="carousel slide" data-ride="carousel">
                  @if(empty($updatepost[0]))
                    {{trans('global.nodata')}}
                  @else
                  <!--breaking news-->
                  <div class="carousel-inner">
                    <!--post-->
                    <div class="carousel-item active">
                      <a href="#"><span class="position-relative mx-2 badge badge-danger rounded-0">{{$updatepost[0]->type}}</span></a> <a class="text-dark" href="
                      @if($updatepost[0]->news_id)
                      {{route('frontend.news.view', $updatepost[0]->news_id)}}
                      @elseif($updatepost[0]->notice_id)
                      {{route('frontend.notice.show', $updatepost[0]->notice_id)}}
                      @elseif($updatepost[0]->academic_id)
                      {{route('frontend.academic.download', [$updatepost[0]->type, $updatepost[0]->academic_id])}}
                      @else
                      #
                      @endif
                      ">{{$updatepost[0]->title}}</a>
                    </div>

                      <!--post-->

                      <!--post-->
                      @php
                      $lenth = count($updatepost)
                      @endphp
                      @for ($i=1; $i<$lenth; $i++)
                      <div class="carousel-item">
                        <a href="#"><span class="position-relative mx-2 badge badge-danger rounded-0">{{$updatepost[$i]->type}}</span></a> <a class="text-dark" href="
                        @if($updatepost[$i]->news_id)
                        {{route('frontend.news.view', $updatepost[$i]->news_id)}}
                        @elseif($updatepost[$i]->notice_id)
                        {{route('frontend.notice.show', $updatepost[$i]->notice_id)}}
                        @elseif($updatepost[$i]->academic_id)
                        {{route('frontend.academic.download', [$updatepost[$i]->type, $updatepost[$i]->academic_id])}}
                        @else
                        #
                        @endif
                        ">{{$updatepost[$i]->title}}</a>
                      </div>
                      @endfor

                  </div> 
                  <!--end breaking news-->
                    
                  <!--navigation slider-->
                  <div class="navigation-box d-none d-sm-block">
                      <!--nav left-->
                      <a class="carousel-control-prev text-primary" href="#carouselbreaking" role="button" data-slide="prev">
                          <i class="fas fa-angle-double-left"></i>
                          <span class="sr-only">Previous</span>
                      </a>
                      <!--nav right-->
                      <a class="carousel-control-next text-primary" href="#carouselbreaking" role="button" data-slide="next">
                          <i class="fas fa-angle-double-right"></i>
                          <span class="sr-only">Next</span>
                      </a>
                  </div>
                  <!--end navigation slider-->
                  @endif
                </div>
              </div>
            </div>
            <!--end breaking content-->
      </div>
    </section>
    <!-- End Section NewsTicker -->

    <section class="content-rotate mt-3">
      <div class="row">

        <!-- Start SideBar -->
        <!-- This -->
        {!! FrontendController::sideBar()!!}
        <!-- End SideBar -->

        <div class="col-lg-9 right-side-content">

          <!-- Main Slider -->
          <div class="main-slider">
            <div class="bd-example">
              @if(empty($homesliders[0]))
                  <div class="text-center pt-5 pb-5">{{trans('global.nodata')}}</div>
                  @else

              <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                @if(count($homesliders)>0)
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                  @for ($i=1; $i<$size; $i++)
                  <li data-target="#carouselExampleCaptions" data-slide-to="{{$i}}"></li>
                  @endfor
                </ol>
                <div class="carousel-inner">

                  <div class="carousel-item active">
                    <img src="{{asset(trans('global.links.homeslider_show').$homesliders[0]->image)}}" class="d-block w-100" alt="Slider Image">
                    <div class="carousel-caption d-none d-md-block">
                      <h2>
                       {{ $homesliders[0]->title}}
                      </h2>
                      <h4>{{$homesliders[0]->subtitle}}</h4>
                    </div>
                  </div>

                  @for ($i=1; $i<$size; $i++)
                  <div class="carousel-item">
                    <img src="{{asset(trans('global.links.homeslider_show').$homesliders[$i]->image)}}" class="d-block w-100" alt="Slider Image">
                    <div class="carousel-caption d-none d-md-block">
                      <h2>
                       {{ $homesliders[$i]->title}}
                      </h2>
                      <h4>{{$homesliders[$i]->subtitle}}</h4>
                    </div>
                  </div>
                  @endfor
                </div>
                @endif
              </div>
              @endif
            </div>
          </div>
          <!-- End Main Slider -->

          <!-- Start Welcome or About Text -->
          <div class="card mt-3 rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0">
              {{ $bootGeneral->title }} এ স্বাগতম <i class="fas fa-info-circle"></i>
            </div>
            <div class="card-body text-justify">
              @if(empty($bootGeneral->discription))
              {{trans('global.nodata')}}
              @else
              {!! \Str::words($bootGeneral->discription, '320') !!}… <a href="{{route('about')}}" class="hm-details">বিস্তারিত <i class="fas fa-long-arrow-alt-right"></i></a>
              @endif
            </div>
          </div>
          <!-- End Welcome or About Text -->

          <!-- Start Welcome or About Text -->
          <div class="card mt-3 rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0">
              গুগল ম্যাপস <i class="fas fa-map-marker-alt text-right"></i>
            </div>
            <div class="card-body">  
              @if(empty($bootGeneral->map))
              <div style="height: 286px; width: 100%;"></div>
            @else
                {!!  $bootGeneral->map  !!}
            @endif
            </div>
          </div>
          <!-- End Welcome or About Text -->

        </div>

        

      </div>
    </section>

   





@endsection