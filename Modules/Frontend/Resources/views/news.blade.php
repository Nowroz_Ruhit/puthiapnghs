@extends('frontend::layouts.guest')

@section('content')
	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">একাডেমিক</a></li>
        <li class="breadcrumb-item active" aria-current="page">নিউজ</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

    <section class="site-content">
      	<div class="row">
	        <div class="col-sm-12">
	          <!-- Start Welcome or About Text -->
	          	<div class="card rounded-0 theme-border theme-shadow">
		            <div class="card-header theme-border-color rounded-0 theme-bg">
		              নিউজ
		            </div>
		            <!-- Teacher List -->
		            @if(empty($newss[0]))
		            <div class="text-center pt-5 pb-5">{{trans('global.nodata')}}</div>
		            @else
		            <div class="card-body teacher-list">
		            	<div class="row">
		            		@foreach ($newss as $key => $news)
		            		<div class="col-lg-4 col-md-6 mb-4">
		            			<div class="card">
									<img src="{{asset('public/uplodefile/news/'.$news->photo)}}" class="card-img-top" alt="Teacher Images">
									<div class="card-body">
										<p class="text-justify">
										{{ $news->title }}... <a href="{{route('frontend.news.view', $news)}}">বিস্তারিত</a></p>										
									</div>
								</div>
		            		</div>

		            		@endforeach
		            		</div>
<!-- 		            		<div class="col-lg-4 col-md-6 mb-4">
		            			<div class="card">
									<img src="{{asset('public/guest-user/images/hm.jpg')}}" class="card-img-top" alt="Teacher Images">
									<div class="card-body">
										<p class="text-justify">জাতীয় শিক্ষা সপ্তাহ-২০১৮ এর জাতীয় পর্যায়ের প্রতিযোগিতায় রাজশাহী কলেজিয়েট স্কুল,রাজশাহী দেশের সেরা শিক্ষা... <a href="{{route('frontend.news.view', 1)}}">বিস্তারিত</a></p>										
									</div>
								</div>
		            		</div>-->
		            		<!-- <div class="col-lg-4 col-md-6 mb-4">
		            			<div class="card">
									<img src="{{asset('public/guest-user/images/hm.jpg')}}" class="card-img-top" alt="Teacher Images">
									<div class="card-body">
										<p class="text-justify">জাতীয় শিক্ষা সপ্তাহ-২০১৮ এর জাতীয় পর্যায়ের প্রতিযোগিতায় রাজশাহী কলেজিয়েট স্কুল,রাজশাহী দেশের সেরা শিক্ষা... <a href="{{route('frontend.news.view', 1)}}">বিস্তারিত</a></p>										
									</div>
								</div>
		            		</div> 
		            	</div> -->

		            	<!-- </div> -->
		            <!-- End Teacher List -->
		            
		            	<!-- Start Pagination -->
		            	<div class="row">	
		            		<div class="col-sm-12">
		            			<nav aria-label="Page navigation example">
									<ul class="pagination justify-content-center">
										{{ $newss->links() }}
										<!-- <li class="page-item disabled">
											<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
										</li>
										<li class="page-item active"><a class="page-link" href="#">1 </a></li>
										<li class="page-item"><a class="page-link" href="#">2</a></li>
										<li class="page-item"><a class="page-link" href="#">3</a></li>
										<li class="page-item">
											<a class="page-link" href="#">Next</a>
										</li> -->
									</ul>
								</nav>
		            		</div>
		            	</div>
		            	<!-- End Pagination -->
		            
	       		</div>
	       		@endif
	    	</div>
    	</div>
	</section>


@endsection