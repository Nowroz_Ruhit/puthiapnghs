@extends('frontend::layouts.guest')
@section('content')
    <!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$page->title}}</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

    <section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              {{$page->title}}
            </div>
            <div class="card-body text-justify">
              {!!$page->contant!!}
            </div>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>

@endsection