@extends('frontend::layouts.guest')

@section('content')

	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">একাডেমিক</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$type}}</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

	<section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              <!-- সিলেবাস -->
              {{$type}}
            </div>
            @if(empty($academics[0]))
            <div class="text-center pt-5 pb-5">{{trans('global.nodata')}}</div>
            @else
            <div class="card-body">
              	<div class="table-responsive syllabus-table">
				  	<table class="table">
					   	<thead class="thead-light">
							<tr>
								<th scope="col">SL. No.</th>
								<th scope="col">Title</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($academics as $key => $academic)
							<tr>
								<th scope="row">{{$key+1}}</th>
								<td>{{$academic->title}}</td>
								<td>
									<a class="btn btn-outline-success" href="{{route('frontend.academic_view', [$type, $academic->id])}}"><i class="far fa-eye"></i> View</a>
									<a class="btn btn-outline-warning" href="{{route('frontend.academic.download', [$type, $academic->id])}}"><i class="fas fa-download"></i> Download</a>	
								</td>
							</tr>
							@endforeach
						</tbody>
				  	</table>
				</div>				              
            </div>
            @endif
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>




@endsection