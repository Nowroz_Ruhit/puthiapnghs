<div class="col-lg-3 sidebar">

          <!-- Start Slogan -->
          <div class="site-slogan">
            {!!$bootGeneral->slogan!!}
          </div>
          <!-- End Slogan -->

          @foreach ($speechs as $key => $speech)
          <!-- Start Message of Head Master -->
          @php
          if(empty($speech->name))
            continue;
          @endphp
           @if($loop->index != $loop->last)
              <div class="card mt-3 rounded-0 theme-border theme-shadow">
                <div class="card-header theme-border-color rounded-0">
                  {{$speech->designation}}
                </div> 
                <div class="card-body">
                  <img class="hm-img" src="{{asset(trans('global.links.speech_show').$speech->files)}}" alt="Head Master Image">
                  <p class="hm-name">{{$speech->name}}</p>
                  @if(! empty($speech->sortSpeech))
                  <!-- <div class="hm-message text-justify">$speech->sortSpeech</div> -->
                  <a href="{{route('frontend.speech', $speech)}}" class="hm-details">{{$speech->sortSpeech}} <i class="fas fa-long-arrow-alt-right"></i></a>
                  @endif
                </div>
              </div>
            @endif
          <!-- End Message of Head Master -->
          @endforeach

          <!-- Start Notice -->
          <div class="card mt-3 rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0">
              নোটিশ
            </div>
            @if(empty($notices[0]))
            <div class="text-center pt-5 pb-5">{{trans('global.nodata')}}</div>
            @else
            <div class="card-body">
              <ul class="notice-list">
                @foreach ($notices as $key => $notice) 
                <li><a href="{{route('frontend.notice.show', $notice)}}"><i class="far fa-hand-point-right"></i><div class="text-justify">{{$notice->title}}</div></a></li>
                @endforeach
 
              </ul>
              <a href="{{route('frontend.notice')}}" class="hm-details">সকল নোটিশ <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
            @endif
          </div>
          <!-- End Notice -->

        </div>