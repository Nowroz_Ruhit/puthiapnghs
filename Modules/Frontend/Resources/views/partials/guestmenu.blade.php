<!--Section NavBar -->
{{dd($bootDesignation)}}
    <section id="mainNav">

        <nav class="navbar navbar-expand-lg navbar-light">

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav m-auto py-4 py-md-0 nav">
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4 active">
                <a class="nav-link " href="{{route('index')}}">হোম</a>
              </li>              
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="{{route('about')}}">আমাদের সম্পর্কে</a>
              </li>
              <!-- start about -->
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">একাডেমিক
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <!-- academicCategory -->
                  @foreach ($academicCategory as $key => $value)
                  <a class="dropdown-item" href="{{route('frontend.academic', $value->title)}}">{{$value->title}}</a>
                  @endforeach
                </div>
              </li>
              <!-- end about -->
              <!-- start academic -->
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">একাডেমিক
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <!-- academicCategory -->
                  @foreach ($academicCategory as $key => $value)
                  <a class="dropdown-item" href="{{route('frontend.academic', $value->title)}}">{{$value->title}}</a>
                  @endforeach
                </div>
              </li>
              <!-- end academic -->
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">শিক্ষক ও কর্মচারী
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  @foreach ($bootDesignation as $key => $designation) 
                  <a class="dropdown-item" href="{{route('frontend.employee','1')}}">{{$designation->designation}}</a>
                  @endforeach
                  <!-- <a class="dropdown-item" href="{{route('frontend.employee','2')}}">স্কুল কর্মচারী</a> -->
                </div>
              </li>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">নোটিশ
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="{{route('frontend.notice')}}">নোটিশ</a>
                  <a class="dropdown-item" href="{{route('frontend.news')}}">নিউজ</a>
                </div>
              </li>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="{{route('frontend.result')}}">ফলাফল</a>
              </li>              
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="{{route('gallery')}}">ফটো গ্যালারী</a>
              </li>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="{{route('contact')}}">যোগাযোগ</a>
              </li>
            </ul>
          </div>
        </nav>
    </section>
    <!-- End Section NavBar