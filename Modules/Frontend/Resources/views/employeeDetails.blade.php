@extends('frontend::layouts.guest')

@section('content')

<!-- Start Breadcrumb -->
<nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
	<ol class="breadcrumb rounded-0">
		<li class="breadcrumb-item"><a href="#">হোম</a></li>
		<li class="breadcrumb-item"><a href="#">শিক্ষক ও কর্মচারী</a></li>
		<li class="breadcrumb-item"><a href="{{route('frontend.employee.list', [$type ,$in])}}">{{$type}}</a></li>
		<li class="breadcrumb-item active" aria-current="page">{{$employee->name}}</li>
	</ol>
</nav>
<!-- End Breadcrumb -->

<section class="site-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="row justify-content-center p-3">
					<div class="col-lg-3 text-center">
						<div>
							<img src="{{asset(trans('global.links.employee_show').$employee->files)}}" class="card-img-top" alt="{{$employee->name}}">
						</div>
					</div>
				</div>
				<div class="card-body news-ticker">
					<h5 class="card-title text-center">{{$employee->name}}</h5>
				</div>
				<ul class="list-group list-group-flush">
					<li class="list-group-item">পদবী: {{$employee->designation->designation}}</li>
					<li class="list-group-item">Join Date: {{$employee->joindate}}</li>
					<li class="list-group-item">MPO Date: {{$employee->mpo}}</li>
					<li class="list-group-item">Mobile: +88{{$employee->phone}}</li>
					<li class="list-group-item">Mail: {{$employee->email}}</li>
					<li class="list-group-item">Address: {{$employee->address}}</li>

				</ul>
			</div>
			<div class="mt-4">
				{!! $employee->others !!}
			</div>
			
		</div>

	</div>
</section>


@endsection