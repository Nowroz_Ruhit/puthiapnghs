@extends('frontend::layouts.guest')

@section('content')
	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">ফটো গ্যালারী</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ $gallery->title && $gallery->photo ? $gallery->title : $gallery->news->title ?? '' }}</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

  <section class="site-content">
    <div class="row">
      <div class="col-sm-12">
        <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              ফটো গ্যালারী / 
              {{ $gallery->title && $gallery->photo ? $gallery->title : $gallery->news->title ?? '' }}
            </div>

            <div class="card-body teacher-list">

              <center class="col-12">
                <img src="{{ asset('public/uplodefile/news/'.$gallery->news->photo)}}" class="space-ima1" alt="Teacher Images" style="max-width: 100%;">
              </center>

              <div class="row">
                <div class="container mt-5">
                  <ul class="row first">
                   @foreach ($photos as $key => $photo) 
                   <li class="shadow-lg p-3 mb-5 bg-white rounded">    
                    <img alt="Rocking the night away"  src="{{ asset('public/uplodefile/photos/'.$photo->photo)}}">
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>

              <!-- Start Pagination -->
              <div class="row"> 
                <div class="col-sm-12">
                  <nav aria-label="Page navigation example">
                    
                  </nav>
                </div>
              </div>
              <!-- End Pagination -->
            </div>

        </div>
        </div>
      </div>
    </section>

    

@endsection

@section('styles')
@parent
<link href="{{ asset('Modules/PhotoGallery/Resources/assets/css/jquery.bsPhotoGallery.css')}}" rel="stylesheet">
@endsection

@section('scripts')
@parent
<script src="{{ asset('Modules/PhotoGallery/Resources/assets/js/jquery.bsPhotoGallery.js')}}"></script> 
<script>
  $(document).ready(function(){
    $('ul.first').bsPhotoGallery({
      "classes" : "col-xl-3 col-lg-2 col-md-4 col-sm-4",
      "hasModal" : true,
      "shortText" : false  
    });
  });
</script>
@endsection