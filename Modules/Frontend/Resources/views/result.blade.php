@extends('frontend::layouts.guest')

@section('content')


    <!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item active" aria-current="page">ফলাফল</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

	<section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              ফলাফল
            </div>
            <div class="card-body">
              	<div class="table-responsive syllabus-table">
				  	<table class="table">
					   	<thead class="thead-light">
							<tr>
								<th scope="col">SL. No.</th>
								<th scope="col">Class</th>
								<th scope="col">Published Date</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">01</th>
								<td>Class 01</td>
								<td>01/01/2020</td>
								<td>
									<a class="btn btn-outline-success" href="#"><i class="far fa-eye"></i> View</a>
									<a class="btn btn-outline-warning" href="#"><i class="fas fa-download"></i> Download</a>	
								</td>
							</tr>
							<tr>
								<th scope="row">02</th>
								<td>Class 02</td>
								<td>01/01/2020</td>
								<td>
									<a class="btn btn-outline-success" href="#"><i class="far fa-eye"></i> View</a>
									<a class="btn btn-outline-warning" href="#"><i class="fas fa-download"></i> Download</a>						
								</td>
							</tr>
						</tbody>
				  	</table>
				</div>				              
            </div>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>



@endsection