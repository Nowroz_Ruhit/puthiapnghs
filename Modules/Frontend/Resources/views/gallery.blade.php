@extends('frontend::layouts.guest')

@section('styles')
@parent
<!-- <link href="{{ asset('Modules/PhotoGallery/Resources/assets/css/jquery.bsPhotoGallery.css')}}" rel="stylesheet"> -->
@endsection

@section('scripts')
@parent
<!-- <script src="{{ asset('Modules/PhotoGallery/Resources/assets/js/jquery.bsPhotoGallery.js')}}"></script>	
<script>
	$(document).ready(function(){
		$('ul.first').bsPhotoGallery({
			"classes" : "col-xl-3 col-lg-2 col-md-4 col-sm-4",
			"hasModal" : false,
			"shortText" : false  
		});
	});
</script> -->
@endsection

@section('content')

    
    <!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item active" aria-current="page">ফটো গ্যালারী</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

    <section class="site-content">
        <div class="row">
          <div class="col-sm-12">
            <!-- Start Welcome or About Text -->
              <div class="card rounded-0 theme-border theme-shadow">
                <div class="card-header theme-border-color rounded-0 theme-bg">
                  ফটো গ্যালারী
                </div>
                <!-- Teacher List -->
                <div class="card-body teacher-list">
                  @if(empty($album[0]))
                  <div class="text-center pt-5 pb-5">{{trans('global.nodata')}}</div>
                  @else
                  <div class="row">
                    <!--  -->
                    
                    @foreach ($album as $key => $gallery)
                    <div class="col-lg-4 col-md-6 mb-4">
                      <div class="card">
                        <img src="{{ $gallery->title && $gallery->photo ? asset('public/uplodefile/gallery/'.$gallery->photo) : asset('public/uplodefile/news/'.$gallery->news->photo) ?? '' }}" class="card-img-top" alt="Teacher Images">
                        <div class="card-body">
                          <p class="text-center mb-0"><a href="{{route('frontend.gallery.view', $gallery)}}">{{ $gallery->title && $gallery->photo ? $gallery->title : $gallery->news->title ?? '' }}</a></p>
                  
                        </div>
                      </div>
                    </div>
                    @endforeach



                    <!--  -->
<!--                     <div class="col-lg-4 col-md-6 mb-4">
                      <div class="card">
                        <img src="{{asset('public/guest-user/images/hm.jpg')}}" class="card-img-top" alt="Teacher Images">
                        <div class="card-body">
                          <p class="text-center mb-0"><a href="{{route('frontend.gallery.view', 1)}}">জাতীয় শিক্ষা সপ্তাহ-২০১৮</a></p>                   
                        </div>
                      </div>
                    </div> -->
<!--                     <div class="col-lg-4 col-md-6 mb-4">
                      <div class="card">
                        <img src="{{asset('public/guest-user/images/hm.jpg')}}" class="card-img-top" alt="Teacher Images">
                        <div class="card-body">
                          <p class="text-center mb-0"><a href="{{route('frontend.gallery.view', 1)}}">জাতীয় শিক্ষা সপ্তাহ-২০১৮</a></p>                   
                        </div>
                      </div>
                    </div> -->
<!--                     <div class="col-lg-4 col-md-6 mb-4">
                      <div class="card">
                        <img src="{{asset('public/guest-user/images/hm.jpg')}}" class="card-img-top" alt="Teacher Images">
                        <div class="card-body">
                          <p class="text-center mb-0"><a href="{{route('frontend.gallery.view', 1)}}">জাতীয় শিক্ষা সপ্তাহ-২০১৮</a></p>                   
                        </div>
                      </div>
                    </div> -->
                  </div>

                  <!-- Start Pagination -->
                  <div class="row"> 
                    <div class="col-sm-12">
                      <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                          {{ $album->links() }}
                          <!-- <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
                          </li> -->
                          <!-- <li class="page-item active"><a class="page-link" href="#">1 </a></li>
                          <li class="page-item"><a class="page-link" href="#">2</a></li> -->
                          <!-- <li class="page-item"><a class="page-link" href="#">3</a></li> -->
                          <!-- <li class="page-item">
                            <a class="page-link" href="#">Next</a>
                          </li> -->
                        </ul>
                      </nav>
                    </div>
                  </div>
                  <!-- End Pagination -->
                  @endif
                </div>
                <!-- End Teacher List -->
            </div>
        </div>
      </div>
    </section>
    

@endsection