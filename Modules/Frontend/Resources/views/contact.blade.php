<?php
use Modules\Frontend\Http\Controllers\FrontendController;
?>
@extends('frontend::layouts.guest')
@push('css')
<!-- <link href="{{ asset('css/aire.css') }}" rel="stylesheet" /> -->
@endpush
@section('content')

      

<!-- <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script> -->
    <!-- Start Breadcrumb -->
    <!-- <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script> -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item active" aria-current="page">যোগাযোগ</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

    <section class="content-rotate mt-3">
      <div class="row">

{!! FrontendController::sideBar()!!}
 

        <div class="col-lg-9 right-side-content">

          <article class="">
            <div class="entry-header custom-box">
              <h2 class="entry-title page-title">যোগাযোগ</h2> 
                </div>
          <div class="entry-content mh-clearfix">
            <div class="custom-box-shadow fild-box">
                        <p><i class="fas fa-map-marker-alt"></i>{{$bootGeneral->address}}</p>
                        <p><i class="fas fa-mobile-alt"></i> Mobile: {{$bootGeneral->phone}}</p>
                        <p><i class="fas fa-phone-square"></i> Telephone:  {{$bootGeneral->tel_phone}}</p>
                        <p><i class="fas fa-envelope"></i> Email: {{$bootGeneral->email}}</p>
                    </div>
          </div>
          </article>
            <div class="custom-box custom-box-shadow" >
                <h2>আমাদের ইমেইল করুন</h2>
            </div>

            <div class="contact-form">
                <form action="{{route('frontend.email')}}" method="post">
                  @csrf
                  <div class="form-group">
                    <label>আপনার নাম <span class="span-red">*</span></label>
                    <input type="text" name="name" class="form-control" placeholder="আপনার নাম লিখুন">
                  </div>
                  <div class="form-group">
                    <label>ইমেইল আইডি <span class="span-red">*</span></label>
                    <input type="email" name="form" class="form-control" placeholder="ইমেইল আইডি লিখুন">
                  </div>
                  <div class="form-group">
                    <label>মোবাইল নাম্বার <span class="span-red">*</span></label>
                    <input type="text" name="phone" class="form-control" placeholder="মোবাইল নাম্বার লিখুন">
                  </div>
                  <div class="form-group">
                    <label>টেলিফোন নাম্বার</label>
                    <input type="text" name="tel_phone" class="form-control" placeholder="টেলিফোন নাম্বার লিখুন">
                  </div>
                  <div class="form-group">
                    <label>বিষয় <span class="span-red">*</span></label>
                    <input type="text" name="subject" class="form-control" placeholder="বিষয় লিখুন">
                  </div>
                  <div class="form-group">
                    <label>বিস্তারিত <span class="span-red">*</span></label>
                    <textarea class="form-control" id="detail" name="detail" rows="4" placeholder="বিস্তারিত লিখুন"></textarea>
                    <!-- <script>
                        // CKEDITOR.replace( 'detail' );
                    </script> -->
                    <input type="hidden" name="type" value="contact">
                  </div>
                  <button type="submit" class="btn custom_btn">সাবমিট করুন</button>
                </form>
            </div>
        </div>

        

      </div>
    </section>




@endsection

