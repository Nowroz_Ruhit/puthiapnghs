
@extends('frontend::layouts.guest')

@section('content')

	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="{{route('index')}}">হোম</a></li>
        <li class="breadcrumb-item"><a href="{{route('frontend.notice')}}">নোটিশ</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$notice->title}}</li>
      </ol>
    </nav>

	<section class="site-content">
    <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
            {{$notice->title}}
            </div>
            <div class="card-body">
              <p>
              	{!! $notice->discription!!}
              </p>
            @if(substr($notice->file, -3)=='pdf')
              <iframe src="{{asset('public/uplodefile/notice/'.$notice->file)}}" height="750" width="100%"></iframe>
            @elseif((substr($notice->file, -3)=='png' )|| (substr($notice->file, -3)=='jpg') || (substr($notice->file, -3)=='JPEG') )
                <img src="{{asset('public/uplodefile/notice/'.$notice->file)}}" width="100%"/>
            @endif
            </div>
          </div>
        </div>
      </div>
  </section>




@endsection