@extends('frontend::layouts.guest')

@section('content')


    <!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">একাডেমিক</a></li>
        <li class="breadcrumb-item active" aria-current="page">নোটিশ</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

	<section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              নোটিশ
            </div>
            <div class="card-body">
            	@if(empty($notices[0]))
	            <div class="text-center pt-5 pb-5">{{trans('global.nodata')}}</div>
	            @else
              	<div class="table-responsive syllabus-table">
				  	<table class="table">
					   	<thead class="thead-light">
							<tr>
								<th scope="col">SL. No.</th>
								<th scope="col">Notice Title</th>
								<th scope="col">Published Date</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($notices as $key => $notice)
							<tr>
								<th scope="row">{{$key+1}}</th>
								<td>{{$notice->title}}</td>
								<td>{{$notice->updated_at->format('d/m/y')}}</td>
								<td>
									@if($notice->file)
									<a class="btn btn-outline-success" href="{{route('frontend.notice.show', $notice)}}"><i class="far fa-eye"></i> View</a>
									<a class="btn btn-outline-warning" href="{{route('frontend.notice_download', $notice )}}"><i class="fas fa-download"></i> Download</a>
									@endif	
								</td>
							</tr>
							@endforeach
						</tbody>
				  	</table>
				  	<!-- Start Pagination -->
		            	<div class="row">	
		            		<div class="col-sm-12">
		            			<nav aria-label="Page navigation example">
									<ul class="pagination justify-content-center">
										{{ $notices->links() }}
									</ul>
								</nav>
		            		</div>
		            	</div>
		            	<!-- End Pagination -->
		            	
				</div>
				@endif				              
            </div>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>



@endsection