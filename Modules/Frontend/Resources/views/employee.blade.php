@extends('frontend::layouts.guest')

@section('content')
	
	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">শিক্ষক ও কর্মচারী</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$institute->institute}}</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

    <section class="site-content">
      	<div class="row">
	        <div class="col-sm-12">
	        	@if(empty($employees[0]))
	            <div class="text-center pt-5 pb-5">{{trans('global.nodata')}}</div>
	            @else
	          <!-- Start Welcome or About Text -->
	          	<div class="card rounded-0 theme-border theme-shadow">
		            <div class="card-header theme-border-color rounded-0 theme-bg">
		            শিক্ষক ও কর্মচারী
		            </div>
		            <!-- Teacher List -->
		            <div class="card-body teacher-list">
		            	<div class="row">
		            		@foreach ($employees as $key => $employee)
		            		<div class="col-lg-4 col-md-6 mb-4">
		            			<div class="card">
									<img src="{{asset(trans('global.links.employee_show').$employee->files)}}" class="card-img-top" alt="Teacher Images">
									<div class="card-body">
										<h5 class="card-title">{{$employee->name}}</h5>
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">পদবী: {{$employee->designation->designation}}</li>
										<li class="list-group-item">Join Date: {{$employee->joindate}}</li>
										<li class="list-group-item">MPO Date: {{$employee->mpo}}</li>
										<li class="list-group-item">Mobile: +88{{$employee->phone}}</li>
										<li class="list-group-item">Mail: {{$employee->email}}</li>
										<li class="list-group-item">Address: {{$employee->address}}</li>
										@if($employee->others)
										<li class="list-group-item text-center news-ticker">
											<a href="{{route('frontend.employee.details', [$institute, $type, $employee])}}" style="text-decoration: none;">বিস্তারিত</a>
										</li>
										@endif
									</ul>
								</div>
		            		</div>
		            		@endforeach
<!--  -->
<!-- 		            		<div class="col-lg-4 col-md-6 mb-4">
		            			<div class="card">
									<img src="{{asset('public/guest-user/images/hm.jpg')}}" class="card-img-top" alt="Teacher Images">
									<div class="card-body">
										<h5 class="card-title">Dr. Md. Khaza Khaled Lizer</h5>
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">Education: MSC in Mathetatics, BABT</li>
										<li class="list-group-item">Join Date: 01-06-1989</li>
										<li class="list-group-item">Mobile: +880 01913 800800</li>
										<li class="list-group-item">Mail: teachername@domainname.com</li>
										<li class="list-group-item">Address: 22, Hatemkhan (Gorosthan Mor), Rajshahi-6000, Bangladesh.</li>
									</ul>
								</div>
		            		</div>
		            		<div class="col-lg-4 col-md-6 mb-4">
		            			<div class="card">
									<img src="{{asset('public/guest-user/images/hm.jpg')}}" class="card-img-top" alt="Teacher Images">
									<div class="card-body">
										<h5 class="card-title">Dr. Md. Khaza Khaled Lizer</h5>
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">Education: MSC in Mathetatics, BABT</li>
										<li class="list-group-item">Join Date: 01-06-1989</li>
										<li class="list-group-item">Mobile: +880 01913 800800</li>
										<li class="list-group-item">Mail: teachername@domainname.com</li>
										<li class="list-group-item">Address: 22, Hatemkhan (Gorosthan Mor), Rajshahi-6000, Bangladesh.</li>
									</ul>
								</div>
		            		</div>
		            	</div> -->
</div>
<!-- End Teacher List -->
		            	<!-- Start Pagination -->
		            	<div class="row">	
		            		<div class="col-sm-12">
		            			<nav aria-label="Page navigation example">
									 <ul class="pagination justify-content-center">
									<!--	<li class="page-item disabled">
											<a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
										</li>
										<li class="page-item active"><a class="page-link" href="#">1 </a></li>
										<li class="page-item"><a class="page-link" href="#">2</a></li>
										<li class="page-item"><a class="page-link" href="#">3</a></li>
										<li class="page-item">
											<a class="page-link" href="#">Next</a>
										</li>-->
										{{ $employees->links() }}
									</ul> 

								</nav>
		            		</div>
		            	</div>
		            	<!-- End Pagination -->
		            
	       		</div>
	    	</div>
	    	@endif
    	</div>
	</section>


@endsection