@extends('frontend::layouts.guest')

@section('content')

	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="{{route('index')}}">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">একাডেমিক</a></li>
        <li class="breadcrumb-item"><a href="{{route('frontend.academic', $type)}}">{{$type}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$academic->title}}</li>
      </ol>
    </nav>

	<section class="site-content">
    <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              {{$type}} > {{$academic->title}}
            </div>
            <div class="card-body">
              {!! $academic->discription!!}
<!--               <p>বাংলাদেশে মাধ্যমিক শিক্ষার অগ্রযাত্রায় রাজশাহী কলেজিয়েট স্কুল ঐতিহ্যবাহী এবং পথিকৃৎ শিক্ষা প্রতিষ্ঠান। বৃটিশ শাসনামল থেকেই রাজশাহী উত্তরবঙ্গের শিক্ষানগরী নামে পরিচিতি লাভ করে। ইংরেজি শিক্ষার প্রসারের লক্ষ্যে গভর্নর জেনারেল লর্ড উইলিয়াম বেন্টিংক এর সহায়তায় বিদ্যোৎসাহী ব্যক্তি ও ইংরেজ কোম্পানির যৌথ উদ্যোগে ১৮২৮ খ্রিস্টাব্দে রাজশাহী শহরের উপকন্ঠে বেসরকারিভাবে ‘বুয়ালিয়া ইংলিশ স্কুল’ প্রতিষ্ঠিত হয়।</p> -->
              <!-- <iframe src="{{asset('public/guest-user/images/domain_invoice_528430.pdf')}}" height="750" width="100%"></iframe> -->

              <iframe src="{{asset(trans('global.links.academic_show').$academic->file)}}" height="750" width="100%"></iframe>
            </div>
          </div>
        </div>
      </div>
  </section>




@endsection