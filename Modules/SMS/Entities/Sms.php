<?php

namespace Modules\SMS\Entities;

use Illuminate\Database\Eloquent\Model;

class sms extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
