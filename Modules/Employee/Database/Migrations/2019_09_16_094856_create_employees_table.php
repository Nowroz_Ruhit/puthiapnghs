<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->integer('designation_id')->unsigned();
            $table->integer('religion_id')->unsigned();
            $table->integer('employee_type_id')->unsigned();
            $table->integer('institute_id')->unsigned();
            $table->string('address')->nullable();
            $table->string('files');
            $table->string('joindate')->nullable();
            $table->string('employee_order')->nullable();
            $table->timestamps();

            $table->foreign('religion_id')
            ->references('id')->on('religions')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('designation_id')
            ->references('id')->on('designations')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('employee_type_id')
            ->references('id')->on('employee_types')
            ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('institute_id')
            ->references('id')->on('institutes')
            ->onUpdate('cascade')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        // Schema::table('employees', function (Blueprint $table) {
        //     $table->dropForeign('designation_id');
        //     $table->dropForeign('religion_id');
        // });
        Schema::dropIfExists('employees');

    }
}
