<?php

namespace Modules\Employee\Database\Seeders;


use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Modules\{
    Employee\Entities\Religion,
    Employee\Entities\Designation,
    Employee\Entities\EmployeeType,
    Employee\Entities\Institute,
};

class SeedFakeEmployeeRelisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();

        // $this->call("OthersTableSeeder");
        $institute = [
            [
                'id'         => '1',
                'institute'=> 'স্কুল',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '2',
                'institute'=> 'কলেজ',
                'created_at' => '2019-04-15 19:15:42',
                'updated_at' => '2019-04-15 19:15:42',
            ],
            
        ];
        Institute::insert($institute);

        $type = [
            [
                'id'         => '1',
                'institute_id' => '1',
                'type'=> 'শিক্ষক',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '2',
                'institute_id' => '1',
                'type'=> 'কর্মচারী',
                'created_at' => '2019-04-15 19:15:42',
                'updated_at' => '2019-04-15 19:15:42',
            ],
            
        ];
        EmployeeType::insert($type);


        $religions = [
            [
                'id'         => '1',
                'title'      => 'ইসলাম',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '2',
                'title'      => 'হিন্দু',
                'created_at' => '2019-04-15 19:15:42',
                'updated_at' => '2019-04-15 19:15:42',
            ],
            [
                'id'         => '3',
                'title'      => 'খ্রিস্টান',
                'created_at' => '2019-04-15 19:16:42',
                'updated_at' => '2019-04-15 19:16:42',
            ],
            [
                'id'         => '4',
                'title'      => 'বৌদ্ধ',
                'created_at' => '2019-04-15 19:16:42',
                'updated_at' => '2019-04-15 19:16:42',
            ],
            [
                'id'         => '5',
                'title'      => 'অন্য',
                'created_at' => '2019-04-15 19:16:42',
                'updated_at' => '2019-04-15 19:16:42',
            ],
        ];
        Religion::insert($religions);

        $designation = [
            [
                'id'         => '1',
                'institute_id' => '1',
                'types_id'     => '1',
                'designation'=> 'প্রধান শিক্ষক',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '2',
                'institute_id' => '1',
                'types_id'     => '1',
                'designation'=> 'সহকারী শিক্ষক',
                'created_at' => '2019-04-15 19:15:42',
                'updated_at' => '2019-04-15 19:15:42',
            ],
            [
                'id'         => '3',
                'institute_id' => '1',
                'types_id'     => '1',
                'designation'=> 'শিক্ষক',
                'created_at' => '2019-04-15 19:16:42',
                'updated_at' => '2019-04-15 19:16:42',
            ],
        ];
        Designation::insert($designation);

        
        
    }
}
