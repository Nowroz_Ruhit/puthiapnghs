<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::prefix('employee')->group(function() {
/*
    Route::get('/', 'EmployeeController@index');
    Route::get('/',['as'=>'employee.index','uses'=>'EmployeeController@index']);
    Route::get('create',['as'=>'employee.create','uses'=>'EmployeeController@create']);
    Route::get('edit/{id}',['as'=>'employee.edit','uses'=>'EmployeeController@edit']);
    Route::get('show/{id}',['as'=>'employee.show','uses'=>'EmployeeController@show']);
*/
Route::prefix('admin')->group(function() {
    Route::resource('employee', 'EmployeeController');
    
    Route::get('/designation/create',[
        'as'=>'employee.designation.create',
        'uses'=>'EmployeeController@designationCreate'
    ]);
    Route::get('designation',[
        'as'=>'employee.designation.index',
        'uses'=>'EmployeeController@designationIndex'
    ]);
    Route::post('designation/store',[
        'as'=>'employee.designation.store',
        'uses'=>'EmployeeController@designationStore'
    ]);
    Route::get('designations/edit/{designation}',[
        'as'=>'employee.designation.edit',
        'uses'=>'EmployeeController@designationEdit'
    ]);
    Route::post('designation/update/{designation}',[
        'as'=>'employee.designation.update',
        'uses'=>'EmployeeController@designationUpdate'
    ]);
    Route::delete('designation/delete/{designation}',['as'=>'employee.designation.delete','uses'=>'EmployeeController@designationDelete']);


    Route::post('employee/position-change/{designation}',[
        'as'=>'employee.position.change',
        'uses'=>'EmployeeController@updatePosition'
    ]);


Route::post('employee_type',['as'=>'employee.employee_type','uses'=>'EmployeeController@employee_type']);
Route::post('employee_designation',['as'=>'employee.employee_designation','uses'=>'EmployeeController@employee_designation']);

});