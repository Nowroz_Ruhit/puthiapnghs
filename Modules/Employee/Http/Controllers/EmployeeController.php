<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;

// use Modules\Employee\Entities\Religion;
// use Modules\Employee\Entities\Designation;
// use Modules\Employee\Entities\{
use Modules\Employee\Entities\Designation;
use Modules\Employee\Entities\Employee;
use Modules\Employee\Entities\EmployeeType;
use Modules\Employee\Entities\Religion;
use Modules\Employee\Entities\Institute;
// };

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('employee_access'), 403);
        $data = Employee::all();
        return view('employee::index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('employee_create'), 403);
        $data = Religion::all();
        $religion = [];
        $religion[NULL] = 'Select Religion';
        foreach ($data as $key => $value) {
            $religion[$value['id']] = $value['title'];
        }
        // $data1 = Designation::all();
        // $designation[NULL] = 'Select Designation';
        // foreach ($data1 as $key => $value) {
        //     $designation[$value['id']] = $value['designation'];
        // }
        // $data2 = EmployeeType::all();
        // $type[NULL] = 'Select Employee Type';
        // foreach ($data2 as $key => $value) {
        //     $type[$value['id']] = $value['type'];
        // }
        $data3 = Institute::all();
        $institute[NULL] = 'Select Institute';
        foreach ($data3 as $key => $value) {
            $institute[$value['id']] = $value['institute'];
        }
        return view('employee::create', compact('religion', 'institute'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('employee_create'), 403);
        request()->validate([
            'name' => 'required',
            'designation_id' => 'required',
            'religion_id' => 'required',
            'files' => 'image|mimes:jpeg,png,jpg,gif,svg',

        ]);
        $data = $request->all();
        $data['created_by'] = Auth::id();

        if ($request->hasFile('files')){ 
            // $imageName = request()->logo->getClientOriginalName();
            $imageName = now()->format('Y-m-d-H-i-s').'.'.request()->file('files')->getClientOriginalExtension();
            // $request->file('upload')->getClientOriginalExtension()
            // request()->file('files')->move(public_path('img/employee'), $imageName);
            request()->file('files')->move(public_path(trans('global.links.employee')), $imageName);
            $image = Image::make(public_path(trans('global.links.employee')."{$imageName}"))->resize(450, 450);
            $image->save();

            $data['files'] = $imageName;
        }

        if (! ($request->has('phone'))){
            $data['phone'] = '';
        }
        if (!($request->has('address'))){
            $data['address'] = '';
        }
        if (!($request->has('joindate'))){
            $data['joindate'] = '';
            // Carbon::createFromFormat('d/m/Y', $request->stockupdate)->format('Y-m-d');
        }

        $data = Employee::create($data);
        if($data){
            return redirect()->route('employee.index')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('employee.index')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Employee $employee)
    {
        abort_unless(\Gate::allows('employee_show'), 403);
        return view('employee::show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Employee $employee)
    {
        abort_unless(\Gate::allows('employee_edit'), 403);
        
        $data = Religion::all();
        $religion = [];
        $religion[NULL] = 'Select Religion';
        foreach ($data as $key => $value) {
            $religion[$value['id']] = $value['title'];
        }
        $data1 = Designation::all();
        $designation[NULL] = 'Select Designation';
        foreach ($data1 as $key => $value) {
            $designation[$value['id']] = $value['designation'];
        }
        $data2 = EmployeeType::all();
        $type[NULL] = 'Select Employee Type';
        foreach ($data2 as $key => $value) {
            $type[$value['id']] = $value['type'];
        }
        $data3 = Institute::all();
        $institute[NULL] = 'Select Institute';
        foreach ($data3 as $key => $value) {
            $institute[$value['id']] = $value['institute'];
        }
        return view('employee::edit', compact('employee','religion','designation', 'type', 'institute'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Employee $employee)
    {
        abort_unless(\Gate::allows('employee_edit'), 403);
        //dd($request);
        request()->validate([
            'name' => 'required',
            'designation_id' => 'required',
            'religion_id' => 'required',
            // 'files' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        $data = $request->all();
        $data['updated_by'] = Auth::id();
// dd($request);
        if ($request->hasFile('files')){ 
            if(file_exists(public_path(trans('global.links.employee')).$employee->files))
            {
                unlink(public_path(trans('global.links.employee')).$employee->files);
            }
            $imageName = now()->format('Y-m-d-H-i-s').'.'.request()->file('files')->getClientOriginalExtension();
            // $imageName = now()->format('Y-m-d-H-i-s').'-'.request()->input('name').'.'.request()->file('files')->getClientOriginalExtension();
            $data['files'] = $imageName;
            // request()->file('files')->move(public_path('img/employee'), $imageName);
            request()->file('files')->move(public_path(trans('global.links.employee')), $imageName);
            $image = Image::make(public_path(trans('global.links.employee')."{$imageName}"))->resize(450, 450);

            $image->save();

            
        }

        $dat = $employee->update($data);
        // return redirect()->route('employee.index');
        // $data = Employee::create($data);
        if($dat){
            return redirect()->route('employee.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('employee.index')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Employee $employee)
    {
        abort_unless(\Gate::allows('employee_delete'), 403);
        // $employee->delete();
        // return redirect()->route('employee.index');
        $file= $employee->files;
        if($employee->delete())
        {
            $filename = public_path(trans('global.links.employee')).$file;
             if(\File::exists($filename)){
                 \File::delete($filename);
            }
           return redirect()->route('employee.index')->with('delete', 'Delete Successfull');
        }else {
            return redirect()->route('employee.index')->with('error', 'Not Deleted, Somtheing waswrong');
        }
    }

    public function designationIndex()
    {
        abort_unless(\Gate::allows('employee_designation_access'), 403);
        $data = Designation::all();
        return view('employee::designation.show', compact('data'));
    }

    public function designationCreate()
    {
        abort_unless(\Gate::allows('employee_designation_create'), 403);
        $data2 = EmployeeType::all();
        $type[NULL] = 'Select Employee Type';
        foreach ($data2 as $key => $value) {
            $type[$value['id']] = $value['type'];
        }
        $data3 = Institute::all();
        $institute[NULL] = 'Select Institute';
        foreach ($data3 as $key => $value) {
            $institute[$value['id']] = $value['institute'];
        }
        return view('employee::designation.index', compact('type', 'institute'));
    }


    public function designationStore(Request $request)
    {
        abort_unless(\Gate::allows('employee_designation_create'), 403);
        // return view('employee::designation.index');
        request()->validate([            
            'designation' => 'required',
        ]);
        $data = $request->all();
        $data['created_by'] = Auth::id();
        $dis = Designation::create($data);
        // return redirect()->route('employee.designation.index');
        if($dis){
            return redirect()->route('employee.designation.index')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('employee.designation.index')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    public function designationEdit(Designation $designation)
    {
        abort_unless(\Gate::allows('employee_designation_create'), 403);
        $data2 = EmployeeType::all();
        $type[NULL] = 'Select Employee Type';
        foreach ($data2 as $key => $value) {
            $type[$value['id']] = $value['type'];
        }
        $data3 = Institute::all();
        $institute[NULL] = 'Select Institute';
        foreach ($data3 as $key => $value) {
            $institute[$value['id']] = $value['institute'];
        }
        return view('employee::designation.edit', compact('designation', 'type', 'institute'));
    }

    public function designationUpdate(Request $request, Designation $designation)
    {
        abort_unless(\Gate::allows('employee_designation_edit'), 403);
        request()->validate([
            'designation' => 'required',
        ]);
        $data = $request->all();
        $data['updated_by'] = Auth::id();
        $dis = $designation->update($data);
        // return redirect()->route('employee.designation.index');
        if($dis){
            return redirect()->route('employee.designation.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('employee.designation.index')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    public function designationDelete(Designation $designation)
    {
        abort_unless(\Gate::allows('employee_designation_delete'), 403);
        $designation->delete();
        return redirect()->route('employee.designation.index');
    }

    public function employee_type(Request $request)
    {

        $data = $request->get('value');
        $data2 = Institute::find($data)->employeeType;
        $result= "<option value='NULL'>Select Employee Type</option>";
        foreach ($data2 as $key => $value) {
            // $type[$value['id']] = $value['type'];
            $result.="<option value=".$value['id'].">".$value['type']."</option>";
        }
        return $result;
    }

    public function employee_designation(Request $request)
    // public function employee_designation()
    {
        $data = $request->get('value');
        $data2 = EmployeeType::find($data)->emp_designation;
        // dd($data2);
        $result= "<option value='NULL'>Select Designation</option>";
        foreach ($data2 as $key => $value) {
            $result.="<option value=".$value['id'].">".$value['designation']."</option>";
        }
        return $result;
        // return $data2;
    }

    public function updatePositionView()
    {
        abort_unless(\Gate::allows('employee_edit'), 403);
        $data = Employee::orderBy('employee_order', 'ASC')->get();
        return view('employee::position',compact('data'));
    }


    public function updatePosition(Request $request)
    {
        abort_unless(\Gate::allows('employee_edit'), 403);


        $position = $request->position;
        $i=1;
        foreach($position as $k=>$v){
            Employee::where('id',$v)->update(array('employee_order'=>$i++));
            $i++;
        }
        return response()->json($position);
    }
}
