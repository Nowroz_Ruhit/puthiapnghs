<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Employee\Entities\Institute;
use Modules\Employee\Entities\Designation;

class EmployeeType extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }

    public function emp_designation()
    {
        return $this->hasMany(Designation::class);
    }
}
