<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Employee\Entities\EmployeeType;

class Institute extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function employeeType()
    {
        return $this->hasMany(EmployeeType::class);
    }
}
