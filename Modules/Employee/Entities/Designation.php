<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Employee\Entities\EmployeeType;
use Modules\Employee\Entities\Institute;

class Designation extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }

    public function employeeType()
    {
        return $this->belongsTo(EmployeeType::class);
    }

}
