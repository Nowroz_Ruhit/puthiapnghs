<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;
// use Modules\Employee\Entities\;
use Modules\Employee\Entities\{
    Designation,
    Religion,
    Institute,
    EmployeeType
};

class Employee extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function religion()
    {
        return $this->belongsTo(Religion::class, 'religion_id', 'id');
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class, 'designation_id', 'id');
    }

    public function institute()
    {
        return $this->belongsTo(Institute::class);
    }

    public function employeeType()
    {
        return $this->belongsTo(EmployeeType::class);
    }

}
