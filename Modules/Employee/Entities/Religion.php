<?php

namespace Modules\Employee\Entities;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

}
