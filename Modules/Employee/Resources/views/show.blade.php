@extends('layouts.admin')
@push('css')
@endpush

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Employee</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.employe.title_singular') }} {{ trans('global.employe.fields.profile') }}
      @can('product_create')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{route('employee.index')}}">
          <i class="fas fa-user nav-icon"></i>  {{ trans('global.employe.title') }} {{ trans('global.list') }}
        </a>
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{route('employee.edit',$employee)}}">
          <i class="fas fa-edit nav-icon"></i>  {{ trans('global.edit') }} {{ trans('global.employe.title') }}
        </a>
      </form>
      @endcan
    </nav>

    <div class="card-body">
        <div class="row">
            <div class="col-5">
                <!-- <div class="float-right mr-5 mt-2"> -->
                <div class="row justify-content-center ">
                    <img class="" src="{{ asset(trans('global.links.employee_show').$employee->files)}}">
                </div>
            </div>
            <div class="col-7">
                <table class="table table-striped">
                        <tr>
                            <th scope="col" width="10">Name</th>
                            <td scope="col"><b>:</b> {{$employee->name}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Email</th>
                            <td scope="col"><b>:</b> {{$employee->email}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Phone No.</th>
                            <td scope="col"><b>:</b> {{$employee->phone}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Institute</th>
                            <td scope="col"><b>:</b> {{$employee->institute->institute}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Employee Type</th>
                            <td scope="col"><b>:</b> {{$employee->employeeType->type}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Designation</th>
                            <td scope="col"><b>:</b> {{$employee->designation->designation}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Religion</th>
                            <td scope="col"><b>:</b> {{$employee->religion->title}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Address</th>
                            <td scope="col"><b>:</b> {{$employee->address}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Join Date</th>
                            <td scope="col"><b>:</b> {{$employee->joindate}}</td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">MPO Date</th>
                            <td scope="col"><b>:</b> {{$employee->mpo}}</td>
                        </tr>

                </table>

            </div>
        </div>
        <div class="col-12">
            {!!$employee->others!!}
        </div>
    </div>
</div>
@endsection

