@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')


<!-- <script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
<!-- <script src="{{ asset('public/ck-lib/js/ckeditor.js') }}"></script>
<script src="{{ asset('public/ck-lib/js/sample.js') }}"></script> -->
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('employee.index')}}">Employee</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        {{ trans('global.add') }} Employee
    </div>

    <div class="card-body">
        {{ 
            Aire::open()
            ->route('employee.store')
            ->rules([
            'name' => 'required',
            'employee_type_id' => 'required',
            'files'=> 'required|file',
            'email' =>'email',
            'phone' => 'numeric',
            'designation' => 'required',
            'religion_id' => 'required',
            ])
            ->messages([
            'name' => 'You must accept the terms',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')
        }}

        {{ 
            Aire::input()
            ->label('Name*')
            ->id('name')
            ->name('name')
            ->class('form-control') 
        }}

        {{ 
            Aire::input()
            ->label('Email')
            ->id('email')
            ->name('email')
            ->class('form-control') 
        }}

        {{ 
                Aire::input()
                ->label('Phone no.')
                ->id('phone')
                ->name('phone')
                ->class('form-control') 
        }}


        {{ 
            Aire::select($religion)
            ->label('Religion *')
            ->id('religion_id')
            ->name('religion_id')
            ->class('form-control') 
        }}


        {{
            Aire::select($institute)
            ->class('form-control')
            ->label('Institute *')
            ->name('institute_id')
            ->id('institute_id')
            
        }}
<!-- $type -->
    <!-- <div id='type_id'></div> -->
        {{
            Aire::select(['NULL'=>'Select'])
            ->class('form-control')
            ->label('Employee Type *')
            ->name('employee_type_id')
            ->id('employee_type_id')
            
        }}

        
        

        {{
            Aire::select(['NULL'=>'Select'])
            ->class('form-control')
            ->label(trans('global.employe.fields.designation').' *')
            ->name('designation_id')
            ->id('designation_id')
            
        }}


        {{ 
            Aire::input()
            ->label('Address')
            ->id('address')
            ->name('address')
            ->class('form-control') 
        }}

        {{ 
            Aire::input()
            ->label('Join Date')
            ->id('joindate')
            ->name('joindate')
            ->class('form-control') 
        }}

<!-- <input id="datepicker" /> -->
    <script>
        $('#joindate').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

        {{ 
            Aire::input()
            ->label('MPO Date')
            ->id('mpo')
            ->name('mpo')
            ->class('form-control') 
        }}

<!-- <input id="datepicker" /> -->
    <script>
        $('#mpo').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>




        <img src="" id="cover-img-tag" width="200px" class="mb-2" />
        {{
            Aire::input()
            ->type('file')
            ->label('Employe Picture (image size must be 450 × 450) *')
            ->id('files')
            ->name('files')
            
        }}


        {{ 
            Aire::textarea()
            ->label("Others")
            ->id('editor')
            ->name('others')
            ->class('form-control')
        }}

<!--         <script>
            initSample();
        </script> -->
        

        {{ 
            Aire::button()
            ->labelHtml('<strong>Next</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>

<script src="{{ asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js') }}"></script>



<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#files").change(function(){
        readURL(this);
    });

</script>



<!-- New enter -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>


<script type="text/javascript">
    $('#institute_id').change(function(event){
   var value = this.value;
   var _token = $('input[name="_token"]').val();
    $.ajax({
    url:"{{ route('employee.employee_type') }}",
    method:"POST",
    data:{value:value, _token:_token},
    success:function(result){
        $('#employee_type_id').html(result);
        var x = "<option>Select </option>";
        $('#designation_id').html(x);
    }
   })
 });

    $('#employee_type_id').change(function(event){
        var x = "<option>Select </option>";
        $('#designation_id').html(x);
        var value = this.value;
        // console.log('Value = '+v);
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"{{ route('employee.employee_designation') }}",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result){
                $('#designation_id').html(result);
                console.log(result);
            }
        })
    });
</script>

<script type="text/javascript">

    CKEDITOR.replace('editor', {

        filebrowserUploadUrl: "{{route('ckeditor.upload', ['_token' => csrf_token() ])}}",

        filebrowserUploadMethod: 'form'

    });


</script>
@endsection

