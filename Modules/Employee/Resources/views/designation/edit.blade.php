@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush
@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('employee.index')}}">Employee</a></li>
    <li class="breadcrumb-item"><a href="{{route('employee.designation.index')}}">Designation</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.edit') }} {{ trans('global.employe.fields.designation') }} 
      @can('employee_designation_create')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{route('employee.designation.index')}}">
          <i class="fas fa-file nav-icon"></i>  {{ trans('global.employe.fields.designation') }} {{ trans('global.list') }}
        </a>
      </form>
      @endcan
    </nav>



    <div class="card-body">
      {{ 
            Aire::open()
            ->route('employee.designation.update', $designation)
            ->bind($designation)
            ->rules([
            'designation' => 'required',
            ])
            ->messages([
            'designation' => 'You must input this fild',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')
        }}

        {{
            Aire::select($institute)
            ->class('form-control')
            ->label('Institute *')
            ->name('institute_id')
            ->id('institute_id')
            
        }}

        {{
            Aire::select($type)
            ->class('form-control')
            ->label('Employee Type *')
            ->name('employee_type_id')
            ->id('employee_type_id')
            
        }}

        {{ 
            Aire::input()
            ->label(trans('global.employe.fields.designation').' *')
            ->id('designation')
            ->name('designation')
            ->class('form-control') 
            ->value(old("title", isset($designation) ? $designation->designation : ""))
        }}


        {{ 
            Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}
        {{ Aire::close() }}
    </div>
</div>

@endsection