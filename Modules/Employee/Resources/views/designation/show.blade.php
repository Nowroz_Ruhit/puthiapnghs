@extends('layouts.admin')
@section('content')


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('employee.index')}}">Employee</a></li>
    <li class="breadcrumb-item"><a href="{{route('employee.designation.index')}}">Designation</a></li>
    <li class="breadcrumb-item active" aria-current="page">List</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.employe.fields.designation') }} {{ trans('global.list') }}
      @can('employee_access')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{route('employee.index')}}">
          <i class="fas fa-user nav-icon"></i>  {{ trans('global.employe.title') }} {{ trans('global.list') }}
        </a>
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{route('employee.designation.create')}}">
          <i class="fas fa-edit nav-icon"></i>  {{ trans('global.add') }} {{ trans('global.employe.fields.designation') }}
        </a>
      </form>
      @endcan
    </nav>



    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('global.employe.fields.designation') }}
                        </th>
                        <th>Institute</th>
                        <th>Employee Type</th>
                        <th>
                            &nbsp; Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($data as $key => $designation)
                        <tr data-entry-id="{{ $designation->id }}">
                            <td></td>
                            <td>
                                {{ $designation->designation  }}
                            </td>
                            <td>
                                {{ $designation->institute->institute  }}
                            </td>
                            <td>
                                {{ $designation->employeeType->type  }}
                            </td>
                            <td>
                              @can('employee_designation_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('employee.designation.edit',$designation) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('employee_designation_delete')
                                    <form action="{{ route('employee.designation.delete', $designation) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>
                            
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection