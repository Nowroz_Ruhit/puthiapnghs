@extends('layouts.admin')
@section('content')

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Employee</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.employe.title_singular') }} {{ trans('global.list') }}
      @can('employee_create')
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="{{route('employee.create')}}">
          <i class="fas fa-user nav-icon"></i>  {{ trans('global.add') }} {{ trans('global.employe.title') }}
        </a>

      </form>
      @endcan
    </nav>



    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                       
                        <th>
                            Picture
                        </th>
                        <th>
                            {{ trans('global.employe.fields.name') }}
                        </th>
                        <th>
                            Institute
                        </th>
                        <th>
                            {{ trans('global.employe.fields.designation') }}
                        </th>
                        <th>
                            &nbsp; Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($data as $key => $employee)
                        <tr data-entry-id="{{ $employee->id }}">
                            
                            <td>
                                <img height="50" width="50" src="{{ asset(trans('global.links.employee_show').$employee->files)}}">
                            </td>
                            <td>
                                {{ $employee->name  }}
                            </td>
                            <td>{{$employee->institute->institute}}</td>
                            <td>
                                {{ $employee->designation->designation  }}
                            </td>
                            <td>
                              <!--@can('employee_show')-->
                              <!--      <a class="btn btn-xs btn-primary" href="{{ route('employee.show', $employee) }}">-->
                              <!--          {{ trans('global.view') }}-->
                              <!--      </a>-->
                              <!--  @endcan-->
                              @can('employee_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('employee.edit',$employee) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('employee_delete')
                                    <form action="{{ route('employee.destroy', $employee) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>
                            
                    @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection