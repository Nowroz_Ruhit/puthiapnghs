<?php

namespace Modules\News\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\News\Entities\News;
use Modules\News\Entities\NewsCatagory;
use Modules\Notice\Entities\Notice;
use Modules\Notice\Entities\NoticeCatagory;
use Modules\Academic\Entities\Academic;
use Modules\Academic\Entities\AcademicCategory;

class Updatepost extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    // public function news_type()
    // {
    //     return $this->hasOne(Gallery::class);
    // }

    // public function academic_type()
    // {
    //     return $this->hasOne(Gallery::class);
    // }

    // public function notice_type()
    // {
    //     return $this->hasOne(Gallery::class);
    // }
}
