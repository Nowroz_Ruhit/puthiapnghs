<?php

namespace Modules\News\Entities;

use Illuminate\Database\Eloquent\Model;

class NewsCatagory extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
