<?php

namespace Modules\News\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\PhotoGallery\Entities\Gallery;

class News extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function album()
    {
        return $this->hasOne(Gallery::class, 'news_id', 'id');
    }
}
