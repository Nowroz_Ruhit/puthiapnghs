@extends('layouts.admin')
@section('style')
@parent
<!-- STYLE CSS -->
@endsection

@section('content')
<!-- body content -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('news.index')}}">News</a></li>
    <li class="breadcrumb-item active" aria-current="page">view</li>
  </ol>
</nav>


<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Signle News View</b></a>
      <form class="form-inline">
        <a class="form-control mr-sm-2" href="{{route('news.photoes', $news)}}">Add More Photo</a>
        <a class="form-control mr-sm-2" href="{{route('news.edit', $news)}}"><i class="fas fa-edit nav-icon"></i> Edit</a>
        <a class="form-control mr-sm-2" href="{{route('news.index')}}">News List</a>
      </form>
    </nav>
  <div class="card-body">
    <p class="card-text"><h5 class="card-title">News Title: {{ $news->title}}</h5></p>
    <!-- <p class="card-text">lore lip sum</p> -->
    <p class="card-text"><h5>News Description:</h5>
      {!! $news->discription !!}
    </p>

  </div>
    <div class="container">
      <div class="container row mx-auto">
        <div class="col-3">
            <div style="padding: 5px;" data-toggle="modal" data-caption="test caption text" data-image="" alt="recent photo" data-target="#trslphotos"><img src="{{ asset(trans('global.links.news_show').$news->photo)}}" class="img-thumbnail">
            </div>
        </div>
        @foreach ($photos as $key => $photo) 
          <div class="col-3">
            <div style="padding: 5px;" data-toggle="modal" data-caption="test caption text" data-image="" alt="recent photo" data-target="#trslphotos"><img src="{{ asset(trans('global.links.photo_show').$photo->photo)}}" class="img-thumbnail">
            </div>
          </div>
        @endforeach
<!--  -->
      </div>
    </div>
   </br>
</div>


<!-- modal -->
<div class="modal fade" id="trslphotos" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">View Image</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>
            </div>
            <div class="modal-body"><img src="" class="" width="465" height="auto"></div>
        </div>
      <div class="modal-footer">
      </div>
    </div>
</div>
<!-- ./modal -->

@section('scripts')
    @parent
<!-- SCRIPT -->
<script type="text/javascript">
$('#trslphotos').on('shown.bs.modal', function (a, b,c) {
 var clickedImageUrl = a.relatedTarget.childNodes[0].src;
  displayPhotos(clickedImageUrl);
})

function displayPhotos(url) {
 console.log(url);
 $('.modal-body img').attr('src',url);
 $('#trslphotos').modal();
}    
</script>

@endsection
@endsection