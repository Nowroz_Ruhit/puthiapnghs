<?php

namespace Modules\News\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use DB;
use Modules\News\Entities\News;
use Modules\News\Entities\NewsCatagory;
use Modules\News\Entities\Updatepost;
use Modules\PhotoGallery\Entities\Gallery;
use Modules\PhotoGallery\Entities\Photos;
use Intervention\Image\Facades\Image;


// use Validator;
// use Illuminate\Support\Facades\Validator;
// use Illuminate\Validation\Rule;
// use Illuminate\Support\Facades\Validator;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('news_access'), 403);
        $data = News::all();
        // $data = DB::table('news')->orderByRaw('updated_at - created_at DESC')->get();
        return view('news::index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('news_create'), 403);
        $catagorysdata = NewsCatagory::all();
        $catagory = array();
        $catagory[null] = 'Select News Catagory';
        foreach ($catagorysdata as $key => $catagorys) {
            $catagory[$catagorys['id']] = $catagorys['title'];
        }
        return view('news::create', compact('catagory'));

    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('news_create'), 403);

        // $validatedData = $request->validate([
        //     'title'=>'required',
        //     'discrioption'=>'required',
        //     'photo'=>'required',
        // ]);

        $news = $request->all();
        $news['created_by'] = Auth::id();
        $imgName = now()->format('Y-m-d-H-i-s').'-'.request()->photo->getClientOriginalName();
        $news['photo'] = $imgName;
        $data = News::create($news);
        request()->photo->move(public_path(trans('global.links.news')), $imgName);

        $image = Image::make(public_path(trans('global.links.news').$imgName))->resize(450, 450);
            $image->save();

        $gallery = new Gallery();
        $gallery->news_id = $data->id;
        $gallery->created_by = Auth::id();
        $gallery -> save();


        if((request()->input('catagories_id')!=NULL) || (!empty(request()->input('catagories_id')))){
            $category = NewsCatagory::find(request()->input('catagories_id'));
            $updatepost['title'] = request()->input('title');
            $updatepost['type'] = $category->title;
            $updatepost['news_id'] = $data->id;
        }else {
            $updatepost['title'] = request()->input('title');
            $updatepost['news_id'] = $data->id;
        }
        // dd($category);
        $updatepost['created_by'] = Auth::id();
       Updatepost::create($updatepost);
        if($data && $gallery)
        {
            return redirect()->route('news.photoes', $data->id)->with('success', 'Create Successfull');
        } else 
        {
            return redirect()->route('news.index')->with('error', 'Somthing was wrong');
       }    
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(News $news)
    {
        abort_unless(\Gate::allows('news_show'), 403);
        $photos = News::findOrFail($news->id)->album->photos;
        // $phone = User::find(1)->phone;
        // dd($photos);
        return view('news::show', compact('news', 'photos'));
        
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(News $news)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        $catagorysdata = NewsCatagory::all();
        $catagory[null] = 'Select News Catagory';
        foreach ($catagorysdata as $key => $catagorys) {
            $catagory[$catagorys['id']] = $catagorys['title'];
        }
        return view('news::edit', compact('news', 'catagory'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, News $news)
    {
        abort_unless(\Gate::allows('news_edit'), 403);
        // $news->update($request->all());

        $data = $request->all();
        $data['updated_by'] = Auth::id();

        if ($request->hasFile('photo')){
            if(file_exists(public_path(trans('global.links.news')).$news->photo))
            {
                unlink(public_path(trans('global.links.news')).$news->photo);
            }
            $imgName = now()->format('Y-m-d-H-i-s').'-'.request()->photo->getClientOriginalName();
            $data['photo'] = $imgName;

            request()->photo->move(public_path(trans('global.links.news')), $imgName);

            $image = Image::make(public_path(trans('global.links.news').$imgName))->resize(450, 450);
            $image->save();
        }
        $news->update($data);
        $updatepost = Updatepost::where('news_id', $news->id)->firstOrFail();
            $updatepost->title = $news->title;
            $updatepost->updated_by = Auth::id();
            $updatepost->update();

        if($news)
        {
            return redirect()->route('news.index')->with('success', 'Update Successfull');
        } else 
        {
            return redirect()->route('news.index')->with(['error', 'Somthing was wrong']);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    // public function destroy($id)
    // {
    //     //
    // }
    public function destroy(News $news)
    {
        abort_unless(\Gate::allows('news_delete'), 403);
        $gallery = Gallery::where('news_id', $news->id)->firstOrFail();
        $photos = Photos::where('gallery_id', $gallery->id)->get();
        // dd($photos);
        foreach ($photos as $key => $photo) {
            $file= $photo->photo;
            if($photo->delete())
            {
                $filename = public_path(trans('global.links.photo')).$file;
                if(\File::exists($filename)){
                   \File::delete($filename);
               }
           }
       }

       $file= $news->photo;
        if($news->delete())
        {
            $filename = public_path(trans('global.links.news')).$file;
             if(\File::exists($filename)){
                 \File::delete($filename);
            }
           return redirect()->route('news.index')->with('delete', 'Delete Successfull');
        }else {
            return redirect()->route('news.index')->with('error', 'Not Deleted, Somtheing waswrong');
        }
    }



    public function uploadImages(Request $request)
    {   
        abort_unless(\Gate::allows('photo_uplode'), 403);
        $imgName = now()->format('Y-m-d-H-i-s').'-'.request()->file->getClientOriginalName();
        $photos = new Photos();
        $photos->gallery_id  = request()->input('gallery');
        $photos->photo = $imgName;
        $photos->created_by = Auth::id();
        $photos->save();

        request()->file->move(public_path(trans('global.links.photo')), $imgName);
        return response()->json(['uploaded' => '/images/'.$imgName]);

    }

    public function addcatagory()
    {
        abort_unless(\Gate::allows('news_category_create'), 403);
        return view('news::addcatagory');
    }

    public function storecatagory(Request $request)
    {
        abort_unless(\Gate::allows('news_category_create'), 403);
        $validatedData = $request->validate([
            'title'=>'required',
        ]);
        
        // echo str_replace(' ', '', request()->input('slag'));
        $catagory = new NewsCatagory();
        $catagory->title = request()->input('title');
        // $catagory->slag = str_replace(' ', '', request()->input('slag'));
        $catagory->created_by = Auth::id();
        $data = $catagory -> save();
        //return view('news::addcatagory');
        if($data){
            return redirect()->route('news.index')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('news.index')->with('error', 'Not Created, Somtheing waswrong');
        }

    }

    public function photoes ($id) 
    {
        abort_unless(\Gate::allows('photo_access'), 403);
        $news = DB::table('news')->find($id);
        // $gallery = DB::table('galleries')->where('news_id', '4');
        $gallery = DB::table('galleries')->select('id')->where('news_id', $id)->limit(1)->get();
        // dd($gallery);
        // echo $gallery[0]->id;
        // dd($gallery);
        // $gallery = 4;
        return view('news::imgInput',compact('news', 'gallery'));
    }


}
