<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->longtext('discription');
            $table->integer('catagories_id')->unsigned()->nullable();
            $table->string('photo');
            $table->timestamps();
            
			$table->foreign('catagories_id')
            ->references('id')->on('news_catagories')
            ->onDelete('cascade');
        });

        // Schema::table('news', function (Blueprint $table) {
        //     $table->unsignedBigInteger('catagories_id');

        //     $table->foreign('catagories_id')->references('id')->on('news_catagories')->onDelete('cascade');
        // });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
