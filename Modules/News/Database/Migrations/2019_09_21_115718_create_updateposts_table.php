<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdatepostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('updateposts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('type')->nullable();
            $table->bigInteger('news_id')->unsigned()->nullable();
            $table->bigInteger('notice_id')->unsigned()->nullable();
            $table->bigInteger('academic_id')->unsigned()->nullable();
            $table->string('route')->nullable();
            $table->timestamps();

            $table->foreign('academic_id')
                ->references('id')->on('academics')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('news_id')
                ->references('id')->on('news')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('notice_id')
                ->references('id')->on('notices')
                ->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('updateposts');
    }
}
