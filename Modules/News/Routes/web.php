<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
Route::prefix('admin')->group(function() {
// Route::prefix('news')->group(function() {
Route::resource('news', 'NewsController');

    // Route::get('/', 'NewsController@index');
    // Route::get('/create', 'NewsController@create')->name('create');
    // Route::post('store','NewsController@store')->name('store');
    // Route::delete('destroy/{id}','NewsController@destroy')->name('destroy');
    // Route::get('show/{id}','NewsController@show')->name('show');
    // Route::get('edit/{id}','NewsController@edit')->name('edit');
    
    // Route::get('news/photoes/{id}','NewsController@photoes')->name('photoes');
     Route::get('news/photoes/{id}',['as'=>'news.photoes','uses'=>'NewsController@photoes']);
    Route::post('news/upload',['as'=>'news.upload','uses'=>'NewsController@uploadImages']);

    Route::get('news-addcatagory',['as'=>'news.addcatagory','uses'=>'NewsController@addcatagory']);
    Route::post('news-storecatagory',['as'=>'news.storecatagory','uses'=>'NewsController@storecatagory']);

    // Route::get('addcatagory', 'NewsController@addcatagory')->name('addcatagory');
    // Route::post('storecatagory', 'NewsController@storecatagory')->name('storecatagory');
});