@extends('layouts.admin')

@section('styles')
@parent

@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Academic</li>
  </ol>
</nav>

<div class="card">


    <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.academic.title_singular') }} {{ trans('global.academic.fields.post') }} {{ trans('global.list') }}
    @can('academic_create')
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{ route("academic.create") }}">
          <i class="fas fa-edit nav-icon"></i>   {{ trans('global.add') }} {{ trans('global.academic.fields.post') }}
        </a>
      </form>
      @endcan
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>
                            {{ trans('global.academic.fields.post') }} {{ trans('global.academic.fields.title') }}
                        </th>
                        <th>
                            {{ trans('global.academic.fields.post') }} {{ trans('global.academic.fields.catagory') }}
                        </th>
                        <th>
                            {{ trans('global.academic.fields.post') }} {{ trans('global.academic.fields.file') }}
                        </th>
                        <th>
                            &nbsp;Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($academics as $key => $academic)
                        <tr data-entry-id="{{ $academic->id }}">
                            <td></td>
                            <td>
                                {{ $academic->title  }}
                            </td>
                            <td>
                                {{ $academic->category->title  }}
                            </td>
                            <td>
                                <a href="{{route('academic.download', $academic->id)}}">download</a>
                            </td>
                            <td>
                              @can('academic_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('academic.show', $academic) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                              @can('academic_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('academic.edit', $academic) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('academic_delete')
                                    <form action="{{ route('academic.destroy', $academic) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>
                            
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection
