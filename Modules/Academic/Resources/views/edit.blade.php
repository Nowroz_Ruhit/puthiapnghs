@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')

<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('academic.index')}}">Academic</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        Edit Post
    </div>


    <div class="card-body">
    	{{ 
            Aire::open()
            ->route('academic.update',$academic)
            ->bind($academic)
            ->rules([
            'title' => 'required',
            'discription' => 'required',
            ])
            ->messages([
            'title' => 'You must input this fild',
            ]) 
            ->enctype('multipart/form-data')

        }}

        {{ 
            Aire::input()
            ->label('Title *')
            ->id('title')
            ->name('title')
            ->class('form-control') 
        }}

  
        {{ 
            Aire::textarea()
            ->label('Discription*')
            ->id('discription')
            ->name('discription')
            ->class('form-control')
        }}

        <script>
            CKEDITOR.replace( 'discription' );
        </script>

        {{ 
            Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}
        {{ Aire::close() }}
        
    </div>
</div>

@endsection