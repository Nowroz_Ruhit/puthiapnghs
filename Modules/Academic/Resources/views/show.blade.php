@extends('layouts.admin')
@section('style')
@parent
<!-- STYLE CSS -->
@endsection

@section('content')
<!-- body content -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('academic.index')}}">Academic</a></li>
    <li class="breadcrumb-item active" aria-current="page">view</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Signle Post View</b></a>
      <form class="form-inline">
        <a class="form-control mr-sm-2" href="{{route('academic.edit', $academic)}}">
          <i class="fas fa-edit nav-icon"></i> Edit</a>
      </form>
    </nav>
  <div class="card-body">
    <p class="card-text"><h5 class="card-title">Notice Title: {{ $academic->title}}</h5></p>
    <p class="card-text"><h5>Categoty : {{ $academic->category->title}}</h5></p>
    <p class="card-text"><h5>Description:</h5>
      {!!  $academic->discription !!}
    </p>

  </div>
    
   </br>
   <div class="container mb-2">
    Attesment :  <a href="{{ route('academic.download', $academic->id) }}">{{ $academic->file}}</a>
   </div>
   <br>
</div>



@endsection