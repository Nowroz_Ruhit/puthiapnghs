@extends('layouts.admin')

@section('styles')
@parent

@endsection

@section('scripts')
@parent

@endsection

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('academic.index')}}">Academic</a></li>
    <li class="breadcrumb-item active" aria-current="page">Category</li>
  </ol>
</nav>

<div class="card">


  <nav class="navbar navbar-light bg-light justify-content-between">
      {{ trans('global.academic.title_singular') }} {{ trans('global.academic.fields.catagory') }} {{ trans('global.list') }}
      @can('academic_category_create')
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="{{ route('academic.catagory.create') }}">
            <i class="fas fa-edit nav-icon"></i>   {{ trans('global.add') }} {{ trans('global.academic.fields.catagory') }}
        </a>
      </form>
      @endcan
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>

                        <th>
                            {{ trans('global.academic.fields.catagory') }} {{ trans('global.academic.fields.name') }}
                        </th>
<!--                         <th>
                            {{ trans('global.academic.fields.description') }}
                        </th>
                        <th>
                            {{ trans('global.academic.fields.price') }}
                        </th> -->
                        <th>
                            &nbsp;Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $category)
                        <tr data-entry-id="{{ $category->id }}">

                            <td>
                                {{ $category->title ?? '' }}
                            </td>
                            <td>
                              @can('academic_category_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('academic.category.edit', $category) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('academic_category_delete')
                                    <form action="{{ route('academic.category.delete', $category->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>
                            
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
