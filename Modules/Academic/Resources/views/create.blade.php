@extends('layouts.admin')
@push('css')
<link href="{{ asset('css/aire.css') }}" rel="stylesheet" />
@endpush

@section('content')

@if(session()->get('msg'))
 <a href="{{url('academic')}}" class="btn btn-success">Back to Notice List</a>

<div class="alert-success col-12 text-center h1 p-6  mt-5 mb-5">
    "<i>Notice</i>"  Create Successfully
     
</div>

@else
<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{url('academic')}}">Academic</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create Post</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.academic.title_singular') }} {{ trans('global.academic.fields.post') }}
    </div>

    <div class="card-body">
        {{ 
            Aire::open()
            ->route('academic.store')
            ->rules([
            'title' => 'required',
            'discription' => 'required',
            'catagories_id' => 'required',
            'file'=> 'required|file',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')
        }}

        {{ 
            Aire::input()
            ->label('Title*')
            ->id('title')
            ->name('title')
            ->class('form-control') 
        }}

        {{ 
            Aire::textarea()
            ->label('Discription*')
            ->id('discription')
            ->name('discription')
            ->class('form-control') 
        }}

        <script>
            CKEDITOR.replace( 'discription' );
        </script>

        {{
            Aire::select($catagory)
            ->class('form-control')
            ->label('Post Catagory *')
            ->name('catagories_id')
            ->id('catagories_id')
            
        }}

        {{
            Aire::input()
            ->type('file')
            ->label('Post File*')
            ->id('file')
            ->name('file')
            
        }}


        {{ 
            Aire::button()
            ->labelHtml('<strong>Next</strong>')
            ->class('btn btn-danger')
            ->class('mt-2') 
        }}


        {{ Aire::close() }}
    </div>
</div>

@endif
@endsection

