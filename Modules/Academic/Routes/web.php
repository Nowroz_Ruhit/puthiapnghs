<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('admin/academic/category', 'view/category');
Route::prefix('admin')->group(function() {
    Route::resource('academic', 'AcademicController');
    Route::get('academic/download/{id}',[
        'as'=>'academic.download',
        'uses'=>'AcademicController@download'
    ]);



    Route::get('academic/view/category',[
        'as'=>'academic.catagory',
        'uses'=>'AcademicController@category'
    ]);
    // Route::get('academic/view/category',[
    //     'as'=>'academic.catagory',
    //     'uses'=>'AcademicController@category'
    // ]);
    Route::get('academic/category/create',[
        'as'=>'academic.catagory.create',
        'uses'=>'AcademicController@createCategoty'
    ]);
    Route::post('academic/category/store',[
        'as'=>'academic.category.store',
        'uses'=>'AcademicController@storecatagory'
    ]);
    Route::get('academic/category/{catagory}/edit',[
        'as'=>'academic.category.edit',
        'uses'=>'AcademicController@editcatagory'
    ]);
    Route::post('academic/category/update/{id}',[
        'as'=>'academic.category.update',
        'uses'=>'AcademicController@updatecatagory'
    ]);
    Route::delete('academic/category/delete/{id}',[
        'as'=>'academic.category.delete',
        'uses'=>'AcademicController@deletecatagory'
    ]);



    // });

});
    /*Route::get('/', 'AcademicController@index');
    Route::get('create',['as'=>'academic.create','uses'=>'AcademicController@create']);
    Route::get('show/{id}',['as'=>'academic.show','uses'=>'AcademicController@show']);
    Route::post('store',['as'=>'academic.store','uses'=>'AcademicController@store']);
    Route::post('update/{id}',['as'=>'academic.update','uses'=>'AcademicController@update']);
    Route::get('edit/{id}',['as'=>'academic.edit','uses'=>'AcademicController@edit']);
    Route::delete('delete/{id}',['as'=>'academic.delete','uses'=>'AcademicController@destroy']);
    */
    


    // Category Route
    // Route::prefix('academic')->group(function() {
    // Route::get('academic/category',[
    //     'as'=>'academic.category',
    //     'uses'=>'AcademicController@createCategoty'
    // ]);
    // Route::get('academic/category/list', 'AcademicController@category');