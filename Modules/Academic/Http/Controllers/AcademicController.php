<?php

namespace Modules\Academic\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
// use DB;
use Modules\Academic\Entities\Academic;
use Modules\Academic\Entities\AcademicCategory;
use Modules\News\Entities\Updatepost;

class AcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        abort_unless(\Gate::allows('academic_access'), 403);
        $academics = Academic::all();
        return view('academic::index', compact('academics'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        abort_unless(\Gate::allows('academic_create'), 403);
        $catagorysdata = AcademicCategory::all();
        $catagory = array();
        $catagory[0] = 'Select Post Catagory';
        foreach ($catagorysdata as $key => $catagorys) {
            $catagory[$catagorys['id']] = $catagorys['title'];
        }
        return view('academic::create', compact('catagory'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        abort_unless(\Gate::allows('academic_create'), 403);
        $validatedData = $request->validate([
            'title'=>'required',
            'discription'=>'required',
            'file'=>'required',
            'catagories_id'=>'required',
        ]);
// dd($request->all());
        $academic = $request->all();
        $academic['created_by'] = Auth::id();
        // $academic->title = request()->input('title');
        // $academic->discription = request()->input('discrioption');
        // $academic->catagories_id = request()->input('catagory');

        if ($request->hasFile('file'))
        {
            $fileName = now()
                    ->format('Y-m-d-H-i-s').'-'.request()
                    ->file('file')
                    ->getClientOriginalName();
            $academic['file'] = $fileName;
            request()->file('file')->move(public_path(trans('global.links.academic')), $fileName);
        }

        // dd($academic);
        $data = Academic::create($academic);

            // $updatepost = new Updatepost;
            $category = AcademicCategory::where('id', request()->input('catagories_id'))->firstOrFail();
            $updatepost['title'] = request()->input('title');
            $updatepost['type'] = $category->title;
            $updatepost['academic_id'] = $data->id;
            Updatepost::create($updatepost);

        if($data){
            return redirect()->route('academic.index')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('academic.index')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Academic $academic)
    {
        abort_unless(\Gate::allows('academic_show'), 403);
        // $data = Academic::find($id);
        // return $academic;
        return view('academic::show', compact('academic'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Academic $academic)
    {
        abort_unless(\Gate::allows('academic_edit'), 403);
        // $data = Academic::find($id);
        return view('academic::edit', compact('academic'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Academic $academic)
    {
        abort_unless(\Gate::allows('academic_edit'), 403);
        $validatedData = $request->validate([
            'title'=>'required',
            'discription'=>'required',
        ]);
        // $post = Academic::findOrFail($id);
        // $post->title = request()->input('title');
        // $post->discription = request()->input('discription');
        $data = $request->all();
        if($request->hasFile('file')){
            if(file_exists(public_path(trans('global.links.academic')).$academic->file))
            {
                unlink(public_path(trans('global.links.academic')).$academic->file);
            }
            
            $fileName = now()
                    ->format('Y-m-d-H-i-s').'-'.request()
                    ->file('file')
                    ->getClientOriginalName();
            $data['file'] = $fileName;
            request()->file('file')->move(public_path(trans('global.links.academic')), $fileName);
        }
        $data['updated_by'] = Auth::id();
        $academic->update($data);
        $updatepost = Updatepost::where('academic_id', $academic->id)->firstOrFail();
            $updatepost->title = $academic->title;
            $updatepost->update();

        if($academic){
            return redirect()->route('academic.index')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('academic.index')->with('error', 'Not Update, Somtheing waswrong');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Academic $academic)
    {
        abort_unless(\Gate::allows('academic_delete'), 403);
        $file= $academic->file;
        if($academic->delete())
        {
            $filename = public_path(trans('global.links.academic')).$file;
             if(\File::exists($filename)){
                 \File::delete($filename);
            }
           return redirect()->route('academic.index')->with('delete', 'Delete Successfull');
        }else {
            return redirect()->route('academic.index')->with('error', 'Not Deleted, Somtheing waswrong');
        }
    }

    
    public function category()
    {
        abort_unless(\Gate::allows('academic_category_access'), 403);
        // abort_unless(\Gate::allows('academic_category_show'), 403);
        $data = AcademicCategory::all();
        // return "Tasmir";
        return view('academic::category.index', compact('data'));
    }

    public function createCategoty()
    {
        abort_unless(\Gate::allows('academic_category_create'), 403);
        return view('academic::category.create');
    }

    public function storecatagory(Request $request, AcademicCategory $category)
    {
        abort_unless(\Gate::allows('academic_category_create'), 403);
        $validatedData = $request->validate([
            'title'=>'required',
        ]);
        // $catagory = new AcademicCategory();
        $catagory = $request->all();
        $catagory['created_by'] = Auth::id();
        // $catagory->title = request()->input('title');
        // $catagory -> save();

        $data = AcademicCategory::create($catagory);
        if($data){
            return redirect()->route('academic.catagory')->with('success', 'Create Successfull');
        }else {
            return redirect()->route('academic.catagory')->with('error', 'Not Created, Somtheing waswrong');
        }
    }

    public function editcatagory(AcademicCategory $catagory)
    {
        abort_unless(\Gate::allows('academic_category_edit'), 403);
        // $catagory = AcademicCategory::find($id);
        // return $catagory;
        return view('academic::category.edit', compact('catagory'));

    }

    public function updatecatagory(Request $request, AcademicCategory $id)
    {
        abort_unless(\Gate::allows('academic_category_edit'), 403);
        $validatedData = $request->validate([
            'title'=>'required',
        ]);
        // $catagory = AcademicCategory::findOrFail($id);
        // $catagory->title = request()->input('title');
        // $catagory = $request->all();
        // dd($request->all());
        $catagory = $request->all();
        $catagory['updated_by'] = Auth::id();
        $data = $id->update();
        if($data){
            return redirect()->route('academic.catagory')->with('success', 'Update Successfull');
        }else {
            return redirect()->route('academic.catagory')->with('error', 'Not Updeted, Somtheing waswrong');
        }
    }

    public function deletecatagory(AcademicCategory $id)
    {
        abort_unless(\Gate::allows('academic_category_delete'), 403);
        // $var = AcademicCategory::findOrFail($id);
        // $var->delete();
        if($id->delete())
        {
           return redirect()->route('academic.catagory')->with('delete', 'Delete Successfull');
        }else {
            return redirect()->route('academic.catagory')->with('error', 'Not Deleted, Somtheing waswrong');
        }
    }

    public function download($id) 
    {
        $post = Academic::find($id);
        $name = $post->title;
        $file = $post->file;
        $extention = substr($post->file, -3);
        $pathToFile = public_path(trans('global.links.academic')).'/'.$file;
        return response()->download($pathToFile, $name.'.'.$extention);
    }
}
