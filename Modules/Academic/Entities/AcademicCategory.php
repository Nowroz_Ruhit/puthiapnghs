<?php

namespace Modules\Academic\Entities;

use Illuminate\Database\Eloquent\Model;

class AcademicCategory extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];
}
