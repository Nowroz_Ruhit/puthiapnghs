<?php

namespace Modules\Academic\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Academic\Entities\AcademicCategory;

class Academic extends Model
{
    protected $fillable = [];
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function category()
    {
        return $this->belongsTo(AcademicCategory::class, 'catagories_id', 'id');
    }

}
