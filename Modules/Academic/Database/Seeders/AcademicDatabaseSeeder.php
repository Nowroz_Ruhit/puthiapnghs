<?php

namespace Modules\Academic\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Academic\Entities\AcademicCategory;

class AcademicDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Model::unguard();

        // $this->call("OthersTableSeeder");
        $category = [
            [
                'id'         => '1',
                'title'      => 'সিলেবাস',
                'created_at' => '2019-04-15 19:14:42',
                'updated_at' => '2019-04-15 19:14:42',
            ],
            [
                'id'         => '2',
                'title'      => 'ক্লাস রুটিন',
                'created_at' => '2019-04-15 19:15:42',
                'updated_at' => '2019-04-15 19:15:42',
            ],
            [
                'id'         => '3',
                'title'      => 'পরীক্ষা রুটিন',
                'created_at' => '2019-04-15 19:16:42',
                'updated_at' => '2019-04-15 19:16:42',
            ],
            [
                'id'         => '4',
                'title'      => 'রেজাল্ট',
                'created_at' => '2019-04-15 19:16:42',
                'updated_at' => '2019-04-15 19:16:42',
            ],
            [
                'id'         => '5',
                'title'      => 'একাডেমিক ক্যালেন্ডার',
                'created_at' => '2019-04-15 19:16:42',
                'updated_at' => '2019-04-15 19:16:42',
            ],
        ];
        AcademicCategory::insert($category);
    }
}
