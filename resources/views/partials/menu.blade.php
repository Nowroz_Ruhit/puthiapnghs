
<div class="sidebar" style="overflow-y: hidden !important;">
    <nav class="sidebar-nav "><!-- ps ps--active-y -->
        <!-- Menu Option Start -->
        <ul class="nav">
            <!-- Dashboard Start -->
            <li class="nav-item">
                <a href="{{url('admin')}}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            <!-- Dashboard End -->
            @can('admission')
            <!-- Adm-Admission Dashboard Start -->
             <li class="nav-item nav-dropdown {{request()->is('*/lottery/*') || (Route::currentRouteName() == 'admin.admission.result.lottery') || (Route::currentRouteName() == 'admin.admission.result.lottery.create') || (Route::currentRouteName() == 'admin.admission.qouta.select') || 
                (Route::currentRouteName() ==  'admin.admission.qouta.index') ? 'open' : '' }}">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa nav-icon"></i>
                    Admission
                </a>
                <ul class="nav-dropdown-items">
                    @can('admission_start')
                    <li class="nav-item">
                        <a href="{{ route('admin.admission.manage.index') }}" class="nav-link  request()->is('manage') || request()->is('manage/*') ? 'active' : '' ">
                            <i class="fas fa-cog nav-icon">

                            </i>
                            Manage
                        </a>
                    </li>
                    @endcan
                    <!-- Adm-Dashboard Start -->
                    <li class="nav-item">
                        <a href="{{ route('admin.admission.paymentStatus') }}" class="nav-link  request()->is('status') || request()->is('status/*') ? 'active' : '' ">
                            <i class="fa fa-users nav-icon">

                            </i>
                            All Students
                        </a>
                    </li>
                    <!-- Adm-Dashboard End -->
                    <!-- Adm-Add-Class Start -->
                    <li class="nav-item">
                        <a href="{{ route('admin.admission.classAmount') }}" class="nav-link  request()->is('class-amount') || request()->is('class-amount/*') ? 'active' : '' ">
                            <i class="fas fa-edit nav-icon"></i>
                            Class
                        </a>
                    </li>
                    <!-- Adm-Add-Class End -->
                    <!-- Adm-Password Start -->
                    <!--<li class="nav-item nav-dropdown">-->
                        <!--<a class="nav-link  nav-dropdown-toggle">-->
                        <!--    <i class="fas fa-edit nav-icon"></i>-->
                        <!--    Subject-->
                        <!--</a>-->
                         <!--<ul class="nav-dropdown-items">-->
                            {{--<li class="nav-item">
                                <a href="{{ route('admin.admission.subjectSettings.subjects.create') }}" class="nav-link  request()->is('subject-settings/subjects/create') || request()->is('subject-settings/subjects/create/*') ? 'active' : '' ">
                                    <i class="fas fa nav-icon"></i>
                                    Add Subject
                                </a>
                            </li>--}}
                            <!--<li class="nav-item">-->
                            <!--    <a href="{{route('admin.admission.subjectSettings.subject')}}" class="nav-link  request()->is('subject-settings/subjects') || request()->is('subject-settings/subjects/*') ? 'active' : '' ">-->
                            <!--        <i class="fas fa nav-icon"></i>-->
                            <!--        Subject List-->
                            <!--    </a>-->
                            <!--</li>-->
                            {{--<li class="nav-item">
                                <a href="{{ route('admin.admission.subjectSettings.create') }}" class="nav-link  request()->is('subject-settings/create') || request()->is('subject-settings/create') ? 'active' : '' ">
                                    <i class="fas fa nav-icon"></i>
                                    Set Subjects Marks
                                </a>
                            </li>--}}
                            <!--<li class="nav-item">-->
                            <!--    <a href="{{ route('admin.admission.subjectSettings.index') }}" class="nav-link  request()->is('subject-settings') || request()->is('subject-settings/') ? 'active' : '' ">-->
                            <!--        <i class="fas fa nav-icon"></i>-->
                            <!--        Subject Mark View-->
                            <!--    </a>-->
                            <!--</li>-->
                         <!--</ul>-->
                    <!--</li>-->
                    <!--<li class="nav-item">-->
                    <!--    <a href="{{ route('admin.admission.xm.index') }}" class="nav-link  request()->is('xm') || request()->is('xm/*') ? 'active' : '' ">-->
                    <!--        <i class="fas fa-cog nav-icon"></i>-->
                    <!--        Marks-->
                    <!--    </a>-->
                    <!--</li>-->
                    <li class="nav-item nav-dropdown {{request()->is('*/lottery/*') || (Route::currentRouteName() == 'admin.admission.result.lottery') || 
                    (Route::currentRouteName() == 'admin.admission.result.lottery.create') ? 'open' : '' }}">
                        <a class="nav-link  nav-dropdown-toggle">
                            <i class="fas fa-calculator nav-icon"></i>
                            Result
                        </a>
                         <ul class="nav-dropdown-items">
                            <!--<li class="nav-item">-->
                            <!--    <a href="{{ route('admin.admission.result.index') }}" class="nav-link  request()->is('result') || request()->is('result/*') ? 'active' : '' ">-->
                            <!--        <i class="fas fa nav-icon"></i>-->
                            <!--        Create Result-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item">-->
                            <!--    <a href="{{route('admin.admission.result.publish')}}" class="nav-link  request()->is('publish') || request()->is('publish/*') ? 'active' : '' ">-->
                            <!--        <i class="fas fa nav-icon"></i>-->
                            <!--        Publish Result-->
                            <!--    </a>-->
                            <!--</li>-->
                            <li class="nav-item">
                                <a href="{{ route('admin.admission.result.lottery.choose') }}" class="nav-link {{(Route::currentRouteName() == 'admin.admission.result.lottery.create') ? 'active' : '' }}">
                                    <i class="fas fa nav-icon"></i>
                                    Create Lottery Result
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('admin.admission.result.lottery.select') }}" class="nav-link {{request()->is('*/lottery/*/edit') || (Route::currentRouteName() == 'admin.admission.result.lottery') || request()->is('lottery/*/edit') ? 'active' : '' }}">
                                    <i class="fas fa nav-icon"></i>
                                    Check Lottery Result
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('admin.admission.result.lottery.createSMS') }}" class="nav-link {{request()->is('*/lottery/*/edit') || (Route::currentRouteName() == 'admin.admission.result.lottery.createSMS') || request()->is('lottery/*/edit') ? 'active' : '' }}">
                                    <i class="fas fa nav-icon"></i>
                                    Send Lottery Result
                                </a>
                            </li>
                         </ul>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.admission.qouta.select') }}" class="nav-link 
                        {{((Route::currentRouteName() == 'admin.admission.qouta.select') || 
                        (Route::currentRouteName() ==  'admin.admission.qouta.index')) ? 'active' : ''}}">
                            <i class="fas fa-file-alt nav-icon"></i>
                            Applicant Report
                        </a>
                    </li>
                    <!--<li class="nav-item">-->
                    <!--    <a href="{{ route('admin.admission.print.index') }}" class="nav-link  request()->is('print') || request()->is('print/*') ? 'active' : '' ">-->
                    <!--        <i class="fas fa-print nav-icon"></i>-->
                    <!--        Print-->
                    <!--    </a>-->
                    <!--</li>-->

                    <!--<li class="nav-item">-->
                    <!--    <a href="{{ route('admin.admission.sms.index') }}" class="nav-link  request()->is('sms') || request()->is('sms/*') ? 'active' : '' ">-->
                    <!--        <i class="fas fa-send nav-icon"></i>-->
                    <!--        Send SMS-->
                    <!--    </a>-->
                    <!--</li>-->
                    <!-- Adm-Password End -->
                </ul>
            </li>
            <!--Adm-Admission Dashboard End -->
            
            @endcan
            <!-- Profile Start -->
            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-users nav-icon">

                    </i>
                    Profile
                </a>
                <ul class="nav-dropdown-items">
                    <!-- Pro-Profile Start -->
                    <li class="nav-item">
                        <a href="{{ route('profile.index') }}" class="nav-link  request()->is('profile') || request()->is('profile/*') ? 'active' : '' ">
                            <i class="fas fa-user nav-icon">

                            </i>
                            Profile
                        </a>
                    </li>
                    <!-- Pro-Profile End -->
                    <!-- Pro-Update Start -->
                    <li class="nav-item">
                        <a href="{{ route('profile.edit', Auth::id()) }}" class="nav-link  request()->is('profile') || request()->is('profile/*') ? 'active' : '' ">
                            <i class="fas fa-user-edit nav-icon">

                            </i>
                            Update
                        </a>
                    </li>
                    <!-- Pro-Update End -->
                    <!-- Pro-Password Start -->
                    <li class="nav-item">
                        <a href="{{ route('profile.create') }}" class="nav-link  request()->is('profile/change-password') || request()->is('profile/change-password/*') ? 'active' : '' ">
                            <i class="fas fa-edit nav-icon">

                            </i>
                            Change Password
                        </a>
                    </li>
                    <!-- Pro-Password End -->
                </ul>
            </li>
            <!-- Profile End -->
                        <!-- Employe Start -->
            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle" class="nav-link  request()->is('admin/employee') || request()->is('admin/employee/*') ? 'active' : '' ">
                    <i class="fas fa-user-tie nav-icon">

                    </i>
                    Employee
                </a>
                <ul class="nav-dropdown-items">
                    <!-- Emp-Employe Start -->
                    <li class="nav-item">
                        <a href="{{ route('employee.designation.index') }}" class="nav-link  request()->is('admin/employee/designation') || request()->is('admin/employee/designation/*') ? 'active' : '' ">
                            <i class="fas fa-user-tie nav-icon"></i>
                            Designation
                        </a>
                    </li>
                    <!-- Emp-Employe End -->
                    <!-- Emp-Employe Start -->
                    <li class="nav-item">
                        <a href="{{ route('employee.index') }}" class="nav-link  request()->is('employee') || request()->is('employee/*') ? 'active' : '' ">
                            <i class="fas fa-user-tie nav-icon">

                            </i>
                            <!--{{ trans('global.employe.title') }}-->
                            Employee
                        </a>
                    </li>
                    <!-- Emp-Employe End -->

                </ul>
            </li>
            <!-- Employe End -->
            
            <!-- News Start -->
            <li class="nav-item">
                <a href="{{ route('news.index') }}" class="nav-link  request()->is('news') || request()->is('news/*') ? 'active' : '' ">
                    <i class="fas fa-newspaper nav-icon">

                    </i>
                    {{ trans('global.news.title') }}
                </a>
            </li>
            <!-- News End -->
            <!-- Notice Start -->
            <li class="nav-item">
                <a href="{{ route('notice.index') }}" class="nav-link  request()->is('notice') || request()->is('notice/*') ? 'active' : '' ">
                    <i class="fas fa-thumbtack nav-icon">

                    </i>
                    {{ trans('global.notice.title') }}
                </a>
            </li>
            <!-- Notice End -->
            <!-- Photo Gallery Start -->
            <li class="nav-item">
                <a href="{{ route('photogallery.index') }}" class="nav-link  request()->is('photogallery') || request()->is('photogallery/*') ? 'active' : '' ">
                    <i class="fas fa-images nav-icon">

                    </i>
                    Photo Gallery
                </a>
            </li>
            <!-- Photo Gallery End -->
            @can('page_access')
            <!-- Pages Start -->
            <li class="nav-item">
                <a href="{{ route('page.index') }}" class="nav-link  request()->is('admin/page/') || request()->is('admin/page/*') ? 'active' : '' ">
                    <i class="fas fa-file nav-icon">

                    </i>
                    Pages
                </a>
            </li>
            <!-- Pages End -->
            @endcan
            <!-- Academic Start -->
            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-graduation-cap nav-icon">

                    </i>
                    Academic
                </a>
                <ul class="nav-dropdown-items">
                    <!-- Aca-Catagories Start -->
                    <li class="nav-item">
                        <a href="{{ route('academic.catagory')  }}" class="nav-link  request()->is('academic.category') || request()->is('academic.category/*') ? 'active' : '' ">
                            <i class="fas fa-th nav-icon">

                            </i>
                            Catagories
                        </a>
                    </li>
                    <!-- Aca-Catagories End -->
                    <!-- Aca-All Post Start -->
                    <li class="nav-item">
                        <a href="{{ route('academic.index')  }}" class="nav-link  request()->is('academic') || request()->is('academic/*') ? 'active' : '' ">
                            <i class="fas fa-th nav-icon">

                            </i>
                            All Post
                        </a>
                    </li>
                    <!-- Aca-All Post End -->
                </ul>
            </li>
            <!-- Academic End -->
            @can('email_access')
            <!-- Mail Start -->
            <li class="nav-item nav-dropdown">
                <a href="{{route('email.index')}}" class="nav-link  request()->is('admin/email/') || request()->is('admin/email/*') ? 'active' : '' ">
                    <i class="fas fa-envelope nav-icon">

                    </i>
                    E-mail 
                </a>
            </li>
            <!-- Mail End -->
            @endcan
            <!-- Administration Start -->
            <li class="nav-item nav-dropdown">
                <a class="nav-link  nav-dropdown-toggle">
                    <i class="fas fa-school nav-icon">

                    </i>
                    {{ trans('global.administration') }}
                </a>
                <ul class="nav-dropdown-items">
                    <!-- Adm-System Start -->
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link  nav-dropdown-toggle">
                            <i class="fas fa-cogs nav-icon"></i>
                             System Settings 
                        </a>
                        <ul class="nav-dropdown-items">
                            @can('user_create')
                            <!-- Adm-Sys-Visitors Start -->
                            <li class="nav-item nav-dropdown">
                                <a href="{{route('visitor')}}" class="nav-link  request()->is('admin/visitor/') || request()->is('admin/visitor/*') ? 'active' : '' ">
                                    <i class="fas fa-th nav-icon">

                                    </i>
                                    Visitors 
                                </a>
                            </li>
                            <!-- Adm-Sys-Visitors End -->
                            @endcan
                            <!-- Adm-Sys-Important Links Start -->
                            <li class="nav-item nav-dropdown">
                                <a href="{{ route('importantlink.index') }}" class="nav-link  request()->is('admin/importantlink') || request()->is('admin/importantlink/*') ? 'active' : '' ">
                                    <i class="fas fa-link nav-icon">

                                    </i>
                                    Important Links 
                                </a>
                            </li>
                            <!-- Adm-Sys-Important Links End -->
                            <!-- Adm-Sys-Social Links Start -->
                            <li class="nav-item nav-dropdown">
                                <a href="{{ route('sociallink.index') }}" class="nav-link  request()->is('admin/sociallink') || request()->is('admin/sociallink/*') ? 'active' : '' ">
                                    <i class="fas fa-link nav-icon">

                                    </i>
                                    Social Links 
                                </a>
                            </li>
                            <!-- Adm-Sys-Social Links End -->
                        </ul>
                    </li>
                    <!-- Adm-System End -->
                    <!-- Adm-Home Setings Start -->
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link  nav-dropdown-toggle">
                            <i class="fas fa-home nav-icon"></i>
                            Home Settings 
                        </a>
                        <ul class="nav-dropdown-items">
                            <!-- Adm-Home-Speech Start-->
                            <li class="nav-item nav-dropdown">
                                <a href="{{ route('speech.index') }}" class="nav-link  request()->is('speech') || request()->is('speech/*') ? 'active' : '' ">
                                    <i class="fas fa-comment-dots nav-icon">

                                    </i>
                                    Speech 
                                </a>
                            </li>
                            <!-- Adm-Home-Speech End-->
                            <!-- Adm-Home-General Start-->
                            <li class="nav-item nav-dropdown">
                                <a href="{{ route('general.index') }}" class="nav-link  request()->is('general') || request()->is('general/*') ? 'active' : '' ">
                                    <i class="fas fa-cog nav-icon">

                                    </i>
                                    General 
                                </a>
                            </li>
                            <!-- Adm-Home-General End-->
                            <!-- Adm-Home-Home Slider Start-->
                            <li class="nav-item nav-dropdown">
                                <a href="{{ route('homeslider.index') }}" class="nav-link  request()->is('admin/homeslider') || request()->is('admin/homeslider/*') ? 'active' : '' ">
                                    <i class="fas fa-image nav-icon">

                                    </i>
                                    Home Slider 
                                </a>
                            </li>
                            <!-- Adm-Home-Home Slider End-->
                        </ul>
                    </li>
                    <!-- Adm-Home Setings End -->
                    @can('user_create')
                    <!-- Adm-Permission Start -->
                    <li class="nav-item">
                        <a href="{{ route('admin.permissions.index') }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                            <i class="fas fa-unlock-alt nav-icon">

                            </i>
                            {{ trans('global.permission.title') }}
                        </a>
                    </li>
                    <!-- Adm-Permission End -->
                    <!-- Adm-Role Start -->
                    <li class="nav-item">
                        <a href="{{ route('admin.roles.index') }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                            <i class="fas fa-briefcase nav-icon">

                            </i>
                            {{ trans('global.role.title') }}
                        </a>
                    </li>
                    <!-- Adm-Role End -->
                    @endcan
                    @can('user_access')
                    <!-- Adm-User Start -->
                    <li class="nav-item">
                        <a href="{{ route('admin.users.index') }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                            <i class="fas fa-user nav-icon">

                            </i>
                            {{ trans('global.user.title') }}
                        </a>
                    </li>
                    <!-- Adm-User End -->
                    @endcan
                </ul>
            </li>
            <!-- Administration End -->
            @can('user_create')
            <!-- Product Start -->
            <li class="nav-item">
                <a href="{{ route('admin.products.index') }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">
                    <i class="fas fa-cogs nav-icon">

                    </i>
                    {{ trans('global.product.title') }}
                </a>
            </li>
            <!-- Product End -->
            @endcan
            <!-- Logout Start -->
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>
            <!-- Logout End -->
        </ul>
        <!-- Menu Option End -->
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>