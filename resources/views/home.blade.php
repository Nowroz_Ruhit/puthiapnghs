@extends('layouts.admin')
@section('content')
<div class="card text-center mt-3">
  <div class="card-header p-3"></div>
  <div class="card-body">
    <div class="card-title text-primary p-5 display-4 "><p>Welcome To Dashboard</p></div>
  </div>
  <div class="card-footer text-muted p-3"></div>
@endsection
@section('scripts')
@parent

@endsection