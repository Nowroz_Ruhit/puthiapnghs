@extends('frontend::layouts.guest')
@section('content')
        <style>
            
            .full-height {
                height: 100vh;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            .code {
                border-right: 2px solid;
                font-size: 36px;
                padding: 0 15px 0 15px;
                text-align: center;
            }
            .message {
                font-size: 28px;
                text-align: center;
            }
        </style>

        <div class="flex-center position-ref full-height ">
            <div class="code theme-color">
                @yield('code')
            </div>

            <div class="message theme-color" style="padding: 10px;">
                @yield('message')
            </div>
        </div>
@endsection