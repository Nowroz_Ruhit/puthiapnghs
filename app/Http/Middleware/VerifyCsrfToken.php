<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = ['/foo',
        '/admission/payment/success','/payment.fail','/payment.cancel',
        'https://sandbox.sslcommerz.com/gwprocess/v3/api.php',
        '/admission/payment/cancel', '/admission/payment/fail',
    ];
}
