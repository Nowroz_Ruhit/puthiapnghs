<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\View;


use Modules\Employee\Entities\Designation;
use Modules\Employee\Entities\Institute;
use Modules\General\Entities\General;
use Modules\Academic\Entities\AcademicCategory;
use Modules\PhotoGallery\Entities\Gallery;
use Modules\Sociallink\Entities\Sociallink;
use Modules\Speech\Entities\Speech;
use Modules\Importantlink\Entities\Importantlink;
use Modules\Page\Entities\Page;
use Modules\Admission\Entities\AdmissionStart;



class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
        $this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);
        
        $general = General::latest()->first();
        $academicCategory = AcademicCategory::all();
        $designation = Designation::all();
        $institute = Institute::all();
        $sociallink = Sociallink::where('active',1)->get();
        $importantlink = Importantlink::where('active',1)->get();
        $speech = Speech::all();
        $page = Page::where('active',1)->orderBy('page_oder', 'ASC')->get();
        $admissionStart = AdmissionStart::latest()->first();


        View::share('importantlink',$importantlink);
        View::share('sociallink',$sociallink);
        View::share('bootGeneral', $general);
        View::share('bootAcademicCategory', $academicCategory);
        View::share('bootDesignation', $designation);
        View::share('bootInstitute', $institute);
        View::share('bootSpeech', $speech);
        View::share('bootPage', $page);
        View::share('bootAdmissionStart', $admissionStart);
    }
}
