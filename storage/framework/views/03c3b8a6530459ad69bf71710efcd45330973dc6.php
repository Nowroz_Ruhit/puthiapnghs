
<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('frontend.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.paymentStatus')); ?>">Status</a></li>
        <li class="breadcrumb-item active" aria-current="page">Student Info</li>
    </ol>
</nav>
<div class="accordion" id="accordionExample">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Student Information
                </button>
            </h2>
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="row">
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Name :<?php echo e($admissionFormDetails->name); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Date Of Birth :<?php echo e($admissionFormDetails->birth); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Gender :<?php echo e($admissionFormDetails->gender); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Blood :<?php echo e($admissionFormDetails->blood); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Religion :<?php echo e($admissionFormDetails->religion); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Birth Certificate : 
                            <?php echo e($admissionFormDetails->birth_certificate); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Nationality :<?php echo e($admissionFormDetails->nationality); ?>

                        </p>
                        <p class="justify-content-sd-start">Previous Instretute :<?php echo e($admissionFormDetails->previousInstretute); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Previous Roll :<?php echo e($admissionFormDetails->previousRoll); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-2 mt-2">
                    <img id="previewHolder" alt="Student Image" src="<?php echo e($admissionFormDetails->image ? asset('public/uplodefile/admission/'.$admissionFormDetails->image) : asset('public/uplodefile/defult/user.png')); ?>" class="circle mx-auto" width="120" />
                    
                </div>
                
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Gurdian Information
                </button>
            </h2>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
            <div class="row">
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Father Name :<?php echo e($admissionFormDetails->father_name); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Father Nid :<?php echo e($admissionFormDetails->father_nid); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Father Phone Number:<?php echo e($admissionFormDetails->father_phone); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Father Profession :<?php echo e($admissionFormDetails->father_profession); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Father Email :<?php echo e($admissionFormDetails->father_email); ?>

                        </p>
                        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Mother Name :<?php echo e($admissionFormDetails->mother_name); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Mother Nid :<?php echo e($admissionFormDetails->mother_nid); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Mother Phone :<?php echo e($admissionFormDetails->mother_phone); ?>

                        </p>
                        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Mother Profession :<?php echo e($admissionFormDetails->mother_profession); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Mother Email :<?php echo e($admissionFormDetails->mother_email); ?>

                        </p>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingThree">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Present Address
                </button>
            </h2>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
            <div class="row">
                <div class="col-md-6">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Present Address :<?php echo e($admissionFormDetails->present_address); ?>

                        </p>
                        <!--<p class="justify-content-sd-start">
                            Present Village :
                        </p>
                        <p class="justify-content-sd-start">
                            Present Road :
                        </p>
                        <p class="justify-content-sd-start">
                            Present Post Office :
                        </p>-->
                        <p class="justify-content-sd-start">
                            Present Post Code :<?php echo e($admissionFormDetails->present_post_code); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Present Thana :<?php echo e($admissionFormDetails->psnt_thana->name); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Present District :<?php echo e($admissionFormDetails->psnt_district->name); ?>

                        </p>

                        <!-- <p class="justify-content-sd-start">
                            Mother Nid :<?php echo e($admissionFormDetails->mother_nid); ?>

                        </p> -->
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="card-body">
                        
                    </div>
                </div> -->
                <div class="col-md-6">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Permananent Address :<?php echo e($admissionFormDetails->permananent_address ?
                            $admissionFormDetails->permananent_address :
                            $admissionFormDetails->present_address); ?>

                        </p>
                        <!--<p class="justify-content-sd-start">
                            Permananent Village :
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Road :
                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Post Office :
                        </p>-->
                        <p class="justify-content-sd-start">
                            Permananent Post Code :<?php echo e($admissionFormDetails->permananent_post_code ? 
                            $admissionFormDetails->permananent_post_code : 
                            $admissionFormDetails->present_post_code); ?>


                        </p>
                        <p class="justify-content-sd-start">
                            Permananent Thana :<?php echo e($admissionFormDetails->permananent_thana ?
                            $admissionFormDetails->per_thana->name : 
                            $admissionFormDetails->psnt_thana->name); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Permananent District :<?php echo e($admissionFormDetails->permananent_district ?
                            $admissionFormDetails->per_district->name : 
                            $admissionFormDetails->psnt_district->name); ?>

                        </p>
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="card-body">
                        
                    </div>
                </div> -->
                <?php if($admissionFormDetails->PrimaryContact==3): ?>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Name :<?php echo e($admissionFormDetails->localGurdian_name); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Relation :<?php echo e($admissionFormDetails->localGurdian_relation); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Phone Number:<?php echo e($admissionFormDetails->localGurdian_phone); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Email :<?php echo e($admissionFormDetails->localGurdian_email); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Address :<?php echo e($admissionFormDetails->localGurdian_address); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village :<?php echo e($admissionFormDetails->localGurdian_village); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village Road :<?php echo e($admissionFormDetails->localGurdian_road); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Post Office :<?php echo e($admissionFormDetails->localGurdian_post_office); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian PostCode :<?php echo e($admissionFormDetails->localGurdian_postCode); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Thana :<?php echo e($admissionFormDetails->localGurdian_thana); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian District :<?php echo e($admissionFormDetails->localGurdian_district); ?>

                        </p>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingFour">
            <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                    Primary Contact
                </button>
            </h2>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
            <div class="row">
                
                <?php if($admissionFormDetails->PrimaryContact == 3): ?>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Name :<?php echo e($admissionFormDetails->localGurdian_name); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Relation :<?php echo e($admissionFormDetails->localGurdian_relation); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Phone Number:<?php echo e($admissionFormDetails->localGurdian_phone); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Email :<?php echo e($admissionFormDetails->localGurdian_email); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian Address :<?php echo e($admissionFormDetails->localGurdian_address); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village :<?php echo e($admissionFormDetails->localGurdian_village); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Village Road :<?php echo e($admissionFormDetails->localGurdian_road); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Post Office :<?php echo e($admissionFormDetails->localGurdian_post_office); ?>

                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Local Gurdian PostCode :<?php echo e($admissionFormDetails->localGurdian_postCode); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian Thana :<?php echo e($admissionFormDetails->localGurdian_thana); ?>

                        </p>
                        <p class="justify-content-sd-start">
                            Local Gurdian District :<?php echo e($admissionFormDetails->localGurdian_district); ?>

                        </p>
                    </div>
                </div>
                <?php elseif($admissionFormDetails->PrimaryContact == 2): ?>
                <div class="col-lg-12">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Primary Contact Is Set As Mother
                        </p>
                        <p class="justify-content-sd-start">
                            All Information about Admission will resived By <?php echo e($admissionFormDetails->name); ?>'s Mother By SMS & Email
                        </p>
                        
                    </div>
                </div>
                <?php elseif($admissionFormDetails->PrimaryContact == 1): ?>
                <div class="col-lg-12">
                    <div class="card-body">
                        <p class="justify-content-sd-start">
                            Primary Contact Is Set As Father
                        </p>
                        <p class="justify-content-sd-start">
                            All Information about Admission will resived By <?php echo e($admissionFormDetails->name); ?>'s Father By SMS & Email
                        </p>
                        
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/studentDetails.blade.php ENDPATH**/ ?>