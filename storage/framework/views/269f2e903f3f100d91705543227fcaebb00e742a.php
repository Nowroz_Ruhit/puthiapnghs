
<?php $__env->startSection('content'); ?>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('importantsitelink_create')): ?>
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href='<?php echo e(route("importantlink.create")); ?>'>
                Add Important Link
            </a>
        </div>
    </div>
<?php endif; ?>
<div class="card">
    <div class="card-header">
        Important Link List
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>
                        <th>
                            
                        </th>
                        <th>
                            Site tittle
                        </th>
                        <th>
                            Link
                        </th>
                        <th>
                            logo
                        </th>
                        <th>
                            Publish
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $importantlinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $importantlink): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($importantlink->id); ?>">
                            <td  width="1">
                                
                            </td>
                            <td>
                                <?php echo e($importantlink->title ?? ''); ?>

                            </td>
                            <td width="5">
                                <?php echo e($importantlink->link ?? ''); ?>

                            </td>
                            <td>
                                <img src="<?php echo e($importantlink->logo ? asset(trans('global.links.importantlink').$importantlink->logo) : asset('public/img/logo/006-jpg.png') ?? ''); ?>" width="50" height="50">
                            </td>

                            <td>

                                <?php echo e($importantlink->active ? 'Published' : 'Unpublish' ?? ''); ?>

                            </td>

                            <td>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('importantsitelink_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('importantlink.edit',$importantlink)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('importantsitelink_delete')): ?>
                                    <form action="<?php echo e(route('importantlink.destroy', $importantlink)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="submit" class="btn btn-xs btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                    </form>
                                <?php endif; ?>
                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });
</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Importantlink/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>