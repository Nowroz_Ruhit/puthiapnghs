
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>


<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('news.index')); ?>">News</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.news.title_singular')); ?>

    </div>
    <div class="card-body">
        <?php echo e(Aire::open()
            ->route('news.update', $news)
            ->bind($news)
            ->rules([
            'title' => 'required',
            'discrioption' => 'required',
            'photo'=> 'required|file',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')); ?>


        <?php echo e(Aire::input()
            ->label('Title*')
            ->id('title')
            ->name('title')
            ->class('form-control')); ?>


        <?php echo e(Aire::textarea()
            ->label('Discription*')
            ->id('discription')
            ->name('discription')
            ->class('form-control')); ?>

        <script>
            CKEDITOR.replace( 'discription' );
        </script>

        <?php echo e(Aire::select($catagory)
            ->class('form-control')
            ->label('News Catagory')
            ->name('catagories_id')); ?>

 <div class=" mx-sm-3 mb-2">
            <img id="cover-img-tag" width="200px" alt="Image Preview" src="<?php echo e($news->photo ? asset(trans('global.links.news_show').$news->photo ) : asset('public/img/logo/006-jpg.png') ?? ''); ?>"  />
        </div>
        <?php echo e(Aire::input()
            ->type('file')
            ->label('News Cover Photo (image size must be 450 × 450) *')
            ->id('cover-photo')
            ->name('photo')); ?>



<!-- <img src="" id="cover-img-tag" width="200px" class="mb-2" /> -->


        <?php echo e(Aire::button()
            ->labelHtml('<strong>Next</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>



        <?php echo e(Aire::close()); ?>

    </div>
</div>


<script src="<?php echo e(asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js')); ?>"></script>

<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#cover-photo").change(function(){
        readURL(this);
    });

</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/News/Providers/../Resources/views/edit.blade.php ENDPATH**/ ?>