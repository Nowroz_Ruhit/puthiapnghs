
<?php $__env->startSection('content'); ?>


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Administration</a></li>
    <li class="breadcrumb-item"><a href="#">System Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Social Links</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>All Social Links</b></a>

      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('sociallink_create')): ?>
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-success" href="<?php echo e(route('sociallink.create')); ?>">Add Social Link</a>
        </form>
      <?php endif; ?>

    </nav>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>
                        <th>
                            
                        </th>
                        <th>
                            Site tittle
                        </th>
                        <th>
                            Link
                        </th>
                        <th>
                            logo
                        </th>
                        <th>
                            Publish
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $sociallinks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $sociallink): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($sociallink->id); ?>">
                            <td  width="1">
                                
                            </td>
                            <td>
                                <?php echo e($sociallink->title ?? ''); ?>

                            </td>
                            <td width="5">
                                <?php echo e($sociallink->link ?? ''); ?>

                            </td>
                            <td>
                                <img src="<?php echo e($sociallink->logo ? asset('public/img/logo/'.$sociallink->logo) : asset('public/img/logo/006-jpg.png') ?? ''); ?>" width="50" height="50">
                            </td>

                            <td>

                                <?php echo e($sociallink->active ? 'Published' : 'Unpublish' ?? ''); ?>

                            </td>

                            <td>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('sociallink_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('sociallink.edit',$sociallink)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('sociallink_delete')): ?>
                                    <form action="<?php echo e(route('sociallink.destroy', $sociallink)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="submit" class="btn btn-xs btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                    </form>
                                <?php endif; ?>
                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script src="<?php echo e(asset('public/lib/hullabaloo/js/hullabaloo.js')); ?>"></script>
<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });

    var hulla = new hullabaloo();
  <?php if(Session::has('success')): ?> 
          hulla.send("<?php echo e(Session::get('success')); ?>", 'danger') 
     <?php
       Session::forget('success');
     ?>
  <?php endif; ?>
</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Sociallink/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>