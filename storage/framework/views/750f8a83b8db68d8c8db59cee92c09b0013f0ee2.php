<?php
use Modules\Frontend\Http\Controllers\FrontendController;
// use Modules\Visitor\Entities\Visitor;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e($bootGeneral->title); ?></title>
    <meta name="author" content="Puthia P.N Government High School, Puthia, Rajshahi">
    <meta name="keywords" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi, puthia p.n government high school puthia rajshahi, puthia p.n government high school admission form, admission form puthia p.n government high school, online admission form puthia p.n government high school">
    <meta name="description" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi, puthia p.n government high school puthia rajshahi, puthia p.n government high school admission form, admission form puthia p.n government high school, online admission form puthia p.n government high school">
    
    <meta property="og:title" content="PUTHIAPNGHS| puthia p.n government high school, puthia, rajshahi| পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় | PUTHIAPNGHS Admission" />
    <meta property="og:site_name" content="পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়" />
    <meta property="og:description" content="PUTHIAPNGHS, puthia p.n government high school puthia rajshahi, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় , puthia p.n government high school puthia rajshahi" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://puthiapnghs.edu.bd"/>
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-YSYG2CN2XW"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-YSYG2CN2XW');
    </script>
    <?php echo $__env->yieldPushContent('css'); ?>




<!--     <link rel="shortcut icon" href="<?php echo e(asset('public/guest-user/images/favicon.png')); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo e(asset('public/guest-user/images/favicon.png')); ?>" type="image/x-icon"> -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->fab )); ?>">
    
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/css/bootstrap.min.css')); ?>">
    <!-- <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/assets/fontawesome-5.10.2/css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/assets/themify-icons/themify-icons.css')); ?>">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/css/custom.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/css/responsive.css')); ?>">
    <link href="<?php echo e(asset('css/toastr.min.css')); ?>" rel="stylesheet" />
    


    <?php echo $__env->yieldContent('styles'); ?>



</head>

<body>
    <div class="container main-container pt-3">
            <!-- Section Topbar -->
    <section class="top-bar" id="topBar">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-5">
            <div class="top-contact">
              <?php if($bootGeneral->email): ?>
              <p>
                <i class="far fa-envelope"></i>
                <!-- info@schoolname.edu.bd -->
                <?php echo e($bootGeneral->email); ?>

              </p>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 col-sm-4">
            <div class="top-contact">
              <?php if($bootGeneral->phone): ?>
              <p>
                <?php if(!empty($bootGeneral->tel_phone)): ?>
                <i class="fas fa-phone-square"></i>
                <?php echo e($bootGeneral->tel_phone); ?>

                <?php endif; ?>
                <i class="fas fa-mobile-alt"></i>
                <!-- +880 1754781301 -->
                <?php echo e($bootGeneral->phone); ?>

              </p>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-lg-5 col-md-4 col-sm-3">
            <div class="top-social">
              <ul>
                <li>
                  <a href="#">
                    Follow Us:
                  </a>
                </li>
                <li>
                  <a href="<?php echo e($sociallink[0]->link); ?>" target="_blank">
                    <i class="fab fa-facebook-f"></i>
                  </a>
                </li>
                <li>
                  <a href="<?php echo e($sociallink[1]->link); ?>" target="_blank">
                    <i class="fab fa-twitter"></i>
                  </a>
                </li>
<!--                 <li>
                  <a href="<?php echo e(route('admin.home')); ?>" target="_blank">
                    <i class="fas fa-user nav-icon"></i> Login
                  </a>
                </li> -->
              </ul>
            </div>
          </div>
        </div>
    </section>
    <!-- End Section Topbar -->

    <!-- Logo Wrap  -->
    <section class="logo-wrap">
        <div class="row">
          <div class="col-md-12">
            <img class="banner-bg " src="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->banar)); ?>" alt="Header Banner">
            <?php if($bootGeneral->logo): ?>
            <a class="navbar-brand" href="#">
              <!-- <img src="<?php echo e(asset('public/guest-user/images/logo.png')); ?>" alt="PiceIntlBD"> -->
              <!-- <span>Government Laboratory High School, Bangladesh</span> -->
              <img src="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->logo)); ?>" alt="PiceIntlBD">
              <span style="color: #9ab632"><?php echo e($bootGeneral->title); ?></span>
            </a>
            <?php endif; ?>
          </div>
        </div>
    </section>
    <!-- Logo Wrap  -->
<!-- include('partials.guestmenu') -->
<!-- include("route('guest')") -->
<!-- url('guest') -->


<!--Section NavBar -->
    <section id="mainNav">

        <nav class="navbar navbar-expand-lg navbar-light">

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav m-auto py-4 py-md-0 nav">
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link " href="<?php echo e(route('index')); ?>">হোম</a>
              </li>              
              <!-- <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="route('about')">আমাদের সম্পর্কে</a>
              </li> -->
              <!-- Start About -->
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">আমাদের সম্পর্কে
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <!-- academicCategory -->
                  <a class="dropdown-item" href="<?php echo e(route('about')); ?>">প্রতিষ্ঠান সম্পর্কে</a>
                  <?php $__currentLoopData = $bootSpeech; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $speech): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php
                    if(empty($speech->name))
                      continue;
                    ?>
                    <?php if($loop->index != $loop->last): ?>
                      <a class="dropdown-item" href="<?php echo e(route('frontend.speech', $speech)); ?>"><?php echo e($speech->sortSpeech); ?></a>
                    <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                  
                </div>
              </li>
              <!-- End About -->
              <!-- Start academic -->
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">একাডেমিক
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <!-- academicCategory -->
                  <?php $__currentLoopData = $bootAcademicCategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <a class="dropdown-item" href="<?php echo e(route('frontend.academic', $value->title)); ?>"><?php echo e($value->title); ?></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
              </li>
              <!-- End academic -->
           <?php
                    $date = new DateTime('now', new DateTimeZone('Asia/Dhaka'));
                    //echo $date->format("Y/m/d");
                    $start = date('Y/m/d', strtotime($bootAdmissionStart->start));
                    $end = date('Y/m/d', strtotime($bootAdmissionStart->end));
                    $today = $date->format("Y/m/d")
            ?>
               
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">ভর্তি পরীক্ষা
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <!-- academicCategory -->
                  <?php if(($start <= $today) && ($today <= $end)): ?>
                  <a target="_blank" class="dropdown-item" href="<?php echo e(route('frontend.admission.index')); ?>">অনলাইন ভর্তি ফর্ম</a>
                  <?php endif; ?>

                  
                </div>
              </li>
             
              
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">নোটিশ
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="<?php echo e(route('frontend.notice')); ?>">নোটিশ</a>
                  <a class="dropdown-item" href="<?php echo e(route('frontend.news')); ?>">নিউজ</a>
                </div>
              </li>
              <!-- <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="route('frontend.result')">ফলাফল</a>
              </li>  -->             
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="<?php echo e(route('gallery')); ?>">ফটো গ্যালারী</a>
              </li>

              <?php if(!empty($bootPage[0])): ?>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">ফ্যাসিলিটিজ
                  <i class="fas fa-angle-down nav-dd-icon"></i>
                </a>
                <div class="dropdown-menu">
                  <?php $__currentLoopData = $bootPage; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <a class="dropdown-item" href="<?php echo e(route('frontend.page', $page)); ?>"><?php echo e($page->title); ?></a>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
              </li>
              <?php endif; ?>
              <li class="nav-item pl-4 pl-lg-0 ml-0 ml-md-4">
                <a class="nav-link" href="<?php echo e(route('contact')); ?>">যোগাযোগ</a>
              </li>
            </ul>
          </div>
        </nav>
    </section>
    <!-- End Section NavBar-->
<?php echo $__env->yieldContent('content'); ?>


    <!-- Footer -->
    <footer class="mt-3">
        <div class="row">
          <div class="col-md-4 text-left footer-contact">
            <h4><i class="fas fa-square-full"></i> যোগাযোগ</h4>
            <?php if($bootGeneral->phone): ?>
            <p>
              <i class="fas fa-phone-square-alt"></i>
              <!-- + 880 1754 781301 -->
              <?php echo e($bootGeneral->phone); ?>

            </p>
            <?php endif; ?>
            <?php if($bootGeneral->email): ?>
            <p>
              <i class="far fa-envelope"></i>
              <!-- info@schoolname.edu.bd -->
              <?php echo e($bootGeneral->email); ?>

            </p>
            <?php endif; ?>
            <p>
              <i class="fas fa-map-marker-alt"></i>
              <!-- 203/1 Par-Naogaon<br> -->
              <!-- Naogaon-6500, Bangladesh -->
              <?php echo $bootGeneral->address; ?>

            </p>
          </div>

          <div class="col-md-4 text-left footer-service">
            <h4><i class="fas fa-square-full"></i> গুরুত্বপূর্ণ লিংক</h4>
            <ul>
              <?php $__currentLoopData = $importantlink; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <li>
                <a href="<?php echo e($value->link); ?>" target="_blank">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  <?php echo e($value->title); ?></a>
              </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<!--               <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Industrial Consultancy
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Turnkey Project
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Distributor & Agent
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Installation & Maintenance
                </a>
              </li> -->
            </ul>
          </div>
          <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('not_show')): ?>
          <div class="col-md-4 text-left footer-service">
            <h4><i class="fas fa-square-full"></i> ভিজিটর</h4>
            <ul>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Today Visited : <?php echo e(Visitor::where('date', date('Y-m-d'))->sum('hits')); ?>

                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Yesterday Visit : 
                  <!-- <?php echo e(date('d MM Y',strtotime("-1 days"))); ?> -->
                  <?php echo e(Visitor::where('date', date('Y-m-d',strtotime("-1 days")))->sum('hits')); ?>

                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Visited User <?php echo e(Visitor::distinct('ip')->count('ip')); ?></a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Totla Visit : <?php echo e(Visitor::sum('hits')); ?>

                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Today : <?php echo e(date('d M Y')); ?>

                </a>
              </li>
              

              <!-- <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Distributor & Agent
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="ti-angle-double-right" aria-hidden="true"></i>
                  Installation & Maintenance
                </a>
              </li> -->
            </ul>
          </div>
          <?php endif; ?>
        </div>
    </footer>
    <!-- End Footer -->
    <section class="copyright-power">
      <div class="row">
        <div class="col-sm-6">
          <p>Copyright &copy; 2020 | <?php echo e($bootGeneral->title); ?></p>
        </div>
        <div class="col-sm-6 text-right">
          <p>Powered by <a href="http://desktopit.com.bd">Desktop IT</a>  </p>
        </div>
      </div>
    </section>
    </div>
    <!-- </div> -->


    <!-- Bootstrap JS -->
    <script src="<?php echo e(asset('public/guest-user/js/jquery-3.3.1.slim.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_jquery_3.3.1_jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/guest-user/js/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/guest-user/js/bootstrap.min.js')); ?>"></script>

    <!-- Custom JS -->
    <script src="<?php echo e(asset('public/guest-user/js/custom.js')); ?>"></script>
    
    
    <?php echo $__env->yieldContent('scripts'); ?>

<script src="<?php echo e(asset('public/lib/hullabaloo/js/hullabaloo.js')); ?>"></script>
<script type="text/javascript">
  var hulla = new hullabaloo();
  <?php if(Session::has('success')): ?> 
          hulla.send("<?php echo e(Session::get('success')); ?>", 'success') 
     <?php
       Session::forget('success');
     ?>
  <?php endif; ?>

  <?php if(Session::has('danger')): ?> 
          hulla.send("<?php echo e(Session::get('danger')); ?>", 'danger') 
     <?php
       Session::forget('danger');
     ?>
  <?php endif; ?>
  </script>

</body>
</html><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Frontend/Providers/../Resources/views/layouts/guest.blade.php ENDPATH**/ ?>