

<?php $__env->startSection('content'); ?>

	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">একাডেমিক</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo e($type); ?></li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

	<section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              <!-- সিলেবাস -->
              <?php echo e($type); ?>

            </div>
            <?php if(empty($academics[0])): ?>
            <div class="text-center pt-5 pb-5"><?php echo e(trans('global.nodata')); ?></div>
            <?php else: ?>
            <div class="card-body">
              	<div class="table-responsive syllabus-table">
				  	<table class="table">
					   	<thead class="thead-light">
							<tr>
								<th scope="col">SL. No.</th>
								<th scope="col">Title</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $__currentLoopData = $academics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $academic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<th scope="row"><?php echo e($key+1); ?></th>
								<td><?php echo e($academic->title); ?></td>
								<td>
									<a class="btn btn-outline-success" href="<?php echo e(route('frontend.academic_view', [$type, $academic->id])); ?>"><i class="far fa-eye"></i> View</a>
									<a class="btn btn-outline-warning" href="<?php echo e(route('frontend.academic.download', [$type, $academic->id])); ?>"><i class="fas fa-download"></i> Download</a>	
								</td>
							</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</tbody>
				  	</table>
				</div>				              
            </div>
            <?php endif; ?>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Frontend/Providers/../Resources/views/academic.blade.php ENDPATH**/ ?>