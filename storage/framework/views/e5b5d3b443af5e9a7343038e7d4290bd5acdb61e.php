
<?php $__env->startSection('style'); ?>
##parent-placeholder-26ec8d00fb6b55466b3a115f1d559422a7fa7aac##
<!-- STYLE CSS -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<!-- body content -->
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('notice.index')); ?>">Notice</a></li>
    <li class="breadcrumb-item active" aria-current="page">view</li>
  </ol>
</nav>


<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Signle Notice View</b></a>
      <form class="form-inline">
        <a class="form-control mr-sm-2" href="<?php echo e(route('notice.edit', $notice->id)); ?>">Edit Notice</a>
      </form>
    </nav>
  <div class="card-body">
    <p class="card-text"><h5 class="card-title">Notice Title: <?php echo e($notice->title); ?></h5></p>
    <!-- <p class="card-text">lore lip sum</p> -->
    <p class="card-text"><h5>Notice Description:</h5>
      <?php echo $notice->discription; ?>

    </p>

  </div>
    
   </br>
   <div class="container mb-2">
    Attesment :  <a href="<?php echo e(route('notice.download', $notice->id)); ?>"><?php echo e($notice->file); ?></a>
   </div>
   <br>
</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Notice/Providers/../Resources/views/show.blade.php ENDPATH**/ ?>