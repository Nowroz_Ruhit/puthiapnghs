

<?php $__env->startSection('content'); ?>


    <!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">একাডেমিক</a></li>
        <li class="breadcrumb-item active" aria-current="page">নোটিশ</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

	<section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              নোটিশ
            </div>
            <div class="card-body">
            	<?php if(empty($notices[0])): ?>
	            <div class="text-center pt-5 pb-5"><?php echo e(trans('global.nodata')); ?></div>
	            <?php else: ?>
              	<div class="table-responsive syllabus-table">
				  	<table class="table">
					   	<thead class="thead-light">
							<tr>
								<th scope="col">SL. No.</th>
								<th scope="col">Notice Title</th>
								<th scope="col">Published Date</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $__currentLoopData = $notices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $notice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<tr>
								<th scope="row"><?php echo e($key+1); ?></th>
								<td><?php echo e($notice->title); ?></td>
								<td><?php echo e($notice->updated_at->format('d/m/y')); ?></td>
								<td>
									<?php if($notice->file): ?>
									<a class="btn btn-outline-success" href="<?php echo e(route('frontend.notice.show', $notice)); ?>"><i class="far fa-eye"></i> View</a>
									<a class="btn btn-outline-warning" href="<?php echo e(route('frontend.notice_download', $notice )); ?>"><i class="fas fa-download"></i> Download</a>
									<?php endif; ?>	
								</td>
							</tr>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</tbody>
				  	</table>
				  	<!-- Start Pagination -->
		            	<div class="row">	
		            		<div class="col-sm-12">
		            			<nav aria-label="Page navigation example">
									<ul class="pagination justify-content-center">
										<?php echo e($notices->links()); ?>

									</ul>
								</nav>
		            		</div>
		            	</div>
		            	<!-- End Pagination -->
		            	
				</div>
				<?php endif; ?>				              
            </div>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Frontend/Providers/../Resources/views/notice.blade.php ENDPATH**/ ?>