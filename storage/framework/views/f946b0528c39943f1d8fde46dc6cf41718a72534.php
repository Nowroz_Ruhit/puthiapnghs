
<?php $__env->startSection('content'); ?>

<div class="card">
	<div class="card-header">
		Visitor List
	</div>

	<div class="card-body">
		<div class="table-responsive">
			<table class=" table table-bordered table-striped table-hover datatable">
				<thead>
					<tr>
						<th>
							id
						</th>
						<th>
							ip
						</th>
						<th>
							hits
						</th>
						<!-- <th>
							date
						</th> -->
						<!-- <th>
							visit_time
						</th> -->
						<th>
							Public IP
						</th>
						<th>
							City
						</th>
						<th>
							Region Name
						</th>
						<th>
							Country
						</th>
						<th>
							created_at
						</th>
						<th>
							updated_at
						</th>
					</tr>
				</thead>
				<tbody>
					<?php $__currentLoopData = $visitors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $visitor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<tr data-entry-id="<?php echo e($visitor->id); ?>">
						<td>
							<?php echo e($visitor->id ?? ''); ?>

						</td>
						<td>
							<?php echo e($visitor->ip ?? ''); ?>

						</td>
						<td>
							<?php echo e($visitor->hits ?? ''); ?>

						</td>
					<!-- <td>
							 $visitor->date ?? '' 
						</td> -->
						<!-- <td>
							 $visitor->visit_time ?? '' 
						</td> -->
						<td>
							<?php echo e($visitor->public_ip ?? ''); ?>

						</td>
						<td>
							<?php echo e($visitor->city ?? ''); ?>

						</td>
						<td>
							<?php echo e($visitor->regionName ?? ''); ?>

						</td>
						<td>
							<?php echo e($visitor->country ?? ''); ?>

						</td>
						<td>
							<?php echo e($visitor->created_at->format('d M Y h:i:s a') ?? ''); ?>

						</td>
						<td>
							<?php echo e($visitor->updated_at->format('d M Y h:i:s a') ?? ''); ?>

						</td>
					</tr>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
	$(function () {
		let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
		$('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
	})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Visitor/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>