
<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<!-- <link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" /> -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.subjectSettings.index')); ?>">Subject Settings</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.subjectSettings.subject')); ?>">Subjects</a></li>
        <li class="breadcrumb-item active" aria-current="page">Update</li>
    </ol>
</nav>
<div class="card">
    <div class="card-header">
        Update
    </div>
    
    <div class="card-body">

        <?php echo e(Aire::open() 
            ->route('admin.admission.subjectSettings.subjects.update', $subjectList)
            ->bind($subjectList)
            ->rules([
                'subject_name' => 'required',
                ])
            ->enctype('multipart/form-data')); ?>


        <?php echo e(Aire::input()
            ->class('form-control')
            ->id('')
            ->label('Subject Code (optional)')
            ->name('subject_code')
            ->placeholder('')); ?>


        <?php echo e(Aire::input()
            ->class('form-control')
            ->id('')
            ->label('Subject')
            ->name('subject_name')
            ->placeholder('Bangla')); ?>


        <div class="d-flex justify-content-center" >
            <div class="row" id="apply_button_responsive">
              <?php echo e(Aire::button()
                ->labelHtml('Update')
                ->id('subBtn')
                ->class(' btn btn-info')); ?>


        </div>
    </div>
</div>
<?php echo e(Aire::close()); ?>


</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/subject/subjectEdit.blade.php ENDPATH**/ ?>