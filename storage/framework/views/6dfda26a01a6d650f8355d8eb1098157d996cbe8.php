

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">SMS</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
        SMS <a class="btn bg-success text-white mr-2">Available SMS : <?php echo e($sms_count['SMSINFO']['BALANCE']); ?></a>
        <form class="form-inline">
            <a class="btn btn-outline-dark" href="<?php echo e(route('admin.admission.sms.create')); ?>">
                <i class="fas fa-edit nav-icon"></i> Create New
            </a>
        </form>

    </nav>
    <!-- <div class="card-header">SMS</div> -->
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Subject</th>
                        <th>message</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $sms; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $message): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr data-entry-id="">
                        <td><?php echo e(++$key); ?></td>
                        <td><?php echo e($message->subject); ?></td>
                        <td><?php echo e($message->message); ?></td>
                        <td>
                            <a class="btn btn-xs btn-success" href="<?php echo e(route('admin.admission.sms.sendSMS', $message)); ?>">
                                <i class="fas fa-send nav-icon"></i> Send SMS
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/sms/index.blade.php ENDPATH**/ ?>