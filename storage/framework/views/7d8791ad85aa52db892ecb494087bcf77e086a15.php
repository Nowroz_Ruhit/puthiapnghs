

<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<!--  -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="">Administration</a></li>
    <li class="breadcrumb-item"><a href="">Home Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Spech</li>
  </ol>
</nav>

<div class="card">
  <!-- <nav class="navbar navbar-light bg-light justify-content-between">
       trans('global.speech.title') 
      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('speech_edit')): ?>
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-success" href="route('speech.edit',1)">
          <i class="fas fa-edit nav-icon"></i>  trans('global.edit') 
        </a>
      </form>
      <?php endif; ?>
    </nav> -->
<div class="card-header">
        <?php echo e(trans('global.speech.title')); ?>

    </div>
<div class="card-body">
<div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>
                            শিরনাম
                        </th>
                        <th>
                            ছবি
                        </th>
                        <th>
                            বক্তার নাম
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                   <?php $__currentLoopData = $speechs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $speech): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($speech->id); ?>">
                            <td>
                                <?php echo e($speech->designation ?? ''); ?>

                            </td>
                            <td>
                              <img height="70" width="70" src="<?php echo e(asset(trans('global.links.speech_show').$speech->files)); ?>">
                            </td>
                            <td>
                                <?php echo e($speech->name ?? ''); ?>

                            </td>
                            <td>
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('speech_show')): ?>
                                    <a class="btn btn-xs btn-primary" href="<?php echo e(route('speech.show', $speech)); ?>">
                                        <?php echo e(trans('global.view')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('speech_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('speech.edit', $speech)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                            </td>

                            
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                </tbody>
            </table>
        </div>
</div>
</div>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Speech/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>