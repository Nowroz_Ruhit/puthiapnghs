

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Applicant Report</li>
    </ol>
</nav>
<div class="card">
    
    <div class="card-header">Result
      <div class="float-right">
        <a class="btn btn-outline-dark mr-1" id="print-btn">
            <i class="fas fa-print nav-icon"></i> Print 
        </a>
      </div>
    </div>
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>SL #</th>
                        <th>Image</th>
                        <th>Admission Lottery</th>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Father's Name</th>
                        <th>Primary Contact</th>
                        <?php if($qoutaStatus): ?>
                          <th>Qouta</th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $qoutaReports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $qoutaReport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr data-entry-id="">
                        <td><?php echo e(++$key); ?></td>
                        <td><img style="width: 80px; height:93px" src="<?php echo e(asset('public/uplodefile/admission/'.$qoutaReport->image)); ?>" alt="" srcset=""></td>
                        <td><?php echo e($qoutaReport->student_id->admission_id); ?></td>
                        <td><?php echo e($qoutaReport->name); ?></td>
                        <td><?php echo e($qoutaReport->className->name); ?></td>
                        <td><?php echo e($qoutaReport->father_name); ?></td>
                        <?php if($qoutaReport->PrimaryContact == 2): ?>
                            <td><?php echo e($qoutaReport->mother_phone); ?></td>
                        <?php elseif($qoutaReport->PrimaryContact == 1): ?> 
                            <td><?php echo e($qoutaReport->father_phone); ?></td>
                        <?php endif; ?>
                        <?php if($qoutaStatus): ?>
                          <td><?php echo e($qoutaReport->qoutaName); ?></td>
                        <?php endif; ?>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
    </div>
</div>

<div class="printDiv" style="visibility: hidden; display:inline;">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-12">
            <div class="table-responsive table table-bordered print-container p-3" id="div-id-name"><br>
              <div class="row justify-content-center align-items-center mb-3">
                <div class="float-left mr-4">
                  <img id='img' src="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->logo)); ?>" class="">
                </div>
                <div class="float-right mb-4">
                    <span class="font-weight-bold"
                        style="font-size: 25px; margin-top:150px"><?php echo e($bootGeneral->title); ?></span>
                    <br>
                   
                </div>
            </div>
                <h3  style="text-align:center"><?php echo e(($qoutaStatus) ? 'Qouta' : 'Non Qouta'); ?> Student List</h3>
                <br>
                <table id="order-listing" class="table">
                  <thead>
                    <tr>
                      <th style="font-size: 20px;">SL #</th>
                      <th style="font-size: 20px;">Image</th>
                      <th style="font-size: 20px;">Admission Lottery</th>
                      <th style="font-size: 20px;">Name</th>
                      <th style="font-size: 20px;">Class</th>
                      <th style="font-size: 20px;">Father's Name</th>
                      <th style="font-size: 20px;">Primary Contact</th>
                      <?php if($qoutaStatus): ?>
                        <th style="font-size: 20px;">Qouta</th>
                      <?php endif; ?>  
                    </tr>
                  </thead>
                  <?php $i=0; ?>
                  <tbody>
                    <?php $__currentLoopData = $qoutaReports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$qoutaReport): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                        <tr>
                          <td style="font-size:18px;"><?php echo e(++$key); ?></td>
                          <td><img style="width: 80px; height:93px" src="<?php echo e(asset('public/uplodefile/admission/'.$qoutaReport->image)); ?>" alt="" srcset=""></td>
                          <td style="font-size:18px;"><?php echo e($qoutaReport->student_id->admission_id); ?></td>
                          <td style="font-size:18px;"><?php echo e($qoutaReport->name); ?></td>
                          <td style="font-size:18px;"><?php echo e($qoutaReport->className->name); ?></td>
                          <td style="font-size:18px;"><?php echo e($qoutaReport->father_name); ?></td>
                            <?php if($qoutaReport->PrimaryContact == 2): ?>
                                <td style="font-size:18px;"><?php echo e($qoutaReport->mother_phone); ?></td>
                            <?php elseif($qoutaReport->PrimaryContact == 1): ?> 
                                <td style="font-size:18px;"><?php echo e($qoutaReport->father_phone); ?></td>
                            <?php endif; ?>
                            <?php if($qoutaStatus): ?>
                              <td style="font-size:18px;"><?php echo e($qoutaReport->qoutaName); ?></td>
                            <?php endif; ?>
                        </tr>
                      
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
                </table>
                <br>
                <div class="float-left ml-2">
                  <p>Powered By : Desktopit.com.bd</p>
                </div>
                <div class="float-right">
                  <p>Date: <?php echo e(date('d-m-Y')); ?></p>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})
  $('#print-btn').click( function(){
        $('.print-container').printThis();
  })
</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/result/qoutaReport.blade.php ENDPATH**/ ?>