<?php
// use Modules\Frontend\Http\Controllers\FrontendController;
// use Modules\Visitor\Entities\Visitor;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e($bootGeneral->title); ?></title>
    <meta name="author" content="Puthia P.N Government High School, Puthia, Rajshahi">
    <meta name="keywords" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi, puthia p.n government high school puthia rajshahi, puthia p.n government high school admission form, admission form puthia p.n government high school, online admission form puthia p.n government high school">
    <meta name="description" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi, puthia p.n government high school puthia rajshahi, puthia p.n government high school admission form, admission form puthia p.n government high school, online admission form puthia p.n government high school">

    <meta property="og:title" content="PUTHIAPNGHS| puthia p.n government high school, puthia, rajshahi| পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় | PUTHIAPNGHS Admission" />
    <meta property="og:site_name" content="পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়" />
    <meta property="og:description" content="PUTHIAPNGHS, puthia p.n government high school puthia rajshahi, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় , puthia p.n government high school puthia rajshahi" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://puthiapnghs.edu.bd/admission"/>
	
	<meta property="og:image" content="http://smmplsc.edu.bd/public/admission.jpg" />
	<meta property="og:type" content="website" /> 
	<meta property="og:url" content="https://puthiapnghs.edu.bd/admission"/>
	<meta property="og:title" content="online admission puthia p.n government high school" />
  <meta property="og:description" content="PUTHIAPNGHS, PUTHIAPNGHS Admission Form, PUTHIAPNGHS Admission 2021, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় ভর্তি ফরম, Puthia P.N Government High School Puthia Rajshahi" />
    
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-YSYG2CN2XW"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-YSYG2CN2XW');
    </script>

    <?php echo $__env->yieldPushContent('css'); ?>

  


<!--     <link rel="shortcut icon" href="<?php echo e(asset('public/guest-user/images/favicon.png')); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo e(asset('public/guest-user/images/favicon.png')); ?>" type="image/x-icon"> -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->fab )); ?>">
    
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/css/bootstrap.min.css')); ?>">
    <!-- <link rel="stylesheet" href="assets/font-awesome-4.7.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/assets/fontawesome-5.10.2/css/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/assets/themify-icons/themify-icons.css')); ?>">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600,600i,700,800&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/css/custom.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/css/admission.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('public/guest-user/css/responsive.css')); ?>">
    <link href="<?php echo e(asset('css/toastr.min.css')); ?>" rel="stylesheet" />
    
    <link href="<?php echo e(asset('css/admission/admission.css')); ?>" rel="stylesheet" />


    <?php echo $__env->yieldContent('styles'); ?>



</head>

<body>
    <div class="container main-container pt-1">




<!--Section NavBar -->
    
    <!-- End Section NavBar-->
<?php echo $__env->yieldContent('content'); ?>



    <section class="copyright-power mt-3">
      <div class="row">
        <div class="col-sm-6">
          <p>Copyright &copy; <?php echo e(date('Y')); ?> | <?php echo e($bootGeneral->title); ?></p>
        </div>
        <div class="col-sm-6 text-right">
          <p>Powered by <a href="http://desktopit.com.bd">Desktop IT</a>  </p>
        </div>
      </div>
    </section>
    </div>
    <!-- </div> -->


    <!-- Bootstrap JS -->
    <script src="<?php echo e(asset('public/guest-user/js/jquery-3.3.1.slim.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_jquery_3.3.1_jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/guest-user/js/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/guest-user/js/bootstrap.min.js')); ?>"></script>

    <!-- Custom JS -->
    <script src="<?php echo e(asset('public/guest-user/js/custom.js')); ?>"></script>
    
    
    <?php echo $__env->yieldContent('scripts'); ?>

<script src="<?php echo e(asset('public/lib/hullabaloo/js/hullabaloo.js')); ?>"></script>
<script type="text/javascript">
  var hulla = new hullabaloo();
  <?php if(Session::has('success')): ?> 
          hulla.send("<?php echo e(Session::get('success')); ?>", 'success') 
     <?php
       Session::forget('success');
     ?>
  <?php endif; ?>

  <?php if(Session::has('danger')): ?> 
          hulla.send("<?php echo e(Session::get('danger')); ?>", 'danger') 
     <?php
       Session::forget('danger');
     ?>
  <?php endif; ?>

  <?php if(Session::has('warning')): ?> 
          hulla.send("<?php echo e(Session::get('warning')); ?>", 'warning') 
     <?php
       Session::forget('warning');
     ?>
  <?php endif; ?>
  </script>

</body>
</html>
<?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/layouts/guest.blade.php ENDPATH**/ ?>