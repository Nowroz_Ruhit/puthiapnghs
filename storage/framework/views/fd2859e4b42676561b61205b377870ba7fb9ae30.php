<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Admid Card - <?php echo e($data->name); ?></title>
    <style>
    @page  {
        size: A4;
        margin: 25mm 25mm 25mm 25mm;
    }

    .text-center{
        text-align: center;
    }
    table {
      border-collapse: collapse;
    }

    table, th, td {
      border: 1px solid #dee2e6;
      font-size: 16px;
    }
    td, th{
        padding-left: 10px;
    }
    </style>
</head>

<body>

<!-- <span class="bel" id="bel">Powered By : desktopit.com.bd</span> -->
<div id="printBody">
        <div style="width: 100%; height: 80px; margin-right: auto; margin-left: auto;">
            <img src="<?php echo e(asset('public/uplodefile/general/'.$bootGeneral->logo)); ?>"  width="80px" style="position:absolute; left:50%; margin-left:-40px;">
        </div>

        <div style="border-bottom: 1px solid #dee2e6; font-size: 24px; margin-bottom: 15px; text-align: center; padding-bottom: 15px; padding-top: 90px;">
            Shahid Mamun Mahmud Police Lines School and College<br>Rajshahi<br style="margin-bottom: 15px;"><?php echo e($bootAdmissionStart->title); ?>

        </div>
<div style="width: 100%;">
    <strong><div style="text-align: center; font-size: 22px; margin-bottom: 10px;">Admit Card</div></strong>
    <div style="width: 75%; float: left;">
        <table style="width: 100%;">
            <tr>
                <th>Admission Roll</th>
                <th><?php echo e($data->student_id->admission_id); ?></th>
            </tr>
            <tr>
                <th>Class</th>
                <th><?php echo e($data->className->name); ?></th>
            </tr>
            <tr>
                <td>Name</td>
                <td><?php echo e($data->name); ?></td>
            </tr>
            <tr>
                <td>Father's Name</td>
                <td><?php echo e($data->father_name); ?></td>
            </tr>
            <tr>
                <td>Mother's Name</td>
                <td><?php echo e($data->mother_name); ?></td>
            </tr>
            <tr>
                <td>Exam Date & Time</td>
                <td><?php echo e(date('jS M Y \a\t g:i a', strtotime($data->className->admission_date))); ?></td>
            </tr>
        </table>
        <br>
    </div>
    <div style="width: 25%; float: left;">
        <img src="<?php echo e(asset('public/uplodefile/admission/'.$data->image)); ?>" alt=""  width="100"  style="margin-left: 20px; margin-top: 5px;" />
    </div>

</div>
<br>
<div style=" clear: both; width: 100%; font-size: 13px">
    <ol type="1">
        <li>Only viva will be held for Pre-rimary(baby) , class One & class Two</li>
        <li>Marks dristibution of admission written test for  class Three, Four & Five: Bangla-15, English-15, Math-20; Total-50; Duration 1 hour</li>
        <li>Marks dristibution of admission written test for  class Six to Nine: Bangla-30, English-30, Math-40; Total-100; Duration 2 hour</li>
    </ol> 
</div>
<div style="clear: both; border-top: 1px solid #dee2e6; margin-top: 20px; text-align: center; font-size: 16px;"><br>Powered By : Desktop IT <br> <span style="font-size: 13px;">https//:desktopit.com.bd</span> </div>
</div>
</body>
</html>
<?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/print/admit.blade.php ENDPATH**/ ?>