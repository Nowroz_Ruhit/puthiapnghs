

<?php $__env->startSection('content'); ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item" aria-current="page">Admission</li>
    <li class="breadcrumb-item">Result</li>
    <li class="breadcrumb-item active" aria-current="page">Lottery Results</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      Lottery Result List <?php echo e(($lotteryResults->isNotEmpty()) ? 'for '.$lotteryResults->first()->paymentAmount->name : ''); ?>

    
      <form class="form-inline">
            <?php if($lotteryResults->isNotEmpty()): ?>
                <a class="btn btn-outline-dark mr-1" id="download-pdf" href="<?php echo e(route('admin.admission.result.lottery.pdf', $lotteryResults->first()->payment_amount_id)); ?>">
                  <i class="fas fa-file-pdf nav-icon"></i> Download PDF 
                </a>
            <?php endif; ?>
            <a class="btn btn-outline-dark mr-1" id="print-btn">
                <i class="fas fa-print nav-icon"></i> Print 
            </a>
            <a class="btn btn-outline-dark mr-1" href="<?php echo e(route('admin.admission.result.lottery.choose')); ?>">
                <i class="fas fa-edit nav-icon"></i> Create Lottery Result
            </a>
      </form>
      
    </nav>



    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">
                            Serial
                        </th>
                        <th>
                            Student Name
                        </th>
                        <th>
                            Admission Lottery ID
                        </th>
                        <th>
                            Contact
                        </th>
                        <th>
                            Class
                        </th>
                        <th>
                            Status
                        </th>
                        <th>
                           Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $lotteryResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$lotteryResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                          <td><?php echo e(++$key); ?></td>
                          <td><?php echo e($lotteryResult->student_name); ?></td>
                          <td><?php echo e($lotteryResult->admission_lottery_id); ?></td>
                          <td><?php echo e($lotteryResult->contact); ?></td>
                          <td><?php echo e($lotteryResult->paymentAmount->name); ?></td>
                          <td><?php echo e(($lotteryResult->status) ? 'Selected' : 'Not Selected'); ?></td>
                          <td class="text-center">
                            <a href="<?php echo e(route("admin.admission.result.lottery.editlotteryResult", $lotteryResult)); ?>"
                              class="btn btn-primary btn-sm"
                              title="Edit"
                              role="button">
                                <i class="fa fa-edit"></i>
                            </a>
                          </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="printDiv" style="visibility: hidden; display:inline;">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <div class="table-responsive table table-bordered print-container p-3" id="div-id-name"><br>
                <div class="row justify-content-center align-items-center mb-3">
                  <div class="float-left mr-4">
                    <img id='img' src="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->logo)); ?>" class="">
                  </div>
                  <div class="float-right mb-4">
                      <span class="font-weight-bold"
                          style="font-size: 25px; margin-top:150px"><?php echo e($bootGeneral->title); ?></span>
                      <br>
                      <span class="font-weight-bold" style="font-size: 25px"><?php echo e($lotteryResults->first()->paymentAmount->name ?? ''); ?></span>
                  </div>
              </div>
                  <h3  style="text-align:center">Admission Lottery Result</h3>
                  <br>
                  <table id="order-listing" class="table">
                    <thead>
                      <tr>
                        <th style="font-size: 20px;">Serial</th>
                        <th style="font-size: 20px;">Student Name</th>
                        <th style="font-size: 20px;">Admission Lottery ID</th>
                        <th style="font-size: 20px;">Contact</th>
                        <th style="font-size: 20px;">Father Name</th>
                        <th style="font-size: 20px;">Status</th>
                      </tr>
                    </thead>
                    <?php $i=0; ?>
                    <tbody>
                      <?php $__currentLoopData = $lotteryResults; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$lotteryResult): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($lotteryResult->status): ?>
                          <tr>
                            <td style="font-size:18px;"><?php echo e(++$i); ?></td>
                            <td style="font-size:18px;"><?php echo e($lotteryResult->student_name); ?></td>
                            <td style="font-size:18px;"><?php echo e($lotteryResult->admission_lottery_id); ?></td>
                            <td style="font-size:18px;"><?php echo e($lotteryResult->contact); ?></td>
                            <td style="font-size:18px;"><?php echo e($lotteryResult->student->father_name); ?></td>
                            <td style="font-size:18px;"><?php echo e(($lotteryResult->status) ? 'Selected' : 'Not Selected'); ?></td>
                          </tr>
                        <?php endif; ?>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                  </table>
                  <br>
                  <div class="float-left ml-2">
                    <p>Powered By : Desktopit.com.bd</p>
                  </div>
                  <div class="float-right">
                    <p>Date: <?php echo e(date('d-m-Y')); ?></p>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "<?php echo e(route('admin.products.massDestroy')); ?>",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

        return
      }

      if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})
  $('#print-btn').click( function(){
        $('.print-container').printThis();
  })

  

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/lotteryResult/index.blade.php ENDPATH**/ ?>