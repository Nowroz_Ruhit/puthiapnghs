<?php /** @var \Galahad\Aire\Elements\Attributes\Collection $attributes */ ?>

<form <?php echo e($attributes); ?>>

	<?php if(isset($_token) && 'GET' !== $method): ?>
		<input type="hidden" name="_token" value="<?php echo e($_token); ?>" />
	<?php endif; ?>
	
	<?php if(isset($_method)): ?>
		<input type="hidden" name="_method" value="<?php echo e($_method); ?>" />
	<?php endif; ?>
	
	<?php echo e($fields); ?>

	
	<?php echo e($validation); ?>

	
</form>
<?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/vendor/aire/form.blade.php ENDPATH**/ ?>