
<?php $__env->startSection('content'); ?>
    <!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item active" aria-current="page">আমাদের সম্পর্কে</li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

    <section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              আমাদের সম্পর্কে
            </div>
            <div class="card-body text-justify">
              <?php if(empty($bootGeneral->discription)): ?>
              <?php echo e(trans('global.nodata')); ?>

              <?php else: ?>
              <?php echo $bootGeneral->discription; ?>

              <?php endif; ?>
            </div>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Frontend/Providers/../Resources/views/about.blade.php ENDPATH**/ ?>