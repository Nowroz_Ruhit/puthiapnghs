
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('notice.index')); ?>">Notice</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.update')); ?> <?php echo e(trans('global.notice.title_singular')); ?>

    </div>

    <div class="card-body">
         <!-- old('name', isset($product) ? $product->name : '')  -->
        <?php echo e(Aire::open()
            ->route('notice.update', $notice)
            ->bind($notice)
            ->rules([
            'title' => 'required',
            'discription' => 'required',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')); ?>


        <?php echo e(Aire::input()
            ->label('Title*')
            ->id('title')
            ->name('title')
            ->class('form-control')); ?>


        <?php echo e(Aire::textarea()
            ->label('Discription*')
            ->id('discription')
            ->name('discription')
            ->class('form-control')); ?>


        <script>
            CKEDITOR.replace( 'discription' );
        </script>

<div class=" mx-sm-3 mb-2" style="display: block">
    
    <a href="<?php echo e(route('notice.download', $notice)); ?>"><?php echo e($notice->file); ?></a>
</div>
        <?php echo e(Aire::input()
            ->type('file')
            ->label('News Cover Photo*')
            ->id('cover-photo')
            ->name('file')); ?>



<!-- <img src="" id="cover-img-tag" width="200px" class="mb-2" /> -->
 


        <?php echo e(Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>



        <?php echo e(Aire::close()); ?>

    </div>
</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Notice/Providers/../Resources/views/edit.blade.php ENDPATH**/ ?>