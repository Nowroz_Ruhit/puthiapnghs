
<?php $__env->startSection('content'); ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="">Administration</a></li>
    <li class="breadcrumb-item"><a href="">Home Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">General</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      General Information
      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('employee_create')): ?>
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-success" href="<?php echo e(route('general.edit',$general)); ?>">
          <i class="fas fa-edit nav-icon"></i>  <?php echo e(trans('global.edit')); ?>

        </a>
      </form>
      <?php endif; ?>
    </nav>

    <div class="card-body">
    	<table class="table table-borderless table-responsive">
        <tr>
          <th scope="row" >Name</th>
          <td ><?php echo e($general->name); ?></td>
        </tr>
        <tr>
          <th scope="row">Site Title</th>
          <td><?php echo e($general->title); ?></td>
        </tr>
        <tr>
          <th scope="row">Site URL</th>
          <td><?php echo e($general->url); ?></td>
        </tr>
        <tr>
          <th scope="row">Telephone</th>
          <td ><?php echo e($general->tel_phone); ?></td>
        </tr>
        <tr>
          <th scope="row">Phone</th>
          <td ><?php echo e($general->phone); ?></td>
        </tr>
        <tr>
          <th scope="row">Email</th>
          <td ><?php echo e($general->email); ?></td>
        </tr>
        <tr>
          <th scope="row" >Address</th>
          <td ><?php echo e($general->address); ?></td>
        </tr>

        <tr>
          <th scope="row" >Slogan</th>
          <td ><?php echo $general->slogan; ?></td>
        </tr>
        <tr>
          <th scope="row" >Discription </th>
          <td ><div class="text-justify"><?php echo $general->discription; ?></div></td>
        </tr>

        

        <tr>
          <th>Banar</th>
         <td colspan="3">
           <img class="col-12"   src="<?php echo e(asset(trans('global.links.general_show').$general->banar)); ?>">
        </td>
      </tr>

      <tr>
        <th>Logo</th>
         <td colspan="2">
          <img class="col-3"   src="<?php echo e(asset(trans('global.links.general_show').$general->logo)); ?>">
        </td>
      </tr>
      <tr>
        <th>Fab-icon</th>
        <td colspan="2">
          <img class="col-1"   src="<?php echo e(asset(trans('global.links.general_show').$general->fab)); ?>">
        </td>
      </tr>
      <tr>
        <th>Google Map Location</th>
        <td colspan="3">
          <?php echo $general->map; ?>

        </td>
      </tr>
    </table>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/General/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>