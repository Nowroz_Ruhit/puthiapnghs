
<?php $__env->startSection('content'); ?>


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('employee.index')); ?>">Employee</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('employee.designation.index')); ?>">Designation</a></li>
    <li class="breadcrumb-item active" aria-current="page">List</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      <?php echo e(trans('global.employe.fields.designation')); ?> <?php echo e(trans('global.list')); ?>

      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('employee_access')): ?>
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="<?php echo e(route('employee.index')); ?>">
          <i class="fas fa-user nav-icon"></i>  <?php echo e(trans('global.employe.title')); ?> <?php echo e(trans('global.list')); ?>

        </a>
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="<?php echo e(route('employee.designation.create')); ?>">
          <i class="fas fa-edit nav-icon"></i>  <?php echo e(trans('global.add')); ?> <?php echo e(trans('global.employe.fields.designation')); ?>

        </a>
      </form>
      <?php endif; ?>
    </nav>



    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            <?php echo e(trans('global.employe.fields.designation')); ?>

                        </th>
                        <th>Institute</th>
                        <th>Employee Type</th>
                        <th>
                            &nbsp; Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $designation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($designation->id); ?>">
                            <td></td>
                            <td>
                                <?php echo e($designation->designation); ?>

                            </td>
                            <td>
                                <?php echo e($designation->institute->institute); ?>

                            </td>
                            <td>
                                <?php echo e($designation->employeeType->type); ?>

                            </td>
                            <td>
                              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('employee_designation_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('employee.designation.edit',$designation)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('employee_designation_delete')): ?>
                                    <form action="<?php echo e(route('employee.designation.delete', $designation)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="submit" class="btn btn-xs btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                    </form>
                                <?php endif; ?>
                            </td>
                            
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Employee/Providers/../Resources/views/designation/show.blade.php ENDPATH**/ ?>