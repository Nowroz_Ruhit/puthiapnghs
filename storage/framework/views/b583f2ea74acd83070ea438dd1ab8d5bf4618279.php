
<?php $__env->startSection('content'); ?>
    <!-- Start Breadcrumb -->
    <!-- <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item active" aria-current="page">আমাদের সম্পর্কে</li>
      </ol>
    </nav> -->
    <!-- End Breadcrumb -->
<style type="text/css">
  .list-item li {
    display: block;
    padding-bottom: 9px;
    list-style-type: none;
    float: none;
    font-size: 16px;
}
</style>
    <section class="site-content mt-3">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              এক নজরে অনলাইনে ভর্তির নির্দেশাবলীঃ
            </div>
            <div class="card-body text-justify">       
              <!-- <?php echo e(trans('global.nodata')); ?> -->
              <!-- <a href="<?php echo e(route('admission.student.login')); ?>">Login</a> -->
              <!-- Home start -->

<div class="container-fluid content_area">
    <div class="container">
        <div class="page-header text-center">
            <h2 class="school-title">পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয় </h2>
            <h4 class="school-estab">স্থাপিত: ১৮৬৫ ইং</h4>
            
            <img src="<?php echo e(asset('public/LOGO-SCHOOL.png')); ?>" style="height: 120px;width: 120px;margin-bottom: 1rem;"> 
        </div>


        <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info text-center">
                <h3>অনলাইনে ভর্তির নির্দেশাবলী (২০২১ শিক্ষাবর্ষ):</h3>
                <p><a href="http://puthiapnghs.edu.bd/">http://puthiapnghs.edu.bd/</a> ওয়েবসাইটের মাধ্যমে আবেদন প্রক্রিয়া সম্পন্ন করার জন্য নিম্নের তথ্যগুলি অত্যাবশ্যকঃ</p>
            </div>
            <ul class="list-item">
                <li>১. ৬ষ্ট ও নবম শ্রেণির অনলাইনে আবেদনের সময়: ১৭ ডিসেম্বর, ২০২০ থেকে ৭ জানুয়ারী, ২০২১ রাত ১১.৫৯ মিনিট পর্যন্ত ভর্তির আবেদন online এ করা যাবে। ভর্তির সময়সীমা এক সপ্তাহ বর্ধিত করা হয়েছে। আবেদন ফরম পূরণের জন্য online address: <a href="http://puthiapnghs.edu.bd/admission">http://puthiapnghs.edu.bd/admission</a> (আবেদন ফরম পূরণের নির্দেশিকা ও ভর্তি সংক্রান্ত বিজ্ঞপ্তি উক্ত address এ পাওয়া যাবে। )</li>
                <li>২. ভর্তির আবেদন পত্রের মূল্য: অনলাইনে আবেদন ফি ১১৫/-টাকা অনলাইনের মাধ্যমে প্রদান করতে হবে। এক্ষেত্রে ১১০ টাকা ভর্তি ফি ও ৫ টাকা সার্ভিস চার্জ হিসেবে গণ্য হবে।</li>
                <li>৩. সকল প্রার্থীর জন্য জন্ম নিবন্ধনের সনদ অনুযায়ী আবেদন পত্রে জন্ম তারিখ ও জন্ম নিবন্ধনের নম্বর, পিতা-মাতার NID নম্বর, পূর্ববর্তী বিদ্যালয়ের নাম এবং শ্রেণি রোল সঠিকভাবে পূরণ করতে হবে।</li>
                <li>৪. প্রার্থীর সদ্য তোলা Passport size ছবি online এ upload করতে হবে (<span style="color: red;">উচ্চতা ৩৫০ পিক্সেল ও প্রস্থ ৩০০ পিক্সেলের বেশি হওয়া যাবে না</span>) । </li>
                <li>৫. ৬ষ্ট শ্রেণিতে ভর্তি ইচ্ছুক ছাত্র-ছাত্রীদের প্রাথমিক বিদ্যালয়ের নামসহ ২০২০ সালের ৫ম শ্রেণীর রোল নম্বর ও ৯ম শ্রেণীতে ভর্তি ইচ্ছুক ছাত্র-ছাত্রীদের পূর্ববর্তী বিদ্যালয়ের নামসহ ২০২০ সালের ৮ম শ্রেণীর রোল নম্বর উল্লেখ করতে হবে |  </li>
                <li>৬. নির্ভুল এবং সম্পূর্ণ পূরণকৃত ফরম  গ্রহণযোগ্য হবে , অসম্পূর্ণ  ফরম ও ভুল তথ্য প্রদান করলে আবেদন বাতিল বলে গণ্য হবে |    </li>
                <li>৮. আবেদন ফরম পূরণের পর ফরমটি প্রতিষ্ঠানের ওয়েবসাইট <a href="https://puthiapnghs.edu.bd/admission/student/login">http://puthiapnghs.edu.bd</a> হতে ডাউনলোড করে প্রিন্টকপি পরবর্তী প্রয়োজনের জন্য শিক্ষার্থীদের সংরক্ষণ করতে হবে|</li>
                <li>৯. অভিভাবকবৃন্দ নির্ধারিত তারিখ ও সময় এর মধ্যে অনলাইন-এ ফরম পূরণ করবেন, নির্ধারিত তারিখ ও সময়ের পরে কোনক্রমে আবেদন ফরম পূরণ করা যাবে না বা গ্রহণযোগ্য হবে না|</li>
                <li>১০. ফরম পূরনের সময় যে সমস্ত কাগজ সঙ্গে রাখতে হবে
                    <ol style="margin-left: 50px;">
                    
                        <li>১. শিক্ষার্থীর জন্ম নিবন্ধন সনদের ফটোকপি</li>  
                        <li>২. পিতার NID card এর ফটোকপি</li>  
                        <li>৩. মাতার NID card এর ফটোকপি</li>  
                        <li>৪. ষষ্ট শ্রেণীর ক্ষেত্রে প্রাথমিক বিদ্যালয়ের প্রধান শিক্ষকের প্রত্যয়নপত্র </li>  
                        <li>৫.নবম শ্রেণীর ক্ষেত্রে পূর্ববর্তী বিদ্যালয়ের প্রধান শিক্ষকের প্রত্যয়নপত্র </li>  
                        <li>৬.কোটার জন্য আবেদন করলে কোটা সংক্রান্ত প্রয়োজনীয় কাগজপত্র </li>  
                    </ol>
                </li>
                <li>১১. লটারি ৩০/১২/২০২০-এ অনুষ্ঠিত হবে| উক্ত লটারির ফলাফল বিদ্যালয় এর ওয়েবসাইট এর নোটিশবোর্ড পাওয়া যাবে। নোটিশবোর্ড এর লিঙ্ক: <a href="http://puthiapnghs.edu.bd/notice">http://puthiapnghs.edu.bd/notice</a> | এছাড়াও অভিভাবকগণ বিদ্যালয়ের ওয়েবসাইট হতে Birth Certificate Number ও Date of Birth ব্যবহার করে log in করে Student Form ডাউনলোড করতে পারবে|</li>
                <li>১২. ভর্তি ফর্মটি অবশ্যই ইংরেজিতে পূরণ করতে হবে।</li>
                <li>১৩. ভর্তি আবেদনের অনলাইন পেমেন্টর নির্দেশাবলী <a target="_blank" href="https://www.youtube.com/watch?v=5Zx8fzT3sUg&feature=youtu.be">ভিডিও লিঙ্ক</a>।</li>
                <li>১৪. ইতোপূর্বে বয়সের কারণে আবেদন করতে না পারা শিক্ষার্থীরা এখন আবেদন করতে পারবে।  </li>
            </ul>
           <div class="float-right">
                   <h4> প্রশান্ত কুমার চৌধুরী  </h4>
  &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;প্রধান শিক্ষক
            </div>
            <br><br><br><br>
            <div class="text-center">
                
                <h3 class="alert alert-warning">
                    <!-- <input type="checkbox" id="checkbox">  -->
                আমি সম্পূর্ন নির্দেশাবলী পড়েছি এবং অনলাইনে ভর্তি প্রক্রিয়া সম্পন্ন করতে চাই।</h3>

                <a href="<?php echo e(route('admission.student.login')); ?>" class="btn btn-success">Download Submitted Form</a>
                <a href="<?php echo e(route('frontend.admission.apply')); ?>" class="btn btn-primary" id="applyNowBTN">Apply Online</a>
                <br><br>
                
            </div>

            <fieldset>
                <legend>ঠিকানা</legend>
                    পুঠিয়া পি.এন সরকারি উচ্চ বিদ্যালয়, পুঠিয়া, রাজশাহী
                    <br>পোস্ট-জিপিও-৬২৬০, উপজেলা-পুঠিয়া, জেলা-রাজশাহী
                    <br>ইমেইল: 
                    puthiapnmhs15@gmail.com
                    <br>ফোন: ০৭২২৮-৫৬১৩৪
                    <br><br>
                    <h3 class="alert alert-info text-center">অনলাইনে আবেদন সংক্রান্ত তথ্যের জন্য : ০১৮৫৬৪৩২৫০০ সহ প্র.শি., ০১৭৪০৩৮৫১৩২ অফিস সহকারী</h3>
                    <h4 class="alert alert-warning text-center"> টেকনিক্যাল সমস্যার জন্য : ০১৯১৩-৮০০৮৭৭</h4> 
            </fieldset>
        </div>
    </div>

<!--   </div>
</div> -->



              <!-- home end -->
            </div>
          </div>
          <!-- End Welcome or About Text -->
        </div>
      </div>
    </section>
 <?php $__env->startSection('scripts'); ?>   
<script type="text/javascript">
    // applyNow
    // $("#applyNow" ).attr('disabled', true);
    // $("#applyNow" ).disabled = true;
    // document.getElementById("applyNow").disabled = true;
    $('#applyNowBTN').attr('disabled','disabled');


    // $( "#checkbox" ).change(function() {
    //         // console.log('ok');
    //         if(jQuery('#checkbox').is(":checked"))
    //         {
    //             // alert();
                
    //             $("#applyNow").prop('disabled', false);

    //         }else{
    //             $("#applyNow" ).prop('disabled', true);
    //         }
    //     });
</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admission::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admission/index.blade.php ENDPATH**/ ?>