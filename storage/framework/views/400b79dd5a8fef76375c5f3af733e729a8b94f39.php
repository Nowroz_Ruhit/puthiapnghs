

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Print</li>
    </ol>
</nav>
<div class="card">

    <div class="card-header">Print</div>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Class</th>
                        <th>1st Time Total</th>
                        <th>2nd  Time Total</th>
                        <th>1st Time publish Status</th>
                        <th>2nd Time publish Status</th>
                        <th>1st Time</th>
                        <th>2nd Time</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $classlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr data-entry-id="">
                        <td><?php echo e($class->name); ?></td>
                        <td><?php echo e(count($class->studentCount)); ?></td>
                        <td><?php echo e(count($class->studentCount1)); ?></td>
                        <td>
                            <?php echo $class->publish ? '<span class="text-success font-weight-bold">Result Published</span>' : '<span class="text-danger">Pending</span>' ?? ''; ?>

                        </td>
                        <td>
                            <?php echo $class->publish1 ? '<span class="text-success font-weight-bold">Result Published</span>' : '<span class="text-danger">Pending</span>' ?? ''; ?>

                        </td>
                        <td>
                            <?php if($class->publish==1): ?>
                            <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.admission.print.studentDetails', $class)); ?>">
                               <i class="fas fa-print nav-icon"></i> Print Student Info
                            </a>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($class->publish1==1): ?>
                            <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.admission.print.studentDetails1', $class)); ?>">
                               <i class="fas fa-print nav-icon"></i> Print 2nd Time Student Info
                            </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/print/index.blade.php ENDPATH**/ ?>