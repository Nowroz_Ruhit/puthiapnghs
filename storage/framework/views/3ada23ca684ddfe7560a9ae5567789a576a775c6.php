<?php /** @var \Galahad\Aire\Elements\Attributes\Collection $attributes */ ?>

<label <?php echo e($attributes->label); ?>>
	<input <?php echo e($attributes); ?> />
	<span <?php echo e($attributes->wrapper); ?>>
		<?php echo e($label_text); ?>

	</span>
</label>
<?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/vendor/aire/checkbox.blade.php ENDPATH**/ ?>