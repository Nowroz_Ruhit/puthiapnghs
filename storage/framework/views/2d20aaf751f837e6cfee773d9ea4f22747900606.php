

<?php $__env->startSection('content'); ?>
	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="#">হোম</a></li>
        <li class="breadcrumb-item"><a href="#">একাডেমিক</a></li>
        <li class="breadcrumb-item"><a href="#">নিউজ</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo e($news->title); ?></li>
      </ol>
    </nav>
    <!-- End Breadcrumb -->

    <section class="site-content">
      <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
              নিউজ /  <?php echo e($news->title); ?>

            </div>
            <div class="card-body">
              <center class="col-12">
                <img src="<?php echo e(asset('public/uplodefile/news/'.$news->photo)); ?>" class="space-ima1" alt="Teacher Images" style="max-width: 100%;">
              </center>
              
              <div class="card rounded-0 theme-border theme-shadow mt-4">
                <div class="card-header theme-border-color rounded-0 theme-bg">
                  <?php echo e($news->title); ?>

                </div>

                <div class=" card-body text-justify ">
                  <?php echo $news->discription; ?>

                </div>
              </div>
              
            </div>

            <div class="container mt-5">

              <ul class="row first">
               <?php $__currentLoopData = $photos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
               <li class="shadow-lg p-3 mb-5 bg-white rounded">    
                <img alt="Rocking the night away"  src="<?php echo e(asset('public/uplodefile/photos/'.$photo->photo)); ?>">
              </li>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
          </div>
        </div>
  <!-- End Welcome or About Text -->
  </div>
</div>
    </section>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<link href="<?php echo e(asset('Modules/PhotoGallery/Resources/assets/css/jquery.bsPhotoGallery.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script src="<?php echo e(asset('Modules/PhotoGallery/Resources/assets/js/jquery.bsPhotoGallery.js')); ?>"></script> 
<script>
  $(document).ready(function(){
    $('ul.first').bsPhotoGallery({
      "classes" : "col-xl-3 col-lg-2 col-md-4 col-sm-4",
      "hasModal" : true,
      "shortText" : false  
    });
  });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Frontend/Providers/../Resources/views/newsShow.blade.php ENDPATH**/ ?>