
<?php $__env->startSection('content'); ?>
<section class="site-content mt-5 mb-5">
    <div class="row">
        <div class="col-lg-6 col-sm-12 mx-auto">
            <div class="card rounded-0 theme-border theme-shadow">
                <div class="card-header theme-border-color rounded-0 theme-bg">
                    Login
                </div>
                <div class="card-body text-justify"> 
<!--                     <?php if ($errors->has('title')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('title'); ?>
                    <div class="alert alert-danger"><?php echo e($message); ?></div>
                    <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>  -->
<?php if(\Session::has('watning')): ?>
    <div class="alert alert-danger">
        <?php echo \Session::get('watning'); ?>

    </div>
<?php endif; ?>    
                    <div>
                        <?php echo e(Aire::open()
                            ->route('admission.student.loginpage')
                            ->rules([
                            'user' => 'required',
                            'password' => 'required',
                            ])
                            ->messages([
                            'title' => 'You must accept the terms',
                            'photo' => 'Cover Photo is Rrequired',
                            ]) 
                            ->enctype('multipart/form-data')
                            ->method('POST')); ?>

                        <?php echo e(Aire::input()
                            ->label("Student's Birth Certificate No")
                            ->id('birth_certificate')
                            ->name('birth_certificate')
                            ->class('form-control')); ?>

                        <?php echo e(Aire::date()
                            ->label('Date Of Birth')
                            ->id('birth')
                            ->name('birth')
                            ->class('form-control')); ?>


                        <!-- <div class="d-flex justify-content-center" id="apply_button_responsive">
                            <div class="row">
                              <a class="btn btn-info inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Previous</a>
                              <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Cancel</a>
                              <a class="btn btn-warning inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900 mr-2" href="#">Clear</a>
                        </div>
                    </div -->
                    <!-- <?php echo e(Aire::checkbox()
                        ->label('Remamber Me')
                        ->name('remamber')); ?> -->
                    <?php echo e(Aire::button()
                        ->labelHtml('Login')
                        ->class(' btn theme-bg theme-border-color')); ?>


                    <?php echo e(Aire::close()); ?>

                </div>
            </div>
        </div>
    </div>
</div>

</section>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admission::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admission/login.blade.php ENDPATH**/ ?>