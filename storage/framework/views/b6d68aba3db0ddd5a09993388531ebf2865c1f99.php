<?php /** @var \Galahad\Aire\Elements\Attributes\Attributes $attributes */ ?>
<?php /** @var \Galahad\Aire\Support\OptionsCollection $options */ ?>

<select <?php echo e($attributes->except('value')); ?>>
	
	<?php if(isset($prepend_empty_option)): ?>
		<option value="<?php echo e($prepend_empty_option->value); ?>" <?php echo e($attributes->isValue($prepend_empty_option->value) ? 'selected' : ''); ?>>
			<?php echo e($prepend_empty_option->label); ?>

		</option>
	<?php endif; ?>
	
	<?php $__currentLoopData = $options->getOptions(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value => $label): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		
		<option value="<?php echo e($value); ?>" <?php echo e($attributes->isValue($value) ? 'selected' : ''); ?>>
			<?php echo e($label); ?>

		</option>
	
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</select>
<?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/vendor/aire/select.blade.php ENDPATH**/ ?>