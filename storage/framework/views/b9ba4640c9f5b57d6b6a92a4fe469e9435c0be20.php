
<?php $__env->startSection('content'); ?>


<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Page</li>
  </ol>
</nav>

<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>All Pages</b></a>

      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('page_create')): ?>
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-outline-dark" href="<?php echo e(route('page.create')); ?>"> <i class="fas fa-edit nav-icon"></i> Add New Page</a>
          <a class="form-control mr-sm-2 btn btn-outline-dark" href="<?php echo e(route('page.position')); ?>"><i class="fas fa-edit nav-icon"></i> Change Page Oder</a>
        </form>
      <?php endif; ?>

    </nav>

    <div class="card-body">
        <div class="table-responsive">
            <table id="example" class=" table table-bordered table-striped table-hover datatable text-center">
                <thead>
                    <tr>

                        <th>
                            Page tittle
                        </th>
                        <!-- <th>
                            Page Link
                        </th> -->
                        <th>
                            Publish
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $page): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr id="<?php echo e($page->id); ?>">

                            <td>
                                <?php echo e($page->title ?? ''); ?>

                            </td>
                            <!-- <td>
                                 $page->page_link ?? '' 
                            </td> -->
                            <td>

                                <?php echo $page->active ? '<span class="text-success font-weight-bold">Published</span>' : '<span class="text-warning">Unpublish</span>' ?? ''; ?>

                            </td>

                            <td>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('page_access')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('page.show',$page)); ?>">
                                        <?php echo e(trans('global.show')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('page_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('page.edit',$page)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('page_delete')): ?>
                                    <form action="<?php echo e(route('page.destroy', $page)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="submit" class="btn btn-xs btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                    </form>
                                <?php endif; ?>
                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>




<?php $__env->startSection('scripts'); ?>

##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script src="<?php echo e(asset('public/lib/hullabaloo/js/hullabaloo.js')); ?>"></script>

<script>
    $(document).ready(function() {   
      let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
      $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    });

    var hulla = new hullabaloo();
    <?php if(Session::has('success')): ?> 
        hulla.send("<?php echo e(Session::get('success')); ?>", 'success') 
        <?php
           Session::forget('success');
        ?>
    <?php endif; ?>


</script>

<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Page/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>