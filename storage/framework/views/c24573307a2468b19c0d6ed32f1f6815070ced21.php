
<?php $__env->startSection('content'); ?>
<section class="site-content mt-3">
  <div class="row">
    <div class="col-sm-12">
      <div class="card rounded-0 theme-border theme-shadow">
        <div class="card-header theme-border-color rounded-0 theme-bg text-center h3">
          Select Class For <?php echo e($bootAdmissionStart->title); ?>

        </div>
        <div class="card-body text-justify">  
    
          <div class="table-responsive ">
            <table class="table table-bordered ">
              <thead>
                <tr>
                  <th class="text-center">No.</th>
                  <th>Class for <?php echo e($bootAdmissionStart->title); ?></th>
                  <th class="text-center">Taka</th>
                  <th class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php $__currentLoopData = $amounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $amount): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <tr>
                  <td class="text-center"><?php echo e(++$key); ?></td>
                  <td><?php echo e($amount->name); ?></td>
                  <td class="text-center"><?php echo e($amount->amount); ?></td>
                  <td class="text-center">
                    
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter" onclick="data(<?php echo e($amount->id); ?>, <?php echo e($student->id); ?>);">Apply</button>
                  </td>
                </tr> 
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
              </tbody>
            </table>
          </div>


<!-- Button trigger modal -->

<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header theme-bg">
        <h5 class="modal-title" id="exampleModalLongTitle">Payment Confirmation</h5>
        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <h5>
          <p class="text-center text-success" id="pay-text">...
        </p>
        </h5>
      </div>

      <div class="modal-footer">
        <a href="#"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></a>
        <!-- <a href="<?php echo e(route('admission.student.home', $student)); ?>"><button type="button" class="btn btn-primary" data-dismiss="modal">Pay Later</button></a> -->
        <a id="pay-button" href="<?php echo e(route('payment.create',$student)); ?>"><button type="button" class="btn btn-success desable" desabled>Pay Now</button></a>
      </div>

    </div>
  </div>
</div>

        </div>
      </div>

    </div>

  </div>
  <!-- </div> -->
</section>
<?php $__env->startSection('scripts'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">

    function data(data,data2) {
        // var student = <?php echo e($student->id); ?>

        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url:"<?php echo e(route('frontend.admission.modal')); ?>",
            type:'post',
            data:{position:data,position2:data2},
            success:function(result){
              $('#pay-text').html(result);
            }
        });
    }
</script>

<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admission::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admission/classSelection.blade.php ENDPATH**/ ?>