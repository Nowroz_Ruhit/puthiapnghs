<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e($bootGeneral->title); ?></title>

    <?php echo $__env->yieldPushContent('css'); ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _stackpath.bootstrapcdn.com_bootstrap_4.1.3_css_bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdnjs.cloudflare.com_ajax_libs_font-awesome_4.7.0_css_font-awesome.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _use.fontawesome.com_releases_v5.2.0_css_all.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _use.fontawesome.com_releases_v5.2.0_css_all.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdn.datatables.net_1.10.19_css_jquery.dataTables.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdn.datatables.net_1.10.19_css_dataTables.bootstrap4.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdn.datatables.net_buttons_1.2.4_css_buttons.dataTables.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdn.datatables.net_select_1.3.0_css_select.dataTables.min.css')); ?>">
    <!-- this -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _unpkg.com_@coreui_coreui@2.1.12_dist_css_coreui.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdnjs.cloudflare.com_ajax_libs_select2_4.0.5_css_select2.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdnjs.cloudflare.com_ajax_libs_bootstrap-datetimepicker_4.17.47_css_bootstrap-datetimepicker.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/css/https _cdnjs.cloudflare.com_ajax_libs_dropzone_5.5.1_min_dropzone.min.css')); ?>">
    <!-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" /> -->
    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
    <?php echo $__env->yieldPushContent('aire-css'); ?>
    <!-- <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdn.datatables.net/select/1.3.0/css/select.dataTables.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://unpkg.com/@coreui/coreui/dist/css/coreui.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" /> -->
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" /> -->
    <link href="<?php echo e(asset('css/custom.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('css/toastr.min.css')); ?>" rel="stylesheet" />


    <?php echo $__env->yieldContent('styles'); ?>


<!-- <link rel="apple-touch-icon" sizes="57x57" href="<?php echo e(asset('public/img/favicon/apple-icon-57x57.png')); ?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo e(asset('public/img/favicon/apple-icon-60x60.png')); ?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo e(asset('public/img/favicon/apple-icon-72x72.png')); ?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('public/img/favicon/apple-icon-76x76.png')); ?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo e(asset('public/img/favicon/apple-icon-114x114.png')); ?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('public/img/favicon/apple-icon-120x120.png')); ?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo e(asset('public/img/favicon/apple-icon-144x144.png')); ?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('public/img/favicon/apple-icon-152x152.png')); ?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('public/img/favicon/apple-icon-180x180.png')); ?>">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo e(asset('public/img/favicon/android-icon-192x192.png')); ?>">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('public/img/favicon/favicon-32x32.png')); ?>">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('public/img/favicon/favicon-96x96.png')); ?>">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('public/img/favicon/favicon-16x16.png')); ?>"> -->
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->fab )); ?>">
<!-- <link rel="manifest" href="<?php echo e(asset('public/img/favicon/manifest.json')); ?>"> -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo e(asset('public/img/favicon//ms-icon-144x144.png')); ?>">
<meta name="theme-color" content="#ffffff">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed pace-done sidebar-lg-show">
    <header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <span class="navbar-brand-full">Admin Panel</span>
            <span class="navbar-brand-minimized">A</span>
        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>

        <ul class="nav navbar-nav ml-auto">
            <?php if(count(config('panel.available_languages', [])) > 1): ?>
                <li class="nav-item dropdown d-md-down-none">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <?php echo e(strtoupper(app()->getLocale())); ?>

                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <?php $__currentLoopData = config('panel.available_languages'); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $langLocale => $langName): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a class="dropdown-item" href="<?php echo e(url()->current()); ?>?change_language=<?php echo e($langLocale); ?>"><?php echo e(strtoupper($langLocale)); ?> (<?php echo e($langName); ?>)</a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </li>
            <?php endif; ?>
        </ul>



        <ul class="nav navbar-nav mr-3">
                <li class="nav-item">
                    <img src="https://img.icons8.com/wired/64/000000/circled-user.png" alt="..." style="
    width: 32px;
    border-radius: 65%;
    height: 32px;
">
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                        
                        <?php echo e(Auth::user()->name); ?>

                    </a>
                    <div class="dropdown-menu dropdown-menu-right">

                        <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">Logout</a>
                    </div>
                </li>
         </ul>
    </header>

    <div class="app-body">
        <?php echo $__env->make('partials.menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <main class="main">


            <div style="padding-top: 20px" class="container-fluid">

                <?php echo $__env->yieldContent('content'); ?>

            </div>


        </main>
        <form id="logoutform" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
            <?php echo e(csrf_field()); ?>

        </form>
    </div>
     
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_jquery_3.3.1_jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _stackpath.bootstrapcdn.com_bootstrap_4.1.1_js_bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_popper.js_1.14.3_umd_popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/http _cdn.datatables.net_1.10.19_js_jquery.dataTables.min.js')); ?>"></script>
    
    <script src="<?php echo e(asset('js/js/https _unpkg.com_@coreui_coreui@2.1.12_dist_js_coreui.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.datatables.net_1.10.19_js_dataTables.bootstrap4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/http _cdn.datatables.net_buttons_1.2.4_js_dataTables.buttons.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/http _cdn.datatables.net_buttons_1.2.4_js_buttons.flash.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.datatables.net_buttons_1.2.4_js_buttons.html5.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.datatables.net_buttons_1.2.4_js_buttons.print.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.datatables.net_buttons_1.2.4_js_buttons.colVis.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.rawgit.com_bpampuch_pdfmake_0.1.18_build_pdfmake.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.rawgit.com_bpampuch_pdfmake_0.1.18_build_vfs_fonts.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_jszip_2.5.0_jszip.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.datatables.net_select_1.3.0_js_dataTables.select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdn.ckeditor.com_ckeditor5_11.0.1_classic_ckeditor.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_moment.js_2.22.2_moment.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_bootstrap-datetimepicker_4.17.47_js_bootstrap-datetimepicker.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_select2_4.0.5_js_select2.full.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/js/https _cdnjs.cloudflare.com_ajax_libs_dropzone_5.5.1_min_dropzone.min.js')); ?>"></script>

    <script src="<?php echo e(asset('js/main.js')); ?>"></script>
    <script src="<?php echo e(asset('js/toastr.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/printThis.js')); ?>"></script>
    <script>
        $(function() {
  let copyButtonTrans = '<?php echo e(trans('global.datatables.copy')); ?>'
  let csvButtonTrans = '<?php echo e(trans('global.datatables.csv')); ?>'
  let excelButtonTrans = '<?php echo e(trans('global.datatables.excel')); ?>'
  let pdfButtonTrans = '<?php echo e(trans('global.datatables.pdf')); ?>'
  let printButtonTrans = '<?php echo e(trans('global.datatables.print')); ?>'
  let colvisButtonTrans = '<?php echo e(trans('global.datatables.colvis')); ?>'

  let languages = {
    'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'
  };

  $.extend(true, $.fn.dataTable.Buttons.defaults.dom.button, { className: 'btn' })
  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      url: languages.<?php echo e(app()->getLocale()); ?>

    },
    
    order: [],
    scrollX: true,
    pageLength: 20,
    dom: 'lBfrtip<"actions">',
    buttons: [
      {
        extend: 'copy',
        className: 'btn-default',
        text: copyButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'csv',
        className: 'btn-default',
        text: csvButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        className: 'btn-default',
        text: excelButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        className: 'btn-default',
        text: pdfButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        className: 'btn-default',
        text: printButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'colvis',
        className: 'btn-default',
        text: colvisButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      }
    ]
  });

  $.fn.dataTable.ext.classes.sPageButton = '';
});

<?php if(session()->has('success')): ?>
    toastr.success('<?php echo e(session()->get("success")); ?>')
    <?php echo e(Session::forget('success')); ?>

<?php elseif(session()->has('warning')): ?>
    toastr.warning('<?php echo e(session()->get("warning")); ?>')
    <?php echo e(Session::forget('warning')); ?>

<?php elseif(session()->has('error')): ?>
    toastr.error('<?php echo e(session()->get("error")); ?>')
    <?php echo e(Session::forget('error')); ?>

<?php elseif(session()->has('delete')): ?>
    toastr.error('<?php echo e(session()->get("delete")); ?>')
    <?php echo e(Session::forget('delete')); ?>

<?php endif; ?>
    </script>
   <?php echo $__env->yieldContent('scripts'); ?>
</body>

</html><?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/layouts/admin.blade.php ENDPATH**/ ?>