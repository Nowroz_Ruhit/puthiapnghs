
<?php $__env->startSection('content'); ?>
<div class="row justify-content-center">
    <div class="col-md-5">
        <div class="card-group">
            <div class="card p-4">
                <div class="card-body">
                    <?php if(\Session::has('message')): ?>
                        <p class="alert alert-info">
                            <?php echo e(\Session::get('message')); ?>

                        </p>
                    <?php endif; ?>
                    <form method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo e(csrf_field()); ?>

                        <h1>
                            <div class="login-logo">
                                <div class="d-flex justify-content-center">
                                    <img width="100" src="<?php echo e(asset(trans('global.links.general_show').$bootGeneral->logo )); ?>">
                                </div>
                                <div class="d-flex justify-content-center text-justify h6 mt-4 mb-2" >
                                    <a href="<?php echo e($bootGeneral->url); ?>" target="_blank">
                                    <?php echo e($bootGeneral->title); ?>

                                </a>
                                </div>
                            </div>
                        </h1>
                        <p class="text-muted"><?php echo e(trans('global.login')); ?> informaion</p>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user"></i></span>
                            </div>
                            <input name="email" type="text" class="form-control" placeholder="<?php echo e(trans('global.login_email')); ?>">
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-lock"></i></span>
                            </div>
                            <input name="password" type="password" class="form-control" placeholder="<?php echo e(trans('global.login_password')); ?>">
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <input type="submit" class="btn btn-primary px-4" value='<?php echo e(trans('global.login')); ?>'>
                                <label class="ml-2">
                                    <input name="remember" type="checkbox" /> <?php echo e(trans('global.remember_me')); ?>

                                </label>
                            </div>
                            <div class="col-6 text-right">
                                <a class="btn btn-link px-0" href="<?php echo e(route('password.request')); ?>">
                                    <?php echo e(trans('global.forgot_password')); ?>

                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/auth/login.blade.php ENDPATH**/ ?>