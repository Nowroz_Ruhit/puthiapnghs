

<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('academic.index')); ?>">Academic</a></li>
    <li class="breadcrumb-item active" aria-current="page">Category</li>
  </ol>
</nav>

<div class="card">


  <nav class="navbar navbar-light bg-light justify-content-between">
      <?php echo e(trans('global.academic.title_singular')); ?> <?php echo e(trans('global.academic.fields.catagory')); ?> <?php echo e(trans('global.list')); ?>

      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('academic_category_create')): ?>
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="<?php echo e(route('academic.catagory.create')); ?>">
            <i class="fas fa-edit nav-icon"></i>   <?php echo e(trans('global.add')); ?> <?php echo e(trans('global.academic.fields.catagory')); ?>

        </a>
      </form>
      <?php endif; ?>
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>

                        <th>
                            <?php echo e(trans('global.academic.fields.catagory')); ?> <?php echo e(trans('global.academic.fields.name')); ?>

                        </th>
<!--                         <th>
                            <?php echo e(trans('global.academic.fields.description')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.academic.fields.price')); ?>

                        </th> -->
                        <th>
                            &nbsp;Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($category->id); ?>">

                            <td>
                                <?php echo e($category->title ?? ''); ?>

                            </td>
                            <td>
                              <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('academic_category_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('academic.category.edit', $category)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('academic_category_delete')): ?>
                                    <form action="<?php echo e(route('academic.category.delete', $category->id)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="submit" class="btn btn-xs btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                    </form>
                                <?php endif; ?>
                            </td>
                            
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Academic/Providers/../Resources/views/category/index.blade.php ENDPATH**/ ?>