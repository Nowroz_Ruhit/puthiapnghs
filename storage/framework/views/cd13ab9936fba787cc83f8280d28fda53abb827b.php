

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Manasge</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
      Manage
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('admission_reset')): ?>
      <form class="form-inline">
        <a class="btn btn-info mr-3" href="<?php echo e(route('admin.admission.manage.resetbackup')); ?>">
          <i class="fas fa-file-upload"></i> Backup & Reset
        </a>
        <a class="btn btn-outline-dark" href="<?php echo e(route('admin.admission.manage.reset')); ?>">
          <i class="fas fa-undo nav-icon"></i> Reset
        </a>
      </form>
    <?php endif; ?>
    </nav>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Admission</th>
                        <th>Start</th>
                        <th>End</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $admission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr data-entry-id="">
                        <td><?php echo e($class->title); ?></td>
                        <td><?php echo e($class->start); ?></td>
                        <td><?php echo e($class->end); ?></td>
                        <td>
                            <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.admission.manage.edit', $class)); ?>">
                               <i class="fas fa-edit nav-icon"></i> Edit
                            </a>

                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/manage/index.blade.php ENDPATH**/ ?>