
<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<!-- <link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" /> -->
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.subjectSettings.index')); ?>">Subject Settings</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create</li>
    </ol>
</nav>
<div class="card">
    <div class="card-header">
        Create
    </div>
    
    <div class="card-body">
        <div class="row border-bottom">
            <div class="col-lg-5 ">
                <div class="text-right pt-2">Select Class </div> 
            </div>
            <div class="col-lg-3">
                <?php echo e(Aire::open() 
                    ->route('admin.admission.subjectSettings.store')
                    ->rules([
                    'subject_pass_marks' => 'same:subject_marks',
                    ])
                    ->messages([
                    'accepted' => 'You must accept the terms',
                    ])
                    ->enctype('multipart/form-data')); ?>

                <?php echo e(Aire::select($classList)
                    ->name('class_id')
                    ->id('class_id')
                    ->class('form-control')); ?>

                
            </div>
        </div>
        <div class="container" id="sub-body">
            <br>
            <table class="table table-borderless" id="subject-table">
                <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Subject</th>
                        <th class="text-center">Marks</th>
                        <th class="text-center">Pass Marks</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $subjectList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr >
                        <td>
                            <input type="checkbox" name="checkbox[]" id="checkbox<?php echo e($i+1); ?>" onclick="myfunction(<?php echo e($i); ?>);" class="mt-2">
                        </td>
                        <td>
                            <div class="mt-2"><?php echo e($subject->subject_name ?? ''); ?></div>
                            <input type="hidden" name="subject_list_id[]" id="subject_id<?php echo e($i+1); ?>" value="<?php echo e($subject->id); ?>">
                            <input type="hidden" name="subject_list_d[]" id="subject_d<?php echo e($i+1); ?>" >
                        </td>
                        <td>
                            <?php echo e(Aire::number()
                                ->class('form-control')
                                ->id('total'.($i+1))
                                ->name('subject_marks[]')); ?>

                        </td>
                        <td>
                            <?php echo e(Aire::number()
                                ->class('form-control')
                                ->id('pass'.($i+1))
                                ->name('subject_pass_marks[]')); ?>

                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
            <div class="d-flex justify-content-center" >
                <div class="row" id="apply_button_responsive">
                  
                  <?php echo e(Aire::button()
                    ->labelHtml('Save')
                    ->id('subBtn')
                    ->class(' btn btn-primary')); ?>


            </div>
        </div>
        </div>
        <?php echo e(Aire::close()); ?>

    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

<script type="text/javascript">
   /* var selector = document.getElementById('class_id');
    console.log(selector[selector.selectedIndex].value);
*/
document.getElementById("subBtn").disabled  = true;
$("#class_id").change(function() {
    // console.log($( "#class_id option:selected" ).val());
    // var selector = document.getElementById('class_id');
    if($( "#class_id option:selected" ).val() != 0)
    {
        document.getElementById("subBtn").disabled  = false;
    }else {
        document.getElementById("subBtn").disabled  = true;
    }
});



    let table = document.getElementById("subject-table");

    for(var i = 1; i < table.rows.length; i++)
    {
        document.getElementById("total"+i).disabled  = true;
        document.getElementById("pass"+i).disabled  = true;
        // $("#total"+i).prop('disabled', true);
        // $("#pass"+i).prop('disabled', true);
        // document.getElementById("checkbox"+i).onclick = function() { myfunction("checkbox"+i); };
    }
    function myfunction(id) {
        id=parseInt(id+1);
        if(jQuery("#checkbox"+id).is(":checked"))
        {
            document.getElementById("total"+id).disabled  = false;
            document.getElementById("pass"+id).disabled  = false;
            document.getElementById("subject_id"+id).disabled  = false;
            document.getElementById("subject_d"+id).value  = parseInt(id);
        }else {
            document.getElementById("total"+id).disabled  = true;
            document.getElementById("pass"+id).disabled  = true;
            document.getElementById("total"+id).value  = "";
            document.getElementById("pass"+id).value  = "";
            document.getElementById("subject_d"+id).value  = "";
            document.getElementById("subject_id"+id).disabled  = true;
        }

    }
</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<!-- $("#permananent_address " ).val("").prop('disabled', false); -->

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/subject/create.blade.php ENDPATH**/ ?>