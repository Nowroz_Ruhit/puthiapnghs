
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>


<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('notice.index')); ?>">Notice</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
  </ol>
</nav>

<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.notice.title_singular')); ?>

    </div>

    <div class="card-body">
        <?php echo e(Aire::open()
            ->route('notice.store')
            ->rules([
            'title' => 'required',
            'discrioption' => 'required',
            'files'=> 'required|file',
            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')); ?>


        <?php echo e(Aire::input()
            ->label('Title*')
            ->id('title')
            ->name('title')
            ->class('form-control')); ?>


        <?php echo e(Aire::textarea()
            ->label('Discription*')
            ->id('discrioption')
            ->name('discrioption')
            ->class('form-control')); ?>


        <script>
            CKEDITOR.replace( 'discrioption' );
        </script>

        <?php echo e(Aire::select($catagory)
            ->class('form-control')
            ->label('Notice Catagory *')
            ->name('catagory')
            ->id('catagory')); ?>


        <?php echo e(Aire::input()
            ->type('file')
            ->label('Notice File*')
            ->id('files')
            ->name('files')); ?>



        <?php echo e(Aire::button()
            ->labelHtml('<strong>Next</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>



        <?php echo e(Aire::close()); ?>

    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Notice/Providers/../Resources/views/create.blade.php ENDPATH**/ ?>