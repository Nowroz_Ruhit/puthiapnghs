

<?php $__env->startSection('content'); ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item" aria-current="page">Admission</li>
    <li class="breadcrumb-item" aria-current="page">Result</li>
    <li class="breadcrumb-item active" aria-current="page">Create Lottery Result</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      Create Lottery Result <?php echo e(($students->isNotEmpty()) ? 'for '.$students->first()->payFor->name : ''); ?>

    </nav>

    <div class="card-body">
        <form action="<?php echo e(route('admin.admission.result.lottery.store')); ?>" method="post">
        <?php echo csrf_field(); ?>
        <div class="row">
            <div class="table-responsive col-9">
                <table class=" table table-bordered table-striped table-hover table-responsive">
                    <thead>
                        <tr>
                            <tr>
                                <th>SL No.</th>
                                <th>Admission Lottery ID</th>
                                <th>Name</th>
                                <th>Father Name</th>
                                
                                <th>Action</th>
                                
                            </tr>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $__currentLoopData = $students; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$student): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e(++$key); ?></td>
                                <td><?php echo e($student->admission_id); ?></td>
                                <td class="d-none"><input type="hidden" name="student_id[]" value="<?php echo e($student->student->id); ?>"></td>
                                <td><?php echo e($student->student->name); ?></td>
                                <td><?php echo e($student->student->father_name); ?></td>
                                <td>
                                  
                                  <input type="hidden" name="status[]" value="0"><input type="checkbox" class="status-checkbox" onclick="this.previousSibling.value=1-this.previousSibling.value">
                                  
                                </td>
                                
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                
            </div>
            <div class="col-3">
              <div class="card" style="position:fixed;">
                <div class="card-body">
                  <span id="checkbox-count">0</span>
                  <span> Students Selected</span>
                  <br>
                  <button class="btn btn-primary mt-2" type="submit">Create</button>
                </div>
              </div>
            </div>
          </div>  
        </form>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "<?php echo e(route('admin.products.massDestroy')); ?>",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

        return
      }

      if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

$('input[type="checkbox"]').click(function(){
    $('#checkbox-count').text($('.status-checkbox:checked').length);
});

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/lotteryResult/create.blade.php ENDPATH**/ ?>