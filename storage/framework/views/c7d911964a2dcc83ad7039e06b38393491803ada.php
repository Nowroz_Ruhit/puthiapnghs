<?php /** @var \Galahad\Aire\Elements\Attributes\Collection $attributes */ ?>

<label <?php echo e($attributes); ?>>
	<?php echo e($text ?? ''); ?>

</label>
<?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/vendor/aire/label.blade.php ENDPATH**/ ?>