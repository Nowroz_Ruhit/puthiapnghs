
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>


<!-- <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script> -->
<!-- <script src="https://cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
<!-- <script src="<?php echo e(asset('public/ck-lib/js/ckeditor.js')); ?>"></script>
<script src="<?php echo e(asset('public/ck-lib/js/sample.js')); ?>"></script> -->
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('employee.index')); ?>">Employee</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.notice.title_singular')); ?>

    </div>

    <div class="card-body">
        <?php echo e(Aire::open()
            ->route('employee.update',$employee)
            ->bind($employee)
            ->rules([
            'name' => 'required',
            'type_id' => 'required',
            'discrioption' => 'required',
            'files'=> 'required|file',
            'email' =>'email',
            'phone' => 'numeric',
            'designation' => 'required',
            ])
            ->messages([
            'name' => 'You must accept the terms',
            ]) 
            ->enctype('multipart/form-data')); ?>


        <?php echo e(Aire::input()
            ->label('Name*')
            ->id('name')
            ->name('name')
            ->class('form-control')); ?>


        <?php echo e(Aire::input()
            ->label('Email')
            ->id('email')
            ->name('email')
            ->class('form-control')); ?>


        <?php echo e(Aire::input()
            ->label('Phone No.')
            ->id('phone')
            ->name('phone')
            ->class('form-control')); ?>


        <?php echo e(Aire::select($religion)
            ->label('Religion')
            ->id('religion_id')
            ->name('religion_id')
            ->class('form-control')); ?>


        <?php echo e(Aire::select($institute)
            ->class('form-control')
            ->label('Institute *')
            ->name('institute_id')
            ->id('institute_id')); ?>


        <?php echo e(Aire::select($type)
            ->class('form-control')
            ->label('Employee Type *')
            ->name('employee_type_id')
            ->id('employee_type_id')); ?>



        <?php echo e(Aire::select($designation)
            ->class('form-control')
            ->label(trans('global.employe.fields.designation').' *')
            ->name('designation_id')
            ->id('designation_id')); ?>


        <?php echo e(Aire::input()
            ->label('Address')
            ->id('address')
            ->name('address')
            ->class('form-control')); ?>


        <?php echo e(Aire::input()
            ->label('Join Date')
            ->id('joindate')
            ->name('joindate')
            ->class('form-control')); ?>


<!-- <input id="datepicker" /> -->
    <script>
        $('#joindate').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>

        <?php echo e(Aire::input()
            ->label('Join Date')
            ->id('mpo')
            ->name('mpo')
            ->class('form-control')); ?>


<!-- <input id="datepicker" /> -->
    <script>
        $('#mpo').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>




        <!-- <img src="" id="cover-img-tag" width="200px" class="mb-2" /> -->
        <div class=" mx-sm-3 mb-2">
            <img id="cover-img-tag" width="200px" alt="<?php echo e($employee->name); ?>" src="<?php echo e($employee->files ? asset(trans('global.links.employee_show').$employee->files) : asset('public/img/logo/006-jpg.png') ?? ''); ?>"  />
        </div>

        <?php echo e(Aire::input()
            ->type('file')
            ->label('Employe Picture (image size must be 450 × 450) *')
            ->id('files')
            ->value('')
            ->name('files')); ?>


        <?php echo e(Aire::textarea()
            ->label("Others")
            ->id('editor')
            ->name('others')
            ->class('form-control')); ?>


<!--         <script>
            initSample();
        </script> -->

        <?php echo e(Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>



        <?php echo e(Aire::close()); ?>

    </div>
</div>
<script src="<?php echo e(asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js')); ?>"></script>

<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#files").change(function(){
        readURL(this);
    });

</script>

<script type="text/javascript">

    CKEDITOR.replace('editor', {

        filebrowserUploadUrl: "<?php echo e(route('ckeditor.upload', ['_token' => csrf_token() ])); ?>",

        filebrowserUploadMethod: 'form'

    });


</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Employee/Providers/../Resources/views/edit.blade.php ENDPATH**/ ?>