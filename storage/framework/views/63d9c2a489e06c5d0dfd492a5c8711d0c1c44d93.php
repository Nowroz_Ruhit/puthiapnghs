
<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Subject Settings</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
        Subject Settings
        
        <form class="form-inline">
            <a class="btn btn-outline-dark mr-3" href="<?php echo e(route('admin.admission.subjectSettings.subject')); ?>">
                <i class="fas fa-list nav-icon"></i> Subject List
            </a>
            <a class="btn btn-outline-dark" href="<?php echo e(route('admin.admission.subjectSettings.create')); ?>">
                <i class="fas fa-edit nav-icon"></i> Set Subjects Marks
            </a>
            
        </form>
        
    </nav>
    
    <div class="card-body">
        <div class="row">
            <?php $__currentLoopData = $classList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

            <div class="col-lg-4">
                <div class="card">
                    <nav class="navbar navbar-light bg-light justify-content-between border border-bottom-0">
                        <?php echo e($class->name); ?>

                        <form class="form-inline">
                            <a class="" href="<?php echo e(route('admin.admission.subjectSettings.edit', $class)); ?>">
                                <i class="fas fa-edit nav-icon"></i>
                            </a>
                        </form>
                    </nav>
                    <table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th>বিষয়</th>
                                <th>নম্বর</th>
                                <th>পাশ নাম্বার</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $__currentLoopData = $class->subjectsList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($val->subjecName->subject_name); ?></td>
                                <td><?php echo e($val->subject_marks); ?></td>
                                <td><?php echo e($val->subject_pass_marks); ?></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <!-- End -->
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/subject/index.blade.php ENDPATH**/ ?>