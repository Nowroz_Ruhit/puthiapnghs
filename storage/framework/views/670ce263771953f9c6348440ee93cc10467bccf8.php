

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Result</li>
    </ol>
</nav>
<div class="card">
    
    <div class="card-header">Result Publish</div>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Class</th>
                        <th>Total Students</th>
                        <th>Mark Status</th>
                        <th>Result Status</th>
                        <th>1st Time</th>
                        <th>2nd Time</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $classlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr data-entry-id="">
                        <td><?php echo e($class->name); ?></td>
                        <td><?php echo e(count($class->studentCount)); ?></td>
                        <td>
                            <?php echo $class->input_mark ? '<span class="text-success font-weight-bold">Mark Input Complete</span>' : '<span class="text-danger">Pending</span>' ?? ''; ?>

                        </td>
                        <td>
                            <?php echo $class->publish ? '<span class="text-success font-weight-bold">Result Published</span>' : '<span class="text-danger">Unpublished</span>' ?? ''; ?>

                        </td>
                        <td>
                            <?php if($class->subject_set==1 && $class->input_mark==1 && $class->make_result==1): ?>
                            <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.admission.result.show', $class)); ?>">
                               <i class="fas fa-eye nav-icon"></i> View
                            </a>
                            <?php if($class->publish==0 ): ?>
                            <a class="btn btn-xs btn-success" href="<?php echo e(route('admin.admission.result.publish.class.result', $class)); ?>">
                               <i class="fas fa-eye nav-icon"></i> Publish Now
                            </a>
                            <?php endif; ?>
                            <?php endif; ?>
                            </td>
                            <td>
                            <?php if($class->subject_set==1 && $class->input_mark1==1 && $class->make_result1==1): ?>
                            <a class="btn btn-xs btn-info" href="<?php echo e(route('admin.admission.result.show1', $class)); ?>">
                               <i class="fas fa-eye nav-icon"></i> View
                            </a>
                            <?php if($class->publish1==0 ): ?>
                            <a class="btn btn-xs btn-warning" href="<?php echo e(route('admin.admission.result.publish.class.result1', $class)); ?>">
                               <i class="fas fa-eye nav-icon"></i> Publish Now
                            </a>
                            <?php endif; ?>
                            <?php endif; ?>

                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/result/publish.blade.php ENDPATH**/ ?>