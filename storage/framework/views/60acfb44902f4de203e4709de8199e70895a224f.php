
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

<?php if(session()->get('msg')): ?>
 <a href="<?php echo e(url('notice')); ?>" class="btn btn-success">Back to Notice List</a>

<div class="alert-success col-12 text-center h1 p-6  mt-5 mb-5">
    "<i>Notice</i>"  Create Successfully
     
</div>

<?php else: ?>
<script src="https://cdn.ckeditor.com/4.12.1/basic/ckeditor.js"></script>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(url('employee')); ?>">Employee</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.notice.title_singular')); ?>

    </div>

    <div class="card-body">
        <?php echo e(Aire::open()
            ->route('profile.update',$user)
            ->bind($user)
            ->rules([
            'name' => 'required',
            'discrioption' => 'required',
            'files'=> 'required|file',
            'email' =>'email',
            'phone' => 'numeric',
            'designation' => 'required',
            ])
            ->messages([
            'name' => 'You must accept the terms',
            ]) 
            ->enctype('multipart/form-data')); ?>


        <?php echo e(Aire::input()
            ->label('Name*')
            ->id('name')
            ->name('name')
            ->class('form-control')); ?>


        <?php echo e(Aire::input()
            ->label('Email')
            ->id('email')
            ->name('email')
            ->class('form-control')); ?>


        
        <!-- <img src="" id="cover-img-tag" width="200px" class="mb-2" /> -->
<!--         <div class=" mx-sm-3 mb-2">
            <img id="cover-img-tag" width="200px" alt="Uploaded Image Preview Holder" src=" $employee->files ? asset('public/img/employee/'.$employee->files) : asset('public/img/logo/006-jpg.png') ?? '' "  />
        </div> -->
        
<!--             Aire::input()
            ->type('file')
            ->label('Employe Picture *')
            ->id('files')
            ->name('files') -->
            
        

        <?php echo e(Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>



        <?php echo e(Aire::close()); ?>

    </div>
</div>
<script src="<?php echo e(asset('js/js/http _ajax.googleapis.com_ajax_libs_jquery_1.9.1_jquery.js')); ?>"></script>

<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#cover-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#files").change(function(){
        readURL(this);
    });

</script>
<?php endif; ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Profile/Providers/../Resources/views/edit.blade.php ENDPATH**/ ?>