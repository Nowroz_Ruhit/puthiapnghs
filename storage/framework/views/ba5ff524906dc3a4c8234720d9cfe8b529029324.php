
<?php $__env->startPush('css'); ?>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('modules/admission/Resources/assets/css/aire.css')); ?>">
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>



<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.classAmount')); ?>">Class</a></li>
    <li class="breadcrumb-item active" aria-current="page">Edit Class</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        Edit Class
    </div>

    <div class="card-body">
        <?php echo e(Aire::open()
            ->route('admin.admission.manage.update', $admission)
            ->bind($admission)
            ->rules([
            'title' => 'required'


            ])
            ->messages([
            'title' => 'You must accept the terms',
            'photo' => 'Cover Photo is Rrequired',
            ]) 
            ->enctype('multipart/form-data')); ?>


        <?php echo e(Aire::input()
            ->label('Title *')
            ->id('title')
            ->name('title')
            ->class('form-control') 
            ->placeholder('Admission - 2019')); ?>


        <?php echo e(Aire::input()
            ->label('Admission Open *')
            ->id('start')
            ->name('start')
            ->class('form-control')
            ->placeholder('YYYY-MM-DD HH:MM')); ?>


        <?php echo e(Aire::input()
            ->label('Admission Close *')
            ->id('end')
            ->name('end')
            ->class('form-control') 
            ->placeholder('YYYY-MM-DD HH:MM')); ?>

        

        <?php echo e(Aire::button()
            ->labelHtml('<strong>Save</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>



        <?php echo e(Aire::close()); ?>

    </div>
</div>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/manage/edit.blade.php ENDPATH**/ ?>