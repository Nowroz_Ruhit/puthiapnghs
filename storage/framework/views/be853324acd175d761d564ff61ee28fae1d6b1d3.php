

<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<link href="<?php echo e(asset('Modules/PhotoGallery/Resources/assets/css/jquery.bsPhotoGallery.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script src="<?php echo e(asset('Modules/PhotoGallery/Resources/assets/js/jquery.bsPhotoGallery.js')); ?>"></script>	
<script>
	$(document).ready(function(){
		$('ul.first').bsPhotoGallery({
			"classes" : "col-xl-3 col-lg-2 col-md-4 col-sm-4",
			"hasModal" : false,
			"shortText" : false  
		});
	});
</script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Photo Gallery</li>
  </ol>
</nav>


    <nav class="navbar navbar-light bg-light justify-content-between">
      Photo Gallery
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('photo_uplode')): ?>
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="<?php echo e(route('photogallery.create')); ?>">
          <i class="fas fa-edit nav-icon"></i>   Create Album
        </a>
      </form>
      <?php endif; ?>
    </nav>

    <div class="card-body">
  	<div class="container">
  		<div>
  			<!-- Hedder -->
  		</div>
  		<ul class="row first">
  			<?php $__currentLoopData = $album; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
  			<li>
  				<a href="<?php echo e(route('photogallery.show', $gallery->id)); ?>">
  					<?php if( ($gallery->title == NULL) && ($gallery->photo == NULL)): ?>
  					<img alt="Rocking the night away"  src="<?php echo e(asset(trans('global.links.news_show').$gallery->news->photo)); ?>">
  					<p><?php echo e($gallery->news->title); ?></p>
  					<?php else: ?>
  					<img alt="Rocking the night away"  src="<?php echo e(asset(trans('global.links.gallery_show').$gallery->photo)); ?>">
  					<p><?php echo e($gallery->title); ?></p>
  					<?php endif; ?>
  				</a>
  			</li>
  			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

  		</ul>
  	</div>
    </div> 
  <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/PhotoGallery/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>