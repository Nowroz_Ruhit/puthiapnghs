
<?php $__env->startPush('css'); ?>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Profile</li>
  </ol>
</nav>

<div class="card">
<div class="card-header">
        <?php echo e(trans('global.profile.fields.user')); ?> <?php echo e(trans('global.profile.title_singular')); ?>

    </div>

    <div class="card-body">
        <div class="row">
            <div class="col-5" style="height: 200px;">
                <!-- <div class="float-right mr-5 mt-2"> -->
                <div class="row justify-content-center align-self-center float-right mr-5" >
                    <img class=" rounded-circle" height="180" width="180" src=" <?php echo e(asset('public/img/user/default-user-image.png')); ?>">
                </div>
            </div>
            <div class="col-7 row  align-self-center">
                <table class="table table-striped col-6">
                        <tr>
                        	
                            <th scope="col" width="10">Name</th>
                            <td scope="col"><b>:</b> <?php echo e($user->name); ?></td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Email</th>
                            <td scope="col"><b>:</b> <?php echo e($user->email); ?></td>
                        </tr>
                        <tr>
                            <th scope="col" width="10">Role</th>
                            <td scope="col"><b>:</b> 
                                <?php $__currentLoopData = $user->roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $id => $roles): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <span class="label label-info label-many"><?php echo e($roles->title); ?></span>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </td>
                        </tr>
                        

                </table>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Profile/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>