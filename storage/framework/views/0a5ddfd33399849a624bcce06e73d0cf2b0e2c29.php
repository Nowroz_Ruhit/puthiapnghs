
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('employee.index')); ?>">Employee</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('employee.designation.index')); ?>">Designation</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update</li>
  </ol>
</nav>

<div class="card">
  <nav class="navbar navbar-light bg-light justify-content-between">
      <?php echo e(trans('global.edit')); ?> <?php echo e(trans('global.employe.fields.designation')); ?> 
      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('employee_designation_create')): ?>
      <form class="form-inline">
        <a class="form-control mr-sm-2 btn btn-outline-dark" href="<?php echo e(route('employee.designation.index')); ?>">
          <i class="fas fa-file nav-icon"></i>  <?php echo e(trans('global.employe.fields.designation')); ?> <?php echo e(trans('global.list')); ?>

        </a>
      </form>
      <?php endif; ?>
    </nav>



    <div class="card-body">
      <?php echo e(Aire::open()
            ->route('employee.designation.update', $designation)
            ->bind($designation)
            ->rules([
            'designation' => 'required',
            ])
            ->messages([
            'designation' => 'You must input this fild',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')); ?>


        <?php echo e(Aire::select($institute)
            ->class('form-control')
            ->label('Institute *')
            ->name('institute_id')
            ->id('institute_id')); ?>


        <?php echo e(Aire::select($type)
            ->class('form-control')
            ->label('Employee Type *')
            ->name('employee_type_id')
            ->id('employee_type_id')); ?>


        <?php echo e(Aire::input()
            ->label(trans('global.employe.fields.designation').' *')
            ->id('designation')
            ->name('designation')
            ->class('form-control') 
            ->value(old("title", isset($designation) ? $designation->designation : ""))); ?>



        <?php echo e(Aire::button()
            ->labelHtml('<strong>Update</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>

        <?php echo e(Aire::close()); ?>

    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Employee/Providers/../Resources/views/designation/edit.blade.php ENDPATH**/ ?>