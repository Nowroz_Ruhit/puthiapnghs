

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item active" aria-current="page">Mark</li>
    </ol>
</nav>
<div class="card">
    
    <div class="card-header">Mark Input</div>
    
    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>Class</th>
                        <th>Total Students</th>
                        <th>Total Students 2nd Time</th>
                        <th>Mark Input Status</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $classlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $class): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr data-entry-id="">
                        <td><?php echo e($class->name); ?></td>
                        <td><?php echo e(count($class->studentCount)); ?></td>
                        <td><?php echo e(count($class->studentCount1)); ?></td>
                        <td>
                            
                            <?php echo $class->input_mark ? '<span class="text-success font-weight-bold">Mark Input Complete</span>' : '<span class="text-danger">Pending</span>' ?? ''; ?>

                        </td>
                        <td class="text-center">
                            <?php if($class->subject_set==1): ?>
                            <?php if($class->make_result==0): ?>
                            <?php if($class->input_mark==1): ?>
                            <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.admission.xm.show', $class)); ?>">
                               <i class="fas fa-eye nav-icon"></i> View
                            </a>
                            <?php else: ?>
                            <a class="btn btn-xs btn-success" href="<?php echo e(route('admin.admission.xm.make', $class)); ?>">
                                <i class="fas fa-edit nav-icon"></i> Make Input
                            </a>
                            <?php endif; ?>
                            <?php else: ?>
                            <span class="text-success font-weight-bold ml-3"><i class="fa fa-check nav-icon"></i> 1st Time Marit List Complete</span>

                            <?php if($class->make_result1==0): ?>
                            <?php if($class->input_mark1==1): ?>
                            <a class="btn btn-xs btn-primary" href="<?php echo e(route('admin.admission.xm.show1', $class)); ?>">
                               <i class="fas fa-eye nav-icon"></i> 2nd Time View
                            </a>
                            <?php else: ?>
                            <a class="btn btn-xs btn-warning" href="<?php echo e(route('admin.admission.xm.make1', $class)); ?>">
                                <i class="fas fa-edit nav-icon"></i>2nd Time Make Input
                            </a>
                            <?php endif; ?>
                            <?php else: ?>
                            <span class="text-success font-weight-bold"><i class="fa fa-check nav-icon"></i> 2nd Time Marit List Complete</span>
                            <?php endif; ?>

                            <?php endif; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/xm/index.blade.php ENDPATH**/ ?>