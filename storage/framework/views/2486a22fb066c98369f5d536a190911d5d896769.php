<!-- detailsPrint.blade.php -->



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <style>
        @page  {
            size: A4;
            /*margin: 25mm 25mm 25mm 25mm;*/
            margin: 25mm 20mm 15mm 20mm;
        }
    </style>
</head>
<body>

        <div style="width: 100%;">
            <div style="width: 100%; text-align: center; font-size: 23px;">
                Puthia P.N Government High School
            </div>
            <div style="width: 16%; float: left;">
                <img src="<?php echo e(asset('public/LOGO-SCHOOL.png')); ?>" width="100px" style="margin-top: 5px;">
            </div>
            <div style="width: 60%; float: left; text-align: center;">

                <div style="width: 100%; text-align: center; font-size: 20px;">
                    Puthia, Rajshahi-6260<br>Established-1865
				</div>
				<br>
                <div style="width: 100%; text-align: center;">
                    <div style="width: 60%; text-align: center; font-size: 16px; border: 2px solid black; padding: 15px; margin-right: auto; margin-left: auto;"><?php echo e($bootAdmissionStart->title); ?></div>
                </div>
            </div>
            <div style="width: 24%; float: left;">
                <img src="<?php echo e(asset('public/uplodefile/admission/'.$complete->student->image)); ?>" width="120px" style="margin-top: 5px;">
            </div>
        </div>
        <!-- Header End -->

        <!-- Body Start -->
        <div style="width: 100%; margin-top: 15px; clear: both;">
            <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;">Student Information</div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Name : <?php echo e($complete->student->name); ?>

            </div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Birth Cirtificate No. : <?php echo e($complete->student->birth_certificate); ?>

            </div>
            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Birth Day : <?php echo e($complete->student->birth); ?>

            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Nationality : Bangladeshi
                </div>
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Religion : <?php echo e($complete->student->religion); ?>

                </div>
                <div style="width: 33.33%; float: left; padding-top:3px; ">
                    Gender : <?php echo e($complete->student->gender); ?>

                </div>
            </div>
            <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                Previous Institute Name : <?php echo e($complete->student->previousInstretute ? $complete->student->previousInstretute : ""); ?>

            </div>
   <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;">Parents</div>
            <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                Father's Name : <?php echo e($complete->student->father_name); ?>

            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px; ">
                    NID No. : <?php echo e($complete->student->father_nid); ?>

                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Occupation : <?php echo e($complete->student->father_profession); ?>

                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Annual income : 
                </div>
            </div>
            <div style="width: 100%; clear: both;">

                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px;">
                    Email : <?php echo e($complete->student->father_email); ?>

                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Mobile No. : <?php echo e($complete->student->father_phone); ?>

                </div>
            </div>
            <!-- M -->
            <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                Mothers's Name : <?php echo e($complete->student->mother_name); ?>

            </div>
            <div style="width: 100%; clear: both;">
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    NID No. : <?php echo e($complete->student->mother_nid); ?>

                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Occupation : <?php echo e($complete->student->mother_profession); ?>

                </div>
                <div style="width: 33.33%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Annual income : 
                </div>
            </div>
            <div style="width: 100%; clear: both;">

                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Email : <?php echo e($complete->student->mother_email); ?>

                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; font-size: 16px; padding-top:3px; ">
                    Mobile No. : <?php echo e($complete->student->mother_phone); ?>

                </div>
            </div>

            <div style="width: 100%; clear: both;">
                <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold; ">Address</div>
                            <div style="width: 100%; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                Present Address : <?php echo e($complete->student->present_address); ?>

                            </div>
                            <div style="width: 100%; clear: both;">
                                <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                    Post Code: <?php echo e($complete->student->present_post_code); ?>

                                </div>
                                <div style="width: 33.33%; float: left; padding-top:3px;">
                                    Thana : <?php echo e($complete->student->psnt_thana->name); ?>

                                </div>
                                <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                    District : <?php echo e($complete->student->psnt_district->name); ?>

                                </div>
                            </div>
                                    <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                                        Permanent Address: <?php echo e($complete->student->permananent_address ? $complete->student->permananent_address : $complete->student->present_address); ?>

                                    </div>
                                    <div style="width: 100%; clear: both;">
                                        <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                            Post Code: <?php echo e($complete->student->permananent_post_code ? $complete->student->permananent_post_code : $complete->student->present_post_code); ?>

                                        </div>
                                        <div style="width: 33.33%; float: left; padding-top:3px;">
                                            Thana : <?php echo e($complete->student->permananent_thana ? $complete->student->per_thana->name : $complete->student->psnt_thana->name); ?>

                                        </div>
                                        <div style="width: 33.33%; float: left; padding-top:3px;"> 
                                            District : <?php echo e($complete->student->permananent_district ? $complete->student->per_district->name : $complete->student->psnt_district->name); ?>

                                        </div>
                                    </div>
                        </div>

                <!--<div style="width: 100%; clear: both; border-top: 1px solid #dee2e6;">-->

                <!--    <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;"> Local Gurdian</div>-->
                <!--        <div style="width: 70%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--            Name : <?php echo e($complete->student->localGurdian_name ? $complete->student->localGurdian_name : ""); ?>-->
                <!--        </div>-->
                <!--        <div style="width: 30%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--                Relation : <?php echo e($complete->student->localGurdian_relation ? $complete->student->localGurdian_relation : ""); ?>-->
                <!--            </div>-->
                <!--        <div style="width: 100%; clear: both;">-->
                            
                <!--            <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--                Phone No : <?php echo e($complete->student->localGurdian_phone ? $complete->student->localGurdian_phone : ""); ?>-->
                <!--            </div>-->
                <!--            <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--                Email  : <?php echo e($complete->student->localGurdian_email ? $complete->student->localGurdian_email : ""); ?>-->
                <!--            </div>-->
                <!--        </div>-->
                <!--        <div style="width: 100%; clear: both; border-bottom: 1px solid #dee2e6; padding-top:3px;">-->
                <!--            Address : <?php echo e($complete->student->localGurdian_address ? $complete->student->localGurdian_address : ""); ?>-->
                <!--        </div>-->
                <!--        <div style="width: 100%; clear: both;">-->
                            
                <!--            <div style="width: 33.33%; float: left; padding-top:3px;"> -->
                <!--                Post Code: <?php echo e($complete->student->localGurdian_postCode ? $complete->student->localGurdian_postCode : ""); ?>-->
                <!--            </div>-->
                <!--            <div style="width: 33.33%; float: left; padding-top:3px;">-->
                <!--                Thana : <?php echo e($complete->student->localGurdian_thana ? $complete->student->loc_thana->name : ""); ?>-->
                <!--            </div>-->
                <!--            <div style="width: 33.33%; float: left; padding-top:3px;"> -->
                <!--                District : <?php echo e($complete->student->localGurdian_district ? $complete->student->loc_district->name : ""); ?>-->
                <!--            </div>-->
                <!--        </div>-->
                <!--</div>  -->


            <div style="width: 100%; clear: both; border-top: 1px solid #dee2e6;">
                <div style="width: 100%;  border-bottom: 1px solid #dee2e6; padding-top: 10px; font-size: 20px; font-weight: bold;"> Others Information</div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Admission Class : <?php echo e($complete->payFor->name); ?>

                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    
                    Payment: <?php echo e($complete->tran->card_issuer); ?>

                </div>
                </div>
                <div style="width: 100%; clear: both; ">
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    <b>Admission Lottery ID</b> : <b><?php echo e($complete->admission_id); ?></b>
                </div>
                <div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
                    Pay Day : <?php echo e($complete->tran->tran_date); ?>

				</div>
				
			</div>
			<div style="width: 100%; clear: both; ">
				<?php if($complete->student->hasQouta): ?>
					<div style="width: 50%; float: left; border-bottom: 1px solid #dee2e6; padding-top:3px;">
						Qouta : <?php echo e($complete->student->qoutaName); ?>

					</div>
				<?php endif; ?>
			</div>
        </div>

        <!-- Body End -->

        <!-- Footer Start -->
            <div style="width: 100%; text-align: center; margin-top: 85px;" >Powered By : DesktopIT<br>
                <span style="font-size: 14px">http//:desktopit.com.bd</span>
            </div>

    </body>
    </html><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/detailsPrint.blade.php ENDPATH**/ ?>