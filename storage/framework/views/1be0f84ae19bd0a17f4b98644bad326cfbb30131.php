
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('notice.index')); ?>">Notice</a></li>
    <li class="breadcrumb-item"><a href="">Catagory</a></li>
    <li class="breadcrumb-item active" aria-current="page">Create</li>
  </ol>
</nav>
<div class="card">
    <div class="card-header">
        <?php echo e(trans('global.create')); ?> <?php echo e(trans('global.notice.fields.catagory')); ?>

    </div>

    <div class="card-body">
    	<?php echo e(Aire::open()
            ->route('notice.storecatagory')
            ->rules([
            'title' => 'required',
            'slag' => 'required',
            ])
            ->messages([
            'title' => 'You must input this fild',
            'slag' => 'You must input this fild',
            ]) 
            ->enctype('multipart/form-data')
            ->method('POST')); ?>


        <?php echo e(Aire::input()
            ->label(trans('global.news.catagory').' *')
            ->id('title')
            ->name('title')
            ->class('form-control')); ?>



        <?php echo e(Aire::button()
            ->labelHtml('<strong>Create</strong>')
            ->class('btn btn-danger')
            ->class('mt-2')); ?>

        <?php echo e(Aire::close()); ?>

        
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Notice/Providers/../Resources/views/addcategory.blade.php ENDPATH**/ ?>