<div class="col-lg-3 sidebar">

          <!-- Start Slogan -->
          <div class="site-slogan">
            <?php echo $bootGeneral->slogan; ?>

          </div>
          <!-- End Slogan -->

          <?php $__currentLoopData = $speechs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $speech): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <!-- Start Message of Head Master -->
          <?php
          if(empty($speech->name))
            continue;
          ?>
           <?php if($loop->index != $loop->last): ?>
              <div class="card mt-3 rounded-0 theme-border theme-shadow">
                <div class="card-header theme-border-color rounded-0">
                  <?php echo e($speech->designation); ?>

                </div> 
                <div class="card-body">
                  <img class="hm-img" src="<?php echo e(asset(trans('global.links.speech_show').$speech->files)); ?>" alt="Head Master Image">
                  <p class="hm-name"><?php echo e($speech->name); ?></p>
                  <?php if(! empty($speech->sortSpeech)): ?>
                  <!-- <div class="hm-message text-justify">$speech->sortSpeech</div> -->
                  <a href="<?php echo e(route('frontend.speech', $speech)); ?>" class="hm-details"><?php echo e($speech->sortSpeech); ?> <i class="fas fa-long-arrow-alt-right"></i></a>
                  <?php endif; ?>
                </div>
              </div>
            <?php endif; ?>
          <!-- End Message of Head Master -->
          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          <!-- Start Notice -->
          <div class="card mt-3 rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0">
              নোটিশ
            </div>
            <?php if(empty($notices[0])): ?>
            <div class="text-center pt-5 pb-5"><?php echo e(trans('global.nodata')); ?></div>
            <?php else: ?>
            <div class="card-body">
              <ul class="notice-list">
                <?php $__currentLoopData = $notices; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $notice): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                <li><a href="<?php echo e(route('frontend.notice.show', $notice)); ?>"><i class="far fa-hand-point-right"></i><div class="text-justify"><?php echo e($notice->title); ?></div></a></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 
              </ul>
              <a href="<?php echo e(route('frontend.notice')); ?>" class="hm-details">সকল নোটিশ <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
            <?php endif; ?>
          </div>
          <!-- End Notice -->

        </div><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Frontend/Providers/../Resources/views/partials/rightSide.blade.php ENDPATH**/ ?>