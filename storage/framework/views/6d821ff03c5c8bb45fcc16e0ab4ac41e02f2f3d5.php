
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>


<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.sms.index')); ?>">SMS</a></li>
        <li class="breadcrumb-item active" aria-current="page">Create</li>
    </ol>
</nav>


<div class="card">
    <div class="card-header">
        Create
    </div>

    <div class="card-body">

            <?php echo e(Aire::open()
                ->route('admin.admission.sms.store')
                ->enctype('multipart/form-data')
                ->rules([
                    'subject' => 'required',
                    'subject' => 'required',
                ])); ?>


            <?php echo e(Aire::input('input', 'SMS Subject:*')
              ->id('sms_subject')
              ->name('sms_subject')
              ->placeholder('Site Name')
              ->value('')
              ->class('form-control mb-2')); ?>




              <?php echo e(Aire::textarea()
                  ->label("SMS Body : *")
                  ->id('message')
                  ->name('message')
                  ->class('form-control')); ?>




            <?php echo e(Aire::submit('Save SMS')->class('btn btn-success')); ?>


            <a class="btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="<?php echo e(route('admin.admission.sms.index')); ?>">Cancel</a>

            <?php echo e(Aire::close()); ?>

          
    </div>
</div>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/sms/create.blade.php ENDPATH**/ ?>