

<?php $__env->startSection('content'); ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item" aria-current="page">Admission</li>
    <li class="breadcrumb-item" aria-current="page">Result</li>
    <li class="breadcrumb-item" aria-current="page">Lottery Results</li>
    <li class="breadcrumb-item active" aria-current="page">Edit</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      Choose Class
 </nav>



    <div class="card-body">
        <form id="lottery" action="<?php echo e(route('admin.admission.result.lottery.updatelotteryResult', $lotteryResult)); ?>" method="POST">
          <?php echo csrf_field(); ?>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-5">
                    <p><?php echo e($lotteryResult->student_name); ?></p>
                </div>
                <div class="w-100"></div>
                <label class="col-sm-2 col-form-label">Admission Lottery ID</label>
                <div class="col-sm-5">
                    <p><?php echo e($lotteryResult->admission_lottery_id); ?></p>
                </div>
                <div class="w-100"></div>
                <label class="col-sm-2 col-form-label">Contact</label>
                <div class="col-sm-5">
                    <p><?php echo e($lotteryResult->contact); ?></p>
                </div>
                <div class="w-100"></div>
                <label class="col-sm-2 col-form-label">Father Name</label>
                <div class="col-sm-5">
                    <p><?php echo e($lotteryResult->student->father_name); ?></p>
                </div>
                <div class="w-100"></div>
                <label class="col-sm-2 col-form-label">Status<span class="requiredStar" style="color: red"> * </span></label>
                <div class="col-sm-2">
                    <select class="form-control" name="status" id="class_name" required>
                        <option value = 0 <?php echo e(($lotteryResult->status) ? '' : 'selected'); ?>>Not Selected</option>
                        <option value = 1 <?php echo e(($lotteryResult->status) ? 'selected' : ''); ?>>Selected</option>
                    </select>
                </div>
                <div class="w-100"></div>
                <div class="col-sm-2"></div>
                <div class="col-sm-5 mt-2">
                  <button class="btn btn-primary" type="submit">Update</button>
                </div>  
            </div>
        </form>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "<?php echo e(route('admin.products.massDestroy')); ?>",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

        return
      }

      if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/lotteryResult/edit.blade.php ENDPATH**/ ?>