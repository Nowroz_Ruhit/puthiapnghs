
<?php $__env->startPush('css'); ?>
<link href="<?php echo e(asset('css/aire.css')); ?>" rel="stylesheet" />


<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
    <li class="breadcrumb-item"><a href="#">Administration</a></li>
    <li class="breadcrumb-item"><a href="#">System Settings</a></li>
    <li class="breadcrumb-item"><a href="<?php echo e(route('sociallink.index')); ?>">Social Link</a></li>
    <li class="breadcrumb-item active" aria-current="page">Update Social Link</li>
  </ol>
</nav>

<div class="card">

    <nav class="navbar navbar-light bg-light justify-content-between">
      <a class="navbar-brand"><b>Update Social Link</b></a>

      <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('homeslider_create')): ?>
        <form class="form-inline">
          <a class="form-control mr-sm-2 btn btn-success" href="<?php echo e(route('sociallink.index')); ?>">Back</a>
        </form>
      <?php endif; ?>

    </nav>

    <div class="card-body">

        <?php echo e(Aire::open()
            ->route('sociallink.update',$sociallink)
            ->enctype('multipart/form-data')
            ->bind($sociallink)
            ->rules([
                'title' => ' ',
                'link' => 'url'
            ])->messages([
                'link' => 'Incorect link url'
            ])); ?>

        <div class="form-group">
            <?php echo e(Aire::input('title', 'Link Name:*')
              ->id('title')
              ->placeholder('Site Name')
              ->required()
              ->value()
              ->class('form-control mb-2')); ?>



            <?php echo e(Aire::input('link', 'Linr Url:*')
              ->id('link')
              ->placeholder('http://www.google.com')
              ->required()
              ->class('form-control' )); ?>


              <label class="inline-block mb-2 font-semibold cursor-pointer text-base">Upload Logo:</label>
              <div class="form-inline">
                  <div class="form-group mb-2">
                    <?php echo e(Aire::file('logo')->id('filePhoto')); ?>

                  </div>
                  <div class="form-group mx-sm-3 mb-2">
                    <img id="previewHolder" alt="Uploaded Image Preview Holder" src="<?php echo e($sociallink->logo ? asset('public/img/logo/'.$sociallink->logo) : asset('public/img/logo/006-jpg.png') ?? ''); ?>" class="form-control" />
                  </div>
              </div>

            <?php echo e(Aire::checkbox('active', 'publish')); ?>


            <?php echo e(Aire::submit('Update Link')->id('updateButton')->class('btn btn-success')); ?>


            <a id="Hello" class="success btn btn-danger inline-block text-base rounded p-2 px-4 text-white bg-blue-600 border-blue-700 hover:bg-blue-700 hover:border-blue-900" href="<?php echo e(route('sociallink.index')); ?>">cancel</a>

        </div>
        <?php echo e(Aire::close()); ?>

    </div>
</div>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script src="<?php echo e(asset('public/lib/hullabaloo/js/hullabaloo.js')); ?>"></script>
<script type="text/javascript">
  var hulla = new hullabaloo();
  function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $('#previewHolder').attr('src', e.target.result);
    }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#filePhoto").change(function() {
    readURL(this);
  });

  <?php if(Session::has('success')): ?> 
          hulla.send("<?php echo e(Session::get('success')); ?>", 'success') 
     <?php
       Session::forget('success');
     ?>
  <?php endif; ?>



</script>

<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Sociallink/Providers/../Resources/views/edit.blade.php ENDPATH**/ ?>