
<?php $__env->startSection('content'); ?>
        <style>
            
            .full-height {
                height: 100vh;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            .code {
                border-right: 2px solid;
                font-size: 36px;
                padding: 0 15px 0 15px;
                text-align: center;
            }
            .message {
                font-size: 28px;
                text-align: center;
            }
        </style>

        <div class="flex-center position-ref full-height ">
            <div class="code theme-color">
                <?php echo $__env->yieldContent('code'); ?>
            </div>

            <div class="message theme-color" style="padding: 10px;">
                <?php echo $__env->yieldContent('message'); ?>
            </div>
        </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/errors/minimal.blade.php ENDPATH**/ ?>