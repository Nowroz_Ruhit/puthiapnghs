

<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.subjectSettings.index')); ?>">Subject Settings</a></li>
    <li class="breadcrumb-item active" aria-current="page">Subject List</li>
  </ol>
</nav>

<div class="card">


    <nav class="navbar navbar-light bg-light justify-content-between">
      Subject List
    
      <form class="form-inline">
        <a class="btn btn-outline-dark" href="<?php echo e(route('admin.admission.subjectSettings.subjects.create')); ?>">
          <i class="fas fa-edit nav-icon"></i> Add Subject
        </a>
      </form>
    
    </nav>

    <div class="card-body">
        <div class="table-responsive ">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>
                            Subject
                        </th>
                        <!-- <th>
                            Class name
                        </th>
                        <th>
                            Taka
                        </th> -->
                        <th>
                            &nbsp;Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                  <?php $__currentLoopData = $subjectList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $subject): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr>
                      <td data-entry-id="<?php echo e($subject->id); ?>">
                                <?php echo e($subject->subject_name ?? ''); ?>

                            </td>
                            <td>

                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('admin.admission.subjectSettings.subjects.edit', $subject)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>

                                    <form action="<?php echo e(route('admin.admission.subjectSettings.subjects.delete', $subject)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="submit" class="btn btn-xs btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                    </form>
                                
                            </td>
                  </tr>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/subject/subject.blade.php ENDPATH**/ ?>