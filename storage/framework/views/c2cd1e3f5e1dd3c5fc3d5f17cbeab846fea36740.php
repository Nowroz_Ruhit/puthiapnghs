
<?php $__env->startSection('styles'); ?>
##parent-placeholder-bf62280f159b1468fff0c96540f3989d41279669##
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##

<script src="<?php echo e(asset('js/admission/print.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<!-- <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css"> -->
<style type="text/css">
    @media  print{
        /*page: A4;*/
        table {
            background: none;
        }
        @page  {
            size: A4; 
            margin: 0; 
        }
        body { 
            margin: 1.6cm; 
            background: none;
        }
        .bel {
            /*position:relative;
            margin-left: 38%;
            margin-right: 38%;
            bottom:-25pc;*/
            /*position:absolute;*/
            /*margin-left: 38%;
            margin-right: 38%;*/
            text-align: right;
            /*bottom: 0;*/
            font-size: 15px;
        }
    }

    .print-table {
        text-align: center;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.home')); ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('frontend.admission.index')); ?>">Admission</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('admin.admission.index')); ?>">Dashboard</a></li>


        <li class="breadcrumb-item active" aria-current="page">Status</li>
    </ol>
</nav>
<div class="card">
    <nav class="navbar navbar-light bg-light justify-content-between">
        Admission Dashboard
        
        <form class="form-inline">
            <a class="btn btn-outline-dark" onclick="printDiv('cloneTable')"  id="printKitten">
                <i class="fas fa-print nav-icon"></i> Print
            </a>
        </form>
        
    </nav>

        <div class="col-2 bg-light p-3 ml-4 mx-auto mt-3 text-center">
            Total <?php echo e($ammounts); ?>TK
        </div>

    <div class="ml-4 mt-3 mr-4">
        <div class="row">

            <div class="col-lg-3">
                <label for="filter_class">Class</label>
                <select name="filter_class" id="filter_class" class="form-control">
                    <option value="">Select Class</option>

                    <?php $__currentLoopData = $classes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($value->class); ?>"><?php echo e($value->name); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </select>
            </div>
            <div class="col-lg-3">
                <label for="filter_gender">Gender</label>
                <select name="filter_gender" id="filter_gender" class="form-control">
                    <option value="">Select Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="filter_date_from">From</label>
                        <input type="date" id="filter_date_from" name="filter_date_from" class="form-control">
                    </div>
                    <div class="col-lg-6">
                        <label for="filter_date_to">To</label>
                        <input type="date" id="filter_date_to" name="filter_date_to" class="form-control">
                    </div>
                </div>
            </div>

        </div>
        <div class="form-group mt-3" align="center">
            <button type="button" name="filter" id="filter" class="btn btn-info">Filter</button>

            <button type="button" name="reset" id="reset" class="btn btn-default">Reset</button>
        </div>
    </div>
    <div class="card-body">


        <div class="table-responsive" id="demo">
            <table id="customer_data" class="table table-bordered table-striped" width="100%" style="width: 100% !important;">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Admission ID</th>
                        <th>Name</th>
                        <th>Class</th>
                        <th>Gender</th>
                        <th>Father's Name</th>
                        <th>Payment Date</th>
                        <th>Ammount</th>
                        <th></th>
                    </tr>
                </thead>
                <tr colspan="6" id="viewammount"></tr>
            </table>
            
        </div>



    </div>

</div>
<!-- <button onclick="printTable();">Table</button> -->
<div style="display: none;">
    <!-- <div > -->
<div id="cloneTable" >
    <div id="custom-print-header" class="row custom-print-header">
        <div id="custom-print-img" style="width: 20%; float: left;" class="col-sm-2">
            <img src="<?php echo e(asset('public/uplodefile/general/'.$bootGeneral->logo)); ?>" class="float-right" width="80px">
        </div>
        <div id="custom-print-name" style="width: 80%; float: left;" class="col-sm-10">
            Puthia P.N Government High School<br>Puthia, Rajshahi
        </div>
    </div>
    <table class="table table-bordered" id="myTable">
        <thead>
            <tr>
                <th>SL No.</th>
                <th>Image</th>
                <th>Admission<br>Lottery ID</th>
                <th>Name</th>
                <th>Class</th>
                <th>Gender</th>
                
            </tr>
        </thead>
        <tbody id="table-body-data"></tbody>
    </table>
    <br>
    <span class="bel" id="bel">Powered By : desktopit.com.bd</span>
    <span class="bel" id="bel"><?php echo e(date('d-m-Y')); ?></span>
</div>



<!-- Personal Details Print Start -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/admission/details-print.css')); ?>">
<div id="student-full-print"></div>

<!-- Personal Deaails Print End -->
</div>

<script>
    $("#ammount").css("display", "none");
        // $("#viewammount").css("display", "none");
        $(document).ready(function(){

            let mig_path = '<?php echo e(asset('public/uplodefile/admission/')); ?>/';
            //console.log(mig_path);
            fill_datatable();
            function fill_datatable(filter_gender = '', filter_class = '', filter_date_from = '', filter_date_to='')
            {
                let dataTable = $('#customer_data').DataTable({
                    processing: true,
                    serverSide: true,
                    pageLength: 300,
                    
                    ajax:{
                        url: "<?php echo e(route('admin.admission.paymentFilter')); ?>",
                        data:{filter_gender:filter_gender, filter_class:filter_class, filter_date_from:filter_date_from, filter_date_to:filter_date_to},


                    },
                    columnDefs: [
                    { targets: 0,
                      render: function(data) {
                            //console.log(data);
                            return '<img class="mx-auto" width="80" src="'+mig_path+data+'">'

                        }
                    }   
                    ],
                    columns: [
                    {
                        data:'image',
                        name:'image'
                    },
                    {
                        data:'admission_id',
                        name:'admission_id'
                    },
                    {
                        data:'student_name',
                        name:'student_name'
                    },
                    {
                        data:'class_name',
                        name:'class_name'
                    },
                    {
                        data:'gender',
                        name:'gender'
                    },
                    {
                        data:'father_name',
                        name:'father_name'
                    },
                    {
                        data:'tran_date',
                        name:'tran_date'
                    },
                    {
                        data:'store_amount',
                        name:'store_amount'
                    },
                    {
                        data:'action',
                        name:'action'
                    },
                    ],


                });
                //let d = dataTable.cells.data;
                // console.log(d.tables().context[0].oAjaxData.columns);
                //console.log(dataTable.ajax.json);

                // let table = document.getElementById("customer_data"), sumVal = 0;

                // for(var i = 1; i < table.rows.length; i++)
                // {
                //     sumVal = sumVal + parseInt(table.rows[i].cells[6].innerHTML);
                // }
                // if(sumVal !=0 )
                //     document.getElementById("ammount").innerHTML = "Sum Value = " + sumVal;
                // console.log('Sum Value = '+sumVal);

            }

        //     var table = $('#customer_data').DataTable({
        //       data: data,
        //       columnDefs: [
        //       { targets: 0,
        //           render: function(data) {
        //             return '<img src="'+data+'">'
        //         }
        //     }   
        //     ]
        // })  


        $('#filter').click(function(){
            var filter_gender = $('#filter_gender').val();
            var filter_class = $('#filter_class').val();
            var filter_date_from = $('#filter_date_from').val();
            var filter_date_to = $('#filter_date_to').val();

            $("#view_ammount").css("display", "block");
            $("#ammount").css("display", "none");

            $('#customer_data').DataTable().destroy();
            fill_datatable(filter_gender, filter_class, filter_date_from, filter_date_to);
            // printTable();
            
            

        });

        $('#reset').click(function(){
            $('#filter_gender').val('');
            $('#filter_class').val('');
            $('#filter_date_from').val('');
            $('#filter_date_to').val('');

            $("#view_ammount").css("display", "block");
            $("#ammount").css("display", "none");

            $('#customer_data').DataTable().destroy();
            fill_datatable();
            // printTable();
        });

    });
function amount()
{
    $("#view_ammount").css("display", "none");
    $("#ammount").css("display", "block");
    let table = document.getElementById("customer_data"), sumVal = 0;
    for(var i = 1; i < table.rows.length; i++)
    {
        sumVal = sumVal + parseFloat(table.rows[i].cells[7].innerHTML);
    }
    if(sumVal !=0 )
        document.getElementById("ammount").innerHTML = "Total  "+sumVal.toFixed(2)+" Tk";
        // document.getElementById("viewammount").innerHTML = "Total  "+sumVal+" Tk";
    // console.log('Sum Value = '+sumVal);
}

</script>

<script>



    function hideCol() {
    /*// var col = document.getElementById("txtCol").value;
    // if (isNaN(col) || col == "") {
    //     alert("Invalid Column");
    //     return;
    // }*/



    var col = parseInt(8-1);
    // col = parseInt(col, 10);
    // col = col - 1;
    //console.log(col);
    var tbl = document.getElementById("customer_data");
    if (tbl != null) {
        // if (col < 0 || col >= tbl.rows.length - 1) {
        //     alert("Invalid Column");
        //     return;
        // }
        for (var i = 0; i < tbl.rows.length; i++) {
            // for (var j = 0; j < tbl.rows[i].cells.length; j++) {
            //     tbl.rows[i].cells[j].style.display = "";
            //     if (j == col)
            //         tbl.rows[i].cells[j].style.display = "none";
            // }
            tbl.rows[i].cells[7].style.display = "none";
        }
    }
    /*if($('#ammount:visible').length == 1)
    {
        // console.log($('#tk'));
        // document.getElementById("customer_data").append();
        
        // $("#customer_data").html();

    }*/
}

    function printTable() {
        
        // $("#table-body-data").remove();
        $("#myTable").find("tr:gt(0)").remove();
        var tbl = document.getElementById("customer_data");
        var table = document.getElementById("table-body-data");
        var tk = 0;
        let sl = 0;
        for (var x = 1; x < tbl.rows.length; x++) {
            sl = 0;
            var row = table.insertRow(parseInt(x-1));
            for (var j = parseInt(tbl.rows[x].cells.length-4); j >=0 ; j--) {
                if(sl==0){
                var serial = row.insertCell(0); 
                serial.innerHTML = x;
                sl++;
                }else{
                    var cell = row.insertCell(parseInt(1));
                    //    table.rows[x].cells[0].innerHTML = '<td>'.x.'</td>';
                    cell.innerHTML = $('#customer_data').find("tr:eq("+x+")").find("td:eq("+j+")").html();
                    cell.classList.add('print-table');
                    //console.log(x+' = '+$('#customer_data').find("tr:eq("+x+")").find("td:eq("+j+")").html());
                }

                }
            }

    if(tk != 0){
            $('#table-body-data tr:last').after('<tr><td colspan="5"></td><td>Total Amount</td><td>'+tk.toFixed(2)+' Tk</td></tr>');
        }
}

function tableDelete()
{
    for(var i = document.getElementById("table-body-data").rows.length; i > 0;i--)
    {
        document.getElementById("table-body-data").deleteRow(i -1);
    }
}

function printDiv(divName) {
    printTable();
    printJS({
        printable: divName,
        type: 'html',
        style: '@page  { size: A4 portrait; }',
        css: "<?php echo e(asset('css/admission/custom-print.css')); ?>",
        header: false,
        scanStyles: true,
        documentTitle : '',
        modalMessage : '',
    });

  // $("#viewammount").css("display", "none");

}

</script>

<script type="text/javascript">
function divSelectionforPrint(id)
{
    var value = id;
        // console.log('Value = '+value);
        var _token = $('input[name="_token"]').val();
        $.ajax({
            url:"<?php echo e(route('admin.admission.studentDownload', 1)); ?>",
            method:"POST",
            data:{value:value, _token:_token},
            success:function(result){
                $('#student-full-print').html(result.html);
                // console.log(result);
                // $('#student-full-print').html(result.html);
                printstudent('student-full-print');
            }
        })

}

    function printstudent(divName1) {
        // divSelectionforPrint(id);
        printJS({
            printable: divName1,
            type: 'html',
            style: '@page  { size: A4 portrait; }',
            css: "<?php echo e(asset('css/admission/details-print.css')); ?>",
            header: false,
            scanStyles: true,
            documentTitle : '',
            modalMessage : '',
        });

      // $("#viewammount").css("display", "none");

    }

    function studentDetails(id)
    {
        divSelectionforPrint(id);
        printstudent('student-full-print');
    }

    
</script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Admission/Providers/../Resources/views/admin/paymentStatus.blade.php ENDPATH**/ ?>