


<?php $__env->startSection('content'); ?>

	<!-- Start Breadcrumb -->
    <nav class="bread-crumb mt-3 rounded-0" aria-label="breadcrumb">
      <ol class="breadcrumb rounded-0">
        <li class="breadcrumb-item"><a href="<?php echo e(route('index')); ?>">হোম</a></li>
        <li class="breadcrumb-item"><a href="<?php echo e(route('frontend.notice')); ?>">নোটিশ</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo e($notice->title); ?></li>
      </ol>
    </nav>

	<section class="site-content">
    <div class="row">
        <div class="col-sm-12">
          <!-- Start Welcome or About Text -->
          <div class="card rounded-0 theme-border theme-shadow">
            <div class="card-header theme-border-color rounded-0 theme-bg">
            <?php echo e($notice->title); ?>

            </div>
            <div class="card-body">
              <p>
              	<?php echo $notice->discription; ?>

              </p>
            <?php if(substr($notice->file, -3)=='pdf'): ?>
              <iframe src="<?php echo e(asset('public/uplodefile/notice/'.$notice->file)); ?>" height="750" width="100%"></iframe>
            <?php elseif((substr($notice->file, -3)=='png' )|| (substr($notice->file, -3)=='jpg') || (substr($notice->file, -3)=='JPEG') ): ?>
                <img src="<?php echo e(asset('public/uplodefile/notice/'.$notice->file)); ?>" width="100%"/>
            <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
  </section>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend::layouts.guest', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/Frontend/Providers/../Resources/views/noticeShow.blade.php ENDPATH**/ ?>