<?php /** @var \Galahad\Aire\Elements\Attributes\Collection $attributes */ ?>

<button <?php echo e($attributes); ?>>
	<?php echo e($slot); ?>

</button>
<?php /**PATH /home2/puthiapnghsedu/public_html/resources/views/vendor/aire/button.blade.php ENDPATH**/ ?>