

<?php $__env->startSection('content'); ?>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo e(url('admin')); ?>">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">News</li>
  </ol>
</nav>

<div class="card">
 <nav class="navbar navbar-light bg-light justify-content-between">
      <?php echo e(trans('global.news.title_singular')); ?> <?php echo e(trans('global.list')); ?>

    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news_create')): ?>
      <form class="form-inline">

            <a class="btn btn-outline-dark mr-1" href="<?php echo e(route('news.create')); ?>">
                <i class="fas fa-edit nav-icon"></i> <?php echo e(trans('global.add')); ?> <?php echo e(trans('global.news.title_singular')); ?>

            </a>

        <a class="btn btn-outline-dark" href="<?php echo e(route('news.addcatagory')); ?>">
          <i class="fas fa-edit nav-icon"></i>   <?php echo e(trans('global.add')); ?> <?php echo e(trans('global.news.fields.newscatagory')); ?>

        </a>
      </form>
      <?php endif; ?>
    </nav>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            <?php echo e(trans('global.news.fields.title')); ?>

                        </th>
                        <th>
                            <?php echo e(trans('global.news.fields.created')); ?>

                        </th>
                        <th width="10">
                            <?php echo e(trans('global.news.fields.publish')); ?>

                        </th>
                        <th width="=10">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $news): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr data-entry-id="<?php echo e($news->id); ?>">
                            <td>

                            </td>
                            <td>
                                <?php echo e($news->title ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($news->created_at ?? ''); ?>

                            </td>
                            <td>
                                <?php echo e($news->price ?? ''); ?>

                            </td>
                            <td>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news_show')): ?>
                                    <a class="btn btn-xs btn-primary" href="<?php echo e(route('news.show', $news)); ?>">
                                        <?php echo e(trans('global.view')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news_edit')): ?>
                                    <a class="btn btn-xs btn-info" href="<?php echo e(route('news.edit', $news)); ?>">
                                        <?php echo e(trans('global.edit')); ?>

                                    </a>
                                <?php endif; ?>
                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('news_delete')): ?>
                                    <form action="<?php echo e(route('news.destroy', $news)); ?>" method="POST" onsubmit="return confirm('<?php echo e(trans('global.areYouSure')); ?>');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="submit" class="btn btn-xs btn-danger" value="<?php echo e(trans('global.delete')); ?>">
                                    </form>
                                <?php endif; ?>
                            </td>

                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $__env->startSection('scripts'); ?>
##parent-placeholder-16728d18790deb58b3b8c1df74f06e536b532695##
<script>
    $(function () {
  let deleteButtonTrans = '<?php echo e(trans('global.datatables.delete')); ?>'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "<?php echo e(route('admin.products.massDestroy')); ?>",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('<?php echo e(trans('global.datatables.zero_selected')); ?>')

        return
      }

      if (confirm('<?php echo e(trans('global.areYouSure')); ?>')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)


  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home2/puthiapnghsedu/public_html/Modules/News/Providers/../Resources/views/index.blade.php ENDPATH**/ ?>